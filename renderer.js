/**
 * Communication processes between modules to handle interactive bounding boxes
 * -------------------------------
 *  - User clicks on a box
 *  - Toast with options showed 
 *    with showToastForBehaviorRecording() in helpers.js
 *    - User confirms selection for recording behaviors
 *      with updateInteractionTable() in helpers.js via click event on #label-save-btn (renderer.js)
 *    - User confirms selection for editting track IDs
 *      with updateTrackingTable in helpers.js via click event on #tracking-edit-btn (renderer.js)
 * 
 */ 


/**
 * Processes when the app window is refreshed
 * --------------------------------
 *  Check for properties in the config file (saved in user directory)
 *  Load them if they exist
 * 
 * 
 * 
 */


/**
 * Tracking file edits
 * --------------------
 *  Create a copy of the opened tracking file
 *  Save the path of this file to config file
 *  Write tracking edits by the user to this file
 *  Save the state of tracking table to config file
 *  Read this file when app is restarted
 *  Populate tracking table with entries in the config
 * 
 */

/**
 * Exporting modified tracking files automatically
 * ----------------------------------------------- 
 *  Make user choose a folder for exported files (in the very beginning)
 *  Tracking file edits are saved to user data folder on the background
 *  Copy these file to the user folder at each edit with the metadata headers
 *  Show UI feedback about successful saving
 */

/**
 * Opening a main video
 * ---------------------
 * Make sure all work is saved to export directory
 * Search for previously opened tracking and behavior files linked to video in user data directory
 * If there is any, load them
 * If this is a different video, clear tracking and behavior maps/tables
 */


import { Player, Button, Hotkey, Input, ControlBar, Observation, Ethogram } from './components.js';
import {
  // createSecondaryVideoDivs, 
  updateInteractionTable,
  updateTracksFromToast, 
  updateTracksFromNameDropdown,
  getFileNameWithoutExtension,
  formatSeconds,
  showAlertToast,
  showAlertModal,
  hideAlertModal,
  secondsToFrames,
  framesToSeconds, 
  addEthogramRow,
  updatePlaybackRateList,
  showShortcutsModal,
  showNamesModal,
  showHelpModal,
  showOrHideSpinner,
  addInfoRow,
  showTrackingSaveStatus,
  handleKeyPress,
  handleBehaviorRecordByClick,
  showToastForBehaviorRecording,
  validateInputs,
  showLastEditForFile,
  loadSecondaryVideos,
  clearEthogramTable,
  dragElement,
} from './helpers.js';

// Save metadata before user quits the app
window.electronAPI.onAppQuit(async () => {
  
  const metadata = Player.getMetadata();
  if (metadata) {
    // Show information
    showAlertToast('Saving metadata before quitting...', 'info');
    
    // Save metadata to file
    const response = await metadata.writeToFile();
    if (response) {
      // Show information
      showAlertToast('Quitting the app...', 'success', 'Metadata Saved');
      
    } else {
      // Show information
      showAlertToast('Quitting the app...', 'error', 'Saving Metadata Failed');
    }

  }

  // Introduce delay for better user experience
  await new Promise(resolve => setTimeout(resolve, 1000));

  // Clear all intervals IDs
  Player.resetEthogramInterval();
  Player.resetNotesInterval();
  
  // Signal to the main process to quit the app
  await window.electronAPI.respondBeforeQuit();


});

// Validate form inputs
validateInputs();

// Get the current version and write it to HTML elements and save to Player instance
const currentVersion = await window.electronAPI.getVersion();
if (currentVersion) {
  const versionInfoEls = document.querySelectorAll('.version-info');
  versionInfoEls.forEach(el => {
    el.textContent = `v${currentVersion}`;
  });

  Player.setAppVersion(currentVersion);

}

// Testing new Hotkey class
Player.createDefaultHotkeys();

// Listen for key press for playback and other app shortcuts
// Behavior recording shortcuts are handled separately
document.addEventListener('keydown', handleKeyPress, true);

// If a path was saved to config file, get the videos from that
// let experimentPath = await window.electronAPI.getExperimentDirPath();
let configData = await window.electronAPI.getFromConfig();
if (configData) {

  // Check if app has just been updated
  const isUpdated = configData.version ? Player.getAppVersion() !== configData.version : true;

  // Show release notes if the app has just been updated
  if (isUpdated) {
    const releaseNotesModalEl = document.getElementById('release-notes-modal');
    if (releaseNotesModalEl) {
      const modal = bootstrap.Modal.getOrCreateInstance(releaseNotesModalEl);
      modal.show();
    }

    // Write the new version to config file
    await window.electronAPI.saveToConfig({version: Player.getAppVersion()});

  }


  // Add export directory path to the settings menu
  const exportDirPathInputEl = document.getElementById('change-export-dir-input');
  const openExportDirBtn = document.querySelector('#open-export-dir-btn');
  if (exportDirPathInputEl && configData.exportDirPath) {
    exportDirPathInputEl.value = configData.exportDirPath;
    openExportDirBtn.dataset.path = configData.exportDirPath;
  }

  // Add zoom scale (in percentage) to the settings menu
  const zoomScaleInputEl = document.getElementById('change-zoom-scale-input');
  const configZoomScale = configData.zoomScale;
  if (zoomScaleInputEl && configZoomScale) {
    zoomScaleInputEl.value = configZoomScale;
    const response = await Player.setZoomScale(configZoomScale);
  }


  // Check if username and export folder was chosen 
  // for saving modified tracking/behavior files automatically
  if (configData.username) {
   
    // Add username to the setting menu
    const usernameInputEl = document.getElementById('change-username-input');
    if (usernameInputEl) {
      usernameInputEl.placeholder = configData.username;
      usernameInputEl.value = configData.username;
    }

    // Save username from config to Player
    await Player.setUsername(configData.username);

  // Otherwise, prompt user to choose one
  } else {

    // Ask for a username and export directory path
    const modalBootstrap = bootstrap.Modal.getOrCreateInstance('#username-modal');
    
    // Show modal (inputs in modal will be validated by validateInput() funcion)
    modalBootstrap.show();

  }  

  // Check if individual names, actions and shortcuts were already saved into config file before
  // if (configData.shortcuts) {
  //   Player.setShortcuts(configData.shortcuts);
  // }

  

  // Check if main video path is saved in config file
  if (configData.mainVideoPath) {
    const mainPlayerSrc = configData.mainVideoPath;

    // // Check if tracking map was saved in previous session
    // let tracks, firstAvailTrackIds, trackingEditHistory;
    // if (configData.tracks && configData.firstAvailTrackIds) {
    //   tracks = new Map(Object.entries(configData.tracks));
    //   firstAvailTrackIds = new Map(Object.entries(configData.firstAvailTrackIds)); // firstAvailTrackIds is also a Map object
      
    //   // If any track edit was saved in history, add it to TrackingMap object
    //   if (configData.trackingEditHistory) {
    //     trackingEditHistory = configData.trackingEditHistory;
    //     // console.log(trackingEditHistory)
    //   }

    //   // Populate the tracking edit table
    //   // TODO!
      
    // }

    

    // Search for tracking or identification file
    // let trackingFilePath, tracks, firstAvailTrackIds
    // const trackFileSearchResponse = await window.electronAPI.findTrackingFile(mainPlayerSrc);
    // if (trackFileSearchResponse) {
      
    //   // First check if an identification file was saved before
    //   if (trackFileSearchResponse.idFilePath) {
    //     trackingFilePath = trackFileSearchResponse.idFilePath;
     
    //     // If no identification file, look for a tracking file
    //   } else if (trackFileSearchResponse.trackFilePath) {
    //     trackingFilePath = trackFileSearchResponse.trackFilePath;
     
    //   }

    //   console.log(trackingFilePath);

    //   // Read the tracking file
    //   if (trackingFilePath) {
    //     const response = await window.electronAPI.readTrackingFile(trackingFilePath);
    //     if (response) {
    //       tracks = response.trackingMap;
    //       firstAvailTrackIds = response.firstAvailTrackIds;
  
    //     }

    //   }



    // }

    // let trackingEditHistory;
    // if (configData.trackingEditHistory) {
    //   trackingEditHistory = configData.trackingEditHistory;

    // }

    // Populate tracking table if previous tracking edits were saved to config file
    // if (configData.trackingTableHTML) {
    //   console.log(JSON.parse(configData.trackingTableHTML))

    //   // Get the tracking table
    //   const trackingTable = document.getElementById('tracking-table'); 
    //   if (trackingTable) {

    //     // Check if tracking table HTML from config contains any table rows
    //     if (configData.trackingTableHTML.includes('</tr>')) {
            
    //       // Check if table was empty before
    //         const tableBody = trackingTable.querySelector('tbody');
    //         const infoRow = tableBody.querySelector('.empty-table-info');
    //         console.log(infoRow)
    //         if (infoRow) {
    //           // Remove info row before adding new rows
    //           infoRow.remove();
    //         }
    
    //         // Add tbody HTML from config file
    //         tableBody.insertAdjacentHTML('afterbegin', JSON.parse(configData.trackingTableHTML));
    
    //     } else {
    //       const infoRow = addInfoRow(trackingTable, 'Modified tracking labels will appear here');

    //     }

    //   } 

    // }

    // Load the videos
    await Player.loadMainPlayer(mainPlayerSrc);

    // // Search for previously saved notes file in user data directory
    // const notesFilePath = await window.electronAPI.findNotesFile(mainPlayerSrc);

    // // Fill the notes text area if the notes file was found
    // if (notesFilePath) {

    //   // Read the file content
    //   const notesContent = await window.electronAPI.readNotesFile(notesFilePath);
    //   if (notesContent) {

    //     // Find the relevant DOM element
    //     const notesTextArea = document.getElementById('notes-text-area');
    //     if (notesTextArea) {

    //       // Fill the DOM element
    //       notesTextArea.value = notesContent;

    //       // Show success
    //       showAlertToast('Notes imported!', 'success');

    //       // Show the last edit time
    //       Player.showLastEditForNotes(notesFilePath);
        
    //     }

    //   } 

    // }




    // Check if experiment folder is also saved
    // if (configData.experimentDirPath) {
    //   // Update the input on settings menu
    //   const experimentDirPathInput = document.getElementById('experiment-dir-path-input');
    //   if (experimentDirPathInput) {
    //     experimentDirPathInput.value = configData.experimentDirPath;
    //     experimentDirPathInput.placeholder = configData.experimentDirPath;
    //   }
    // }




  } 

  if (configData.secondaryVideoPaths) {
    await loadSecondaryVideos(configData.secondaryVideoPaths);
  }

  if (configData.individualNames) {
    await Player.setIndividualNames(configData.individualNames);
  }

  if (configData.actionNames) {
    await Player.setActionNames(configData.actionNames);
  }

  if (configData.skipSeconds) {
    await Player.setSkipSeconds(configData.skipSeconds);
  }

 
}

// Handle reloading the window
const relaunchBtn = document.getElementById('relaunch-btn');
if (relaunchBtn) {
  relaunchBtn.addEventListener('click', async () => await window.electronAPI.relaunch());
}

// Handle changing username
const changeUsernameBtn = document.getElementById('change-username-btn');
if (changeUsernameBtn) {
  changeUsernameBtn.addEventListener('click', async () => {
    const changeUsernameInput = document.getElementById('change-username-input');
    if (changeUsernameInput) {
      const newUsername = changeUsernameInput.value;
      if (newUsername) {
        const response = await window.electronAPI.saveToConfig({username: newUsername});
        if (response) {
          await Player.setUsername(newUsername);
          showAlertToast(`Username changed to <strong>${newUsername}</strong>`, 'success');
        }
      } else {
        showAlertToast('Please enter a valid username!', 'error');
      }
    }

  });
}


// Handle changing export directory
const changeExportDirBtn = document.getElementById('change-export-dir-btn');
if (changeExportDirBtn) {
  changeExportDirBtn.addEventListener('click', async () => {

    const exportDirPath = await window.electronAPI.openDirectory();
    if (exportDirPath) {
      const response = await window.electronAPI.saveToConfig({exportDirPath: exportDirPath});
      if (response) {

        // Add export directory path to the menu
        const exportDirPathInputEl = document.getElementById('change-export-dir-input');
        if (exportDirPathInputEl) {
          exportDirPathInputEl.value = exportDirPath;
        }

        // ADd path to HTML element for opening the directory with OS file manager
        const openExportDirPathEl = document.getElementById('open-export-dir-btn');
        if (openExportDirPathEl) {
          openExportDirPathEl.dataset.path = exportDirPath;
        }

        // Show success
        showAlertToast(`New location: ${exportDirPath}`, 'success', 'Export Directory Changed!');

      }

    } else {
      // Show alert
      showAlertToast(`Selection ${exportDirPath} cannot be read!`, 'error', 'Export Directory Inaccesible!');
    }


  })
}

// Handle toggling tracking frames
const toggleTrackingBtn = document.getElementById('toggle-tracking-btn');
if (toggleTrackingBtn) {
  toggleTrackingBtn.addEventListener('click', () => {
    const mainPlayer = Player.getMainPlayer();
    // Check if the main player exists
    if (!mainPlayer) {
      showAlertToast(
        'Open the main video first!',
        'warning',
        'Tracking Unavailable'
      );
      return;
    } else { 
      if (mainPlayer.getTrackingMap().isEmpty()) {
        // If no tracking file is uploaded, show a warning
        showAlertToast('Upload the tracking file first!', 'warning', 'Tracking Mode Disabled');
        return;
      }

      // If everything is okay, change button states and tracking visibility
      toggleTrackingBtn.classList.toggle('off');
      const mainCanvas = document.getElementById('main-tracking-canvas');
      if (mainCanvas) {
        mainCanvas.classList.toggle('d-none');
        if (toggleTrackingBtn.classList.contains('off')) {
          toggleTrackingBtn.querySelector('span').textContent = 'frame_person_off';
        } else {
          toggleTrackingBtn.querySelector('span').textContent = 'frame_person';
        }
      }
    }

  });

}


// Handle saving modified tracking file
const outputTrackingFile = document.getElementById('save-tracking-file-btn');
if (outputTrackingFile) {
  const mainPlayer = Player.getMainPlayer();
  outputTrackingFile.addEventListener('click' , async () => {
    const tracks = mainPlayer.getTrackingMap().getTracks();
    if (tracks) {
      const mainVideoFileName = await getFileNameWithoutExtension(mainPlayer.getSource());
      if (mainVideoFileName) {
        const individualNamesArr = Player.getIndividualNames();
        const output = await window.electronAPI.outputTrackingFile(tracks, mainVideoFileName, individualNamesArr);
      }

       
    }
  })
}

// Handle showing list of keyboard shortcuts
const showKeyboardShortcutsBtn = document.getElementById('show-keyboard-shortcuts-btn');
if (showKeyboardShortcutsBtn) {
  showKeyboardShortcutsBtn.addEventListener('click', () => showShortcutsModal(Player.getHotkeys()))
}

// Handle showing help
const showHelpBtn = document.getElementById('show-help-btn');
if (showHelpBtn) {
  showHelpBtn.addEventListener('click', () => showHelpModal());
}

// Handle showing feedback modal
// const showFeedbackBtn = document.getElementById('show-feedback-btn');
// if (showFeedbackBtn) {
//   showFeedbackBtn.addEventListener('click', () => {
//     const feedbackModalEl = document.getElementById('feedback-modal');
//     if (feedbackModalEl) {
//       const modal = bootstrap.Modal.getOrCreateInstance(feedbackModalEl);
//       modal.show();
//     }

//   });

// }

// Handle showing individual names modal
const showNamesBtn = document.getElementById('show-names-btn');
if (showNamesBtn) {
  showNamesBtn.addEventListener('click', showNamesModal)
}


// Handle opening secondary views
const openSecondaryVideosBtn = document.getElementById('open-secondary-videos-btn');

if (openSecondaryVideosBtn) {
  openSecondaryVideosBtn.addEventListener('click', async () => {    
    
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) {
      showAlertToast(
        'Please open the main video first!',
        'warning',
        'Secondary Views Disabled'
      );
      return;
    }
    
    // Pause main player
    mainPlayer.pause();
    // Get the secondary video file paths
    const videoSrcArr = await window.electronAPI.openMultipleVideos();
    
    // Load the secondary videos
    await loadSecondaryVideos(videoSrcArr);
    
    // Save/update the secondary video paths to the config file
    await Player.saveSecondaryVideosToConfig();
    
    // if (List) {
    //   // Create secondary video HTML divs
    //   const secondaryVideoElList = await createSecondaryVideoDivs(List);
      
    //   // Set up secondary players
    //   if (secondaryVideoElList) {
    //     const secondaryVideoCount = secondaryVideoElList.length;

    //     // If there are valid videos show grid view button and move button for opening videos to upper bar
    //     if (secondaryVideoCount > 0) {
    //       document.getElementById('secondary-video-colsize-btn').classList.remove('d-none');
    //       document.querySelector('#secondary-videos-control-div .button-div').prepend(openSecondaryVideosBtn);
    //       openSecondaryVideosBtn.classList.remove('mt-3');
    //       openSecondaryVideosBtn.classList.replace('btn-icon-large', 'btn-icon-small');
    //     }
        
    //     // Hide the information text
    //     document.getElementById('secondary-video-info-text').classList.add('d-none');
  
    //     // Initialize players for each video
    //     secondaryVideoElList.forEach(videoEl => {
    //       const player = new Player(videoEl.id);
    //       player.setSource(videoEl.src);
    //       player.mute();
    //       player.load();
    //       player.setCurrentTime(Player.getMainPlayer().getCurrentTime());
    //       player.setPlaybackRate(Player.getMainPlayer().getPlaybackRate());
    //       // player.on('click', () => player.switchMainView(mainPlayer));


    //       // Show spinner when video is loading
    //       player.on('loadstart', () => {
    //         player.showSpinner();  
              
    //       });

    //       player.on('loadedmetadata', () => {
    //         // Set frame rate 
    //         player.setFrameRate();
    //         // Dispose player is close button is clicked
    //         const playerColDiv = videoEl.parentNode.parentNode;
    //         if (playerColDiv) {
    //           const disposeButton = playerColDiv.querySelector('.btn-dispose-player');
    //           disposeButton.addEventListener('click', () => {
    //             player.dispose();
    //             showAlertToast('Player removed!', 'success');
    //           });
    //         }

    //       })

    //       player.on('canplaythrough', () => player.hideSpinner());

    //     });
    //   }
    // }
  });
}

// Adjust column size of secondary videos by user input
const secondaryVidColSizeBtn = document.getElementById('secondary-video-colsize-btn');
if (secondaryVidColSizeBtn) {

  secondaryVidColSizeBtn.addEventListener('click', () => {

    // Get the secondary row
    const secondaryVideoRow = document.getElementById('secondary-video-row');

    // Get the status of view type (grid or list)
    const buttonIcon = secondaryVidColSizeBtn.querySelector('span');
    const tooltip = bootstrap.Tooltip.getInstance(secondaryVidColSizeBtn);
    
    if (buttonIcon.dataset.viewType === 'grid') {
      secondaryVideoRow.classList.replace('row-cols-1', 'row-cols-2'); // Adjust column numbers
      buttonIcon.textContent = 'view_list' // Change the icon to list view
      tooltip.setContent({ '.tooltip-inner': 'List view' }); // Change the tooltip
      buttonIcon.dataset.viewType = 'list' // Update dataset
    
    } else if (buttonIcon.dataset.viewType === 'list') {
      secondaryVideoRow.classList.replace('row-cols-2', 'row-cols-1'); // Adjust column numbers
      buttonIcon.textContent = 'grid_view' // Change the icon to grid view
      tooltip.setContent({ '.tooltip-inner': 'Grid view' }); // Change the tooltip
      buttonIcon.dataset.viewType = 'grid' // Update dataset
    }

  });

}


// Handle entering labelling mode (to activate keyboard shortcuts for recording observations)
const labellingModeBtn = document.getElementById('toggle-labelling-mode-btn');
if (labellingModeBtn) {
  labellingModeBtn.addEventListener('click', () => Player.toggleLabellingMode());
}


// Handle saving tracking table to file
// Handle saving interaction table to file
const saveTrackingTableBtn = document.getElementById('save-tracking-table-btn');
const trackingTable = document.getElementById('tracking-table');
if (saveTrackingTableBtn && trackingTable) {
  saveTrackingTableBtn.addEventListener('click', async () => {
    // File delimiter for output file
    const fileDelimiter = ' '; 
    
    // Initialize array for expanded table content
    // It is required to pass on the main process to save the file
    let tableContentArr = []
    
    // Iterate over table cells to format them
    for (let row of trackingTable.rows) {
      const timeStartCell = row.querySelector('.edit-start');
      const timeEndCell = row.querySelector('.edit-end');
      const oldIdCell = row.querySelector('.old-id');
      const newIdCell = row.querySelector('.new-id');
      const typeCell = row.querySelector('.edit-type');

      if (timeStartCell && timeEndCell && oldIdCell && newIdCell && typeCell) {
          // Get the values of cells saved in datasets
          const startFrame = row.dataset.editStartFrame;
          const endFrame = row.dataset.editEndFrame;
          // const editType = row.dataset.editType.value;
          // const oldId = row.dataset.oldId.value;
          // const newId = row.dataset.newId.value;

          let rowContentArr = [];
          rowContentArr.push(startFrame, endFrame)

          // Add space between the cells
          tableContentArr.push(rowContentArr.join(fileDelimiter));
          
          
        }
    }
      
    // Save the tracking table to file
    const mainPlayerSrc = Player.getMainPlayer().getSource();
    const mainVideoFileName = await getFileNameWithoutExtension(mainPlayerSrc);
    const tableContent = tableContentArr.join('\n');
    const output = await window.electronAPI.outputTrackingTable(tableContent, mainVideoFileName);


  });

}


// Handle saving interaction table to file
const exportEthogramBtn = document.getElementById('export-ethogram-btn');
const ethogramTableEl = document.getElementById('behavior-table');
if (exportEthogramBtn && ethogramTableEl) {
  exportEthogramBtn.addEventListener('click', async () => {

    // Get the main player
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) return;

    // Export the behavior records to a CSV file named after experiment/video name
    const ethogram = mainPlayer.getEthogram();
    if (!ethogram) return;

    // Get all observations in the ethogram as Objects
    const obsArr = ethogram.getAllAsObjects()

    // Get the video name without extension
    const fileName = mainPlayer.getName();

    // Get the username
    const username = Player.getUsername();

    // Get the video frame rate
    const videoFPS = mainPlayer.getFrameRate();

    // Do not write metadata by default
    const withMetadata = false;

    // Attempt to export the ethogram to a CSV file
    const response = await window.electronAPI.exportEthogram(obsArr, fileName, videoFPS, username, withMetadata);
    
    // Handle the intentional cancelation by users quitely
    if (response.canceled) return;

    // Show failure
    if (!response) {
      showAlertToast(
        `Ethogram could <strong>NOT</strong> be exported! Please try again.`, 
        'error', 
        'Ethogram Export Failed'
      );

      return;

    }

    // Show success
    if (response.filePath) {
      
      // Construct the badge for file path
      const filePathHtml = `<span class="badge text-bg-success">${response.filePath}<span>`;
      
      showAlertToast(`Ethogram exported to: ${filePathHtml}`, 'success', `Ethogram Exported`);
      
      return;

    }




  });


}



// Handle settings button
const showSettingsBtn = document.getElementById('show-settings-btn');
if (showSettingsBtn) {
  showSettingsBtn.addEventListener('click', () => {
    const modalEl = document.getElementById('settings-modal');
    if (modalEl) {
      const modalBootstrap = bootstrap.Modal.getOrCreateInstance(modalEl);
      modalBootstrap.show();
    }
  })
}


const clearAppDataBtn = document.getElementById('clear-app-data-btn');
if (clearAppDataBtn) {
  // Remove the config file from user folder
  clearAppDataBtn.addEventListener('click', async () => {

    // Hide the settings modal
    const settingsModalEl = document.getElementById('settings-modal');
    if (settingsModalEl) {
      const settingsModal = bootstrap.Modal.getOrCreateInstance(settingsModalEl);
      settingsModal.hide();
    }

    // Show alert modal
    showAlertModal(
      'Proceed with Caution', 
      [
        `This will result in losing all unexported work! The exported files will remain. This action cannot be undone.`,
        'Are you sure you want to continue?'
      ]
    );

    // Get user confirmation
    const alertConfirmBtn = document.getElementById('alert-confirm-btn');
    if (alertConfirmBtn) {
      const confirmed = await getUserConfirmation(alertConfirmBtn);
      
      // Do NOT proceed if not confirmed
      if (!confirmed) return;

      // Clear the app data folder and its contents
      const response = await window.electronAPI.clearAppData();
      if (response) {
        showAlertToast('Reloading...', 'success', 'App Data Cleared');
        await window.electronAPI.relaunch();
      }

    }


  })
}

// Handle opening name file with a list of the names of individuals
const openNameFileBtn = document.getElementById('open-name-list-file-btn');
if (openNameFileBtn) {
  openNameFileBtn.addEventListener('click', async () => {
    // Get indicator element to show loading progress
    const indicatorBtn = document.getElementById('loading-indicator-btn');

    // Check validity of the chosen file path
    const name = await window.electronAPI.openSingleFile('individuals');
    if (name) {

      // Show loading indicator 
      if (indicatorBtn) indicatorBtn.classList.remove('d-none');

      // Read the file
      const nameArray = await window.electronAPI.readNameFile(name);
      if (nameArray) {
        showAlertToast('Individual names imported!', 'success', 'File Imported');

        // Save names to Player object
        await Player.setIndividualNames(nameArray);

        // Save names to config file
        await window.electronAPI.saveToConfig({individualNames: nameArray});

      } else {
        showAlertToast('Please try again with a valid file!', 'error', 'File Import Failed');

      }

      // Hide loading indicator  
      if (indicatorBtn) indicatorBtn.classList.add('d-none');
    }
  })
}

// Handle opening tracking file
const importTrackingFileBtn = document.getElementById('import-tracking-file-btn');
if (importTrackingFileBtn) {
  importTrackingFileBtn.addEventListener('click', async() => {
    const mainPlayer = Player.getMainPlayer();

    // Warn if no video is opened yet
    if (!mainPlayer) {
      showAlertToast(
        'Open the main video first!',
        'warning',
        'Tracking Unavailable'
      );
      return;

    }

    // Pause the player
    mainPlayer.pause();
    
    // Check validity of the chosen file path
    const userTrackFilePath = await window.electronAPI.openSingleFile('tracking');
    if (userTrackFilePath) {
      
      // Show loading indicator 
      showOrHideSpinner('show');
     
      // Read the file
      const result = await window.electronAPI.readTrackingFile(userTrackFilePath);
      if (result) {
        const trackingMap = result.trackingMap;
        const firstAvailTrackIds = result.firstAvailTrackIds;
        mainPlayer.setTrackingMap(trackingMap, firstAvailTrackIds);

        const fileName = mainPlayer.getName();
        const orderedNames = Player.getIndividualNames();
        const username = Player.getUsername();

        // Copy tracking file to the user directory
        const response = await window.electronAPI.copyToUserDataDir(userTrackFilePath, `${fileName}_tracking.txt`);
        
        // Show notification on success/error
        if (response) {
          showAlertToast('Tracking file loaded!', 'success'); 
        } else {
          showAlertToast('Tracking file could NOT be loaded! Please try again.', 'error'); 
        }

        // Hide loading indicator
        showOrHideSpinner('hide');

      } else {
        // Hide loading indicator  
        showOrHideSpinner('hide');
      
        // Show alert
        showAlertToast('Could not process tracking file!', 'danger');

      }

      // if (indicatorBtn) indicatorBtn.classList.add('d-none');

      // console.log('Opened tracking map', mainPlayer.getTrackingMap())


    }

  });

}

// Handle opening action types file
const openActionFileBtn = document.getElementById('open-action-types-btn');
if (openActionFileBtn) {
  openActionFileBtn.addEventListener('click', async () => {
    // Check validity of the chosen file path
    const action = await window.electronAPI.openSingleFile('actions');
    if (action) {
    const actionNameArr = await window.electronAPI.readNameFile(action);
      if (actionNameArr) {

        await Player.setActionNames(actionNameArr); // Save action types
        showAlertToast('Action names imported!', 'success', 'File Imported');

        // Save to config file
        const response = await window.electronAPI.saveToConfig({actionNames: actionNameArr});
        if (!response) {
          console.log('Action names could not be saved to config file!');
        }

      } else {
        showAlertToast('Please try again with a valid file!', 'error', 'File Import Failed');

      }

    }

  });

}




// Handle tracking and interaction nav tab clicks (sync toast and table nav active states)
const trackingToastTabBtn = document.getElementById('tracking-toast-tab');
if (trackingToastTabBtn) {
  trackingToastTabBtn.addEventListener('click', () => {
    const trackingTableTab = document.getElementById('tracking-table-tab');
    if (trackingTableTab) trackingTable.click();

  });

}

const interactionToastTabBtn = document.getElementById('interaction-toast-tab');
if (interactionToastTabBtn) {
  interactionToastTabBtn.addEventListener('click', () => {
    const interactionTableTab = document.getElementById('interaction-table-tab');
    if (interactionTableTab) interactionTableTab.click();

  });

}

// // Handle showing seconds or frames for time
// const timeFormatButtons = document.querySelectorAll('.time-format-btn');
// if (timeFormatButtons) {
//   timeFormatButtons.forEach(button => {button.addEventListener('click', toggleTimeFormat)});
// }

// Handle exporting notes
const notesExportBtn = document.getElementById('export-notes-btn');
if (notesExportBtn) {
  
  notesExportBtn.addEventListener('click', async () => {
    
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) return;

    const notesTextArea = document.getElementById('notes-text-area');
    if (!notesTextArea) return;
    
    const text = notesTextArea.value;
    if (!text) return;

    const fileName = mainPlayer.getName();
    if (!fileName) return;

    const username = Player.getUsername();
          
    const response = await window.electronAPI.exportNotes(text, fileName, username);

    // Handle the intentional cancelation by users quitely
    if (response.canceled) return;

    // Show failure
    if (!response) {
      showAlertToast(
        `Notes could <strong>NOT</strong> be exported! Please try again.`, 
        'error', 
        'Notes Export Failed'
      );

      return;

    }
    
    // Show success
    if (response.filePath) {
      
      // Construct the badge for file path
      const filePathHtml = `<span class="badge text-bg-success">${response.filePath}<span>`;

      showAlertToast(`Notes exported to: ${filePathHtml}!`, 'success', `Notes Exported`);
      
      return;

    }



  })

}



// Keep track of interval ID for updating save status for notes
let notesIntervalId;

const notesTextAreaEl = document.getElementById('notes-text-area');
if (notesTextAreaEl) {
  // Handle saving edits to notes
  notesTextAreaEl.addEventListener('input', async () => {

    // Check if the main player exists
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) return;

    // Check if the file name exists
    const fileName = mainPlayer.getName();
    if (!fileName) return;

    // Get the note content
    const text = notesTextAreaEl.value;

    // Get the username
    const username = Player.getUsername();

    // Save changes to a file in the user data directory
    const filePath = await window.electronAPI.writeNotesToFile(text, fileName, username);
    if (filePath) {

      // Show last edit time
      Player.showLastEditForNotes(filePath);

    }

          

  });

  // Disable/enable labelling when notes DOM element is focused/blurred
  notesTextAreaEl.addEventListener('focus', () => {
    Player.setTypingStatus(true);
  });

  notesTextAreaEl.addEventListener('blur', () => {
    Player.setTypingStatus(false);
  });



}





// Handle opening interaction labelling file
// const openInteractionFileBtn = document.getElementById('open-interaction-file-btn');
// if (openInteractionFileBtn) {
//   openInteractionFileBtn.addEventListener('click', async () => {
//     const  = await window.electronAPI.openSingleFile('interactions');
//     if () {
//       const observations = await window.electronAPI.readInteractionFile();
//       if (observations) {
//         observations.forEach(observation => {
//           addInteractionRow(interactionTable, observation)
//         })
//       }
//     }
//   })
// }





// Handle overlapping track selection button
const overlappingTracksSelectBtn = document.getElementById('overlapping-track-select-btn');
if (overlappingTracksSelectBtn) {
  overlappingTracksSelectBtn.addEventListener('click', (event) => {
    const trackSelect = document.getElementById('overlapping-track-select');
    if (trackSelect) {

      
      // Show warning if no track id is selected
      if (trackSelect.selectedIndex === 0) {
        const alertDiv = document.getElementById('overlapping-toast-alert-div');
        if (alertDiv) {
          alertDiv.textContent = 'A track ID must be selected!';
          alertDiv.classList.remove('d-none');
        }
      } else {
        const selectedOption = trackSelect.options[trackSelect.selectedIndex];
        // Get the selected track id and species
        const species = parseInt(selectedOption.dataset.species);
        const trackId = parseInt(selectedOption.value);
        
        // Find the selection in boxes in the current frame
        const mainPlayer = Player.getMainPlayer();
        if (mainPlayer) {
          const boxesInFrame = mainPlayer.getTrackingBoxesInFrame();
          if (boxesInFrame) {
            const selectedBox = boxesInFrame.filter(box => box.species === species && box.trackId === trackId)[0];
            // console.log(selectedBox)
            if (selectedBox) {
              const toastEl = document.getElementById('overlapping-tracks-toast');
              const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toastEl);
              toastBootstrap.dispose();

              // Show the toast for recording a behavior
              showToastForBehaviorRecording(event, selectedBox, boxesInFrame, mainPlayer.getCurrentTime(), mainPlayer.getCurrentFrame());

            }
          }
        }
        return {species: species, trackId: trackId}
      }
    }
  } )
}

// Handle cancelling adding an action to table 
// const cancelActionSaveBtn = document.getElementById('cancel-label-save-btn');
// if (cancelActionSaveBtn) {
//   // Clear toast content
//   const actionTab = toast.querySelector('#interaction-toast-tab');
//   if (actionTab) {
//     actionTab.dataset.obsStatus = 'new';
//   }

//   // Remove the incomplete observation from table

//   // Dispose toast
// }





/**
 * Change time format to frames to seconds (MM:SS) and vice versa
 */
function toggleTimeFormat() {
  const timeFormatButtons = document.querySelectorAll('.time-format-btn');
  
  // Get the time format status (frames or seconds)
  const currentFormat = this.dataset.timeFormat;
  // const tooltip = bootstrap.Tooltip.getInstance(this);

  let iconText;
  let timeFormat;
  let tooltipTitle;

  // Get the cells with time in them (either frame numbers or minutes:seconds)
  const timeCells = document.querySelectorAll('.time-cell');
  if (currentFormat === 'frames') {
    timeFormat = 'seconds';
    iconText = 'timer_off';
    tooltipTitle = 'Hide seconds';


    if (timeCells.length > 0) {
      // Convert frames to seconds
      timeCells.forEach(cell => {
        if (cell.dataset.frameNumber) {
          cell.textContent = formatSeconds(framesToSeconds(cell.dataset.frameNumber));
        } else {
          cell.textContent = '-'; // If no data is on the cell element yet

        }
      })
    }
  } else if (currentFormat === 'seconds') {
    timeFormat = 'frames';
    iconText = 'timer';
    tooltipTitle = 'Show seconds';
    
    if (timeCells.length > 0) {
      // Convert seconds to frames
      timeCells.forEach(cell => {
        if (cell.dataset.frameNumber) {
          cell.textContent = cell.dataset.frameNumber;
        } else {
          cell.textContent = '-'; // If no data is on the cell element yet
        }
      })
    }
  }

  

  // Update the button status
  if (timeFormatButtons) {
    timeFormatButtons.forEach(button => {
      button.dataset.timeFormat = timeFormat; // Update the time format status
      button.querySelector('span').textContent = iconText; // Change the icon
      const tooltip = bootstrap.Tooltip.getInstance(button);
      tooltip.setContent({ '.tooltip-inner': tooltipTitle })
    })
  }
}

/**
 * 
 * @param {Array} pathsInVideoDir | Video file paths in directory
 * 
 */
// async function loadVideo(mainVideoPath, tracks, firstAvailTrackIds, trackingEditHistory) {

//   console.log('Tracks:', tracks)
//   console.log('Tracks avail ids', firstAvailTrackIds)

//   if (!mainVideoPath) return;

//   // Save main video path to config file
//   const configResponse = await window.electronAPI.saveToConfig({mainVideoPath: mainVideoPath});
//   console.log(configResponse)
//   if (!configResponse) return;

//   // Create or get the main player
//   const mainPlayer = Player.getMainPlayer() ? Player.getMainPlayer() : new Player('main-video');

//   console.log(mainPlayer);

//   // Load the video source
//   mainPlayer.setSource(mainVideoPath);
//   mainPlayer.load();

//   // Set the tracking map obtained from input to the function
//   if (tracks && firstAvailTrackIds) {
//     console.log('Tracks inside loadvideo:', tracks)
//     console.log('Track ids inside loadvideo:', firstAvailTrackIds)
//     let editHistory = trackingEditHistory ? trackingEditHistory : [];
//     mainPlayer.setTrackingMap(tracks, firstAvailTrackIds, editHistory);
//   } else {
//     // Clear the previous map and tracking file path from config file
//     mainPlayer.resetTrackingMap();
//   }
    
//   // Show spinner when video is loading
//   mainPlayer.on('loadstart', () => mainPlayer.showSpinner());
//   // mainPlayer.on('canplaythrough', () => mainPlayer.hideSpinner());
    
//   // Synchronize all videos
//   mainPlayer.on('pause', () => {
//     mainPlayer.updateButtonIcons();
//     Player.getSecondaryPlayers().forEach(player => player.pause());
//   });
      
//   mainPlayer.on('playing', () => {
//     mainPlayer.updateButtonIcons();
//     Player.getSecondaryPlayers().forEach(player => player.play())
//   });
    
//   mainPlayer.on('ratechange', () => {
//     // Sync playback speed
//     const currentSpeed = mainPlayer.getPlaybackRate();
//     Player.getSecondaryPlayers().forEach(player => {
//       player.setPlaybackRate(currentSpeed);
//     });
    
//     // Update the text inside rate button
//     const playbackRateButton = document.getElementById('playback-rate-btn');
//     playbackRateButton.textContent = `Speed: ${currentSpeed}x`
  
//     // Update active states of the selection
//     const playbackRateList = document.getElementById('playback-rate-list');
//     // console.log(playbackRateList)
//     if (playbackRateList) {
//       const listItems = playbackRateList.querySelectorAll('.dropdown-item');
//       if (listItems) {
//         listItems.forEach(item => {
//           // Make the item with the same speed as the main player active
//           console.log(item.dataset.playbackRate);
//           if (parseFloat(item.dataset.playbackRate) === currentSpeed) {
//             item.classList.add('active');
//           } else {
//             // Remove active states from all items
//             item.classList.remove('active');
//           }
//         });

//       }

//     }

//   });

//   // Events when the main video is loaded
//   mainPlayer.on('loadeddata', async () => {

//     const mainPlayerSrc = mainPlayer.getSource();
      
//     // Set the frame rate
//     mainPlayer.setFrameRate();

//     // Update the progress bar
//     const controlBar = mainPlayer.getControlBar();
//     if (controlBar) controlBar.updateProgressBar();
  
//     // Set the attirbutes of hidden video element 
//     // This hidden video element is used for showing individual video frames when hovering over 
//     mainPlayer.setCanvasOnHover();
    
//     // Setup the maximum duration on the "jump to frame" input element
//     const jumpToFrameInput = document.getElementById('jump-to-frame-input');
//     if (jumpToFrameInput) {
//       const frameCount = secondsToFrames(mainPlayer.getDuration());
//       // jumpToFrameInput.placeholder = `Frame number <${frameCount}`;
//       jumpToFrameInput.placeholder = `Frame no.`;
//       jumpToFrameInput.max = frameCount;
//     }

    
  
//     // Clear the ethogram
//     mainPlayer.clearEthogram();

//     // Search for previously recorded ethogram file in user data directory
//     const ethogramFilePath = await window.electronAPI.findEthogramFile(mainPlayerSrc);

//     // If the ethogram file has been found
//     if (ethogramFilePath) {

//       // Read the ethogram file (get the array of observations each as a Map)
//       const obsArr = await window.electronAPI.readEthogramFile(ethogramFilePath);

//       // Check if the file can be read
//       if (obsArr) {

//         // Get the Ethogram instance for the main player
//         const ethogram = mainPlayer.getEthogram();
//         if (ethogram) {

//           // Create an Observation instance for each Map instance in the array
//           // Add this Observation instance to the Ethogram
//           // Add a new row for each Observation into the HTML table for Ethogram
//           obsArr.forEach(obsMap => {
//             const newObs = new Observation(obsMap);
//             ethogram.add(newObs);
            
//             const hideAlert = true;
//             addEthogramRow(newObs, hideAlert);

//           });

//           // Get the number of observations in the Ethogram
//           const obsCount = ethogram.size();

//           if (obsCount > 0) {
//             // Show success notification
//             const recordText = obsCount === 1 ? `${obsCount} record` : `${obsCount} records`;
//             showAlertToast(`${recordText} imported!`, 'success', 'Ethogram Imported');
          
//           }

//         }

//       }


//     // If no ethogram file has been found
//     } else {
//       showAlertToast('No ethogram file for this video!', 'info');

//     }

//     // Search for saved notes file
//     const notesTextArea = document.getElementById('notes-text-area');
//     if (notesTextArea) {
      
//       // Clear the notes text area
//       notesTextArea.value = '';
      
//       showAlertToast('Searching for notes...!', 'success');

//       // Search for previously saved notes file in user data directory
//       const notesFilePath = await window.electronAPI.findNotesFile(mainPlayerSrc);
      
//       // Fill the notes text area if the notes file was found
//       if (notesFilePath) {

//         // Read the file content
//         const notesContent = await window.electronAPI.readNotesFile(notesFilePath);
//         if (notesContent) {
//           // Fill the DOM element
//           notesTextArea.value = notesContent;

//           showAlertToast('Notes imported!', 'success');
        
//         }

//       } 

//     }

//     // Hide the loading indicator element
//     mainPlayer.hideSpinner()
  
//   });
      
//   // Update the progress bar when the current time of the main video is changed
//   mainPlayer.on('timeupdate', () => {
//     const controlBar = mainPlayer.getControlBar();
//     if (controlBar) controlBar.updateProgressBar();

//     mainPlayer.drawTrackingBoxes();

//     // mainPlayer.indicateAnnotatedBehaviors();
//     // if (trackingBoxesInFrame) {
//     //   Player.getMainPlayer().setTrackingBoxesInFrame(trackingBoxesInFrame);
//     // }

//   });
      
//   /**
//    * Set up the control buttons
//    */
//   // Set up the play-pause btn
//   // const playPauseButton = new Button('#play-pause-btn');
//   // playPauseButton.setIcon('play_circle', 'size-48');
//   // playPauseButton.on('click', () => mainPlayer.playPause());

//   // // Set up the mute button
//   // const muteButton = new Button('#mute-btn');
//   // muteButton.setIcon('volume_up');
//   // muteButton.on('click', () => mainPlayer.toggleMute());
      
//   // // Set up playback speed button
//   // const playbackRateList = document.getElementById('playback-rate-list');
//   // if (playbackRateList) {
//   //   const listItems = playbackRateList.querySelectorAll('.dropdown-item');
//   //   if (listItems) {
//   //     listItems.forEach(item => {
//   //       item.addEventListener('click', () => {
//   //         const selectedValue = parseFloat(item.dataset.playbackRate);
//   //         console.log(selectedValue)
//   //         if (selectedValue) {
//   //           mainPlayer.setPlaybackRate(selectedValue);
//   //         }
//   //       })
//   //     })

//   //   }
//   // }
      
//   // // Get the default skip forward and back seconds selection
//   // const skipSecondsSelectEl = document.getElementById('skip-seconds-select');
//   // if (skipSecondsSelectEl) {
//   //   // let selectedSeconds = skipSecondsSelectEl.selectedOptions[0].value;
//   //   // Player.setSkipSeconds(selectedSeconds);
    
//   //   // Change skip forward and back seconds when user changes the relevant setting
//   //   skipSecondsSelectEl.addEventListener('change', () => {
//   //     console.log(skipSecondsSelectEl.selectedOptions[0].value)
//   //     Player.setSkipSeconds(skipSecondsSelectEl.selectedOptions[0].value);
      
//   //   });
//   // }
    
//   // // Set up the skip forward button
//   // const forwardButton = new Button('#forward-btn');
//   // forwardButton.on('click', () => mainPlayer.forward());

//   // // Set up the skip backward button
//   // const replayButton = new Button('#replay-btn');
//   // replayButton.on('click', () => mainPlayer.replay());  


//   // Video file name without extension to show as the title of the panel
//   const mainVideoName = await getFileNameWithoutExtension(mainVideoPath); 
//   if (mainVideoName) {
//     const mainVideoTitleEl = document.getElementById('main-video-title');
//     if (mainVideoTitleEl) {
//       mainVideoTitleEl.textContent = mainVideoName;
//     }         
    
//   }

//   // const controlBar = mainPlayer.getControlBar();
//   // if (!controlBar) return;
  
//   // const progressBarEl = controlBar.progressBar;
//   // if (!progressBarEl) return;
  
//   // // Get the click event to change the current time with user input
//   // progressBarEl.addEventListener('click', (e) => { 
//   //   const progressBarRect = progressBarEl.getBoundingClientRect();
//   //   const clickPosition = e.clientX - progressBarRect.left;
//   //   const videoTime = (clickPosition / progressBarRect.width) * Player.getMainPlayer().getDuration();
    
//   //   Player.getAllInstances().forEach(player => {
//   //     // player.pause();
//   //     player.setCurrentTime(videoTime);

//   //   });


//   // });
    
//   // // Show video time when hovering over the progress bar 
//   // progressBarEl.addEventListener('mousemove', (e) => {
    
//   //   const hoverTimeEl = document.getElementById('hover-time-div');
//   //   if (!hoverTimeEl) return;
    
//   //   const mainPlayer = Player.getMainPlayer();
//   //   if (!mainPlayer) return;

//   //   if (!mainPlayer.getDuration()) return;

//   //   const progressBarRect = progressBarEl.getBoundingClientRect();
//   //   const clickPosition = e.clientX - progressBarRect.left;
//   //   const hoveredTime = (clickPosition / progressBarRect.width) * mainPlayer.getDuration();
//   //   const formattedTime = formatSeconds(hoveredTime);

//   //   // hoverTimeEl.style.left = (e.clientX - hoverTimeEl.offsetWidth / 2) + 'px';
//   //   hoverTimeEl.textContent = formattedTime; 
//   //   hoverTimeEl.classList.remove('d-none');

    
//   //   // Show video frame when hovering over the progress bar
//   //   const hoverVideoEl = document.getElementById('hover-video');
//   //   if (!hoverVideoEl) return;

//   //   // Get the DOM element for showing video frames on hover
//   //   const hoverFrameEl = document.getElementById('hover-frame-div');
//   //   if (!hoverFrameEl) return;

//   //   // Get the canvas 
//   //   const hoverFrameCanvas = hoverFrameEl.querySelector('canvas');
//   //   if (!hoverFrameCanvas) return;

//   //   // Check if canvas context exists
//   //   if (!hoverFrameCanvas.getContext) return;

//   //   if ("requestVideoFrameCallback" in HTMLVideoElement.prototype) {

//   //     // Show the hover frame element 
//   //     hoverFrameEl.classList.remove('d-none');
      
//   //     // Update the on-hover time
//   //     hoverVideoEl.currentTime = hoveredTime;
        
//   //     // Update the hover element position values
//   //     hoverFrameEl.style.left = (e.clientX - hoverTimeEl.offsetWidth / 2) + 'px';
//   //     hoverFrameEl.style.top = progressBarRect.top - hoverFrameEl.offsetHeight - 10 + 'px';


//   //     // Update the canvas
//   //     const updateCanvas = (now, metadata) => {
        
//   //       const ctx = hoverFrameCanvas.getContext('2d')

//   //       // Draw the relevant video frame to canvas on hover
//   //       ctx.drawImage(hoverVideoEl, 0, 0, hoverFrameCanvas.width, hoverFrameCanvas.height);
        
//   //       // Re-register the callback to run on the next frame
//   //       hoverVideoEl.requestVideoFrameCallback(updateCanvas);
        
//   //     };
      
//   //     // Initial registration of the callback to run on the first frame
//   //     hoverVideoEl.requestVideoFrameCallback(updateCanvas);

//   //   } else {
//   //     alert("Your browser does not support requestVideoFrameCallback().");

//   //   }

//   // });
  
//   // // Hide the on-hover element when mouse leaves the progress bar
//   // progressBarEl.addEventListener('mouseleave', (e) => {
//   //   const hoverFrameEl = document.getElementById('hover-frame-div');
//   //   if (!hoverFrameEl) return;
//   //   hoverFrameEl.classList.add('d-none');

//   // });
      
//   // // Decrease the column size of the main panel
//   // const mainVideoCol = document.getElementById('main-video-col');
//   // if (mainVideoCol) {
//   //   // mainVideoCol.classList.remove('col-12');
//   //   // mainVideoCol.classList.add('col');
//   //   mainVideoCol.classList.remove('d-none'); // Make the main panel visible
//   // }
  
//   // // Make the secondary panel visible
//   // const secondaryVideosCol = document.getElementById('secondary-video-col');
//   // if (secondaryVideosCol) {
//   //   secondaryVideosCol.classList.remove('d-none');
//   // }
      
//   // // Buttons on the toast for editing labels
//   // const behaviorAddBtn = new Button('#label-save-btn');
//   // behaviorAddBtn.on('click', handleBehaviorRecordByClick)

//   // const trackingEditButton = new Button('#tracking-edit-btn');
//   // trackingEditButton.on('click', updateTracksFromToast)

//   // const nameEditButton = new Button('#name-edit-btn');
//   // nameEditButton.on('click', updateTracksFromNameDropdown);
      
  

//   // Move the open video button to the top bar of the main video
//   const mainVideoBar = document.getElementById('main-video-bar');
//   const openMainVideoBtn = document.getElementById('open-main-video-btn'); 
//   const openExperimentFolderBtn = document.getElementById('open-experiment-folder-btn');
//   const initialPanelDiv = document.getElementById('initial-panel-div');

//   // Move the buttons to the main video bar
//   if (mainVideoBar){
//     const videoBarBtnDiv = mainVideoBar.querySelector('.button-div');
//     if (videoBarBtnDiv) {
//       if (openMainVideoBtn) {
//         videoBarBtnDiv.appendChild(openMainVideoBtn);
//         openMainVideoBtn.classList.remove('btn-icon-large');
//         openMainVideoBtn.classList.add('btn-icon-small');
//       } 

//       if (openExperimentFolderBtn) {
//         videoBarBtnDiv.appendChild(openExperimentFolderBtn);
//         openExperimentFolderBtn.classList.remove('btn-icon-large');
//         openExperimentFolderBtn.classList.add('btn-icon-small');
//       }

//     }

//   }

//   // Remove the initial panel
//   if (initialPanelDiv) {
//     initialPanelDiv.remove();
//   }

//   // Get the canvas on the main player to show tracking rectangles
//   const mainCanvas = document.getElementById('main-tracking-canvas');
//   if (mainCanvas) {
//     mainPlayer.attachCanvas(mainCanvas);
    
//     // Make tracking boxes clickable and interactive
//     mainCanvas.addEventListener('click', (e) => {
//       mainPlayer.makeBoxesInteractive(e);
//     });

//     // Show dropdown for individuals when right-clicked on tracking boxes
//     mainCanvas.addEventListener('contextmenu', (e) => {
//       e.preventDefault();
//       mainPlayer.showRightClickMenu(e);
//     })

//     // Enable labelling mode when pointer is over the canvas or control bar
//     const mainVideoDiv = document.getElementById('main-video-col');
//     if (mainVideoDiv) {
//       mainVideoDiv.addEventListener('mouseenter', () => {
//         Player.enableLabelling();
//       });

//       // Disable labelling mode when pointer is outside of the the canvas or control bar
//       mainVideoDiv.addEventListener('mouseleave' , () => {
//         Player.disableLabelling();
//       });

//     }

//   }


// }



/**
 * 
 * @param {Array} pathsInVideoDir | Video file paths in directory
 * 
 */
// async function loadVideoOld(mainVideoPath, tracks, firstAvailTrackIds, trackingEditHistory) {
//   if (mainVideoPath) {

//     if (Player.getMainPlayer()) {

//       const mainPlayer = Player.getMainPlayer();
      
//       // Update the main player
//       mainPlayer.setSource(mainVideoPath);
//       mainPlayer.load();
      
//       // Video file name without extension to show as the title of the panel
//       const mainVideoName = await getFileNameWithoutExtension(mainVideoPath); 
//       const mainVideoTitleEl = document.getElementById('main-video-title');
//       if (mainVideoTitleEl) mainVideoTitleEl.textContent = mainVideoName;

//     } else {

//       const controlBar = new ControlBar('#control-bar');
//       const mainPlayer = new Player('main-video');
      
//       mainPlayer.setSource(mainVideoPath);
//       mainPlayer.load();
    
//       // Show spinner when video is loading
//       mainPlayer.on('loadstart', () => mainPlayer.showSpinner());
//       // mainPlayer.on('canplaythrough', () => mainPlayer.hideSpinner());
    
//       // Synchronize all videos
//       mainPlayer.on('pause', () => {
//         mainPlayer.updateButtonIcons();
//         Player.getSecondaryPlayers().forEach(player => player.pause());
//       });
      
//       mainPlayer.on('playing', () => {
//         mainPlayer.updateButtonIcons();
//         Player.getSecondaryPlayers().forEach(player => player.play())
//       });
    
//       mainPlayer.on('ratechange', () => {
//         // Sync playback speed
//         const currentSpeed = mainPlayer.getPlaybackRate();
//         Player.getSecondaryPlayers().forEach(player => {
//           player.setPlaybackRate(currentSpeed);
//         });
    
//         // Update the text inside rate button
//         const playbackRateButton = document.getElementById('playback-rate-btn');
//         playbackRateButton.textContent = `Speed: ${currentSpeed}x`
    
//         // Update active states of the selection
//         const playbackRateList = document.getElementById('playback-rate-list');
//         // console.log(playbackRateList)
//         if (playbackRateList) {
//           const listItems = playbackRateList.querySelectorAll('.dropdown-item');
//           if (listItems) {
//             listItems.forEach(item => {
//               // Make the item with the same speed as the main player active
//               console.log(item.dataset.playbackRate);
//               if (parseFloat(item.dataset.playbackRate) === currentSpeed) {
//                 item.classList.add('active');
//               } else {
//                 // Remove active states from all items
//                 item.classList.remove('active');
//               }
//             });

//           }

//         }

//       });

//       // Events when the main video is loaded
//       mainPlayer.on('loadeddata', async () => {
          
//         // Set the frame rate
//         mainPlayer.setFrameRate();
    
//         mainPlayer.attachButton(playPauseButton, 'play-pause'); // Attach button to main player
//         mainPlayer.attachButton(muteButton, 'mute');
//         mainPlayer.attachButton(forwardButton, 'forward');
//         mainPlayer.attachButton(replayButton, 'replay');
//         mainPlayer.attachControlBar(controlBar);
      
//         // Update the progress bar
//         controlBar.updateProgressBar();
      
//         // Set the attirbutes of hidden video element 
//         // This hidden video element is used for showing individual video frames when hovering over 
//         mainPlayer.setCanvasOnHover();
        
//         // Update the canvas
//         mainPlayer.drawTrackingBoxes();
        
//         // Setup the maximum duration on the "jump to frame" input element
//         const jumpToFrameInput = document.getElementById('jump-to-frame-input');
//         if (jumpToFrameInput) {
//           const frameCount = secondsToFrames(mainPlayer.getDuration());
//           // jumpToFrameInput.placeholder = `Frame number <${frameCount}`;
//           jumpToFrameInput.placeholder = `Frame no.`;
//           jumpToFrameInput.max = frameCount;
//         }

//         // Set the tracking map obtained from input to the function
//         if (tracks && firstAvailTrackIds) {
//           let editHistory = trackingEditHistory ? trackingEditHistory : [];
//           mainPlayer.setTrackingMap(tracks, firstAvailTrackIds, editHistory);
//         } else {
//           // Clear the previous map and tracking file path from config file
//           mainPlayer.resetTrackingMap();
//         }
      
//         // Clear the ethogram
//         mainPlayer.clearEthogram();

//         // Search for previously recorded ethogram file in user data directory
//         const ethogramFilePath = await window.electronAPI.findEthogramFile(mainVideoPath);

//         // If the ethogram file has been found
//         if (ethogramFilePath) {

//           // Read the ethogram file (get the array of observations each as a Map)
//           const obsArr = await window.electronAPI.readEthogramFile(ethogramFilePath);

//           // Check if the file can be read
//           if (obsArr) {

//             // Get the Ethogram instance for the main player
//             const ethogram = mainPlayer.getEthogram();
//             if (ethogram) {

//               // Create an Observation instance for each Map instance in the array
//               // Add this Observation instance to the Ethogram
//               // Add a new row for each Observation into the HTML table for Ethogram
//               obsArr.forEach(obsMap => {
//                 const newObs = new Observation(obsMap);
//                 ethogram.add(newObs);
                
//                 const hideAlert = true;
//                 addEthogramRow(newObs, hideAlert);
    
//               });
    
//               // Get the number of observations in the Ethogram
//               const obsCount = ethogram.size();
    
//               if (obsCount > 0) {
//                 // Show success notification
//                 const recordText = obsCount === 1 ? `${obsCount} record` : `${obsCount} records`;
//                 showAlertToast(`${recordText} imported!`, 'success', 'Ethogram Imported');
              
//               }

//             }

//           }
    

//         // If no ethogram file has been found
//         } else {

//           showAlertToast('No ethogram file for this video!', 'info');
//         }

//         // Hide the loading indicator element
//         mainPlayer.hideSpinner()
      
//       });
      
//       // Update the progress bar when the current time of the main video is changed
//       mainPlayer.on('timeupdate', () => {
//         const controlBar = mainPlayer.getControlBar();
//         if (controlBar) {
//           controlBar.updateProgressBar();
//         }
    
//         const trackingBoxesInFrame = mainPlayer.drawTrackingBoxes();
//         // mainPlayer.indicateAnnotatedBehaviors();
//         // if (trackingBoxesInFrame) {
//         //   Player.getMainPlayer().setTrackingBoxesInFrame(trackingBoxesInFrame);
//         // }
//       });
      
//       /**
//        * Set up the control buttons
//        */
//       // Set up the play-pause btn
//       const playPauseButton = new Button('#play-pause-btn');
//       playPauseButton.setIcon('play_circle', 'size-48');
//       playPauseButton.on('click', () => mainPlayer.playPause());
    
//       // Set up the mute button
//       const muteButton = new Button('#mute-btn');
//       muteButton.setIcon('volume_up');
//       muteButton.on('click', () => mainPlayer.toggleMute());
      
//       // Set up playback speed button
//       const playbackRateList = document.getElementById('playback-rate-list');
//       if (playbackRateList) {
//         const listItems = playbackRateList.querySelectorAll('.dropdown-item');
//         if (listItems) {
//           listItems.forEach(item => {
//             item.addEventListener('click', () => {
//               const selectedValue = parseFloat(item.dataset.playbackRate);
//               console.log(selectedValue)
//               if (selectedValue) {
//                 mainPlayer.setPlaybackRate(selectedValue);
//               }
//             })
//           })
    
//         }
//       }
      
//       // Get the default skip forward and back seconds selection
//       const skipSecondsSelectEl = document.getElementById('skip-seconds-select');
//       if (skipSecondsSelectEl) {
//         // let selectedSeconds = skipSecondsSelectEl.selectedOptions[0].value;
//         // Player.setSkipSeconds(selectedSeconds);
        
//         // Change skip forward and back seconds when user changes the relevant setting
//         skipSecondsSelectEl.addEventListener('change', () => {
//           console.log(skipSecondsSelectEl.selectedOptions[0].value)
//           Player.setSkipSeconds(skipSecondsSelectEl.selectedOptions[0].value);
          
//         });
//       }
    
//       // Set up the skip forward button
//       const forwardButton = new Button('#forward-btn');
//       // forwardButton.setIcon(`forward_${Player.getSkipSeconds()}`);
//       forwardButton.on('click', () => mainPlayer.forward());
    
//       // Set up the skip backward button
//       const replayButton = new Button('#replay-btn');
//       // replayButton.setIcon(`replay_${Player.getSkipSeconds()}`);
//       replayButton.on('click', () => mainPlayer.replay());  
    
//       // Video file name without extension to show as the title of the panel
//       const mainVideoName = await getFileNameWithoutExtension(mainVideoPath); 
    
//       const mainVideoTitleEl = document.getElementById('main-video-title');
//       if (mainVideoTitleEl) {
//         mainVideoTitleEl.textContent = mainVideoName;
//       }
      
      
//       // Get the click event to change the current time with user input
//       const progressBarEl = controlBar.progressBar;
//       if (!progressBarEl) return;
      
//       progressBarEl.addEventListener('click', (e) => { 
//         const progressBarRect = progressBarEl.getBoundingClientRect();
//         const clickPosition = e.clientX - progressBarRect.left;
//         const videoTime = (clickPosition / progressBarRect.width) * Player.getMainPlayer().getDuration();
        
//         Player.getAllInstances().forEach(player => {
//           // player.pause();
//           player.setCurrentTime(videoTime);
    
//         });
    
    
//       });
    
//       progressBarEl.addEventListener('mousemove', (e) => {
        
//         // Show video time when hovering over the progress bar 
//         const hoverTimeEl = document.getElementById('hover-time-div');
//         if (!hoverTimeEl) return;
        
//         const mainPlayer = Player.getMainPlayer();
//         if (!mainPlayer) return;
    
//         if (!mainPlayer.getDuration()) return;
    
//         const progressBarRect = progressBarEl.getBoundingClientRect();
//         const clickPosition = e.clientX - progressBarRect.left;
//         const hoveredTime = (clickPosition / progressBarRect.width) * mainPlayer.getDuration();
//         const formattedTime = formatSeconds(hoveredTime);
    
//         // hoverTimeEl.style.left = (e.clientX - hoverTimeEl.offsetWidth / 2) + 'px';
//         hoverTimeEl.textContent = formattedTime; 
//         hoverTimeEl.classList.remove('d-none');
    
        
//         // Show video frame when hovering over the progress bar
//         const hoverVideoEl = document.getElementById('hover-video');
//         if (!hoverVideoEl) return;
    
//         // Get the DOM element for showing video frames on hover
//         const hoverFrameEl = document.getElementById('hover-frame-div');
//         if (!hoverFrameEl) return;
    
//         // Get the canvas 
//         const hoverFrameCanvas = hoverFrameEl.querySelector('canvas');
//         if (!hoverFrameCanvas) return;
    
//         // Check if canvas context exists
//         if (!hoverFrameCanvas.getContext) return;
    
//         if ("requestVideoFrameCallback" in HTMLVideoElement.prototype) {
    
//           // Show the hover frame element 
//           hoverFrameEl.classList.remove('d-none');
          
//           // Update the on-hover time
//           hoverVideoEl.currentTime = hoveredTime;
            
//           // Update the hover element position values
//           hoverFrameEl.style.left = (e.clientX - hoverTimeEl.offsetWidth / 2) + 'px';
//           hoverFrameEl.style.top = progressBarRect.top - hoverFrameEl.offsetHeight - 10 + 'px';
    
    
//           // Update the canvas
//           const updateCanvas = (now, metadata) => {
            
//             const ctx = hoverFrameCanvas.getContext('2d')
    
//             // Draw the relevant video frame to canvas on hover
//             ctx.drawImage(hoverVideoEl, 0, 0, hoverFrameCanvas.width, hoverFrameCanvas.height);
            
//             // Re-register the callback to run on the next frame
//             hoverVideoEl.requestVideoFrameCallback(updateCanvas);
            
//           };
          
//           // Initial registration of the callback to run on the first frame
//           hoverVideoEl.requestVideoFrameCallback(updateCanvas);
    
//         } else {
//           alert("Your browser does not support requestVideoFrameCallback().");
    
//         }
    
//       });
      
//       // Hide the on-hover element when mouse leaves the progress bar
//       progressBarEl.addEventListener('mouseleave', (e) => {
//         const hoverFrameEl = document.getElementById('hover-frame-div');
//         if (!hoverFrameEl) return;
//         hoverFrameEl.classList.add('d-none');
    
//       });
      
//       // Decrease the column size of the main panel
//       const mainVideoCol = document.getElementById('main-video-col');
//       if (mainVideoCol) {
//         // mainVideoCol.classList.remove('col-12');
//         // mainVideoCol.classList.add('col');
//         mainVideoCol.classList.remove('d-none'); // Make the main panel visible
//       }
      
//       // Make the secondary panel visible
//       const secondaryVideosCol = document.getElementById('secondary-video-col');
//       if (secondaryVideosCol) {
//         secondaryVideosCol.classList.remove('d-none');
//       }
      
//       // Buttons on the toast for editing labels
//       const behaviorAddBtn = new Button('#label-save-btn');
//       behaviorAddBtn.on('click', handleBehaviorRecordByClick)
    
//       const trackingEditButton = new Button('#tracking-edit-btn');
//       trackingEditButton.on('click', updateTracksFromToast)
    
//       const nameEditButton = new Button('#name-edit-btn');
//       nameEditButton.on('click', updateTracksFromNameDropdown);
      
//     }
    
//     const mainPlayer = Player.getMainPlayer();

//     // Save main video path to config file
//     const configResponse = await window.electronAPI.saveToConfig({mainVideoPath: mainVideoPath});
//     console.log(configResponse)

//     // Move the open video button to the top bar of the main video
//     const mainVideoBar = document.getElementById('main-video-bar');
//     const openMainVideoBtn = document.getElementById('open-main-video-btn'); 
//     const openExperimentFolderBtn = document.getElementById('open-experiment-folder-btn');
//     const initialPanelDiv = document.getElementById('initial-panel-div');

//     // Move the buttons to the main video bar
//     if (mainVideoBar){
//       const videoBarBtnDiv = mainVideoBar.querySelector('.button-div');
//       if (videoBarBtnDiv) {
//         if (openMainVideoBtn) {
//           videoBarBtnDiv.appendChild(openMainVideoBtn);
//           openMainVideoBtn.classList.remove('btn-icon-large');
//           openMainVideoBtn.classList.add('btn-icon-small');
//         } 

//         if (openExperimentFolderBtn) {
//           videoBarBtnDiv.appendChild(openExperimentFolderBtn);
//           openExperimentFolderBtn.classList.remove('btn-icon-large');
//           openExperimentFolderBtn.classList.add('btn-icon-small');
//         }

//       }

//     }

//     // Remove the initial panel
//     if (initialPanelDiv) {
//       initialPanelDiv.remove();
//     }

//     // Get the canvas on the main player to show tracking rectangles
//     const mainCanvas = document.getElementById('main-tracking-canvas');
//     if (mainCanvas) {
//       mainPlayer.attachCanvas(mainCanvas);
      
//       // Make tracking boxes clickable and interactive
//       mainCanvas.addEventListener('click', (e) => {
//         mainPlayer.makeBoxesInteractive(e);
//       });

//       // Show dropdown for individuals when right-clicked on tracking boxes
//       mainCanvas.addEventListener('contextmenu', (e) => {
//         e.preventDefault();
//         mainPlayer.showRightClickMenu(e);
//       })

//       // Enable labelling mode when pointer is over the canvas or control bar
//       const mainVideoDiv = document.getElementById('main-video-col');
//       if (mainVideoDiv) {
//         mainVideoDiv.addEventListener('mouseenter', () => {
//           Player.enableLabelling();
//         });

//         // Disable labelling mode when pointer is outside of the the canvas or control bar
//         mainVideoDiv.addEventListener('mouseleave' , () => {
//           Player.disableLabelling();
//         });
  
//       }

//     }

//   }


// }

// async function updateMainPlayer(videoSrc) {
//   const mainPlayer = Player.getMainPlayer();
//   if (mainPlayer) {
//     mainPlayer.setSource(videoSrc);
//     mainPlayer.load();

//     // Video file name without extension to show as the title of the panel
//     const mainVideoName = getFileNameWithoutExtension(videoSrc); 
//     const mainVideoTitleEl = document.getElementById('main-video-title');
//     if (mainVideoTitleEl) {
//       mainVideoTitleEl.textContent = mainVideoName;
//     }

//     // const tracking = await window.electronAPI.findTrackingFile(mainVideoPath);
//     // if (tracking) {
//     //   const result = await window.electronAPI.readTrackingFile(tracking);
//     //   console.log(result)
//     //   if (result) {
//     //     const trackingMap = result.trackingMap;
//     //     const firstAvailTrackIds = result.firstAvailTrackIds;
//     //     // Set the tracking dictionary obtained from tracking file
//     //     mainPlayer.setTrackingMap(trackingMap, firstAvailTrackIds);

//     //   }
//     // }

    
//   }
// }

// async function setupMainPlayer(videoSrc) {

//   const controlBar = new ControlBar('#control-bar');
//   const mainPlayer = new Player('main-video');

//   mainPlayer.setSource(videoSrc);
//   mainPlayer.load();

//   // Show spinner when video is loading
//   mainPlayer.on('loadstart', () => mainPlayer.showSpinner());
//   // mainPlayer.on('canplaythrough', () => mainPlayer.hideSpinner());

//   // Synchronize all videos
//   mainPlayer.on('pause', () => {
//     mainPlayer.updateButtonIcons();
//     Player.getSecondaryPlayers().forEach(player => player.pause());
//   });

//   mainPlayer.on('playing', () => {
//     mainPlayer.updateButtonIcons();
//     Player.getSecondaryPlayers().forEach(player => player.play())
//   });

//   mainPlayer.on('ratechange', () => {
//     // Sync playback speed
//     const currentSpeed = mainPlayer.getPlaybackRate();
//     Player.getSecondaryPlayers().forEach(player => {
//       player.setPlaybackRate(currentSpeed);
//     });

//     // Update the text inside rate button
//     const playbackRateButton = document.getElementById('playback-rate-btn');
//     playbackRateButton.textContent = `Speed: ${currentSpeed}x`

//     // Update active states of the selection
//     const playbackRateList = document.getElementById('playback-rate-list');
//     // console.log(playbackRateList)
//     if (playbackRateList) {
//       const listItems = playbackRateList.querySelectorAll('.dropdown-item');
//       if (listItems) {
//         listItems.forEach(item => {
//           // Make the item with the same speed as the main player active
//           console.log(item.dataset.playbackRate);
//           if (parseFloat(item.dataset.playbackRate) === currentSpeed) {
//             item.classList.add('active');
//           } else {
//             // Remove active states from all items
//             item.classList.remove('active');
//           }
//         })
//       }
//     }
//   });

  

//   /**
//    * Set up the control buttons
//    */
//   // Set up the play-pause btn
//   const playPauseButton = new Button('#play-pause-btn');
//   playPauseButton.setIcon('play_circle', 'size-48');
//   playPauseButton.on('click', () => mainPlayer.playPause());

//   // Set up the mute button
//   const muteButton = new Button('#mute-btn');
//   muteButton.setIcon('volume_up');
//   muteButton.on('click', () => mainPlayer.toggleMute());

//   // Set up playback speed button
//   const playbackRateList = document.getElementById('playback-rate-list');
//   if (playbackRateList) {
//     const listItems = playbackRateList.querySelectorAll('.dropdown-item');
//     if (listItems) {
//       listItems.forEach(item => {
//         item.addEventListener('click', () => {
//           const selectedValue = parseFloat(item.dataset.playbackRate);
//           console.log(selectedValue)
//           if (selectedValue) {
//             mainPlayer.setPlaybackRate(selectedValue);
//           }
//         })
//       })

//     }
//   }

//   // Get the default skip forward and back seconds selection
//   const skipSecondsSelectEl = document.getElementById('skip-seconds-select');
//   if (skipSecondsSelectEl) {
//     // let selectedSeconds = skipSecondsSelectEl.selectedOptions[0].value;
//     // Player.setSkipSeconds(selectedSeconds);
    
//     // Change skip forward and back seconds when user changes the relevant setting
//     skipSecondsSelectEl.addEventListener('change', () => {
//       console.log(skipSecondsSelectEl.selectedOptions[0].value)
//       Player.setSkipSeconds(skipSecondsSelectEl.selectedOptions[0].value);
      
//     });
//   }

//   // Set up the skip forward button
//   const forwardButton = new Button('#forward-btn');
//   // forwardButton.setIcon(`forward_${Player.getSkipSeconds()}`);
//   forwardButton.on('click', () => mainPlayer.forward());

//   // Set up the skip backward button
//   const replayButton = new Button('#replay-btn');
//   // replayButton.setIcon(`replay_${Player.getSkipSeconds()}`);
//   replayButton.on('click', () => mainPlayer.replay());

//   // // Sync seeked event
//   // mainPlayer.on('seeked', () => {
//   //   const currentTime = mainPlayer.getCurrentTime();
//   //   Player.getSecondaryPlayers().forEach(player => player.setCurrentTime(currentTime));
//   // })

//   // replayButton.on('click', () => Player.getAllInstances().forEach(player => player.replay()));
    
  

//   // Video file name without extension to show as the title of the panel
//   const mainVideoName = await getFileNameWithoutExtension(videoSrc); 

//   const mainVideoTitleEl = document.getElementById('main-video-title');
//   if (mainVideoTitleEl) {
//     mainVideoTitleEl.textContent = mainVideoName;
//   }

  
//   // Events when the main video is loaded
//   mainPlayer.on('canplaythrough', () => {
    
//     // Set the frame rate
//     mainPlayer.setFrameRate();

//     // Attach the control elements
//     mainPlayer.attachButton(playPauseButton, 'play-pause'); // Attach button to main player
//     mainPlayer.attachButton(muteButton, 'mute');
//     mainPlayer.attachButton(forwardButton, 'forward');
//     mainPlayer.attachButton(replayButton, 'replay');
//     mainPlayer.attachControlBar(controlBar);

//     // Update the progress bar
//     controlBar.updateProgressBar();

//     // Set the attirbutes of hidden video element 
//     // This hidden video element is used for showing individual video frames when hovering over 
//     mainPlayer.setCanvasOnHover();
    
//     // Update the canvas
//     mainPlayer.drawTrackingBoxes();
    
//     // Setup the maximum duration on the "jump to frame" input element
//     const jumpToFrameInput = document.getElementById('jump-to-frame-input');
//     if (jumpToFrameInput) {
//       const frameCount = secondsToFrames(mainPlayer.getDuration());
//       // jumpToFrameInput.placeholder = `Frame number <${frameCount}`;
//       jumpToFrameInput.placeholder = `Frame no.`;
//       jumpToFrameInput.max = frameCount;
//     }

//     // Hide the loading indicator element
//     mainPlayer.hideSpinner()

//   });

//   // Update the progress bar when the current time of the main video is changed
//   mainPlayer.on('timeupdate', () => {
//     const controlBar = mainPlayer.getControlBar();
//     if (controlBar) {
//       controlBar.updateProgressBar();
//     }

//     const trackingBoxesInFrame = mainPlayer.drawTrackingBoxes();
//     // mainPlayer.indicateAnnotatedBehaviors();
//     // if (trackingBoxesInFrame) {
//     //   Player.getMainPlayer().setTrackingBoxesInFrame(trackingBoxesInFrame);
//     // }
//   });

//   // Get the click event to change the current time with user input
//   const progressBarEl = controlBar.progressBar;
//   if (!progressBarEl) return;

//   progressBarEl.addEventListener('click', (e) => { 
//     const progressBarRect = progressBarEl.getBoundingClientRect();
//     const clickPosition = e.clientX - progressBarRect.left;
//     const videoTime = (clickPosition / progressBarRect.width) * Player.getMainPlayer().getDuration();
    
//     Player.getAllInstances().forEach(player => {
//       // player.pause();
//       player.setCurrentTime(videoTime);

//     });


//   });

//   progressBarEl.addEventListener('mousemove', (e) => {
    
//     // Show video time when hovering over the progress bar 
//     const hoverTimeEl = document.getElementById('hover-time-div');
//     if (!hoverTimeEl) return;
    
//     const mainPlayer = Player.getMainPlayer();
//     if (!mainPlayer) return;

//     if (!mainPlayer.getDuration()) return;

//     const progressBarRect = progressBarEl.getBoundingClientRect();
//     const clickPosition = e.clientX - progressBarRect.left;
//     const hoveredTime = (clickPosition / progressBarRect.width) * mainPlayer.getDuration();
//     const formattedTime = formatSeconds(hoveredTime);

//     // hoverTimeEl.style.left = (e.clientX - hoverTimeEl.offsetWidth / 2) + 'px';
//     hoverTimeEl.textContent = formattedTime; 
//     hoverTimeEl.classList.remove('d-none');

    
//     // Show video frame when hovering over the progress bar
//     const hoverVideoEl = document.getElementById('hover-video');
//     if (!hoverVideoEl) return;

//     // Get the DOM element for showing video frames on hover
//     const hoverFrameEl = document.getElementById('hover-frame-div');
//     if (!hoverFrameEl) return;

//     // Get the canvas 
//     const hoverFrameCanvas = hoverFrameEl.querySelector('canvas');
//     if (!hoverFrameCanvas) return;

//     // Check if canvas context exists
//     if (!hoverFrameCanvas.getContext) return;

//     if ("requestVideoFrameCallback" in HTMLVideoElement.prototype) {

//       // Show the hover frame element 
//       hoverFrameEl.classList.remove('d-none');
      
//       // Update the on-hover time
//       hoverVideoEl.currentTime = hoveredTime;
        
//       // Update the hover element position values
//       hoverFrameEl.style.left = (e.clientX - hoverTimeEl.offsetWidth / 2) + 'px';
//       hoverFrameEl.style.top = progressBarRect.top - hoverFrameEl.offsetHeight - 10 + 'px';


//       // Update the canvas
//       const updateCanvas = (now, metadata) => {
        
//         const ctx = hoverFrameCanvas.getContext('2d')

//         // Draw the relevant video frame to canvas on hover
//         ctx.drawImage(hoverVideoEl, 0, 0, hoverFrameCanvas.width, hoverFrameCanvas.height);
        
//         // Re-register the callback to run on the next frame
//         hoverVideoEl.requestVideoFrameCallback(updateCanvas);
        
//       };
      
//       // Initial registration of the callback to run on the first frame
//       hoverVideoEl.requestVideoFrameCallback(updateCanvas);

//     } else {
//       alert("Your browser does not support requestVideoFrameCallback().");

//     }

//   });

//   // Hide the on-hover element when mouse leaves the progress bar
//   progressBarEl.addEventListener('mouseleave', (e) => {
//     const hoverFrameEl = document.getElementById('hover-frame-div');
//     if (!hoverFrameEl) return;
//     hoverFrameEl.classList.add('d-none');

//   });



//   // Decrease the column size of the main panel
//   const mainVideoCol = document.getElementById('main-video-col');
//   if (mainVideoCol) {
//     // mainVideoCol.classList.remove('col-12');
//     // mainVideoCol.classList.add('col');
//     mainVideoCol.classList.remove('d-none'); // Make the main panel visible
//   }

//   // Make the secondary panel visible
//   const secondaryVideosCol = document.getElementById('secondary-video-col');
//   if (secondaryVideosCol) {
//     secondaryVideosCol.classList.remove('d-none');
//   }

//   // // Update the progress bar when the current time of the main video is changed
//   // mainPlayer.on('timeupdate', () => {
//   //   controlBar.updateProgressBar();
//   //   if (trackingDict) {
//   //     const trackingBoxesInFrame = mainPlayer.drawTrackingBoxes(trackingDict);
//   //     // console.log(trackingBoxesInFrame)
//   //     // Make tracking boxes and clickable and interactive
//   //     mainCanvas.addEventListener('click', (e) => {
//   //       mainPlayer.makeBoxesInteractive(e, trackingBoxesInFrame);
//   //     });
//   //   }
//   // });

//   // Buttons on the toast for editing labels
//   const behaviorAddBtn = new Button('#label-save-btn');
//   behaviorAddBtn.on('click', handleBehaviorRecordByClick)

//   const trackingEditButton = new Button('#tracking-edit-btn');
//   trackingEditButton.on('click', updateTracksFromToast)

//   const nameEditButton = new Button('#name-edit-btn');
//   nameEditButton.on('click', updateTracksFromNameDropdown);


// }




// console.log(await getVideoPaths())

function resizeMainPanel(arg) {
  const secondaryPanelEl = document.getElementById('secondary-video-col');
  if (secondaryPanelEl) {
    const currentClassName = secondaryPanelEl.className;
    let secondaryColSize = parseInt(currentClassName.match(/\d+/)[0]); // Get the current column size of the secondary panel
    const minColSize = 3;
    const maxColSize = 6;

    if (arg === 'enlarge') {
      if (secondaryColSize > minColSize && secondaryColSize < 12) {
        // Decrease the secondary column size
        secondaryColSize = secondaryColSize - 1; 
      } else {
        // Place the secondary panel below main panel if user enlarges the main panel too much
        secondaryColSize = 12; 
      }

    } else if (arg === 'shrink') {
      if (secondaryColSize < maxColSize) {
        secondaryColSize = secondaryColSize + 1; // Increase the secondary column size
      } else if (secondaryColSize === 12) {
        // Place the secondary panel to the right of the main panel if user shrinks the main panel too much
        secondaryColSize = 3;
      }
    } else if (arg === 'reset') {
      secondaryColSize = 4;
    }

    const newClassName = `col-${secondaryColSize}`
    secondaryPanelEl.classList.remove(currentClassName) // Remove the old class (e.g. col-4)
    secondaryPanelEl.classList.add(newClassName) // Add the new class (e.g. col-3)
  }




}



/**
 * Enlarge and shrink main panel
 */
const enlargeMainPanelBtn = document.getElementById('enlarge-main-panel-btn');
if (enlargeMainPanelBtn) {
  enlargeMainPanelBtn.addEventListener('click', () => resizeMainPanel('enlarge'))
}

const shrinkMainPanelBtn = document.getElementById('shrink-main-panel-btn');
if (shrinkMainPanelBtn) {
  shrinkMainPanelBtn.addEventListener('click', () => resizeMainPanel('shrink'))
}

const resetMainPanelBtn = document.getElementById('reset-main-panel-btn');
if (resetMainPanelBtn) {
  resetMainPanelBtn.addEventListener('click', () => resizeMainPanel('reset'));
}

// Handle changing maximum playback rate
const maxPlaybackRateSelectEl = document.getElementById('max-playback-rate-select');
if (maxPlaybackRateSelectEl) {
  // Change the maximum playback rate when user changes the relevant setting
  maxPlaybackRateSelectEl.addEventListener('change', () => {
    let selectedMaxRate = parseFloat(maxPlaybackRateSelectEl.selectedOptions[0].value);
    // playbackRateInput.setMax(selectedMaxRate);
    Player.setMaxPlaybackRate(selectedMaxRate);
    updatePlaybackRateList();

  });

}

// Handle jumping to a specific frame in a video
const jumpToFrameBtn = document.getElementById('jump-to-frame-btn');
const jumpToFrameInput = document.getElementById('jump-to-frame-input');
if (jumpToFrameBtn && jumpToFrameInput) {
  
  jumpToFrameBtn.addEventListener('click', () => {
    
    const mainPlayer = Player.getMainPlayer();
    const frameNumber = jumpToFrameInput.value;
    const videoDuration = secondsToFrames(mainPlayer.getDuration());
    if (frameNumber >= 0 && frameNumber < videoDuration) {
      mainPlayer.setCurrentTime(framesToSeconds(frameNumber));
    
    } 
      
  });
    
  // Disable/enable labelling when input element is focused/blurred
  jumpToFrameInput.addEventListener('focus', () => {
    Player.setTypingStatus(true);

  });

  jumpToFrameInput.addEventListener('blur', () => {
    Player.setTypingStatus(false)
    
  });

}

// Handle tooltips to show info over buttons and other elements
// Only show tooltip while hovering not after clicking (default is "hover focus"
const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl, {'trigger': 'hover'}));

// Initialize popovers
// const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]');
// const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl));

// Handle undo tracking edits
const undoTrackEditBtn = document.getElementById('undo-tracking-change-btn');
if (undoTrackEditBtn) {
  undoTrackEditBtn.addEventListener('click', () => {
    // Remove the last change from tracking table
    const firstRow = trackingTable.querySelector('tbody tr:not(.empty-table-info)'); // O-th zero is info row (invisible if there are data on table)
    if (firstRow) {
      Player.getMainPlayer().getTrackingMap().undo();
      firstRow.remove();
    }

    // Show info row if the table is empty of data
    if (!trackingTable.querySelector('tbody tr:not(.empty-table-info)')) {
      const infoRow = addInfoRow(trackingTable, 'Modified tracking labels will appear here');
    }


  })
}

// async function handleExperimentFolderButton() {
//   const experimentDirPath = await window.electronAPI.openExperimentDir();
//     console.log(experimentDirPath);
//     if (experimentDirPath) {
//       const response = await window.electronAPI.saveToConfig({experimentDirPath: experimentDirPath});
//       if (response.success) {
//         const configData = response.configData;
//         const experimentDirPathInput = document.getElementById('experiment-dir-path-input');
//         if (experimentDirPathInput) {
//           experimentDirPathInput.value = configData.experimentDirPath;
//           experimentDirPathInput.placeholder = configData.experimentDirPath;
//           let pathsInVideoDir = await window.electronAPI.getVideosFromDir(experimentDirPath);
//           console.log(pathsInVideoDir)
//           if (!pathsInVideoDir) {
//             showAlertModal('Invalid Experiment Directory!', 'Selected directory does not contain valid video files', 'Please choose another directory');
//           } else {
//             loadVideos(pathsInVideoDir);
//           }
//         }
//       };
//     }
// }



function saveTableToFile(fileDelimiter) {
  const saveLabelTableBtn = document.getElementById('save-label-table-btn');
  if (!fileDelimiter) {
    fileDelimiter = ' ';
  }

  if (saveLabelTableBtn) {
    const labelTable = document.getElementById('interaction-table');
    saveLabelTableBtn.addEventListener('click', async () => {
      let tableContentArr = []

      // Iterate over table cells to format them
      for (let row of labelTable.rows) {
        let rowContentArr = [];
        for (let cell of row.cells) {
          // Convert MM:SS to seconds or maybe frames for the start and end of an interaction
          // let cellContent = cell.textContent;
          // if (cellContent.includes(':')) {
          //   cellContent = formatMinutes(cellContent)
          // } 
          // rowContent.push(cellContent);
          rowContentArr.push(cell.textContent)
        }
        const rowContent = rowContentArr.join(fileDelimiter); // Add tab between the cells and a newline after each row
        tableContentArr.push(rowContent);

      }
      const mainPlayerSrc = Player.getMainPlayer().getSource();
      const mainVideoFileName = await getFileNameWithoutExtension(mainPlayerSrc);
      const tableContent = tableContentArr.join('\n');
      const output = await window.electronAPI.outputTrackingTable(tableContent, mainVideoFileName);
    })
  }
}


// Proceed only after user click on the confirmation button
function getUserConfirmation(buttonEl) {
  if (!buttonEl) return;

  return new Promise((resolve) => {

      function handleClick() {
          // Remove the event listener to prevent multiple resolves
          buttonEl.removeEventListener('click', handleClick);
          
          // Resolve the promise
          const confirmed = true;
          resolve(confirmed);
      }

      // Add the event listener to the button
      buttonEl.addEventListener('click', handleClick);
  });
}




// Handle opening a new main video
const openMainVideoBtn = document.getElementById('open-main-video-btn'); 
if (openMainVideoBtn) {
  openMainVideoBtn.addEventListener('click', async () => {
    
    // Pause the main video
    const mainPlayer = Player.getMainPlayer();
    if (mainPlayer) {
      mainPlayer.pause();   
      // Save the metadata of the existing video before opening a new video
      // Update metadata
      const metadata = Player.getMetadata();
      if (metadata) {
        await metadata.writeToFile();
      }
      // const writeToFile = true;
      // const response = await Player.updateMetadata('lastViewedFrameNum', mainPlayer.getCurrentFrame(), writeToFile);
      // showAlertToast(response['lastViewedFrameNum'], 'success', 'Metadata Written');

    }
    
    // Let user open a new video
    const mainVideoPath = await window.electronAPI.openSingleVideo();
    if (mainVideoPath) {
     
      // Show the spinner
      showOrHideSpinner('show');
      
      // Show info
      showAlertToast('Loading the main video...', 'info');

      // Load the new video
      await Player.loadMainPlayer(mainVideoPath);
      
      // Hide the spinner
      showOrHideSpinner('hide');

    }

  });

}



// Handle taking a snapshot
const snapshotModalBtn = document.getElementById('snapshot-btn');
if (snapshotModalBtn) {
  snapshotModalBtn.addEventListener('click', () => {
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) {
      showAlertToast('Please open the main video first!', 'warning', 'Snapshots Disabled');
      return;
    }

    mainPlayer.drawSnapshot();

  });

}

// Handle saving a snapshot
const saveSnapshotBtn = document.getElementById('save-snapshot-btn');
if (saveSnapshotBtn) {
  saveSnapshotBtn.addEventListener('click', async () => {
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) {
      showAlertToast('Please open the main video first!', 'warning', 'Snapshots Disabled');
      return;
    }

    await mainPlayer.saveSnapshot();
    
  });

}


// Handle collapsing panels
const collapseElementList = document.querySelectorAll('.collapse');
if (collapseElementList) {
  collapseElementList.forEach(collapseEl => {
    collapseEl.addEventListener('hide.bs.collapse', () => {
      // Get the button which controls collapsing of this element
      const collapseBtn = document.querySelector(collapseEl.dataset.collapseButton);
      collapseBtn.querySelector('span').textContent = 'visibility_off';
    })
    collapseEl.addEventListener('show.bs.collapse', () => {
      // Get the button which controls collapsing of this element
      const collapseBtn = document.querySelector(collapseEl.dataset.collapseButton);
      collapseBtn.querySelector('span').textContent = 'visibility';
    })
  })
}


// Handle undo button for recording an observation
const undoCurrentObsBtn = document.getElementById('undo-current-observation-btn');
if (undoCurrentObsBtn) {
  undoCurrentObsBtn.addEventListener('click', () => {
    const currentObs = Player.getCurrentObservation();
    if (!currentObs.isEmpty()) currentObs.undo();
  })
}

// Handle jumping to the starting frame of the current observation
const startTimeCurrentObsEl = document.getElementById('current-observation-start');
if (startTimeCurrentObsEl) {
  startTimeCurrentObsEl.addEventListener('click', () => {
    // Get the starting frame from dataset
    const startFrame = parseInt(startTimeCurrentObsEl.dataset.frameNumber);

    // Check if it is valid
    if (Number.isInteger(startFrame)) {

      // Get the all players
      const allPlayers = Player.getAllInstances();
      
      if (allPlayers.length > 0) {
        allPlayers.forEach(player => {  
          // Pause the player
          player.pause();
          
          // Convert frames to seconds and set the time of each players
          player.setCurrentTime(framesToSeconds(startFrame));
  
        });
      }
        
    }

  });

}

// Handle jumping to the ending frame of the current observation
const endTimeCurrentObsEl = document.getElementById('current-observation-end');
if (endTimeCurrentObsEl) {
  endTimeCurrentObsEl.addEventListener('click', () => {
    // Get the starting frame from dataset
    const endFrame = parseInt(endTimeCurrentObsEl.dataset.frameNumber);

    // Check if it is valid
    if (Number.isInteger(endFrame)) {

      // Get the all players
      const allPlayers = Player.getAllInstances();
      
      if (allPlayers.length > 0) {
        allPlayers.forEach(player => {  
          // Pause the player
          player.pause();
          
          // Convert frames to seconds and set the time of each players
          player.setCurrentTime(framesToSeconds(endFrame));
  
        });
      }
        
    }

  });

}



// Trial for reading the ethogram
const importEthogramBtn = document.getElementById('import-ethogram-btn');
if (importEthogramBtn) {
  
  importEthogramBtn.addEventListener('click', async () => {
    
    // Check if the main video is opened
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) {
      showAlertToast('Open the main video before importing the ethogram!', 'warning');
      return;
    }

    // Check if the Ethogram instance was initialized
    const ethogram = mainPlayer.getEthogram();
    if (!ethogram) {
      console.log('No Ethogram instance for this player could be found!');
      return;
    }


    // ---------------------------------------------------------
    // Handle importing a new ethogram when there is already one
    // ---------------------------------------------------------
    // Check if an ethogram file with non-zero observations is already saved
    const mainVideoPath = mainPlayer.getSource();
    const ethogramFilePath = await window.electronAPI.findEthogramFile(mainVideoPath);

    if (!ethogramFilePath) {
      showAlertToast('Ethogram file could not be found!', 'error');
      return;
    }

    // Read the ethogram file (get the array of observations, each as a Map)
    const obsArr = await window.electronAPI.readEthogramFile(ethogramFilePath);

    if (!obsArr) {
      showAlertToast('Ethogram file could not be read!', 'error');
      return;
    }

    const obsCount = obsArr.length;
    if (obsCount > 0) {
      const recordText = obsCount === 1 ? 'record' : 'records';
      // Show modal
      showAlertModal(
        'Ethogram Overwriting Warning', 
        [
          `An ethogram with ${obsCount} ${recordText} already exists for this video. Importing a new ethogram file will overwrt the existing records.`,
          'Are you sure you want to continue?'
        ]
      );

      // Get the user confirmation
      const alertConfirmBtn = document.getElementById('alert-confirm-btn');
      if (alertConfirmBtn) {
        
        const confirmed = await getUserConfirmation(alertConfirmBtn);
        
        // If the user cancels the importing process, do not proceed
        if (!confirmed) return;

        // Hide the modal for the alert
        hideAlertModal(); 

      }

    }

    // ----------------------------------------------------------
    // End of handling user confirmation for ethogram overwriting
    // ----------------------------------------------------------

    try {
  
      // Get the array (Map[]) of observations
      const obsArr = await window.electronAPI.importEthogramFile();

      if (!obsArr) {
        console.log('File selection cancelled!');
        return;
      }

      const obsCount = obsArr.length;

      // Check if the array is empty
      if (obsCount === 0) {
        showAlertToast('No valid record in the ethogram file!', 'error');
        return;
      }

      // Remove the current observations in the ethogram
      const isCleared = await ethogram.clear();

      if (!isCleared) {
        showAlertToast('Ethogram could not be updated! Please try again.', 'error');
        return;
      }

      // Create an Observation instance for each Map instance in the array
      // Add this Observation instance to the Ethogram
      // Add a new row for each Observation into the HTML table for Ethogram
      obsArr.forEach(obsMap => {
      
        const newObs = new Observation(obsMap);
        ethogram.add(newObs);
        
        // Add a row for each observation in the HTML table for the ethogram
        const hideAlert = true;
        addEthogramRow(newObs, hideAlert);
      
      });

      // Show success notification
      const recordText = obsCount === 1 ? `${obsCount} record` : `${obsCount} records`;
      showAlertToast(`${recordText} imported!`, 'success', 'Ethogram Imported');

    } catch (err) {
      console.log(err);
    }

  });

}




// Handle exporting all saved files to a directory
const exportAllBtn = document.getElementById('export-all-btn');
if (exportAllBtn) {

  // Get the modal element
  const modalEl = document.getElementById('export-all-modal');
  
  if (modalEl) {

    const showExportFilesBtn = modalEl.querySelector('#show-exported-files-btn');

    // Show a modal to inform the user when export all button is clicked
    exportAllBtn.addEventListener('click', () => {
  
      const mainPlayer = Player.getMainPlayer();
      if (!mainPlayer) return;

      const exportInfoEl = modalEl.querySelector('#export-dir-info');
      if (exportInfoEl) {
        const videoName = mainPlayer.getName();
        if (videoName) {
          exportInfoEl.innerHTML = `A subdirectory named <span class="badge text-bg-secondary">${videoName}</span> will be created under the main directory`
        }

      }
  
      // Get the modal instance
      const modalBootstrap = bootstrap.Modal.getOrCreateInstance(modalEl);
      if (!modalBootstrap) return;
      
      // Hide the result indicator elements
      const errorEls = Array.from(modalEl.querySelectorAll('.error-indicator'));
      const successEls = Array.from(modalEl.querySelectorAll('.success-indicator'));
      const warningEls = Array.from(modalEl.querySelectorAll('.warning-indicator'));
      errorEls.forEach(el => el.classList.add('d-none'));
      successEls.forEach(el => el.classList.add('d-none'));
      warningEls.forEach(el => el.classList.add('d-none'));
    
      // Hide the button for showing exported files
      if (showExportFilesBtn) showExportFilesBtn.classList.add('invisible');

      // Show the pending indicator elements
      const pendingEls = Array.from(modalEl.querySelectorAll('.pending-indicator'));
      pendingEls.forEach(el => el.classList.remove('d-none'));
  
      // Show the modal
      modalBootstrap.show(); 
  
    });

    // Export all files linked to main video
    const exportConfirmBtn = modalEl.querySelector('#export-confirm-btn');
    if (exportConfirmBtn) {
      
      exportConfirmBtn.addEventListener('click', async() => {

        const mainPlayer = Player.getMainPlayer();
        if (!mainPlayer) return;

        // First update the metadata file
        const metadata = Player.getMetadata();
        if (metadata) {
          await metadata.writeToFile();
        }

        // Check if an export directory was chosen
        const configData = await window.electronAPI.getFromConfig();
        if (!configData || !configData.exportDirPath) {
          showAlertToast('Please select an export directory first!', 'warning');
          return;
        }
        
        // Hide pending indicators
        const pendingEls = modalEl.querySelectorAll('.pending-indicator');
        pendingEls.forEach(el => el.classList.add('d-none'));

        // Hide error indicators
        const errorEls = modalEl.querySelectorAll('.error-indicator');
        errorEls.forEach(el => el.classList.add('d-none'));

        // Show loading indicators
        const loadingEls = modalEl.querySelectorAll('.loading-indicator');
        loadingEls.forEach(el => el.classList.remove('d-none'));
  
        // Get the response
        const response = await window.electronAPI.exportAll();
  
        // Hide the loading indicators - with delay for better user experience
        await new Promise(resolve => setTimeout(resolve, 1000));
        loadingEls.forEach(el => el.classList.add('d-none'));
        
        // If no response, show error icon for all files
        if (!response) {
          errorEls.forEach(el => el.classList.remove('d-none'));
          showAlertToast('Selected export directory either does not exist or inaccessible!', 'error', 'Invalid Export Directory')
          return;
  
        }

        // Save the final output directory path to the relevant button to open it in the OS file manager
        const showExportFilesBtn = modalEl.querySelector('#show-exported-files-btn');
        if (showExportFilesBtn && response.outDirPath) {
          showExportFilesBtn.classList.remove('invisible');
          showExportFilesBtn.dataset.path = response.outDirPath;
        }
        
        // Get the HTML element for behavior
        const behaviorEl = modalEl.querySelector('[data-file-type="behaviors"]');
        if (!behaviorEl) return;
  
        // Check if the behavior file was exported
        if (response.behaviors.exported) {
          const successEl = behaviorEl.querySelector('.success-indicator');
          if (successEl) successEl.classList.remove('d-none');
        
        // Check if the behavior file was found but not exported
        } else if (response.behaviors.found) {
          const errorEl = behaviorEl.querySelector('.error-indicator');
          if (errorEl) errorEl.classList.remove('d-none');
          
        // Check if the behavior file was not found or exported
        } else {
          const warningEl = behaviorEl.querySelector('.warning-indicator');
          if (warningEl) warningEl.classList.remove('d-none');
          
        }
  
        // Get the HTML element for tracking
        const trackingEl = modalEl.querySelector('[data-file-type="tracking"]');
        if (!trackingEl) return;

        // Check if the tracking file is exported
        if (response.tracking.exported) {
          const successEl = trackingEl.querySelector('.success-indicator');
          if (successEl) successEl.classList.remove('d-none');

        // Check if the tracking file was found but not exported
        } else if (response.tracking.found) {
          const errorEl = trackingEl.querySelector('.error-indicator');
          if (errorEl) errorEl.classList.remove('d-none');
          
        // Check if the tracking file was not found or exported
        } else {
          const warningEl = trackingEl.querySelector('.warning-indicator');
          if (warningEl) warningEl.classList.remove('d-none');
          
        }
  
        // Get the HTML element for identification
        const identificationEl = modalEl.querySelector('[data-file-type="identification"]');
        if (!identificationEl) return;
  
        // Check if the identification file is exported
        if (response.identification.exported) {
          const successEl = identificationEl.querySelector('.success-indicator');
          if (successEl) successEl.classList.remove('d-none');

        // Check if the identification file was found but not exported
        } else if (response.identification.found) {
          const errorEl = identificationEl.querySelector('.error-indicator');
          if (errorEl) errorEl.classList.remove('d-none');
          
        // Check if the identification file was not found or exported
        } else {
          const warningEl = identificationEl.querySelector('.warning-indicator');
          if (warningEl) warningEl.classList.remove('d-none'); 
          
        }
  
        // Get the HTML element for notes
        const notesEl = modalEl.querySelector('[data-file-type="notes"]');
        if (!notesEl) return;
  
        // Check if the notes file is exported
        if (response.notes.exported) {
          const successEl = notesEl.querySelector('.success-indicator');
          if (successEl) successEl.classList.remove('d-none');

        // Check if the notes file was found but not exported
        } else if (response.notes.found) {
          const errorEl = notesEl.querySelector('.error-indicator');
          if (errorEl) errorEl.classList.remove('d-none');
          
        // Check if the notes file was not found or exported
        } else {
          const warningEl = notesEl.querySelector('.warning-indicator');
          if (warningEl) warningEl.classList.remove('d-none');
          
        }

        // Get the HTML element for metadata
        const metadataEl = modalEl.querySelector('[data-file-type="metadata"]');
        if (!metadataEl) return;
  
        // Check if the notes file is exported
        if (response.metadata.exported) {
          const successEl = metadataEl.querySelector('.success-indicator');
          if (successEl) successEl.classList.remove('d-none');

        // Check if the notes file was found but not exported
        } else if (response.metadata.found) {
          const errorEl = metadataEl.querySelector('.error-indicator');
          if (errorEl) errorEl.classList.remove('d-none');
          
        // Check if the notes file was not found or exported
        } else {
          const warningEl = metadataEl.querySelector('.warning-indicator');
          if (warningEl) warningEl.classList.remove('d-none');
          
        }
  
      });
  
    }

    // Open the output directory under the main export directory with OS file manager
    if (showExportFilesBtn) {
      showExportFilesBtn.addEventListener('click', async () => {
        const dirPath = showExportFilesBtn.dataset.path;
        if (dirPath) {
          await window.electronAPI.openPath(dirPath);
        }

      });
    }

    // Open the main export directory with OS file manager
    const openExportDirBtn = modalEl.querySelector('#open-export-dir-btn');
    if (openExportDirBtn) {
      openExportDirBtn.addEventListener('click', async () => {
        const dirPath = openExportDirBtn.dataset.path;
        if (dirPath) {
          const response = await window.electronAPI.openPath(dirPath);
          if (response != '') {
            showAlertToast(response, 'error');

          }
        }

      });
    }

  }

}

// Show release notes
const showReleaseNotesModalBtn = document.getElementById('show-release-notes-btn');
if (showReleaseNotesModalBtn) {
  showReleaseNotesModalBtn.addEventListener('click', () => {
    const releaseNotesModalEl = document.getElementById('release-notes-modal');
    if (releaseNotesModalEl) {
      const modal = bootstrap.Modal.getOrCreateInstance(releaseNotesModalEl);
      modal.show();
    }
  });

}

// Make the zoomed canvas div draggable
const zoomVideoDiv = document.getElementById('zoom-video-div');
if (zoomVideoDiv) {
  dragElement(zoomVideoDiv);

  // Dismiss the entire element for the zoomed region
  const btnDismiss = zoomVideoDiv.querySelector('.btn-dismiss');
  if (btnDismiss) {
    btnDismiss.addEventListener('click', () => {
      const mainPlayer = Player.getMainPlayer();
      if (!mainPlayer) return;

      const zoomRect = mainPlayer.zoomRect;
      if (!zoomRect) return;

      // Save zoom display status
      zoomRect.shouldHideEl = true;

      // Hide the zoom element
      zoomVideoDiv.classList.add('d-none');
      
      // Clear the zoom selection rectangle on the main video
      const zoomSelectCanvas = document.getElementById('main-zoom-select-canvas');
      if (zoomSelectCanvas) {
        const ctx = zoomSelectCanvas.getContext('2d');
        if (ctx) {
          ctx.clearRect(0, 0, zoomSelectCanvas.width, zoomSelectCanvas.height);
        }
      }

      // Clear request id for zooming
      window.cancelAnimationFrame(Player.zoomRequestId);
      Player.zoomRequestId = undefined;

    });

  }

  // Collapse only the canvas. Show the panel for control buttons
  const zoomCollapseBtn = zoomVideoDiv.querySelector('.btn-collapse');
  if (zoomCollapseBtn) {
    zoomCollapseBtn.addEventListener('click', () => {
      const canvas = zoomVideoDiv.querySelector('canvas');
      if (!canvas) return;
      
      const iconEl = zoomCollapseBtn.querySelector('span');
      if (!iconEl) return;
      
      if (!canvas.parentElement.classList.contains('d-none')){
        canvas.parentElement.classList.add('d-none');
        iconEl.textContent = 'expand_content';

      } else {
        canvas.parentElement.classList.remove('d-none');
        iconEl.textContent = 'collapse_content'
      }

    });

  }

}

// Handle changing the zoom scale
const changeZoomScaleInput = document.getElementById('change-zoom-scale-input');
if (changeZoomScaleInput) {
  changeZoomScaleInput.addEventListener('change', async () => {
    const newValue = changeZoomScaleInput.value;
    if (newValue) {
      const response = await Player.setZoomScale(newValue);
      if (!response) {
        showAlertToast('Zoom scale must be between 100% and 300%!', 'error', 'Invalid Zoom Scale Input')
        return;
      }

      showAlertToast(`Zoom scale changed to ${Player.getZoomScale()}%`, 'success');

    }

  });

}





// // Zoom in and out of the main video
// const zoomBtn = document.getElementById('zoom-btn');
// if (zoomBtn) {
//   const mainPlayer = Player.getMainPlayer();
//   if (mainPlayer) {
//     zoomBtn.addEventListener('click', () => mainPlayer.zoom());
//   }

// }

// const resetZoomBtn = document.getElementById('reset-zoom-btn');
// if (resetZoomBtn) {
//   const mainPlayer = Player.getMainPlayer();
//   if (mainPlayer) {
//     resetZoomBtn.addEventListener('click', () => mainPlayer.resetZoom());
//   }

// }


// // Handle toggling tracking frames
// const toggleZoomTracksBtn = document.getElementById('toggle-zoom-tracks-btn');
// if (toggleZoomTracksBtn) {
//   toggleZoomTracksBtn.addEventListener('click', () => {
//     const mainPlayer = Player.getMainPlayer();
//     // Check if the main player exists
//     if (!mainPlayer) {
//       showAlertToast('Open the main video first!', 'warning', 'Tracking Unavailable');
//       return;
//     } else { 
      
//       // If no tracking file is uploaded, show a warning
//       if (mainPlayer.getTrackingMap().isEmpty()) {
//         showAlertToast('Upload the tracking file first!', 'warning', 'Tracking Mode Disabled');
//         return;
//       }

//       // If everything is okay, change button states and tracking visibility
//       toggleZoomTracksBtn.classList.toggle('off');
//       const zoomTrackingCanvas = document.getElementById('zoom-tracking-canvas');
//       if (zoomTrackingCanvas) {
//         zoomTrackingCanvas.classList.toggle('d-none');
//         if (toggleZoomTracksBtn.classList.contains('off')) {
//           toggleZoomTracksBtn.querySelector('span').textContent = 'frame_person_off';
//         } else {
//           toggleZoomTracksBtn.querySelector('span').textContent = 'frame_person';
//         }
//       }
//     }

//   });

// }