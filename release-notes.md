## Version 0.4.0
### New Features
- Added zoom functionality. Hold down your mouse over the main view and draw a rectangle to zoom in on a specific area. Release the mouse to view the zoomed area in a new window, where you can adjust the zoom scale or use the settings menu. The window for zoomed area can be moved and hidden freely. 

### Improvements
- The mouse pointer changes to a cursor over the tracks, indicating that they are clickable.

## Version 0.3.6

## Bug Fixes
- Fixed the playback syncing issue with the replay and forward buttons
- Fixed the duplicate secondary player issue
- Fixed the overflowing issue with the behavior table

## Version 0.3.5

### Improvements
- Moved the skip seconds selection menu to the control bar from the settings menu
- Automatic display of release notes after an update
- Increased the hover video size
- Made the hover time text more prominent

### Bug Fixes
- Fixed the issue where the play/pause functionality was not working consistently when using the space bar

## Version 0.3.4

### New Features
- Go to start/end frame of an observation when start/end frame row element is clicked
- Help page with documentation

## Version 0.3.3

### New Features
- Automatic updates for macOS and Windows
- Custom title bar with refined control button icons

### Bug Fixes
- Fixed inconsistencies when saving a snapshot

