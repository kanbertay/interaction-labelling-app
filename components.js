/**
 * Components used on the interface
 */


// const path = require('node:path');
// const bootstrap = require('bootstrap');
// const {
//   showAlert, 
//   getFilesInFolder, 
//   getVideoFilePaths, 
//   readTrackingFile,
//   formatSeconds,
//   createSecondaryVideoDivs
// } = require('./helpers')

// import {formatSeconds} from './helpers.js'

import {

  getClickedBoxes, 
  showAlertToast,
  getFileNameWithoutExtension, 
  getSpeciesName,
  secondsToFrames,
  minutesToFrames,
  framesToSeconds,
  formatSeconds,
  showOverlappingTracksToast,
  getRandomLetterNotIn,
  clearEthogramTable,
  showTrackingSaveStatus,
  showOrHideSpinner,
  produceLabelText,
  updateHotkeyDomElement,
  showToastForBehaviorRecording,
  showLastEditForFile,
  handleBehaviorRecordByClick,
  updateTracksFromToast,
  updateTracksFromNameDropdown,
  addEthogramRow,
  dragElement,
  updateZoomScaleDomEls

} from "./helpers.js";


class Observation {
  /**
   * 
   * @param {Map} entries 
   */
  
  constructor(entries) {

    // Check if the input is given and an instance of Map
    if (entries && entries instanceof Map) {
      this.entries = entries;
    } else {
      // If no input is given or it is not in correct format, initialize the instance
      this.entries = new Map([
        ['index', null],
        ['subjectName', null],
        ['subjectId', null],
        ['subjectSpecies', null],
        ['action', null],
        ['targetName', null],
        ['targetId', null],
        ['targetSpecies', null],
        ['startFrame', null],
        ['endFrame', null],
      ]);

    }

    // Track the last update for undo functionality
    this.lastUpdatedKeys = [];

  }


  get(key) {
    return this.entries.get(key);

  }

  /**
   * Updates the observation
   * @param {Array} entries Array of key-value pairs
   */
  update(...entries) {
    let updatedKeys = [];
    for (const [key, value] of entries) {
      this.entries.set(key, value);
      updatedKeys.push(key);
    }

    // Keep track of the last updated keys
    this.lastUpdatedKeys.push(updatedKeys);
    
    // Show the changes on the HTML element
    this.show();

  }

  /**
   * Undoes the last selection
   * @param  {...any} keys 
   */
  undo() {
    // Undo the values of the saved keys from the last selection
    const undoKeyArr = this.lastUpdatedKeys.pop();
    
    // Set the given key's value to null
    if (undoKeyArr) {
      undoKeyArr.forEach(key => this.entries.set(key, null));

    }

    // Show the changes on the HTML element
    this.show();

  }

  /**
   * Gets the entries of an observation
   * @returns {Map} - Map of entries
   */
  getEntries() {
    return this.entries;
  }

  /**
   * Gets the last updated keys of an observation
   * @returns {Array} - Last updated key strings
   */
  getLastSelection() {
    return this.lastUpdatedKeys;
  }


  /**
   * Gets the index of an observation
   * @returns {Number} - Index within the ethogram
  */
  get index() {
   return this.entries.get('index');
  }

  /**
   * Gets the subject name of an observation
   * @returns {String} - Subject name
   */
  get subjectName() {
    return this.entries.get('subjectName');
  }

  /**
   * Gets the subject id of an observation
   * @returns {Number}
   */
  get subjectId() {
    return this.entries.get('subjectId');
  }

  /**
   * Gets the action of an observation
   * @returns {String}
   */
  get action() {
    return this.entries.get('action');
  }

  /**
   * Gets the target name of an observation
   * @returns {String}
   */
  get targetName() {
    return this.entries.get('targetName');
  }

  /**
   * Gets the target id of an observation
   * @returns {Number}
   */
  get targetId() {
    return this.entries.get('targetId');
  }

  /**
   * Gets the target species of an observation
   * @returns {Number} - Target species ID
   */
  get targetSpecies() {
    return this.entries.get('targetSpecies');
  }

  /**
   * Gets the starting frame of an observation
   * @returns {Number}
   */
  get startFrame() {
    return this.entries.get('startFrame');
  }

  /**
   * Gets the starting frame of an observation
   * @returns {Number}
   */
  get endFrame() {
    return this.entries.get('endFrame');
  }

  /**
   * Gets the subject ID of an observation
   * @returns {Number} - Subject ID
   */
  get subjectId() {
    return this.entries.get('subjectId');
  }

  /**
   * Gets the subject species of an observation
   * @returns {Number} - Subject species ID
   */
   get subjectSpecies() {
    return this.entries.get('subjectSpecies');
  }

  /**
   * Checks if the current observation is empty
   * @returns - True if all values of the current observation is null
   */
  isEmpty() {
    return this.entries.values().every(value => value === null);
  }

  /**
   * Shows the current observation on its associated HTML element
   */
  show() {
    // Get the HTML element for current observation
    const divEl = document.getElementById('current-observation-div');
    if (!divEl) return;
  
    // Check whether the current observation is empty and no previous observation exists
    if (Player.getCurrentObservation().isEmpty()) {
      
      // Hide the div element
      divEl.classList.add('d-none');
      
    } else {
      
      // Get the selection from the current observation
      const subjectName = this.subjectName;
      const action = this.action;
      const targetName = this.targetName;
      const startTime = this.startFrame;
      const endTime = this.endFrame;

      // Make the div element visible
      divEl.classList.remove('d-none');

      // Show the description of the observation (subject name, action, target name)
      const descriptionEl = divEl.querySelector('#current-observation-description');
      if (descriptionEl) {
        // Show the fields which are not null
        const selections = [subjectName, action, targetName].filter(selection => selection !== null);
        descriptionEl.textContent = selections.join('-');
        
      }
      
      // Show the starting and ending time of the observation
      const startTimeEl = divEl.querySelector('#current-observation-start');
      if (startTimeEl) {
        if (startTime !== null) {
          startTimeEl.textContent = formatSeconds(framesToSeconds(startTime));

          // Add starting frame to dataset to allow user to jump to that frame via clicking
          startTimeEl.dataset.frameNumber = startTime;

        }
      }

      // Show the starting and ending time of the observation
      const endTimeEl = divEl.querySelector('#current-observation-end');
      if (endTimeEl) {
        // Hide the element by default
        endTimeEl.classList.add('d-none');
        
        // Check if end time is given
        if (endTime !== null) {
          // Make the element visible
          endTimeEl.classList.remove('d-none');

          // Convert frames to MM:SS
          endTimeEl.textContent = formatSeconds(framesToSeconds(endTime));

          // Add starting frame to dataset to allow user to jump to that frame via clicking
          endTimeEl.dataset.frameNumber = endTime;

        }
      }
        

      // Show the last selected action as a badge
      const badgeEl = divEl.querySelector('#current-observation-badge');
      if (badgeEl) {
        
        // Determine the last selection
        let lastSelection;
        
        if (subjectName !== null) {
          lastSelection = 'subject';
        }
        
        if (action !== null) {
          lastSelection = 'action';
        }
        
        if (targetName !== null && endTime !== null) {
          lastSelection = 'target';
        }

        badgeEl.textContent = lastSelection;

      }
      

    }

  }

  
}

// Player
class Player {
  // get the DOM element id and options for the video
  domId;
  options;

  // Determine whether the operation system is MacOS
  static onMacOS = window.electronAPI.getPlatform() === 'darwin';

  static allInstances = []; // Save all instances of Players
  static secondaryPlayers = [];
  static mainPlayer;
  static skipSeconds = 5;
  // static frameRate = 29.97; // !!Get the frame rate dynamically instead of hard coding!!
  static maxPlaybackRate = 2;
  static minPlaybackRate = 0.25;
  static individualNames; // Names of invidiual primates
  static actionNameArr; // Names of actions
  static labellingMode = false; // Boolean to activate hotkeys for recording behaviors
  static isTyping = false;// Boolean to determine if user is typing in a text area or input
  static pressedKeys = [];
  static timeoutKeyPress = null;
  static keyState = {}; // Track the state of each pressed key (to determine to combined key presses)
  static statusUpdateFreq = 60000; // Set the update frequency for showing last modified times for file (in milliseconds)
  static ethogramIntervalId;
  static notesIntervalId;
  static appVersion; // App version number

  static zoomDragColor = 'yellow'; // Color for drawing a rectangle on a canvas for zooming into a video
  static zoomScale = 200; // Zoom level for the main player
  static zoomRequestId; // Request Id for zooming (to be used with requestAnimationFrame and cleared when the player is paused or ended)

  static draggedDist = 0; // Actual distance in pixels for mouse drag to prevent clicks or accidental mouse moves from being registered as drags
  static minDragDist = 150; // Threshold in pixels to register mouse drags
  static mouseDragTimeThreshold = 1000; // Time in ms to check if the previous mouse down event is more than this threshold to prevent accidental clicks from being registered as drags
  static isMouseDown = false;
  static lastMouseDownTime = 0;


  
  // Track the current observation for behavior recording
  static currentObservation = new Observation();
  
  // Username for saving to metadata of exported files
  static username; 

  static hotkeys = new Map([
    ['playback', []],
    ['labelling', []],
    ['individuals', []],
    ['actions', []]
  ]);

  // Define the properties of the default hotkeys
  // Hotkey objects will be constructed for each of them
  static defaultHotkeyPropArr = [
    {
      category: 'playback',
      name: 'stepBackward',
      description: 'Go back to previous frame',
      key: 'ArrowLeft',
      modifiers: [],
    },
    {
      category: 'playback',
      name: 'stepForward',
      description: 'Skip to next frame',
      key: 'ArrowRight',
      modifiers: [],
    },
    {
      category: 'playback',
      name: 'playPause',
      description: 'Play/pause',
      key: ' ', // Spacebar
      modifiers: [],
    },
    {
      category: 'playback',
      name: 'replay',
      description: 'Seek backward',
      key: 'ArrowLeft',
      modifiers: ['Shift']
    },
    {
      category: 'playback',
      name: 'forward',
      description: 'Seek forward',
      key : 'ArrowRight',
      modifiers: ['Shift']
    },
    {
      category: 'playback',
      name: 'increaseVolume',
      description: 'Increase volume',
      key: 'ArrowUp',
      modifiers: ['Alt']
    },
    {
      category: 'playback',
      name: 'decreaseVolume',
      description: 'Decrease volume',
      key: 'ArrowDown',
      modifiers: ['Alt']
    },
    {
      category: 'playback',
      name: 'muteUnmute',
      description: 'Mute/unmute',
      key: 'm',
      modifiers: []
    },
    {
      category: 'playback',
      name: 'increaseSpeed',
      description: 'Increase playback speed',
      key: 'ArrowUp',
      modifiers: ['Shift']
    },
    {
      category: 'playback',
      name: 'decreaseSpeed',
      description: 'Decrease playback speed',
      key: 'ArrowDown',
      modifiers: ['Shift']
    },
    {
      category: 'labelling',
      name: 'toggleLabelling',
      description: 'Enable/disable labelling',
      key: 'l',
      modifiers: Player.onMacOS ? ['Meta'] : ['Control']
    },
    {
      category: 'labelling',
      name: 'endObservation',
      description: 'End current observation',
      key: 'Enter',
      modifiers: []
    },
    {
      category: 'labelling',
      name: 'boxSelection',
      description: 'Choose box as target',
      key: 'b',
      modifiers: []

    },
    {
      category: 'labelling',
      name: 'undo',
      description: 'Undo last step',
      key: 'z',
      modifiers: Player.onMacOS ? ['Meta'] : ['Control']
    }

  ];

  /**
   * Create Hotkey objects with the default values
   */
  static createDefaultHotkeys() {
    // Initialize hotkey categories as empty arrays 
    // to be filled with Hotkey objects
    const defaultHotkeyMap = new Map([
      ['playback', []],
      ['labelling', []],
      ['individuals', []],
      ['actions', []]
    ]);

    Player.defaultHotkeyPropArr.forEach(hotkeyObj => {
      // Create a Hotkey instance
      const hotkey = new Hotkey(...Object.values(hotkeyObj));

      // Get the category of the hotkey
      const category = hotkeyObj.category;

      // Look for the category in the hotkey map
      if (defaultHotkeyMap.has(category)) {
        defaultHotkeyMap.get(category).push(hotkey);
      } else {
        defaultHotkeyMap.set(category, [hotkey]);
      }
    });

    // Set the default hotkeys
    Player.hotkeys = defaultHotkeyMap;

  }

  

  constructor(domId, options) {
    this.domId = domId;
    this.mainPlayer = domId.includes('main');
    this.el = document.getElementById(domId);

    // Default setup options
    this.el.controls = false;
    this.el.autoplay = false;
    this.el.preload = 'auto';

    // Keep track of added events
    this.events = [];

    // Coordinates of rectangle for selecting a region for zoom 
    this.zoomRect = {
      tempX: null, // Temporary top left x-coord (Before mouseup event and during mousemove event)
      tempY: null,
      tempWidth: null, // Temporary width
      tempHeight: null,
      startX: null, // Final top left x-coord (after mouseup event)
      startY: null,
      width: null, // Final width (after mouseup event)
      height: null,
      canvasWidth: null,
      canvasHeight: null,
      aspectRatio: null,
      shouldHideEl: false,
      shouldUpdate: false,
    };

    // Add user options if provided
    if (options !== undefined) {
      this.options = options;
      for (const option in options) {
        this.el[option] = this.options[option]
      }

    }

    // Adjust the playback rate
    // if (Player.getMainPlayer()) {
    //   console.log(Player.getMainPlayer().getPlaybackRate())
    //   this.el.playbackRate = Player.getMainPlayer().getPlaybackRate();
    // }
    
    if (this.mainPlayer) {
      Player.mainPlayer = this;
      this.ethogram = new Ethogram();
      this.trackingMap = new TrackingMap();
      this.metadata = new Metadata();

      // Set up the play-pause btn
      const playPauseButton = new Button('#play-pause-btn');
      playPauseButton.setIcon('play_circle', 'size-48');
      playPauseButton.on('click', () => Player.getAllInstances().forEach(player => player.playPause()));

      // Set up the mute button
      const muteButton = new Button('#mute-btn');
      muteButton.setIcon('volume_up');
      muteButton.on('click', () => this.toggleMute());

      // Set up the skip forward button
      const forwardButton = new Button('#forward-btn');
      forwardButton.on('click', () =>  Player.getAllInstances().forEach(player => player.forward()));

      // Set up the skip backward button
      const replayButton = new Button('#replay-btn');
      replayButton.on('click', () => Player.getAllInstances().forEach(player => player.replay()));  

      // Attach the playback buttons
      this.attachButton(playPauseButton, 'play-pause');
      this.attachButton(muteButton, 'mute');
      this.attachButton(forwardButton, 'forward');
      this.attachButton(replayButton, 'replay');
      
      // Attach the control bar
      this.attachControlBar(new ControlBar('#control-bar'));

      // Set up playback speed button
      const playbackRateList = document.getElementById('playback-rate-list');
      if (playbackRateList) {
        const listItems = playbackRateList.querySelectorAll('.dropdown-item');
        if (listItems) {
          listItems.forEach(item => {
            item.addEventListener('click', () => {
              const selectedValue = parseFloat(item.dataset.playbackRate);
              if (selectedValue) {
                this.setPlaybackRate(selectedValue);
              }
            });
          });
        
        }

      }

      // Set up zoom scale button located on the zoomed vide region div element
      const zoomScaleDropdownBtn = document.getElementById('zoom-scale-dropdown');
      if (zoomScaleDropdownBtn) {
        const listItems = zoomScaleDropdownBtn.querySelectorAll('.dropdown-item');
        if (listItems) {
          listItems.forEach(item => {
            item.addEventListener('click', async () => {
              const selectedValue = parseFloat(item.dataset.zoomScale);
              if (selectedValue) {
                console.log('select zoom scale', selectedValue)
                await Player.setZoomScale(selectedValue);
              }
            });
          });
        
        }

      }

      // Get the default skip forward and back seconds selection
      const skipSecondsList = document.getElementById('skip-seconds-list');
      if (skipSecondsList) {  
        const listItems = skipSecondsList.querySelectorAll('.dropdown-item');
        if (listItems) {
          listItems.forEach(item => {
            item.addEventListener('click', async () => {
              const selectedValue = parseFloat(item.dataset.skipSeconds);
              if (selectedValue) {
                await Player.setSkipSeconds(selectedValue);x
              }
            });
          });
        
        }

      }
          
      // // Get the default skip forward and back seconds selection
      // const skipSecondsSelectEl = document.getElementById('skip-seconds-select');
      // if (skipSecondsSelectEl) {        
      //   // Change skip forward and back seconds when user changes the relevant setting
      //   skipSecondsSelectEl.addEventListener('change', async () => {
      //     await Player.setSkipSeconds(skipSecondsSelectEl.selectedOptions[0].value);
          
      //   });
      // }

      const controlBar = this.getControlBar();
      if (!controlBar) return;

      const progressBarEl = controlBar.progressBar;
      if (!progressBarEl) return;

      // Get the click event to change the current time with user input
      progressBarEl.addEventListener('click', (e) => { 
        const progressBarRect = progressBarEl.getBoundingClientRect();
        const clickPosition = e.clientX - progressBarRect.left;
        const videoTime = (clickPosition / progressBarRect.width) * this.getDuration();
        
        Player.getAllInstances().forEach(player => {
          // player.pause();
          player.setCurrentTime(videoTime);

        });


      });
    
      // Show video time when hovering over the progress bar 
      progressBarEl.addEventListener('mousemove', (e) => {
        
        const hoverTimeDivEl = document.getElementById('hover-time-div');
        if (!hoverTimeDivEl) return;

        const hoverTimeTextEl = hoverTimeDivEl.querySelector('.hover-time-text');
        if (!hoverTimeTextEl) return;
        
        const mainPlayer = Player.getMainPlayer();
        if (!mainPlayer) return;

        if (!mainPlayer.getDuration()) return;

        const progressBarRect = progressBarEl.getBoundingClientRect();
        const clickPosition = e.clientX - progressBarRect.left;
        const hoveredTime = (clickPosition / progressBarRect.width) * mainPlayer.getDuration();
        const formattedTime = formatSeconds(hoveredTime);

        // hoverTimeDivEl.style.left = (e.clientX - hoverTimeDivEl.offsetWidth / 2) + 'px';
        
        hoverTimeTextEl.textContent = formattedTime; 
        // hoverTimeTextEl.classList.remove('d-none');

        
        // Show video frame when hovering over the progress bar
        const hoverVideoEl = document.getElementById('hover-video');
        if (!hoverVideoEl) return;

        // Get the DOM element for showing video frames on hover
        const hoverFrameEl = document.getElementById('hover-frame-div');
        if (!hoverFrameEl) return;

        // Get the canvas 
        const hoverFrameCanvas = hoverFrameEl.querySelector('canvas');
        if (!hoverFrameCanvas) return;

        // Check if canvas context exists
        if (!hoverFrameCanvas.getContext) return;

        if ("requestVideoFrameCallback" in HTMLVideoElement.prototype) {

          // Show the hover frame element 
          hoverFrameEl.classList.remove('d-none');
          
          // Update the on-hover time
          hoverVideoEl.currentTime = hoveredTime;
            
          // Update the hover element position values
          hoverFrameEl.style.left = (e.clientX - hoverTimeDivEl.offsetWidth / 2) + 'px';
          hoverFrameEl.style.top = progressBarRect.top - hoverFrameEl.offsetHeight - 10 + 'px';


          // Update the canvas
          const updateCanvas = (now, metadata) => {
            
            const ctx = hoverFrameCanvas.getContext('2d')

            // Draw the relevant video frame to canvas on hover
            ctx.drawImage(hoverVideoEl, 0, 0, hoverFrameCanvas.width, hoverFrameCanvas.height);
            
            // Re-register the callback to run on the next frame
            hoverVideoEl.requestVideoFrameCallback(updateCanvas);
            
          };
          
          // Initial registration of the callback to run on the first frame
          hoverVideoEl.requestVideoFrameCallback(updateCanvas);

        } else {
          alert("Your browser does not support requestVideoFrameCallback().");

        }

      });
  
      // Hide the on-hover element when mouse leaves the progress bar
      progressBarEl.addEventListener('mouseleave', (e) => {
        const hoverFrameEl = document.getElementById('hover-frame-div');
        if (!hoverFrameEl) return;
        hoverFrameEl.classList.add('d-none');

      });

      // Show spinner when video is loading
      this.on('loadstart', () => this.showSpinner());
      // mainPlayer.on('canplaythrough', () => mainPlayer.hideSpinner());
        
      // Synchronize all videos
      this.on('pause', () => {
        this.updateButtonIcons();
        Player.getSecondaryPlayers().forEach(player => player.pause());
        // Clear request id for zooming
        // window.cancelAnimationFrame(Player.zoomRequestId);
        // Player.zoomRequestId = undefined;
      });
          
      this.on('playing', () => {
        this.updateButtonIcons();
        Player.getSecondaryPlayers().forEach(player => player.play());
      });

        
      this.on('play', Player.drawZoomedVideoRegion);

      this.on('ended', () => {
        window.cancelAnimationFrame(Player.zoomRequestId);
        Player.zoomRequestId = undefined;
      })
      
      this.on('ratechange', () => {
        // Sync playback speed
        const currentSpeed = this.getPlaybackRate();
        Player.getSecondaryPlayers().forEach(player => {
          player.setPlaybackRate(currentSpeed);
        });
        
        // Update the text inside rate button
        const playbackRateButton = document.getElementById('playback-rate-btn');
        playbackRateButton.textContent = `Speed: ${currentSpeed}x`
      
        // Update active states of the selection
        const playbackRateList = document.getElementById('playback-rate-list');
        if (playbackRateList) {
          const listItems = playbackRateList.querySelectorAll('.dropdown-item');
          if (listItems) {
            listItems.forEach(item => {
              // Make the item with the same speed as the main player active
              if (parseFloat(item.dataset.playbackRate) === currentSpeed) {
                item.classList.add('active');
              } else {
                // Remove active states from all items
                item.classList.remove('active');
              }
            });

          }

        }

      });

      // Events when the main video is loaded
      this.on('loadedmetadata', async () => {

        const mainPlayerSrc = this.getSource();
          
        // Set the frame rate
        await this.setFrameRate();

        // Reset the intevals IDs and DOM elements for last edit status
        Player.resetEthogramInterval();
        Player.resetNotesInterval();

        // Get the default skip forward and back seconds selection
        const skipSecondsSelectEl = document.getElementById('skip-seconds-select');
        if (skipSecondsSelectEl) {        
          await Player.setSkipSeconds(skipSecondsSelectEl.selectedOptions[0].value);

        }

        // Search for metadata file in the user data directory
        const metadataFilePath = await window.electronAPI.findMetadataFile(mainPlayerSrc);
        if (metadataFilePath) {
          const metadata = await window.electronAPI.readMetadataFile(metadataFilePath);
          if (metadata && metadata.timestamp) {
            Player.getAllInstances().forEach(player => player.setCurrentTime(metadata.timestamp));
          }
          
        }

        // Search for tracking or identification file
        let trackingFilePath, tracks, firstAvailTrackIds
        const trackingSearchResponse = await window.electronAPI.findTrackingFile(mainPlayerSrc);
        if (trackingSearchResponse) {
          // First check if an identification file was saved before
          if (trackingSearchResponse.idFilePath) {
            trackingFilePath = trackingSearchResponse.idFilePath;
        
          // If no identification file, look for a tracking file
          } else if (trackingSearchResponse.trackFilePath) {
            trackingFilePath = trackingSearchResponse.trackFilePath;
        
          }

          // Read the tracking file
          if (trackingFilePath) {
            const response = await window.electronAPI.readTrackingFile(trackingFilePath);
            if (response) {
              tracks = response.trackingMap;
              firstAvailTrackIds = response.firstAvailTrackIds;
      
            }

          }

        }

        // Set the tracking map obtained from input to the function
        if (tracks && firstAvailTrackIds) {
          let editHistory = this.trackingEditHistory ? this.trackingEditHistory : [];
          this.setTrackingMap(tracks, firstAvailTrackIds, editHistory);
        } else {
          // Clear the previous map and tracking file path from config file
          this.resetTrackingMap();
        }

        // Update the progress bar
        const controlBar = this.getControlBar();
        if (controlBar) controlBar.updateProgressBar();
      
        // Set the attributes of hidden video element 
        // This hidden video element is used for showing individual video frames when hovering over 
        this.setCanvasOnHover();

        // Get the canvas on the main player to show tracking rectangles
        const mainCanvas = document.getElementById('main-tracking-canvas');
        if (mainCanvas) {
          this.attachCanvas(mainCanvas);
          
          // Make tracking boxes clickable and interactive
          mainCanvas.addEventListener('click', (e) => {
            this.makeBoxesInteractive(e);
          });

          // Show dropdown for individuals when right-clicked on tracking boxes
          mainCanvas.addEventListener('contextmenu', (e) => {
            e.preventDefault();
            this.showRightClickMenu(e);
          });

          // Zoom into the selected region of the main video
          const zoomSelectCanvas = document.getElementById('main-zoom-select-canvas');
          if (zoomSelectCanvas) {
            zoomSelectCanvas.width = this.el.videoWidth;
            zoomSelectCanvas.height = this.el.videoHeight;
            // Draw a rectangle on mouse drag
            // const rectangle = {
            //   startX: null,
            //   startY: null,
            //   width: null,
            //   height: null,
            //   aspectRatio: this.el.videoWidth / this.el.videoHeight
            // };

            if (this.zoomRect) {
              this.zoomRect.aspectRatio = this.el.videoWidth / this.el.videoHeight;

            }

            // Detect mouse dragging on the main canvas
            mainCanvas.addEventListener('mousedown', (e) => {
              const mainPlayer = Player.getMainPlayer();
              if (!mainPlayer) return;

              const zoomRect = mainPlayer.zoomRect;
              if (!zoomRect) return;

              const canvasRect = zoomSelectCanvas.getBoundingClientRect();
              const mouseX = e.clientX - canvasRect.left;
              const mouseY = e.clientY - canvasRect.top;
              const widthRatio = zoomSelectCanvas.width / zoomSelectCanvas.clientWidth;
              const heightRatio =  zoomSelectCanvas.height / zoomSelectCanvas.clientHeight;

              // Reset the rectangle
              // rectangle.startX = mouseX * widthRatio;
              // rectangle.startY = mouseY * heightRatio;
              
              zoomRect.tempX = mouseX * widthRatio;
              zoomRect.tempY = mouseY * heightRatio;

              // Check if the previous click is more than the threshold in ms
              // to prevent accidental clicks from being registered as drags
              // if (performance.now() - Player.lastMouseDownTime > Player.mouseDragTimeThreshold) {
              //   Player.isMouseDown = true;
              //   console.log('New rect!');
              //   Player.lastMouseDownTime = performance.now();
              // } else {
              //   Player.isMouseDown = false;
              //   console.log('NOT a new rect!');
              // }

              Player.isMouseDown = true;
              
            });

            mainCanvas.addEventListener('mousemove', (e) => {
              
              // Check if the event triggered directly over the canvas to exclude events triggered by other elements when they are over the canvas
              if (!e.composedPath().includes(mainCanvas)) {
                console.log('mousemove NOT over main canvas');
                return;
              }

              const mainPlayer = Player.getMainPlayer();
              if (!mainPlayer) return;

              mainPlayer.showCursorOnBoxes(e);

              const zoomRect = mainPlayer.zoomRect;
              if (!zoomRect) return;

              // Check if mouse is moving while mouse button is down
              if (e.buttons !== 1 || !Player.isMouseDown) return;

              if (!Player.getZoomScale()) return;

              // Calculate clicked position relative to canvas  
              const canvasRect = zoomSelectCanvas.getBoundingClientRect();
              const widthRatio = zoomSelectCanvas.width / zoomSelectCanvas.clientWidth;
              const heightRatio =  zoomSelectCanvas.height / zoomSelectCanvas.clientHeight;
              const mouseX = (e.clientX - canvasRect.left) * widthRatio;
              const mouseY = (e.clientY - canvasRect.top) * heightRatio;

              const rectWidth = mouseX - zoomRect.tempX;
              const rectHeight = mouseY - zoomRect.tempY;

              // Update the dragged distance to ignore accidental clicks
              const draggedDist = Math.min(Math.abs(rectWidth) + Math.abs(rectHeight)); 

              // Only draw zoom rectangle if user moves the mouse more than the threshold to avoid registering accidental clicks
              if (draggedDist < Player.minDragDist) {
                // Reset zoom object properties before drawing a new rectangle
                zoomRect.shouldUpdate = false;
                return;
              }

              // Change the cursor to indicate that user can start drawing 
              mainCanvas.style.cursor = 'crosshair';
              
              // Stop drawing rectangle if the rectangle would be too large for the selected zoom scale
              const scaleFactor = Player.getZoomScale() / 100;
              const actualVideoWidth = this.el.videoWidth;
              const actualVideoHeight = this.el.videoHeight;

              // Do not alter the zoom canvas when mouse is still moving
              zoomRect.shouldHideEl = true;
                
              // Update the dragged distance
              Player.draggedDist = draggedDist;
                
              const ctx = zoomSelectCanvas.getContext('2d');
              if (!ctx) return;


              ctx.strokeStyle = Player.zoomDragColor;
              ctx.lineWidth = '5';

              // zoomRect.shouldUpdate = false;
              // Save the zoom rectangle dimensions
              if (Math.abs(rectWidth) * scaleFactor <= actualVideoWidth) {
                zoomRect.tempWidth = rectWidth;
                // zoomRect.shouldUpdate = true;

              } 
                
              if (Math.abs(rectHeight) * scaleFactor <= actualVideoHeight) {
                zoomRect.tempHeight = rectHeight;
                // zoomRect.shouldUpdate = true;
                
              }

              ctx.clearRect(0, 0, zoomSelectCanvas.width, zoomSelectCanvas.height);              
              ctx.strokeRect(zoomRect.tempX, zoomRect.tempY, zoomRect.tempWidth, zoomRect.tempHeight);
              
              zoomRect.shouldUpdate = true;
              zoomRect.shouldHideEl = false;

            });
            
            mainCanvas.addEventListener('mouseup', (e) => {

               // Change the cursor back to default (it was changed to 'crosshair' before to indicate that user can start drawing 
               mainCanvas.style.cursor = 'default';

              const mainPlayer = Player.getMainPlayer();
              if (mainPlayer) {
                const zoomRect = mainPlayer.zoomRect;
                if (zoomRect && zoomRect.shouldUpdate) {
                  Player.updateZoomedCanvas();
                  Player.drawZoomedVideoRegion();
                }

              }

              // Reset the mousedown flag
              Player.isMouseDown = false;

            });
            
            
          }

        }

          

          // // Enable labelling mode when pointer is over the canvas or control bar
          // const mainVideoDiv = document.getElementById('main-video-col');
          // if (mainVideoDiv) {
          //   mainVideoDiv.addEventListener('mouseenter', () => {
          //     Player.enableLabelling();
          //   });

          //   // Disable labelling mode when pointer is outside of the the canvas or control bar
          //   mainVideoDiv.addEventListener('mouseleave' , () => {
          //     Player.disableLabelling();
          //   });

          // }

        
        // Setup the maximum duration on the "jump to frame" input element
        const jumpToFrameInput = document.getElementById('jump-to-frame-input');
        if (jumpToFrameInput) {
          const frameCount = secondsToFrames(this.getDuration());
          // jumpToFrameInput.placeholder = `Frame number <${frameCount}`;
          jumpToFrameInput.placeholder = `Frame no.`;
          jumpToFrameInput.max = frameCount;
        }

        
        // Clear the ethogram
        this.clearEthogram();

        // Search for previously recorded ethogram file in user data directory
        const ethogramFilePath = await window.electronAPI.findEthogramFile(mainPlayerSrc);

        // If the ethogram file has been found
        if (ethogramFilePath) {

          // Read the ethogram file (get the array of observations each as a Map)
          const obsArr = await window.electronAPI.readEthogramFile(ethogramFilePath);

          // Check if the file can be read
          if (obsArr) {

            // Get the Ethogram instance for the main player
            const ethogram = this.getEthogram();
            if (ethogram) {

              // Create an Observation instance for each Map instance in the array
              // Add this Observation instance to the Ethogram
              // Add a new row for each Observation into the HTML table for Ethogram
              obsArr.forEach(obsMap => {
                const newObs = new Observation(obsMap);

                // Do NOT rewrite imported ethogram entries to ethogram file in user data directory
                const doNotOverwrite = true;
                ethogram.add(newObs, doNotOverwrite);
                
                const hideAlert = true;
                addEthogramRow(newObs, hideAlert);

              });

              // Get the number of observations in the Ethogram
              const obsCount = ethogram.size();

              if (obsCount > 0) {
                
                // Show success notification
                const recordText = obsCount === 1 ? `${obsCount} record` : `${obsCount} records`;
                showAlertToast(`${recordText} imported!`, 'success', 'Ethogram Imported');

                // Show last edit time
                Player.showLastEditForEthogram(ethogramFilePath);

              }

            }

          }


        }

        // Search for saved notes file
        const notesTextArea = document.getElementById('notes-text-area');
        if (notesTextArea) {
          
          // Clear the notes text area
          notesTextArea.value = '';
          
          // showAlertToast('Searching for notes...!', 'info');

          // Search for previously saved notes file in user data directory
          const notesFilePath = await window.electronAPI.findNotesFile(mainPlayerSrc);
          // Fill the notes text area if the notes file was found
          if (notesFilePath) {
            try {
              // Read the file content
              const notesContent = await window.electronAPI.readNotesFile(notesFilePath);
              if (notesContent) {
                // Fill the DOM element
                notesTextArea.value = notesContent;
    
                // Show success
                showAlertToast('Notes imported!', 'success');
    
                // Show last edit time
                Player.showLastEditForNotes(notesFilePath);
              }
            
            } catch (err) {
              console.log('Empty notes file');

            }

          } 

        }

        // Hide the loading indicator element
        this.hideSpinner()
      
      });
          
      // Update the progress bar when the current time of the main video is changed
      this.on('timeupdate', async () => {
        const controlBar = this.getControlBar();
        if (controlBar) controlBar.updateProgressBar();

        this.drawTrackingBoxes();

        // Update metadata
        const metadata = Player.getMetadata();
        if (metadata) metadata.updateTimestamp();

        // await Player.updateMetadata('lastViewedFrameNum', this.getCurrentFrame());
        // console.log(Player.metadata);

        
        // this.indicateAnnotatedBehaviors();
        // if (trackingBoxesInFrame) {
        //   Player.getMainPlayer().setTrackingBoxesInFrame(trackingBoxesInFrame);
        // }

      });

      // Buttons on the toast for editing labels
      const behaviorAddBtn = new Button('#label-save-btn');
      behaviorAddBtn.on('click', handleBehaviorRecordByClick)

      const trackingEditButton = new Button('#tracking-edit-btn');
      trackingEditButton.on('click', updateTracksFromToast)

      const nameEditButton = new Button('#name-edit-btn');
      nameEditButton.on('click', updateTracksFromNameDropdown);

      // Reset interval IDs
      Player.resetEthogramInterval();
      Player.resetNotesInterval();

    } else {
      Player.secondaryPlayers.push(this);
      
    }
    
    // Add this instance to the all instances list
    Player.allInstances.push(this);

    

    // // Save changes to config file
    // if (!doNotSaveToConfig) this.saveToConfig();

  }

  /**
   * Updates the metadata object. 
   * Optionally saves the updated metadata to a file in the user data directory.
   * @param {*} key Metadata key 
   * @param {*} value Metadata value
   * @param {Boolean | undefined} writeToFile Whether to save changes to file
   */
  // static async updateMetadata(key, value, writeToFile) {
  //   const metadata = Player.getMetadata();

  //   if (metadata && metadata.hasOwnProperty(key)) {
  //     metadata[key] = value;
  //     // console.log('Updated key, value', [key,value]);
  //     // console.log('Updated metadata:', Player.metadata);

  //   }

  //   const mainPlayer = Player.getMainPlayer();
  //   if (!mainPlayer) return;

  //   if (writeToFile) {
  //     const response = await window.electronAPI.saveMetadata(JSON.stringify(Player.getMetadata()), mainPlayer.getName());
  //     return response;

  //   }


  // }

  /**
   * Updates the metadata object. 
   * Optionally saves the updated metadata to a file in the user data directory.
   * @param {*} key Metadata key 
   * @param {*} value Metadata value
   * @param {Boolean | undefined} writeToFile Whether to save changes to file
   */
  // static async updateMetadata(key, value, writeToFile) {
  //   const mainPlayer = Player.getMainPlayer();
  //   if (!mainPlayer) return;

  //   const metadata = mainPlayer.getMetadata();
  //   if (!metadata) return;

  //   metadata.update(key, value);
  //   const metadata = Player.getMetadata();

  //   if (metadata && metadata.hasOwnProperty(key)) {
  //     metadata[key] = value;
  //     // console.log('Updated key, value', [key,value]);
  //     // console.log('Updated metadata:', Player.metadata);

  //   }

  //   const mainPlayer = Player.getMainPlayer();
  //   if (!mainPlayer) return;

  //   if (writeToFile) {
  //     const response = await window.electronAPI.saveMetadata(JSON.stringify(Player.getMetadata()), mainPlayer.getName());
  //     return response;

  //   }


  // }

  static resetZoomObject() {
    // Reset the zoom rect
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) return;

    const zoomRect = mainPlayer.zoomRect;
    if (!zoomRect) return;
  
    Object.keys(zoomRect).forEach(key => {
      zoomRect[key] = null;
    });

  }

  static getMetadata() {
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) return;
    return mainPlayer.metadata;
  }

  /**
   * Resets the interval Id for showing the last edit time for notes file.
   * Also hides the relevant HTML element
   */
  static resetNotesInterval() {
    if (Player.notesIntervalId) {
      clearInterval(Player.notesIntervalId);
      Player.notesIntervalId = null;
    }

    // Hide the HTML element
    const domEl = document.getElementById('notes-save-status-div');
    if (domEl) {
      domEl.classList.remove('visible');
      domEl.classList.add('invisible');
    }

  }

  /**
   * Resets the interval Id for showing the last edit time for ethogram file.
   * Also hides the relevant HTML element
   */
  static resetEthogramInterval() {
    if (Player.ethogramIntervalId) {
      clearInterval(Player.ethogramIntervalId);
      Player.ethogramIntervalId = null;
    }

    // Hide the HTML element
    const domEl = document.getElementById('ethogram-save-status-div');
    if (domEl) {
      domEl.classList.remove('visible');
      domEl.classList.add('invisible');
    }
  }

  /**
   * Show the last edit time for the ethogram file on an HTML element periodically
   * @param {import("original-fs").PathLike} filePath 
   * @param {Boolean | undefined} isFailed Optional flag to indicate success/failure of writing to file
   * @param {Number | undefined} updateFreq Optional update frequency in milliseconds
   */
  static showLastEditForEthogram(filePath, isFailed, updateFreq) {

    // DOM element ID for showing the last edit time
    const domElId = 'ethogram-save-status-div';

    // Get the status update frequency for last modified time
    const statusUpdateFreq = updateFreq ? updateFreq : Player.getStatusUpdateFreq();

    // Show the changes immediately
    showLastEditForFile(filePath, domElId, isFailed);

    // Show last modified time periodically if the relevant function has not been called already
    if (!Player.ethogramIntervalId) {
      Player.ethogramIntervalId = setInterval(showLastEditForFile, statusUpdateFreq, filePath, domElId, isFailed);
    
    }

    if (isFailed) {
      showAlertToast('Unable to save changes! Please try again.', 'error', 'Save Failure');
    }

  }


  /**
   * Show the last edit time for the notes file on an HTML element periodically
   * @param {import("original-fs").PathLike} filePath 
   * @param {Boolean | undefined} isFailed Optional flag to indicate success/failure of writing to file
   * @param {Number | undefined} updateFreq Optional update frequency in milliseconds
   */
  static showLastEditForNotes(filePath, isFailed, updateFreq) {

    // DOM element ID for showing the last edit time
    const domElId = 'notes-save-status-div';

    // Get the status update frequency for last modified time
    const statusUpdateFreq = updateFreq ? updateFreq : Player.getStatusUpdateFreq();

    // Show the changes immediately
    showLastEditForFile(filePath, domElId, isFailed);

    // Show last modified time periodically if the relevant function has not been called already
    if (!Player.notesIntervalId) {
      Player.notesIntervalId = setInterval(showLastEditForFile, statusUpdateFreq, filePath, domElId, isFailed);
    
    }

    if (isFailed) {
      showAlertToast('Unable to save changes! Please try again.', 'error', 'Save Failure');
    }


  }


  /**
   * Loads the main player 
   * @param {import("node:original-fs").PathLike} mainVideoPath 
   * @returns 
   */
  static async loadMainPlayer(mainVideoPath) {

    // Check if a video path is given
    if (!mainVideoPath) return;

    // Save main video path to config file
    const configResponse = await window.electronAPI.saveToConfig({mainVideoPath: mainVideoPath});
    if (!configResponse) return;

    // Create or get the main player
    const mainPlayer = Player.getMainPlayer() ? Player.getMainPlayer() : new Player('main-video');

    // Load the video source
    await mainPlayer.setSource(mainVideoPath);
    mainPlayer.load();

    const mainVideoName = await getFileNameWithoutExtension(mainVideoPath); 
    if (mainVideoName) {
      const mainVideoTitleEl = document.getElementById('main-video-title');
      if (mainVideoTitleEl) {
        mainVideoTitleEl.textContent = mainVideoName;
      }         
      
    }

    
    // Move the buttons to the main video bar
    const mainVideoBar = document.getElementById('main-video-bar');
    const openMainVideoBtn = document.getElementById('open-main-video-btn'); 
    const openExperimentFolderBtn = document.getElementById('open-experiment-folder-btn');
    const initialPanelDiv = document.getElementById('initial-panel-div');
    if (mainVideoBar){
      const videoBarBtnDiv = mainVideoBar.querySelector('.button-div');
      if (videoBarBtnDiv) {
        if (openMainVideoBtn) {
          videoBarBtnDiv.appendChild(openMainVideoBtn);
          openMainVideoBtn.classList.remove('btn-icon-large');
          openMainVideoBtn.classList.add('btn-icon-small');
        } 

        if (openExperimentFolderBtn) {
          videoBarBtnDiv.appendChild(openExperimentFolderBtn);
          openExperimentFolderBtn.classList.remove('btn-icon-large');
          openExperimentFolderBtn.classList.add('btn-icon-small');
        }

      }

    }

    // Remove the initial panel
    if (initialPanelDiv) {
      initialPanelDiv.remove();
    }

  }

  static getHotkeys() {
    return Player.hotkeys;
  }

  /**
   * Gets the frequency in milliseconds for updating last modified time for files on their corresponding HTML elements
   * @returns {Number}
   */
  static getStatusUpdateFreq() {
    if (!Player.statusUpdateFreq) {
      Player.statusUpdateFreq = 60000; // 1 minute of frequency
    }
    return Player.statusUpdateFreq;
  }

  static async updateShortcuts(category, name, hotkey, description) {
    const shortcuts = Player.getHotkeys();
    if (shortcuts) {
      if (category && name && hotkey) {
        
        // Check if input category is defined within the shortcuts object
        if (category in shortcuts) {

          // If no description is given as input
          if (!description) {

            // Do not change the existing description if it exists
            if ('description' in shortcuts[category][name]) {

              // Get the current description
              const currentDesc = shortcuts[category][name]['description'];

              // If current description is null, assign name to the description
              const newDesc = currentDesc ? currentDesc : name;

              // Update the shortcuts
              shortcuts[category][name] = {
                hotkey: hotkey,
                description: newDesc
              }

            // If no description field is found
            } else {

              // Assign the name to the description
              shortcuts[category][name] = {
                hotkey: hotkey,
                description: name
              }


            }
          
          // If description is given as input, add it to the shortcuts  
          } else {
            shortcuts[category][name] = {
              hotkey: hotkey,
              description: description
            }

          }

          // Save changes to config file
          const response = await window.electronAPI.saveToConfig({shortcuts: shortcuts});
          if(!response) {
            console.log('Shortcuts could not be saved to the config file!');
          }

        }
      }
    }
  }

  static setAppVersion(versionNo) {
    if (versionNo) {
      Player.appVersion = versionNo;
    } else {
      const versionNo = window.electronAPI.getVersion();
      if (versionNo) {
        Player.appVersion = versionNo;
      }
    }

  }


  /**
   * Gets the current app version
   * @returns {String} App version in string format
   */
  static getAppVersion() {
    if (Player.appVersion) {
      return Player.appVersion;
    } 
    
    window.electronAPI.getVersion().then(versionNo => {
      if (versionNo) {
        return versionNo;
      }
    });

  }

  // Use for loading all shortcuts from config file
  static setHotkeys(hotkeysObj) {
    if (hotkeysObj) {
      Player.hotkeys = hotkeysObj;
    }

  }

  /**
   * Get the action names
   * @returns {String[]}
   */
  static getActionNames() {
    return Player.actionNameArr;
  }

  /**
   * Gets the zoom scale in percentage
   * @returns {Number}
   */
  static getZoomScale() {
    return Player.zoomScale;
  }

  /**
   * Sets the zoom scale percentage for the main video to the input value
   * @param {Number | String} zoomScale Number for zoom scale from 100 to 300
   * @returns 
   */
  static async setZoomScale(zoomScale) {
    if (zoomScale) {
      const parsedNum = parseFloat(zoomScale);

      if (!Number.isSafeInteger(parsedNum)) {
        console.log('Input for the zoom scale is not a valid number!');
        return;
      }
      
      // Check if it is between 100% and 500%
      if (parsedNum < 100 || parsedNum > 300) return;

      // Change the zoom scale
      Player.zoomScale = parsedNum;

      // Update the config file
      const response = await window.electronAPI.saveToConfig({ zoomScale: Player.zoomScale });
      if (!response) {
        console.log('Zoom scale could not be saved to config file!');
        return;
      }

      updateZoomScaleDomEls();

      Player.updateZoomedCanvas();

      return response;

    }
    
  }

  /**
   * Gets the username
   * @returns {String}
   */
  static getUsername() {
    return Player.username;
  }

  /**
   * Sets the username
   * @param {String} username 
   */
  static async setUsername(username){
    if (username) {
      Player.username = username;
      
      // Update metadata
      const metadata = Player.getMetadata();
      if (metadata) metadata.updateUsername();

    }

  }

  static async setActionNames(actionNameArr) {
    Player.actionNameArr = actionNameArr;
    const actionNames = Player.getActionNames();

    // Update the metadata
    const metadata = Player.getMetadata();
    if (metadata) metadata.updateActions();
    
    const hotkeyCategory = 'actions';
    
    // Remove all existing Hotkey instances for this category before creating new ones
    const hotkeysInCategory = Hotkey.findAll({category: hotkeyCategory});
    hotkeysInCategory.forEach(hotkey => hotkey.delete());

    // Create shortcuts for actions automatically
    let keyArr = Hotkey.getAllKeysWithoutModifiers();
    if (Array.isArray(actionNames) && actionNames.length > 0) {
      actionNames.forEach(name => {
        let breakExecuted = false;
        let newKey;
        
        // Iterate over the letters of a name
        for (let i = 0; i < name.length; i++) {
          
          // Check whether this was already in the shortcut list and ensure it is not a space character
          const lowerChar = name[i].toLowerCase();
          const conflictingHotkeys = Hotkey.findConflicts(lowerChar, []);
          if (!keyArr.includes(lowerChar) && lowerChar !== ' ') {
            // If not, add it as a new shortcuts
            newKey = lowerChar;
            keyArr.push(newKey);
            breakExecuted = true;
            break;

          }

        }

        // If no suitable letter has been found with traversing the entire string
        if (!breakExecuted) {
          // Assign a random letter
          newKey = getRandomLetterNotIn(keyArr);
          keyArr.push(newKey);

        }

        // Update the hotkeys 
        const newHotkey = new Hotkey(hotkeyCategory, name, name, newKey);


      });

      // Update list in HTML
      updateHotkeyDomElement(hotkeyCategory);

      // Update the datalist element for autocompletion in Ethogram table
      const actionNameArr = Player.getActionNames();
      if (actionNameArr) {
        
        // Update the action datalist element
        const actionDatalistEl = document.getElementById('action-datalist');
        if (actionDatalistEl) {  
        
          // Reset the action datalist before updating it
          while (actionDatalistEl.firstChild) {
            actionDatalistEl.removeChild(actionDatalistEl.firstChild);
          }
        
          // Add up-to-date action names to this datalist element
          if (actionNameArr.length > 0) {
            actionNameArr.forEach(actionName => {
              const optionEl = document.createElement('option');
              optionEl.value = actionName;
              actionDatalistEl.append(optionEl);
            });

          }

        }
      }



    }
  }

  static async setSkipSeconds(seconds) {
    
    // Check if an input is provided
    if (!seconds) {
      console.log('No value for skip seconds was provided!');
      return;
    }

    // Convert the input to float (for seconds smaller than 1)
    const skipSeconds = parseFloat(seconds);

    // Check if the provided number is valid
    if (Number.isNaN(skipSeconds) || !Number.isFinite(skipSeconds)) {
      console.log('Skip seconds input is invalid: either NaN or not finite!');
      return;
      
    }
    
    // Save the skip seconds
    Player.skipSeconds = skipSeconds;

    // Update the text inside rate button
    const skipSecondsBtn = document.getElementById('skip-seconds-btn');
    skipSecondsBtn.textContent = `Skip: ${skipSeconds} sec`
  
    // Update active states of the selection
    const skipSecondsList = document.getElementById('skip-seconds-list');
    // console.log(skipSecondsList)
    if (skipSecondsList) {
      const listItems = skipSecondsList.querySelectorAll('.dropdown-item');
      if (listItems) {
        listItems.forEach(item => {
          // Make the item with the same speed as the main player active
          if (parseFloat(item.dataset.skipSeconds) === skipSeconds) {
            item.classList.add('active');
          } else {
            // Remove active states from all items
            item.classList.remove('active');
          }
          
        });

      }

    }
  
    // // Change selected value on the select element
    // const skipSecondsSelectEl = document.getElementById('skip-seconds-select');
    // if (skipSecondsSelectEl) {
    //   for (let i=0; i < skipSecondsSelectEl.options.length; i++) {
    //     if (skipSecondsSelectEl.options[i].value == Player.getSkipSeconds() ) {
    //       skipSecondsSelectEl.options[i].selected = true;
    //       break;
    //     }
    //   } 
    // }

    // Save to config
    const response = await window.electronAPI.saveToConfig({skipSeconds: this.skipSeconds})
    if (!response) {
      console.log("Couldn't save the skip seconds to config file!");
    }

  }

  static setMaxPlaybackRate(rate) {
    Player.maxPlaybackRate = rate;
  }
  
  static async setIndividualNames(nameArr) {
    Player.individualNames = nameArr;
    const individualNames = Player.getIndividualNames();

    // Update metadata
    const metadata = Player.getMetadata();
    if (metadata) metadata.updateIndividuals();
    
    const hotkeyCategory = 'individuals';

    // Remove all existing Hotkey instances for this category before creating new ones
    const hotkeysInCategory = Hotkey.findAll({category: hotkeyCategory});
    hotkeysInCategory.forEach(hotkey => hotkey.delete());

    // Create shortcuts for actions automatically
    let keyArr = Hotkey.getAllKeysWithoutModifiers();
    if (Array.isArray(individualNames) && individualNames.length > 0) {
      individualNames.forEach(name => {
        let breakExecuted = false;
        let newKey;
        
        // Iterate over the letters of a name
        for (let i = 0; i < name.length; i++) {
          // Check whether this was already in the shortcut list
          const lowerChar = name[i].toLowerCase();
          if (!keyArr.includes(lowerChar) && lowerChar !== ' ') {
            // If not, add it as a new shortcuts
            newKey = lowerChar;
            keyArr.push(newKey);
            breakExecuted = true;
            break;

          }

        }
  
        // If no suitable letter has been found with traversing the entire string
        if (!breakExecuted) {
          // Assign a random letter
          newKey = getRandomLetterNotIn(keyArr);
          keyArr.push(newKey);  

        }

        // Update the hotkeys 
        const newHotkey = new Hotkey(hotkeyCategory, name, name, newKey);

      });
  
  
      // Update the DOM elements
      updateHotkeyDomElement(hotkeyCategory);

      // Update the subject and target datalist elements for autocompletion in ethogram table
      const subjectNameArr = Player.getIndividualNames();
      if (subjectNameArr) {
        
        // Update the subject datalist element
        const subjectDatalistEl = document.getElementById('subject-datalist');
        if (subjectDatalistEl) {
          
          // Reset the subject datalist before updating it
          while (subjectDatalistEl.firstChild) {
            subjectDatalistEl.removeChild(subjectDatalistEl.firstChild);
          }

          // Add up-to-date subject names to this datalist element
          if (subjectNameArr.length > 0) {
            subjectNameArr.forEach(name => {
              const optionEl = document.createElement('option');
              optionEl.value = name;
              subjectDatalistEl.append(optionEl);
              
            });

          }

        }
        

        // Update the target datalist element
        const targetDatalistEl = document.getElementById('target-datalist');
        if (targetDatalistEl) {
          // Add "Box" to the target list
          const targetNameArr = subjectNameArr.concat('Box');
          
          // Reset the target datalist before updating it
          while (targetDatalistEl.firstChild) {
            targetDatalistEl.removeChild(targetDatalistEl.firstChild);

          }
          
          // Add up-to-date target names to this datalist element
          if (targetNameArr.length > 0) {
            targetNameArr.forEach(name => {
              const optionEl = document.createElement('option');
              optionEl.value = name;
              targetDatalistEl.append(optionEl);

            });
        
          }

        }

      }

    }
    
  }

  /**
   * Sets the typing status of the Player. 
   * This should be set to true when one of the input elements has focus.
   * When none of the input elements has focus, it should be set to false.
   * @param {Boolean} isTyping True if user is typing, False otherwise
   */
  static setTypingStatus(isTyping) {
    Player.isTyping = isTyping;

    // Disable labelling immediately if user starts typing in an input element
    if (isTyping) Player.disableLabelling();
    
  }



  /**
   * Enables labelling mode
   * @returns 
   */
  static enableLabelling() {

    // Only enable labelling if user is not typing
    if (Player.isTyping) return;

    // Check if action types and individual names exists
    if (!Player.anyIndividuals() || !Player.anyActions()) {
      showAlertToast(
        'Please upload files for action types and individuals!', 
        'warning', 
        'Labelling Disabled' 
      );

      Player.disableLabelling();
      return;

    } else if (!Player.getMainPlayer()) {
      showAlertToast(
        'Please open a video as the main view!', 
        'warning', 
        'Labelling Disabled' 
      )
      Player.disableLabelling();
      return;
    }
    
    // Only enable labelling if it has not been already activated
    if (!Player.isInLabellingMode()) {
      Player.labellingMode = true;
      const iconText = 'label';

      // Change the labelling button status
      const labellingModeBtn = document.getElementById('toggle-labelling-mode-btn');
      if (labellingModeBtn) {
        labellingModeBtn.querySelector('span').textContent = iconText;
      }

      showAlertToast('Labelling mode ON!', 'success');

    }

  }

  static disableLabelling() {

    // Only disable labelling if it has not been already deactivated
    if (Player.isInLabellingMode()) {
      Player.labellingMode = false;
      const iconText = 'label_off';

      // Change the labelling button status
      const labellingModeBtn = document.getElementById('toggle-labelling-mode-btn');
      if (labellingModeBtn) {
        labellingModeBtn.querySelector('span').textContent = iconText;
      }

      showAlertToast('Labelling mode OFF!', 'success');

    }

  }

  static toggleLabellingMode() {
    const labellingModeBtn = document.getElementById('toggle-labelling-mode-btn');
    if (labellingModeBtn) {
      
      // Check if action types and invidual names exists
      if (!Player.anyIndividuals() || !Player.anyActions()) {
        
        showAlertToast(
          'Please upload files for action types and individuals!', 
          'warning', 
          'Labelling Disabled' 
        );

        Player.disableLabelling();
        return;

      } else if (!Player.getMainPlayer()) {
        showAlertToast(
          'Please open a video as the main view!', 
          'warning', 
          'Labelling Disabled' 
        )
        Player.disableLabelling();
        return;
      }

      // labellingModeBtn.classList.toggle('off');
      // Player.setLabellingMode(!labellingModeBtn.classList.contains('off'));

      if (Player.isInLabellingMode()) {
        Player.disableLabelling();
      } else {
        Player.enableLabelling();
      }

    }

  }

  static setKeyState(pressedKey, state) {
    Player.keyState[pressedKey] = state;
  }

  static getCurrentObservation() {
    return Player.currentObservation;
  }

  static setCurrentObservation(currentObs) {
    // Expects a Map for current observation
    Player.currentObservation = currentObs;

  }

  static resetCurrentObservation() {
    Player.currentObservation = new Observation();
    Player.currentObservation.show();
  }

  /**
   * Checks if any action name was imported
   * @returns {Boolean}
   */
  static anyActions() {
    const anyActions = Player.getActionNames() ? Player.getActionNames().length > 0 : false;
    return anyActions;
  }

  /**
   * Checks if any individual name was imported
   * @returns 
   */
  static anyIndividuals() {
    const anyIndividuals = Player.getIndividualNames() ? Player.getIndividualNames().length > 0 : false;
    return anyIndividuals;
  }

  static savePressedKey(key) {
    Player.pressedKeys.push(key);
  }

  static clearPressedKeys() {
    Player.pressedKeys = [];
  }

  static getPressedKeys() {
    return Player.pressedKeys;
  }

  static getTimeoutKeyPress() {
    return Player.timeoutKeyPress;
  }

  static setTimeoutKeyPress(timeout) {
    Player.timeoutKeyPress = timeout;
  }

  static isInLabellingMode() {
    return Player.labellingMode;
  }

  static getAllInstances() {
    return Player.allInstances;
  }

  static getSecondaryPlayers() {
    return Player.secondaryPlayers;
  }

  static getMainPlayer() {
    return Player.mainPlayer;
  }

  static getSkipSeconds() {
    return Player.skipSeconds;
  }

  static getMaxPlaybackRate() {
    return Player.maxPlaybackRate;
  }

  static getMinPlaybackRate() {
    return Player.minPlaybackRate;
  }

  static getIndividualNames() {
    return Player.individualNames;
  }

  static getKeyState() {
    return Player.keyState;
  }

  static getMainCanvas() {
    return Player.getMainPlayer() ? Player.getMainPlayer().getCanvas() : null;
  }

  static updateZoomedCanvas() {
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) return;
    
    // Adjust dimensions of DOM elements for zoomed region before drawing frames into canvas
    const zoomRect = mainPlayer.zoomRect;
    if (!zoomRect) return;
    
    if (zoomRect.shouldHideEl) return;

    const zoomVideoDiv = document.getElementById('zoom-video-div');
    if (!zoomVideoDiv) return;

    const zoomVideoCanvas = zoomVideoDiv.querySelector('#zoom-video-canvas');
    if (!zoomVideoCanvas) return;


    const mainVideoDiv = document.getElementById('main-video-div');
    if (!mainVideoDiv) return;

    // Get the zoom scale in percentages and convert into decimal
    const zoomScale = Player.getZoomScale();
    if (!zoomScale) return;

    const zoomSelectCanvas = document.getElementById('main-zoom-select-canvas');
    if (!zoomSelectCanvas) return;

    const ctx = zoomSelectCanvas.getContext('2d');
    if (!ctx) return;

    ctx.strokeStyle = Player.zoomDragColor;
    ctx.lineWidth = 5;
    ctx.clearRect(0, 0, zoomSelectCanvas.width, zoomSelectCanvas.height);
      
    // Adjust the aspect ratio for the final rectangle for selection
    const scaledWidth = Math.sign(zoomRect.tempWidth) * Math.round(zoomRect.aspectRatio*Math.abs(zoomRect.tempHeight));

    // Update the rectangle dimensions
    zoomRect.width = scaledWidth;
    zoomRect.height = zoomRect.tempHeight;
    zoomRect.startX = zoomRect.tempX;
    zoomRect.startY = zoomRect.tempY;

    ctx.strokeRect(zoomRect.startX, zoomRect.startY, zoomRect.width, zoomRect.height);

    // Put a descriptive title
    ctx.beginPath();
    ctx.font = '20px tahoma';
    const canvasText = 'Zoom'
    const labelWidth = ctx.measureText(canvasText).width + 15;
    const labelHeight = 30;
    const padding = 5;
    
    let labelX = zoomRect.startX;
    let labelY = zoomRect.startY - labelHeight;

    if (zoomRect.tempWidth < 0) {
      labelX = labelX - labelWidth;
    }

    // TODO: Handle edge cases (when tracking boxes are on the edges of the canvas)

    ctx.rect(labelX, labelY, labelWidth, labelHeight);
    ctx.fillStyle = 'white'
    ctx.fill();
    ctx.fillStyle = 'black';
    // ctx.textAlign = 'left';
    // ctx.textBaseline = 'middle';
    ctx.fillText(canvasText, labelX+padding, labelY+labelHeight-padding)
    ctx.stroke();

    // Calculate visible and actual video dimensions
    const visibleVideoHeight = mainVideoDiv.getBoundingClientRect().height;
    const actualVideoHeight = mainPlayer.el.videoHeight;

    // Get the zoom scale factor
    const scaleFactor = zoomScale / 100;

    // Calculate the visible dimensions of the zoomed region
    // These dimensions are relative to the viewport depending on the user device. They are not to the actual video dimensions
    const visibleZoomedHeight = (zoomRect.height / actualVideoHeight) * visibleVideoHeight * scaleFactor;
    const visibleZoomedWidth = visibleZoomedHeight * zoomRect.aspectRatio;

    // Adjust the canvas dimensions in the new window to display the zoomed region
    zoomRect.canvasWidth = zoomRect.width * scaleFactor;
    zoomRect.canvasHeight = zoomRect.height * scaleFactor;
    zoomVideoCanvas.width = zoomRect.canvasWidth;
    zoomVideoCanvas.height = zoomRect.canvasHeight;

    // Draw video frames 
    const zoomVideoCtx = zoomVideoCanvas.getContext('2d');
    zoomVideoCtx.drawImage(mainPlayer.el, zoomRect.startX, zoomRect.startY, zoomRect.width, zoomRect.height, 0, 0, zoomVideoCanvas.width, zoomVideoCanvas.height);

    // Draw tracking boxes
    const zoomTrackingCanvas = zoomVideoDiv.querySelector('#zoom-tracking-canvas');
    if (zoomTrackingCanvas) {
      const zoomTrackingCtx = zoomTrackingCanvas.getContext('2d');
      const mainTrackingCanvas = mainPlayer.canvas;
      zoomTrackingCtx.drawImage(mainTrackingCanvas, zoomRect.startX, zoomRect.startY, zoomRect.width, zoomRect.height, 0, 0, zoomVideoCanvas.width, zoomVideoCanvas.height);

    }

    // Adjust the padding
    const paddingWidth = 5;
    zoomVideoDiv.style.paddingLeft = paddingWidth + 'px';
    zoomVideoDiv.style.paddingRight = paddingWidth + 'px';
    
    // Adjust the outer div dimension for zoom
    zoomVideoDiv.style.width =  visibleZoomedWidth + 2 * paddingWidth + 'px';

    const zoomTitleDiv = zoomVideoDiv.querySelector('.zoom-title');
    if (zoomTitleDiv) {
      // Assign decimal points for displaying coordinates
      const decimals = 2;
      
      // Create info text for coordinates of the selected zoom region
      const titleArr = [
        `x:${zoomRect.startX.toFixed(decimals)}`, 
        `y:${zoomRect.startY.toFixed(decimals)}`, 
        `w:${zoomRect.width.toFixed(decimals)}`, 
        `h:${zoomRect.height.toFixed(decimals)}`
      ];

      const titleHtmlArr = [];
      
      // Create badges for each info text
      titleArr.forEach(text => {
        titleHtmlArr.push(`<span class="badge text-bg-dark fw-light x-small me-1">${text}</span>`);
      });
      zoomTitleDiv.innerHTML = titleHtmlArr.join('');

      // Show the zoom scale info
      const zoomScaleDropdown = zoomVideoDiv.querySelector('#zoom-scale-dropdown');
      if (zoomScaleDropdown) {
        const btnEl = zoomScaleDropdown.querySelector('.dropdown-toggle');
        if (btnEl) btnEl.textContent = zoomScale + '%';
      }

    }

    // Check the zoom title div width
    const zoomBtnPanelEl = zoomVideoDiv.querySelector('#zoom-btn-panel');
    if (zoomBtnPanelEl) {
      if (visibleZoomedWidth < 500) {
        zoomTitleDiv.classList.add('d-none');
      } else {
        zoomTitleDiv.classList.remove('d-none');
      }
      zoomBtnPanelEl.style.marginTop = paddingWidth + 'px';

    } 

    zoomVideoCanvas.style.paddingBottom = paddingWidth + 'px';

    // Zoom into selected region
    zoomRect.shouldHideEl = false;
    zoomVideoDiv.classList.remove('d-none');

  }


  static drawZoomedVideoRegion() {
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) return;

    const zoomRect = mainPlayer.zoomRect;
    if (!zoomRect) return;
    
    const zoomVideoDiv = document.getElementById('zoom-video-div');
    if (zoomVideoDiv) {
      // Hide the div element if the zoom is disabled
      if (zoomRect.shouldHideEl) {
        zoomVideoDiv.classList.add('d-none');
        return;
      }
      
      const zoomVideoCanvas = zoomVideoDiv.querySelector('#zoom-video-canvas');
      if (!zoomVideoCanvas) return;

      const zoomVideoCtx = zoomVideoCanvas.getContext('2d');

      // Update the request ID
      Player.zoomRequestId = window.requestAnimationFrame(Player.drawZoomedVideoRegion);

      // Draw video frames 
      zoomVideoCtx.drawImage(mainPlayer.el, zoomRect.startX, zoomRect.startY, zoomRect.width, zoomRect.height, 0, 0, zoomVideoCanvas.width, zoomVideoCanvas.height);

      // Display the div element
      zoomVideoDiv.classList.remove('d-none');
    
    }


  }

  /**
   * Saves secondary video file paths to the config file
   */
  static async saveSecondaryVideosToConfig() {
    // Update secondary video file paths in the config file
    const confirmedSrcArr = Player.getSecondaryPlayers().map(player => player.getSource());
    const response = await window.electronAPI.saveToConfig({secondaryVideoPaths: confirmedSrcArr});
    return response;

  }

  /**
   * Sets the canvas attributes to show video frames while hovering over the control bar
   * @returns 
   */
  setCanvasOnHover() {
    
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) return;

    // Get the video element for the hidden downsized copy of the main video
    const hoverVideoEl = document.querySelector('video#hover-video');
    if (!hoverVideoEl) return;

    // Calculate the aspect ratio of the video
    const aspectRatio = mainPlayer.el.videoWidth / mainPlayer.el.videoHeight;
     
    // Calculate the downsized video dimensions
    const scaledHeight = 200;
    const scaledWidth = Math.round(aspectRatio * scaledHeight);

    // Set the hover video DOM element attributes
    hoverVideoEl.width = scaledWidth;
    hoverVideoEl.height = scaledHeight;
    
    // Set the hover canvas DOM element attributes
    const hoverFrameCanvas = document.querySelector('#hover-frame-div canvas');
    if (hoverFrameCanvas) {
      hoverFrameCanvas.height = scaledHeight; 
      hoverFrameCanvas.width = scaledWidth;

    }

    // Set the source
    hoverVideoEl.src = mainPlayer.getSource();

    // Load the video 
    hoverVideoEl.load();

  }

  // Setters
  async setSource(source) {
    this.el.src = source;
    await this.setName();

  }

  async setName(name) {
    // Set the player name
    if (name) {
      this.name = name;
    } else {
      // Extract name of the source video without extension
      const videoSrc = this.getSource(); 
      if (videoSrc) {
        const fileName = await getFileNameWithoutExtension(videoSrc);
        if (fileName) {
          this.name = fileName;
        }
      }
    }

    // Update metadata
    const metadata = Player.getMetadata();
    if (metadata) metadata.updateVideoName();

  }
  
  setCurrentTime(seconds) {
    if (Number.isFinite(seconds)) {
      this.el.currentTime = seconds;
    }
  }
  
  setPlaybackRate(rate) {
    if (rate <= Player.getMaxPlaybackRate() && rate > 0) {
      this.el.playbackRate = rate;
    }
  }

  increaseSpeed(step) {
    const increment = step ? step : 0.25;
    this.setPlaybackRate(this.getPlaybackRate() + increment);
  }

  decreaseSpeed(step) {
    const decrement = step ? step : 0.25;
    this.setPlaybackRate(this.getPlaybackRate() - decrement);
  }


  setMainPlayer() {
    this.mainPlayer = true;
  }

  setTrackingMap(tracks, firstAvailTrackIds, trackEditHistory) {
    this.trackingMap = new TrackingMap(tracks, firstAvailTrackIds, trackEditHistory);

    // Save to config
    this.getTrackingMap().saveToConfig().then(response => {
      if (!response) {
        console.log('Could not save TrackingMap to config file!')
      }
      // Refresh canvas
      this.drawTrackingBoxes();

    })

  }

  setTrackingBoxesInFrame(boxArr) {
    this.trackingBoxesInFrame = boxArr;
  }

  resetTrackingMap() {
    if (this.trackingMap) {
      this.trackingMap.clear();

      // Refresh canvas
      const trackingBoxesInFrame = this.drawTrackingBoxes();
      
    }
  }

  // Getters
  getName() {
    return this.name;
  }

  /**
   * Gets the frame rate of the player instance.
   * @returns {Number} Frames per second
   */
  getFrameRate() {
    return this.frameRate;

  }

  async setFrameRate(frameRate) {
    if (frameRate) {
      this.frameRate = frameRate;
    } else {
      const frameRate = this.calculateFrameRate();
      if (frameRate > 0) this.frameRate = frameRate;
    }
    // Update metadata
    const metadata = Player.getMetadata();
    if (metadata) metadata.updateVideoFPS();

  }
  
  getControlBar() {
    return this.controlBar;
  }

  getTrackingMap() {
    return this.trackingMap;
  }

  /**
   * Gets the Ethogram instance associated with the main player
   * @returns {Ethogram}
   */
  getEthogram() {
    return this.ethogram;
  }

  setEthogram(obj) {
    if (obj !== null && typeof obj === 'object') {
      this.ethogram = new Ethogram(obj);
      return this.ethogram;
    }
  }

  /**
   * Creates a new Ethogram instance and clears the ethogram HTML table
   */
  clearEthogram() {
    this.ethogram = new Ethogram();
    clearEthogramTable();
  }

  getDuration() {
    return this.el.duration;
  }
  
  getCurrentTime() {
    return this.el.currentTime;
  }

  getCurrentFrame() {
    return secondsToFrames(this.getCurrentTime(), this.getFrameRate())
  }
  
  getPlaybackRate() {
    return this.el.playbackRate;
  }

  /**
   * Gets the video source 
   * @returns {String} | Video file path without the file:// protocol
   */
  getSource() {
    return this.el.src.replace(/^file:\/\//, '');
    // videoElement.currentSrc.
  }

  setFirstAvailTrackIds(trackIds) {
    // Set the first the available track IDs for each species
    // Expects a Map {species: firstAvailTrackId}
    this.trackingMap.firstAvailTrackIds = trackIds;
  }

  getFirstAvailTrackIds() {
    // Get the first the available track ID
    return this.trackingMap.firstAvailTrackIds;
  }

  getTrackingBoxesInFrame() {
    return this.trackingBoxesInFrame;
  }

  getCanvas() {
    return this.canvas;
  }

  getEvents() {
    return this.events;
  }
  
  /**
   * Adds an event listener and its callback function to a Player instance if the event has not already been added.
   * Saves the event to the instance to prevent duplicate event listeners.
   * @param {Event} event 
   * @param {Function} callBackFunction 
   */
  on(event, callBackFunction) {

    // Check if this event was already added to prevent duplicates
    if (!this.events.includes(event)) {
      
      // Add the event and callback function
      this.el.addEventListener(event, callBackFunction);
      
      // Add the event to this instance
      this.events.push(event);
      
    }

  }

  /**
   * Checks if this is the main player
   * @returns {Boolean} True if this is the main player, False otherwise
   */
  isMainPlayer() {
    return this.mainPlayer;
  }

  isPaused() {
    return this.el.paused;
  }

  isMuted() {
    return this.el.muted;
  }

  isEnded() {
    return this.el.ended;
  }

  calculateFrameRate() {
    const stream = this.el.captureStream();
    const videoTrack = stream.getVideoTracks()[0];
    const settings = videoTrack.getSettings();
    return settings.frameRate;

    // Wait until the video is loaded


  }

  /**
   * Disposes the player and remove video element from DOM
   */
  async dispose() {

    // Only dispose the secondary players
    if (this.isMainPlayer()) return;

    this.pause();

    // Find the item for this player in the secondary player array and remove it
    Player.secondaryPlayers = Player.secondaryPlayers.filter(player => player.domId !== this.domId);

    // Remove the item from all instances array
    Player.allInstances = Player.allInstances.filter(player => player.domId !== this.domId);

    // Remove the source 
    this.el.removeAttribute('src');

    // Remove the column div for the player
    this.el.parentNode.parentNode.remove(); 
    
    // Save changes to config
    const response = await Player.saveSecondaryVideosToConfig();
    if (response) {
      showAlertToast('Player removed!', 'success');
    } else {
      showAlertToast('Error saving changes!', 'error');
    }

    // If no secondary player is left, move the the button back to the main panel from the title panel
    if (Player.getSecondaryPlayers().length < 1) {
      const openSecondaryVideosBtn = document.getElementById('open-secondary-videos-btn');

      if (openSecondaryVideosBtn) {
        const infoTextEl = document.getElementById('secondary-video-info-text');
        
        if (infoTextEl) {
          infoTextEl.parentNode.prepend(openSecondaryVideosBtn);
          openSecondaryVideosBtn.classList.add('mt-3');
          openSecondaryVideosBtn.classList.replace('btn-icon-small', 'btn-icon-large');
          
          // Show the information text
          infoTextEl.classList.remove('d-none');
        
        }
        
      }

      const openBtnDiv = document.querySelector('#secondary-videos-control-div .button-div')
      if (openBtnDiv) {

      }
    }

    // Delete the instance
    // delete this;

    return response;


  }

  


  // Pause the video
  pause() {
    if (!this.isPaused() && !this.isEnded()) {
      this.el.pause();
      // this.updateButtonIcons();
    }
  }

  // Play the video
  play() {
    if (this.isPaused()) {
      this.el.play();
      // this.updateButtonIcons();
    }
  }

  playPause() {
    if (!this.isPaused() && !this.isEnded()) {
      this.pause();
    } else {
      this.play();
    }
    // this.updateButtonIcons();
  }

  stop() {
    this.pause();
    this.setCurrentTime(0);
  }

  forward(seconds) {
    // If no input, get the default skip seconds
    const skipSeconds = seconds ? seconds : Player.getSkipSeconds();

    if (this.getCurrentTime() < this.getDuration() - skipSeconds) {
      this.setCurrentTime(this.getCurrentTime() + skipSeconds);
    } else {
      this.setCurrentTime(this.getDuration());
    }

  }

  replay(seconds) {
    let skipSeconds;
    if (!seconds) {
      // If no input, get the default skip seconds
      skipSeconds = Player.getSkipSeconds();
    } else {
      skipSeconds = seconds;
    }
    
    if (this.getCurrentTime() > skipSeconds) {
      this.setCurrentTime(this.getCurrentTime() - skipSeconds);
    } else {
      this.stop();
    }


  }

  /**
   * Forward by one frame
   */
  stepForward() {
    this.pause();
    this.forward(1 / this.getFrameRate())
  }

  /**
   * Backward by one frame
   */
  stepBackward() {
    this.pause();
    this.replay(1 / this.getFrameRate())
  }

  decreaseVolume(step) {
    // Decrease volume by 0.1 (10%).
    const increment = step ? step : 0.1;

    // Ensure that the volume doesn't go below 0.
    this.el.volume = Math.max(this.el.volume - increment, 0);

    if (this.el.volume < 0.01) {
      this.muteButton.setIcon('volume_off');
      showAlertToast('Muted', 'success');
    } else if (this.el.volume < 0.5) {
      this.muteButton.setIcon('volume_down');
      showAlertToast(`Volume: ${(this.el.volume * 100).toFixed()}%`, 'success');
    } else {
      this.muteButton.setIcon('volume_up');
      showAlertToast(`Volume: ${(this.el.volume * 100).toFixed()}%`, 'success');
    }



  }

  increaseVolume(step) {
    // Increase volume by 0.1 (10%). 
    const increment = step ? step : 0.10;

    // Ensure that the volume doesn't go above 1.
    this.el.volume = Math.min(this.el.volume + increment, 1);
    if (this.el.volume > 0.5) {
      this.muteButton.setIcon('volume_up');
    } else {
      this.muteButton.setIcon('volume_down');
    }

    showAlertToast(`Volume: ${(this.el.volume * 100).toFixed()}%`, 'success');


  }

  mute() {
    this.el.muted = true;
  }
  
  unMute() {
    this.el.muted = false;
  }
  
  toggleMute() {
    if (typeof this.muteButton !== undefined) { // Check if the mute button is attached to player
      if (this.isMuted()) {
        this.muteButton.setIcon('volume_up');
        this.unMute();
      } else {
        this.muteButton.setIcon('volume_off');
        this.mute();
      }
    }
  }

  showSpinner() {
    const spinnerParentDiv = this.el.parentNode.querySelector('.spinner-parent-div');
    if (spinnerParentDiv) {
      const spinnerEl = document.createElement('div');
      spinnerEl.classList.add('spinner-grow');
      spinnerEl.setAttribute('role', 'status');
      spinnerEl.style.width = '3rem;'
      spinnerEl.style.height = '3rem;'
      const spinnerSpan = document.createElement('span');
      spinnerSpan.classList.add('visually-hidden');
      spinnerSpan.textContent = 'Loading...';
      spinnerEl.append(spinnerSpan);
      spinnerParentDiv.append(spinnerEl);
    }
  }

  hideSpinner() {
    const spinnerParentDiv = this.el.parentNode.querySelector('.spinner-parent-div');
    if (spinnerParentDiv) {
      const spinnerElList = spinnerParentDiv.querySelectorAll('[class^="spinner"]');
      if (spinnerElList) {
        spinnerElList.forEach(spinnerEl => {spinnerEl.remove()})
      }
    }
  }

  close() {

  }
  

  /**
   * Loads the video element source
   */
  load() {
    this.el.load();
  }

  switchMainView(mainPlayer) {
    if (!this.isMainPlayer()) {
      this.domId = mainPlayer.domId;
    }
  }
  
  
  // Attach control buttons
  attachButton(buttonObject, buttonType) {
    if (buttonType === 'play-pause') {
      this.playButton = buttonObject;
    } else if (buttonType === 'mute') {
      this.muteButton = buttonObject;
    }
  }
  
  // Attach control bar
  attachControlBar(controlBarObject) {
    this.controlBar = controlBarObject;
    controlBarObject.attachedPlayers.push(this); // Link player to control bar as well
    // this.controlBar.updateProgressBar();
  }

  attachCanvas(canvasObject) {
    this.canvas = canvasObject;
  }

  updateButtonIcons() {
    if (this.playButton) {
      if (this.isPaused() || this.isEnded()) {
        this.playButton.setIcon('play_circle', 'size-48'); // Change the play button icon
      } else {
        this.playButton.setIcon('pause_circle', 'size-48'); // Change the play button icon
      }

    }
  }

  // // Update player with the changes in the control bar
  // updatePlayer(e) {
  //   if (typeof this.controlBar !== 'undefined') {
  //     this.pause();
  //     const progressBarRect = this.controlBar.progressBar.getBoundingClientRect();
  //     const clickPosition = e.clientX - progressBarRect.left;
  //     const percentage = (clickPosition / progressBarRect.width) * 100;
  //     const videoTime = (percentage / 100) * this.getDuration();
  //     this.setCurrentTime(videoTime);
  //   }
  // }

  drawTrackingBoxes() {
    if (this.isEnded()) {
      return false;
    } 

    const trackingMap = this.getTrackingMap();
    if (trackingMap.isEmpty()) {
      console.log('No tracking object was provided!');
      return;
    }

    // Draw rectangles on video canvas for each frame with a tracking number
    if (this.canvas) {
      const canvas = this.canvas;
      if (canvas.getContext) {
        const ctx = canvas.getContext('2d');
        canvas.width = this.el.videoWidth;
        canvas.height = this.el.videoHeight;

        const boxesInFrame = [];
    
        // Predefined tracking columns from the output of the model 
        // track_number is the frame number where there is an detection of an object
        // species/class: (0 = lemur, 1 = box/inanimate objects)

        // Convert currentFrame to string because trackingMap keys are strings (e.g. "372" instead of 372)
        const currentFrame = secondsToFrames(this.getCurrentTime(), this.getFrameRate()).toString();
        if (trackingMap.has(currentFrame)) {
          const individualNames = Player.getIndividualNames(); // Get Individual primate names
          
          trackingMap.get(currentFrame).forEach(trackInfo => {
            let rectangle = {
              trackNumber: trackInfo['trackNumber'],
              trackId: trackInfo['trackId'],
              x: trackInfo['xCoord'],
              y: trackInfo['yCoord'],
              width: trackInfo['width'],
              height: trackInfo['height'],
              species: trackInfo['species'],
              confidenceTrack: trackInfo['confidenceTrack']
            }

            // Check if it is a identification file
            if (trackInfo.hasOwnProperty('nameOrder') && trackInfo.hasOwnProperty('confidenceId')) {
              rectangle.nameOrder = trackInfo['nameOrder'];
              rectangle.confidenceId = trackInfo['confidenceId'];
            }

            boxesInFrame.push(rectangle);
            
            // Rectangles for boxes and lemurs (or other primates)
            ctx.beginPath();
            if (getSpeciesName(rectangle.species) === 'box') { // boxes
              ctx.strokeStyle = 'red'; 
              ctx.lineWidth = 3;
            }
            else if (getSpeciesName(rectangle.species) === 'lemur') { // lemurs
              ctx.strokeStyle = 'cyan'; 
              ctx.lineWidth = 4;
            }
            ctx.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
            ctx.stroke();

            // Label box default properties
            const defaultLabel = `${rectangle.species}-${rectangle.trackId}`;
            
            // Add name of the identified individual to the label box if there is one
            let canvasText = defaultLabel;
            if (Number.isInteger(rectangle.nameOrder) && individualNames) {
              const extendedLabel = `${defaultLabel}-${individualNames[rectangle.nameOrder]}`;
              canvasText = extendedLabel;
            }
            
            // Draw rectangle for labels
            // ctx.beginPath();
            // ctx.font = '20px tahoma';
            // let labelWidth = ctx.measureText(canvasText).width + 15;
            // let labelHeight = -30;
            // let padding = 5;
            // ctx.rect(rectangle.x, rectangle.y, labelWidth, labelHeight);
            // ctx.fillStyle = 'white';
            // ctx.fill();
            // ctx.fillStyle = 'black';
            // ctx.fillText(canvasText, rectangle.x+padding, rectangle.y-padding)
            // ctx.stroke();

            
            
            // Start drawing label boxes              
            ctx.beginPath();
            ctx.font = '20px tahoma';
            
            // Default label position and dimensions (top left corner)
            let labelWidth = ctx.measureText(canvasText).width + 15;
            let labelHeight = 30;
            let labelX = rectangle.x;
            let labelY = rectangle.y - labelHeight;
            let padding = 5;
            
            // Handle edge cases (when tracking boxes are on the edges of the canvas)
            // Overflowing on the top (put the label below)
            if (labelY < 0) {
              
              // Label on the bottom left
              labelY = rectangle.y + rectangle.height;
              
              // Overflowing on the left and top
              if (rectangle.x <= 0) {
                // Label on the bottom right
                labelX = rectangle.x + rectangle.height;
              }

            // Overflowing on the middle left
            } else if (labelX <= 0) {
              // Shift label x-coordinate to the right (label box overflow on the left)
              labelX = 0;
              
            } else if (labelX + labelWidth > canvas.width) {
              // Shift label x-coordinate to the left (label box overflow on the right)
              labelX = canvas.width - labelWidth;

            }

              
            // Shift label y-coordinate to the bottom (label box overflow at the top)
            // if (labelY + labelHeight < 0) {
            //   labelY = rectangle.y + rectangle.height - labelHeight;
            // }
            
            // Continue drawing after edge cases are handled
            ctx.rect(labelX, labelY, labelWidth, labelHeight);
            ctx.fillStyle = 'white';
            ctx.fill();
            ctx.fillStyle = 'black';
            ctx.fillText(canvasText, labelX+padding, labelY+labelHeight-padding)
            ctx.stroke();


          });

        }

        this.setTrackingBoxesInFrame(boxesInFrame)
        return boxesInFrame 
          
      } else {
        // canvas-unsupported code here
      }
      
    }

  }

  
  /**
   * Draw on video canvas to indicate whether the labelling mode is enabled
   */
  indicateLabellingModeOnCanvas() {
    if (this.isEnded()) {
      return false;
    } 


    // Draw bounding boxes around the canvas
    const canvas = this.canvas;
    if (canvas && canvas.getContext) {
      const ctx = canvas.getContext('2d');
      
      // Adjust canvas size if no tracking file uploaded yet
      if (this.getTrackingMap().isEmpty()) {
        canvas.width = this.el.videoWidth;
        canvas.height = this.el.videoHeight;
      }

      // Highlight the entire frame
      let color;
      if (Player.isInLabellingMode()) {
        color = green;
      } else {
        color = red;
      }
      
      ctx.beginPath();
      ctx.strokeStyle = color 
      ctx.lineWidth = 10;
      ctx.rect(0, 0, canvas.width, canvas.height);
      ctx.stroke();
        
        

      
    }
    
  }

  /**
   * Draw bounding boxes on the frames with annotated behaviors
   */
  indicateAnnotatedBehaviors() {
    if (this.isEnded()) {
      return false;
    } 

    // Get the annotated behaviors with its frame numbers
    const ethogram = this.getEthogram();
    if (ethogram.size() === 0) {
      console.log('No ethogram could be found!');
      return;
    }

    const currentFrame = this.getCurrentFrame();
    let annotations = []; // Save concurrent annotations in an array
    const allObservations = ethogram.getAllObservations();
    for (const [obsIndex, obs] of allObservations) {
      // Check if current frame has any annotated behaviors
      if (currentFrame >= obs.getStartTime() && currentFrame <= obs.getEndTime()) {
        annotations.push(obs.getAction());
      }
    }

    // If so, draw bounding boxes around the canvas on video and label annotations with text boxes
    const canvas = this.canvas;
    if (canvas && canvas.getContext) {
      const ctx = canvas.getContext('2d');
      
      // Adjust canvas size if no tracking file uploaded yet
      if (this.getTrackingMap().isEmpty()) {
        canvas.width = this.el.videoWidth;
        canvas.height = this.el.videoHeight;
      }

      if (annotations.length > 0) {
        
        // Produce text for annotations
        let labelText = annotations.join(', ');

        // Highlight the entire frame
        ctx.beginPath();
        ctx.strokeStyle = 'yellow'; 
        ctx.lineWidth = 10;
        ctx.rect(0, 0, canvas.width, canvas.height);
        ctx.stroke();

        // Draw rectangle for labels
        ctx.beginPath();
        ctx.font = '25px tahoma';
        let labelWidth = ctx.measureText(labelText).width + 25;
        ctx.strokeStyle = 'yellow';
        ctx.lineWidth = 5;

        ctx.rect(0, 0, labelWidth, 30);
        ctx.fillStyle = 'rgba(255, 255, 255, 0.8)' // Transparent rectangle
        ctx.fill();
        ctx.fillStyle = 'black';
        ctx.fillText(labelText, 10, 20)
        ctx.stroke();
        
      }
      
    }
    
  }

  /**
   * Show menu with right-click for individual name edits/additions
   * @param {*} event 
   */
  showRightClickMenu(event) {
    
    // Get individual names
    const individualNames = Player.getIndividualNames();

    if (!individualNames) {
      console.log('No individual name could be found!');
      showAlertToast('Please upload a file for individual names!', 'warning', 'Name Editing Disabled');
      return;
    }

    const boxesInFrame = this.getTrackingBoxesInFrame();
    if (typeof boxesInFrame !== "undefined" && boxesInFrame.length > 0) {
      this.pause();
      const canvas = this.canvas;
      const canvasRect = canvas.getBoundingClientRect();
      const mouseX = event.clientX - canvasRect.left;
      const mouseY = event.clientY - canvasRect.top;
      const widthRatio = canvas.width / canvas.clientWidth;
      const heightRatio =  canvas.height / canvas.clientHeight;

      const clickedRectangles = getClickedBoxes(boxesInFrame, mouseX*widthRatio, mouseY*heightRatio);
      if (!clickedRectangles || clickedRectangles.length < 1) {
        return;
      }

        
      const clickedRectangle = clickedRectangles[0];
      
      const rightClickDiv = document.getElementById('right-click-canvas-div');
      const nameEditSelect = rightClickDiv.querySelector('select');
      if (rightClickDiv && nameEditSelect) {
        
        // Save rectangle info to right click div element
        rightClickDiv.dataset.clickedSpecies = clickedRectangle.species;
        rightClickDiv.dataset.clickedId = clickedRectangle.trackId;
        rightClickDiv.dataset.frameNumber = this.getCurrentFrame();
        if (Number.isInteger(parseInt(clickedRectangle.nameOrder))) {
          rightClickDiv.dataset.clickedNameOrder = clickedRectangle.nameOrder;
        }

        const clickedInfoDiv = rightClickDiv.querySelector('.info-text');
        if (clickedInfoDiv) {
          clickedInfoDiv.textContent = produceLabelText(clickedRectangle);
        }

        // Clear previous options
        nameEditSelect.options.length = 0;

        // Add default option
        const defaultOption = document.createElement('option');
        defaultOption.text = 'Add name'

        // Change the title for name edit div depending on existence of a name for the clicked individual
        // Check if clicked rectangle includes an individual name 
        if (Number.isInteger(clickedRectangle.nameOrder)) {
          // Change the title 
          defaultOption.text = 'Change name';
        }

        nameEditSelect.add(defaultOption);

        // Add all individual names to the options for editting tracks
        individualNames.forEach(name => {
          const option = document.createElement('option');
          option.text = name;
          option.value = individualNames.indexOf(name); // Order of name in the individual list file
          option.dataset.trackId = clickedRectangle.trackId;
          option.dataset.species = clickedRectangle.species;
          option.dataset.nameOrder = individualNames.indexOf(name);
          nameEditSelect.add(option);

        }); 

        // Populate select menu
        rightClickDiv.style.left = mouseX + 'px';
        rightClickDiv.style.top = mouseY + 'px';
        rightClickDiv.classList.remove('d-none');
          
      }
    }


  }

  /**
   * Show cursor pointer when user moves the mouse over the bounding boxes on the video canvas
   * @param {Event} event 
   */
  showCursorOnBoxes(event) {
    const boxesInFrame = this.getTrackingBoxesInFrame();
    if (typeof boxesInFrame !== "undefined" && boxesInFrame.length > 0) {

      // Calculate clicked position relative to canvas 
      const canvas = this.canvas;
      const canvasRect = canvas.getBoundingClientRect();
      const mouseX = event.clientX - canvasRect.left;
      const mouseY = event.clientY - canvasRect.top;
      const widthRatio = canvas.width / canvas.clientWidth;
      const heightRatio =  canvas.height / canvas.clientHeight;

      const clickedRectangles = getClickedBoxes(boxesInFrame, mouseX*widthRatio, mouseY*heightRatio);
      if (clickedRectangles && clickedRectangles.length > 0) {
        canvas.style.cursor = 'pointer';
      } else {
        canvas.style.cursor = 'default';

      }

    }

  }

  /**
   * 
   * @param {Event} event - 'Clicked' event
   * 
   */
  makeBoxesInteractive(event) {
    const boxesInFrame = this.getTrackingBoxesInFrame();
    // console.log(boxesInFrame)

    if (this.canvas) {
      if (typeof boxesInFrame !== "undefined" && boxesInFrame.length > 0) {
        
        // Pause the video
        this.pause();
        
        // Close previously opened menus on canvas
        if (!event.target.matches('#right-click-canvas-div')) {
          const dropdownEl = document.getElementById('right-click-canvas-div');
          if (dropdownEl) {
            dropdownEl.classList.add('d-none');
          }
        }

        if (!event.target.matches('#popover-canvas-div')) {
          const popoverEl = document.getElementById('popover-canvas-div');
          if (popoverEl) {
            const popover = bootstrap.Popover.getOrCreateInstance(popoverEl);
            popover.dispose();
          }
        }
    
        // Calculate clicked position relative to canvas 
        const canvas = this.canvas;
        const canvasRect = canvas.getBoundingClientRect();
        const mouseX = event.clientX - canvasRect.left;
        const mouseY = event.clientY - canvasRect.top;
        const widthRatio = canvas.width / canvas.clientWidth;
        const heightRatio =  canvas.height / canvas.clientHeight;

        const currentFrame = this.getCurrentFrame();
        const clickedRectangles = getClickedBoxes(boxesInFrame, mouseX*widthRatio, mouseY*heightRatio)
        
        if (clickedRectangles && clickedRectangles.length > 0) {

          // Show toast to let user choose one of the overlapping boxes
          if (clickedRectangles.length > 1) {
            showOverlappingTracksToast(clickedRectangles);
          } else {
            let clickedRectangle = clickedRectangles[0];
            const timestamp = this.getCurrentTime();
            // addInteractionRow(timestamp, currentFrame);
            showToastForBehaviorRecording(event, clickedRectangle, boxesInFrame, timestamp, currentFrame);

          }

        }
      } else {
        console.log('No rectangles in the current frame!');
      }
  

    }

  }
  

  drawSnapshot() {
    this.pause();

    const modalEl = document.getElementById('snapshot-modal');
  
    // const tracking = document.getElementById('main-tracking-canvas');
    // const video = document.getElementById('main-video');
    // const snapshotDiv = document.getElementById('snapshot-div');

    // Get the tracking boxes from canvas if it is available
    const tracking = this.canvas;

    // Create new canvas for snapshot
    const canvas = document.createElement('canvas');
    
    // Adjust snapshot dimensions
    const video = this.el;
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    
    let ctx = canvas.getContext('2d');
    ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
    ctx.drawImage(tracking, 0, 0, canvas.width, canvas.height);
  
    // JPEG quality (1.0 -> highest, 0 -> lowest) 
    const quality = 1.0;
    
    // Convert the canvas to image data
    let imageData = canvas.toDataURL('image/png', quality);
  
    // Set the src of the image element to the snapshot
    // HTML Element to show snapshot to user
    const snapshotImg = document.getElementById('snapshot');
    if (snapshotImg) {
      snapshotImg.src = imageData;
      snapshotImg.classList.remove('d-none');// Show the snapshot

      if (modalEl) {
        const modal = bootstrap.Modal.getOrCreateInstance(modalEl);
        const playerName = this.name;
        const currentFrame = this.getCurrentFrame();
        const snapshotTitle = modalEl.querySelector('#snapshot-modal-title');
        if (snapshotTitle) {
          snapshotTitle.textContent = `Snapshot for ${playerName} at frame ${currentFrame}`
        }

        // Handle hiding tracking boxes with user input
        const hideTrackingBtn = modalEl.querySelector('#hide-tracking-btn');
        if (hideTrackingBtn) {
          hideTrackingBtn.checked = this.canvas.classList.contains('d-none');
          hideTrackingBtn.addEventListener('change', () => {
            // Clear canvas
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
            if (!hideTrackingBtn.checked) {
              ctx.drawImage(tracking, 0, 0, canvas.width, canvas.height);
            }
            // Save canvas data to image element
            imageData = canvas.toDataURL('image/png', quality);
            snapshotImg.src = imageData; 
            snapshotImg.classList.remove('d-none');// Show the snapshot

            // Save the snapshot data to Player instance to write it to a file later
            this.snapshotData = {
              videoName: playerName, 
              canvasData: snapshotImg.src, // 'image/png' for PNG format,
              frameNumber: currentFrame,
              fileExtension: 'png'
            }
    
          })
        }

        // Save the snapshot data to Player instance to write it to a file later
        this.snapshotData = {
          videoName: playerName, 
          canvasData: snapshotImg.src, // 'image/png' for PNG format,
          frameNumber: currentFrame,
          fileExtension: 'png'
        }

        

        // Show the modal
        modal.show();
    
        // // Handle saving snapshots
        // const saveSnapshotBtn = document.getElementById('save-snapshot-btn');
        // if (saveSnapshotBtn) {
        //   saveSnapshotBtn.addEventListener('click', async () => {
        //     // const canvasData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        //     const canvasData = canvas.toDataURL('image/png'); // 'image/png' for PNG format
        //     const data = {
        //       videoName: playerName, 
        //       canvasData: canvasData,
        //       frameNumber: currentFrame,
        //       fileExtension: 'png'
        //     };
        //     const success = await window.electronAPI.saveSnapshot(data);
        //     console.log(success)
        //     if (success) {
        //       modal.hide();
        //     }
        //   })
        // }
    
        
      }
    
    }
  
  }

  async saveSnapshot() {
    if (!this.snapshotData) {
      showAlertToast('No data for snapshot could be found!', 'error');
      return;
    }

    const response = await window.electronAPI.saveSnapshot(this.snapshotData);
    if (response) {
      const modalEl = document.getElementById('snapshot-modal');
      if (modalEl) {
        const modal = bootstrap.Modal.getOrCreateInstance(modalEl);
        modal.hide();

      }

    }


  }



 

  
}


// Button
class Button {
  cssSelector;

  constructor(cssSelector) {
    this.el = document.querySelector(cssSelector);
    if (!this.el.querySelector('span')) {
      this.iconEl = document.createElement('span');
      this.el.appendChild(this.iconEl);
    }
    this.iconEl = this.el.querySelector('span');

  }

  // Add an event listener and its callback function
  on(event, callBackFunction) {
    this.el.addEventListener(event, callBackFunction);
  }

  setIcon(iconText, iconSize = 'size-24', iconClass = 'material-symbols-rounded', iconColor = 'text-black') {
    this.iconEl.innerText = iconText;
    this.iconEl.classList.add(iconClass, iconSize, iconColor);
  }

  addClass(className) {
    this.iconEl.classList.add(className);
  }

  removeClass(className) {
    this.iconEl.classList.remove(className);
  }

}

// Input class for range elements (and maybe others)
class Input {
  cssSelector;

  constructor(cssSelector) {
    this.el = document.querySelector(cssSelector);
  }

   // Add an event listener and its callback function
   on(event, callBackFunction) {
    this.el.addEventListener(event, callBackFunction);
  }

  getValue() {
    return this.el.value;
  }

  getMax() {
    return this.el.max;
  }

  getMin() {
    return this.el.min;
  }

  setValue(value) {
    this.el.value = value;
  }

  setMax(value) {
    this.el.max = value;
  }

  setMin(value) {
    this.el.min = value;
  }

  setLabels(textArray) {
    const labelEls = this.el.labels;

    if (labelEls && labelEls.length === textArray.length) {
      for (let i = 0; i < labelEls.length; i++) {
        labelEls[i].textContent = textArray[i];
      }
      // return labelEls.map((labelEl, index) => {
      //   labelEl.textContent = textArray[index];
      // });
    } else {
      console.log("Provide a string array as the number of labels for the input!")
      return
    }
  }

}


class ControlBar {
  cssSelector;

  constructor(cssSelector) {
    this.el = document.querySelector(cssSelector);
    this.progressBar = this.el.querySelector('#progress-bar');
    this.progress = this.el.querySelector('#progress');
    this.currentTimeDiv = this.el.querySelector('#current-time'); // Current time in minutes and seconds
    this.currentFrameDiv = this.el.querySelector('#current-frame'); // Current time in frames
    this.totalTimeDiv = this.el.querySelector('#total-time'); // Total time in minutes and seconds
    this.totalFramesDiv = this.el.querySelector('#total-frames'); // Total time in frames
    this.attachedPlayers = [];
    // this.timeStampTooltip = bootstrap.Tooltip.getOrCreateInstance('#progress-bar');
  }

  // Add an event listener and its callback function
  on(event, callBackFunction) {
    this.el.addEventListener(event, callBackFunction);
  }

  updateProgressBar() {
    // Move progress bar according to the current time 
    if (this.attachedPlayers.length > 0) {
      const currentTimeRatio = (this.attachedPlayers[0].getCurrentTime() / this.attachedPlayers[0].getDuration()) * 100; // Calculate the percentage
      this.progress.style.width = currentTimeRatio + '%'; // Move the bar according to this percentage

      // Show current and remaining time
      const currentTime = this.attachedPlayers[0].getCurrentTime();
      const duration = this.attachedPlayers[0].getDuration();
      this.currentTimeDiv.textContent = formatSeconds(currentTime);
      this.currentFrameDiv.textContent = secondsToFrames(currentTime);
      this.totalTimeDiv.textContent = formatSeconds(duration);
      this.totalFramesDiv.textContent = secondsToFrames(duration);


      // const progressRange = document.getElementById('progress-range');
      // progressRange.min = 0;
      // progressRange.max = this.attachedPlayers[0].getDuration()
      // progressRange.value = this.attachedPlayers[0].getCurrentTime();

    }
    
  }

  updatePlayer(e) {
    if (this.attachedPlayers.length > 0) {
      this.attachedPlayers.foreach(player => player.pause());
      const progressBarRect = this.progressBar.getBoundingClientRect();
      const clickPosition = e.clientX - progressBarRect.left;
      const percentage = (clickPosition / progressBarRect.width) * 100;
      const videoTime = (percentage / 100) * this.attachedPlayers[0].getDuration();
      this.attachedPlayers.foreach(player => player.setCurrentTime(videoTime));
    }
  }


  // showTimeOnHover(e) {
  //   if (typeof this.attachedPlayer !== undefined) {
  //     const progressBarRect = this.progressBar.getBoundingClientRect();
  //     const hoverPosition = e.clientX - progressBarRect.left;
  //     const videoTime = (hoverPosition / progressBarRect.width) * this.attachedPlayer.getDuration();
  //     const tooltipOptions = {
  //       title: 'timestamp', 
  //       container: 'body', 
  //     }
  //     const tooltip = bootstrap.Tooltip.getOrCreateInstance(this.progressBar, tooltipOptions);
  //     tooltip.setContent({'.tooltip-inner': formatSeconds(videoTime)});
  //     tooltip.show();
  //   }

  // }


}





class Ethogram {

  constructor(obj) {
    
    // Ethogram file path in user data directory
    this.pathInUserDataDir;

    // Observations
    this.observations = new Map();
    
    // Update insertionIndex if an object is given in the constructor
    // Useful for retrieving JSON data as object from config files
    if (obj) {
      this.observations = new Map(Object.entries(obj));
    
    }
  }

  getInsertionIndex() {
    // Get the keys and convert them to integers
    const keyIndices = Array.from(this.observations.keys()).map(key => parseInt(key));

    // Set the insertion index to 0 by default
    // Check if the key array is not empty
    // Find the largest index and add 1 to get the insertion index for new observation 
    const newObsIndex = keyIndices.length <= 0 ? 0 : Math.max(...keyIndices) + 1;

    // Return the insertion index
    return newObsIndex;
    
  }

  getAllObservations() {
    return this.observations;
  }

  /**
   * Converts all custom Observation instances to ordinary Objects
   * Necessary to pass observation to functions in the main.js
   * @returns {Object[]} - Array of objects converted from observations
   */
  getAllAsObjects() {
    const observationArr = [...this.observations.values()]
    .map(observation => Object.fromEntries(observation.entries));

    return observationArr;

  }


  
  getObservation(obsIndex) {
    if (!Number.isInteger(parseInt(obsIndex))) {
      console.log('Observation index must be provided!')
      return;
    }

    return this.observations.get(obsIndex.toString());

  }

  /**
   * Adds an observation to the ethogram
   * @param {Observation} obs - Observation to be added 
   * @param {Boolean | undefined} doNotWriteToFile - Do not write observation to file if true 
   * @returns {Boolean} - True if the insertion is successful, False otherwise
   */
  async add(obs, doNotWriteToFile) {
    // Get the insertion index from the Observation to be added
    const indexInObs = parseInt(obs.get('index'));
    
    // If no index exists inside the Observation, get the insertion index from Ethogram
    const newObsIndex = Number.isInteger(indexInObs) ? indexInObs : this.getInsertionIndex();

    // Add new observation object as the value of this insertion key index
    const newObs = this.observations.set(newObsIndex.toString(), obs);

    // Save changes to the file
    if (!doNotWriteToFile) await this.saveToFile();

    return newObs.has(newObsIndex.toString());

  }

  /**
   * Update an observation 
   * @param {*} obsIndex - Observation index
   * @param  {...Array} entries - Array of key-value pairs
   */
  async update(obsIndex, ...entries) {
    // Get the Observation at the given index
    const observation = this.observations.get(obsIndex.toString());
    
    // Validate the selected observation
    if (observation && observation instanceof Observation) {
      
      // Update the entries of the observation
      for (const [key, value] of entries) {
        observation.update([key, value]);
      }

    }

    // Return confirmation
    const isSuccess = [...entries].every(([key, value], index, entries) => { 
      if (this.observations.has(obsIndex.toString())) {
        return this.observations.get(obsIndex.toString()).get(key) === value;

      }
    });

    if (isSuccess) {
      const response = await this.saveToFile();
    }

    return isSuccess;


  }

  /**
   * Removes an observation from the ethogram
   * @param {Number | String} obsIndex - Index of the observation to be removed
   * @returns {Boolean} - True if the removal was successful
   */
  async remove(obsIndex) {
    const isDeleted = this.observations.delete(obsIndex.toString());
    if (isDeleted) {

      const response = await this.saveToFile();

      if (response) return true;

    }

  }

  /**
   * Gets the number of records in the ethogram
   * @returns {Number} - Size of the ethogram
   */
  size() {
    return this.observations.size;
  }

  /**
   * Removes all observations from the ethogram
   * @returns 
   */
  async clear() {
    this.observations.clear();

    const response = await this.saveToFile();
    if (response) {
      // Clear behavior table
      clearEthogramTable();
      return 'Ethogram cleared!';
    }

  }

  async saveToFile() {
    
    // Get the recorded behaviors and 
    // Convert each Observation object to regular JS Object
    // const observationArr = [...this.getAllObservations().values()]
    //   .map(observation => Object.fromEntries(observation.entries));
    const observationArr = this.getAllAsObjects();

    const mainPlayer = Player.getMainPlayer();
    if (mainPlayer) {
      // Get the metadata
      const videoName = mainPlayer.getName();
      const videoFPS = mainPlayer.getFrameRate();
      const username = Player.getUsername();

      // Write the metadata
      const withMetadata = false;

      // Save the recorded behavior to the file in user data directory
      const filePath = await window.electronAPI.writeEthogramToFile(observationArr, videoName, videoFPS, username, withMetadata);
      
      // Flag success or failure
      const isFailed = !filePath;

      if (filePath) {
        // Show last edit time for the ethogram file
        Player.showLastEditForEthogram(filePath, isFailed);

      } else {

      }


      return filePath;

    }

  
  }


  



}

class TrackingMap {
 
  constructor(obj, firstAvailTrackIds, history) {
    this.tracks = new Map();
    this.firstAvailTrackIds = new Map();
    this.history = [];  // Save edit history to undo changes easily

    if (obj) {
      // If obj is already a Map, save it directly. Otherwise, convert it to a Map
      // Useful for retrieving JSON data as object from the config file
      if (obj instanceof Map) {
        this.tracks = obj;
      } else {
        this.tracks = new Map(Object.entries(obj));
      }
    }

    // Save the first available track ID computed by main.js initially
    // Calculated as largest ID + 1 for each species (box, lemur, etc.)
    if (firstAvailTrackIds) {
      this.firstAvailTrackIds = firstAvailTrackIds;
    }

    if (history) {
      this.history = history;
    }
    
  }

  // Add or update an entry
  set(key, value) {
    // this.history.push(new Map(this.tracks));
    this.tracks.set(key, value);
  }

  setIndividualNames(nameArr) {
    if (nameArr) {
      this.individualNames = nameArr;
    }
  }

  get(key) {
    return this.tracks.get(key);
  }

  getTracks() {
    return this.tracks;
  }

  getIndividualNames() {
    return this.individualNames;
  }

  has(key) {
    return this.tracks.has(key);
  }

  keys() {
    return this.tracks.keys();
  }

  // Delete an entry
  delete(key) {
    if (this.tracks.has(key)) {
      // this.history.push(new Map(this.tracks));
      this.tracks.delete(key);
    }
  }

  // Add all edited frames to history with their properties prior to edits
  // Called from outside of the TrackingMap object
  async addToHistory(keyValueArr) {
    // this.history.push({
    //   frameNumber: key, 
    //   trackInfoArr: value
    // });
    this.history.push(keyValueArr);
    const response = await this.saveToConfig();

    // console.log(this.history);
  }

  // Undo the last change
  async undo() {
    if (this.history.length > 0) {

      const latestEditArr = this.history.pop();
      // console.log(latestEditArr)
      latestEditArr.forEach(edit => {
        this.set(edit.frameNumber, edit.trackInfoArr);
      })
      
      // Refresh tracking boxes on canvas
      const mainPlayer = Player.getMainPlayer();
      if (mainPlayer) Player.getMainPlayer().drawTrackingBoxes();
      
      // Update tracking file
      const isSaved = await this.saveEditsToFile();
      if (isSaved) {
        // Save changes to config
        const response = await this.saveToConfig();
        return response;

      }

    }

  }

  // Get the current size of the map
  size() {
    return this.tracks.size;
  }

  // Check if the map is empty
  isEmpty() {
    return this.size() === 0;
  }

  anyEditHistory() {
    return this.history.length > 0;
  }

  // Clear the map
  async clear() {
    this.history = [];
    this.firstAvailTrackIds.clear();
    this.tracks.clear();
    const response = await this.saveToConfig();
  }

  // Update tracking edits in the config file
  async saveToConfig() {
    const trackingTableBody = document.querySelector('#tracking-table > tbody');
    let trackingTableHTML;
    if (trackingTableBody) {
      trackingTableHTML = JSON.stringify(trackingTableBody.innerHTML);

      // Save tracking HTML to file in user data dir
      let fileName;
      const mainPlayer = Player.getMainPlayer();
      if (mainPlayer) {
        fileName = mainPlayer.getName();
      }
      
      // const trackingHTMLFilePath = window.electronAPI.saveTrackingTableHTML(trackingTableHTML, fileName);
      // console.log(trackingHTMLFilePath);

    }

    const response = await window.electronAPI.saveToConfig({
      trackingEditHistory: this.history,
      firstAvailTrackIds: Object.fromEntries(this.firstAvailTrackIds),
      trackingTableHTML: trackingTableHTML

    });

    return response;

  }

  async saveEditsToFile() {

    // Save edits to file in user directory
    const mainPlayer = Player.getMainPlayer();
    if (mainPlayer) {
      const fileName = mainPlayer.getName();
      if (fileName) {
        // Keep track of saved status
        let isSaved;
        
        // Show spinner before the process starts
        showOrHideSpinner('show');
        
        const response = await window.electronAPI.saveTrackingEdits(this.getTracks(), fileName, Player.getIndividualNames(), Player.getUsername());
        
        if (response) {
          // Show success if editted tracking file copied to export directory
          isSaved = response.pathInUserDataDir ? true : false;
        }
          
        // By changing the relevant icon on tracking table
        // showTrackingSaveStatus(isSaved);
        
        // Hide the spinner
        showOrHideSpinner('hide');

        return response;


      }
    }
  }

}


class Hotkey {
  /**
   * Hotkey for playback, behavior recording, app navigation, etc.
   * @param {String} category - Category of the hotkey (e.g. playback, labelling, individuals, actions)
   * @param {String} name - Name of the hotkey
   * @param {String} description - Description of the function of the hotkey
   * @param {Array} keys - Keyboard keys excluding modifiers (ctrl, cmd, meta, shift, alt, option)
   * @param {Array} modifiers - Optional modifier keys (ctrl, cmd, meta, shift, alt, option)
   */

  // Keep track of all Hotkey instances
  static allInstances = [];


  // Map shortcuts to HTML code
  static htmlMap = new Map([
    ['Meta', '&#8984;'],
    ['metaKey', '&#8984;'],
    ['ctrlKey', 'Ctrl'],
    ['shiftKey', 'Shift'],
    ['ArrowUp', '&uarr;'],
    ['ArrowDown', '&darr;'],
    ['ArrowRight', '&rarr;'],
    ['ArrowLeft', '&larr;'],
    ['>', '&gt;'],
    ['<', '&lt;'],
    [' ', '&#9141;'],
    ['Alt', Player.onMacOS ? '&#8997;' : 'Alt'],
    ['Enter', '&#8629;']
    // ['Shift', '&#8679;'],

  ]);

  constructor(category, name, description, key, modifiers) {
    this._category = category;
    this._name = name;
    this._description = description;
    this._key = key; 
    this._modifiers = modifiers ? modifiers : []; 

    // Add the newly created hotkey to the array containing all instances
    Hotkey.allInstances.push(this);

  }

  get category() {
    return this._category;
  }

  get name() {
    return this._name;
  }

  get description() {
    return this._description;
  }

  get key() {
    return this._key;
  }

  get modifiers() {
    return this._modifiers;
  }

  set category(category) {
    this._category = category;
  }

  set name(name) {
    this._name = name;
  }

  set description(description) {
    this._description = description;
  }

  set key(key) {
    this._key = key;
    this.updateDomEls();
  }

  set modifiers(modifiers) {
    this._modifiers = modifiers;
    this.updateDomEls();
  }

  static getAll() {
    return Hotkey.allInstances;
  }

  /**
   * Finds conflicting hotkeys by comparing all Hotkey instances with the input key and modifier combination.
   * @param {String} refKey Reference key
   * @param {String[]} refModifiers Reference modifier key(s)
   * @returns {Hotkey[] | undefined} Array of conflicting Hotkey instances or undefined if no input was given
   */
  static findConflicts(refKey, refModifiers) {
    if (!refKey || !refModifiers) return;

    // If reference has no modifiers, only compare with hotkeys without any modifiers
    if (refModifiers.length < 1) {
      return Hotkey.getAllInstancesWithoutModifiers().filter(
        hotkey => hotkey.key === refKey
      );
    }

    // If reference has modifiers, only compare with hotkeys with modifiers
    if (refModifiers.length > 0) {
      return Hotkey.getAllInstancesWithModifiers().filter(hotkey => 
        hotkey.key === refKey && 
        refModifiers.every(refModifier => hotkey.modifiers.includes(refModifier))
      );
    }



  }

  /**
   * Reflect the update on a Hotkey to its associated HTML elements
   */
  updateDomEls() {

    // Combine the modifiers and keys of the Hotkey object
    const keysAndModifiers = this.modifiers.concat(this.key);
    
    // Find the all associated DOM elements
    const domEls = document.querySelectorAll(`[data-hotkey-name=${this.name}][data-hotkey-category=${this.category}]`);
    domEls.forEach(domEl => {
      
      const kbdEl = domEl.querySelector('.hotkey-kbd');

      if (kbdEl) {
        kbdEl.innerHTML = keysAndModifiers.map(key => {
          const keyName = Hotkey.htmlMap.has(key) ? Hotkey.htmlMap.get(key) : key;
          return (`<kbd>${keyName}</kbd>`);        
        }).join(' + ');

      }


      
      // // Get the kdb element
      // const kbdEls = domEl.querySelectorAll('kbd');

      // if (Array.from(kbdEls).length !== keysAndModifiers.length) {
      //   console.log(`kbd HTML element count must be equal to the total count of keys and modifiers for each Hotkey! kbd count: ${Array.from(kbdEls).length}, key count: ${keysAndModifiers.length}`);
      //   return;
      // }

      // kbdEls.forEach((kbdEl, index) => {

      //   const key = keysAndModifiers[index];

      //   console.log(key);
        
      //   // Fill the kbd element depending on whether key is a special element or not
      //   kbdEl.innerHTML = Hotkey.htmlMap.has(key) ? Hotkey.htmlMap.get(key) : key;

      // })

      
    })

  }

  /**
   * Gets the all Hotkey instances with modifier(s)
   * @returns {Hotkey[]} Array of Hotkey instances
  */
  static getAllInstancesWithModifiers() {
   
    return Hotkey.getAll().filter(hotkey => hotkey.key && hotkey.modifiers.length)

  }

  /**
   * Gets the all Hotkey instances without any modifiers
   * @returns {Hotkey[]} Array of Hotkey instances
  */
  static getAllInstancesWithoutModifiers() {
   
    return Hotkey.getAll().filter(hotkey => hotkey.key && !hotkey.modifiers.length)

  }

  /**
   * Gets the all key strings linked to the Hotkeys without any modifiers
   * @returns {String[]} Array of keys associated with each Hotkey instance
  */
  static getAllKeysWithoutModifiers() {
   
    return Hotkey.getAllInstancesWithoutModifiers().map(hotkey => hotkey.key);

  }

  /**
   * Gets the all key strings linked to the Hotkeys with modifier(s)
   * @returns {String[]} Array of keys associated with each Hotkey instance
  */
  static getAllKeysWithModifiers() {
   
    return Hotkey.getAllInstancesWithModifiers().map(hotkey => hotkey.key);

  }

  /**
   * Finds the multiple Hotkey objects according to a search criteria
   * @param {Object} - Object consisting of optional Strings for category, name, key and modifiers
   * @returns {Hotkey[]} - Array of Hotkey objects matching the search criteria. Returns an empty array if no hits have been found.
   */
  static findAll({category, name, key, modifiers}) {
    const resultArr = Hotkey.getAll().filter(hotkey => {
      if (category || name || key || modifiers) {
        const selectedCategory = category ? hotkey.category === category : true;
        const selectedName = name ? hotkey.name === name : true;
        const selectedKey = key ? hotkey.key === key : true;
        const selectedModifiers = modifiers ? hotkey.modifiers === modifiers : true;
        return selectedCategory && selectedName && selectedKey && selectedModifiers

      }
      
    });

    return resultArr;



  }

  /**
   * Finds a single Hotkey object according to a search criteria
   * @param {Object} - Object consisting of optional Strings for category, name, key and modifiers
   * @returns {Hotkey} -  Single Hotkey object or undefined if no hits have been found.
   */
  static findOne({category, name, key, modifiers}) {
    const resultArr = Hotkey.getAll().filter(hotkey => {
      if (category || name || key || modifiers) {
        const selectedCategory = category ? hotkey.category === category : true;
        const selectedName = name ? hotkey.name === name : true;
        const selectedKey = key ? hotkey.key === key : true;
        const selectedModifiers = modifiers ? hotkey.modifiers === modifiers : true;
        return selectedCategory && selectedName && selectedKey && selectedModifiers

      }
      
    });

    if (resultArr.length === 1) {
      return resultArr[0];
    }


  }


  /**
   * Get the unique hotkey categories
   * @returns {String[]} - Array of category strings
   */
  static getUniqueCategories() {
    const uniqueCategories = [];

    Hotkey.getAll().forEach(hotkey => {
      if (!uniqueCategories.includes(hotkey.category)) {
        uniqueCategories.push(hotkey.category);
      }
    });
    
    return uniqueCategories;
  }

  /**
   * Gets the all pressed keys in a Keyboard Event including modifiers
   * @param {KeyboardEvent} event 
   * @returns {Object} result - Object consisting of pressed modifiers and keys
   * @returns {String[]} - result.modifiers - Array of Strings for pressed modifier keys
   * @returns {String} - result.key - String for pressed keys excluding modifiers
   */
  static getUserKeyPress(event) {
    let modifiersPressed = [];
    let keyPressed;

    if (event.ctrlKey) modifiersPressed.push('Control');
    if (event.shiftKey) modifiersPressed.push('Shift');
    if (event.altKey) modifiersPressed.push('Alt');
    if (event.metaKey) modifiersPressed.push('Meta');
    if (!['Control', 'Alt', 'Meta', 'Shift', 'CapsLock', 'Tab'].includes(event.key)) keyPressed = event.key;
      //     console.log(e.key)
      //     keyPressed = e.key;
  
      //   }

    // let keyupHandler;

    // if (keyupHandler) {
    //   window.removeEventListener('keyup', keyupHandler);
    // }

    // keyupHandler = function(e) {
    //   e.preventDefault();
    //   if (!['Control', 'Alt', 'Meta', 'Shift', 'CapsLock', 'Tab'].includes(e.key)) {
    //     console.log(e.key)
    //     keyPressed = e.key;

    //   }

    //   window.removeEventListener('keyup', keyupHandler);
        
    // }

    // window.addEventListener('keyup', keyupHandler);


  
    

    // console.log({modifiers: modifiersPressed, key: keyPressed});

    return {modifiers: modifiersPressed, key: keyPressed}

  }

  /**
   * Find the Hotkey object associated with the pressed key combination
   * @param {Event} event - Keyboard event
   * @returns {Hotkey} - Selected Hotkey object
   */
  static getSelected(event) {
    const selectedArr = Hotkey.getAll().filter(hotkey => hotkey.isPressed(event));
    if (selectedArr.length === 1) {
      return selectedArr[0];
    }
  }

   /**
   * Checks whether any modifier is defined on the Hotkey
   * @returns {Boolean} True if the Hotkey has modifiers, false otherwise
   */
   hasModifiers() {
    return this._modifiers.length > 0;
  }

  /**
   * Checks whether the key combination of the Hotkey object is pressed
   * @param {KeyboardEvent} event - Keyboard event
   * @returns {Boolean} - True if the Hotkey is pressed, False otherwise.
   */
  isPressed(event) {

    // Prevent default action for space bar
    if (event.key === ' ' && !Player.isTyping) {
      event.preventDefault();
    }

    // Check if modifiers is assigned to the Hotkey
    if (this.hasModifiers()) {

      // Get the modifiers assigned to the Hotkey
      const modifierKeys = this.modifiers;

      // Check if the main key and all modifier keys of the Hotkey are pressed in the same event
      return event.key === this.key && modifierKeys.every(key => event.getModifierState(key))


    // If no modifiers is assigned to the Hotkey
    } else {

      const modifierKeys = ['Control', 'Alt', 'Shift', 'Meta', 'CapsLock'];

      // Check if the main key of the Hotkey and none of the modifier keys is pressed
      return event.key === this.key && modifierKeys.every(key => !event.getModifierState(key))

    }

  }

  /**
   * Execute the function of the hotkey
   */
  execute() {

    if (Player.isTyping) return;

    // Get all players to sync playback 
    const allPlayers = Player.getAllInstances();

    switch (this.name){
      case 'stepBackward':
        // Go back to the previous frame
        allPlayers.forEach(player => player.stepBackward());
        break;
      case 'stepForward':
        // Skip to the next frame
        allPlayers.forEach(player => player.stepForward());
        break;
      case 'playPause':
        allPlayers.forEach(player => player.playPause());
        break;
      case 'replay':
        allPlayers.forEach(player => player.replay());
        break;
      case 'forward':
        allPlayers.forEach(player => player.forward());
        break;
      case 'increaseVolume':
        allPlayers.forEach(player => player.increaseVolume());
        break;
      case 'decreaseVolume':
        allPlayers.forEach(player => player.decreaseVolume());
        break;
      case 'muteUnmute':
        allPlayers.forEach(player => player.toggleMute());
        break;
      case 'increaseSpeed':
        allPlayers.forEach(player => player.increaseSpeed());
        break;
      case 'decreaseSpeed':
        allPlayers.forEach(player => player.decreaseSpeed());
        break;
      case 'toggleLabelling':
        Player.toggleLabellingMode();
        break;
      default:
        console.log('No function to execute could be found for this hotkey!'); 

    }

  }

  /**
   * Delete a Hotkey instance
   * @param {Hotkey} hotkey 
   */
  delete() {


      // Get all instances
      const allHotkeys = Hotkey.getAll();
      
      // Compare input hotkey with all others
      for (let i = 0; i < allHotkeys.length; i++) {
        const hotkey = allHotkeys[i];
        if (hotkey.category === this.category && hotkey.name === this.name) {
          allHotkeys.splice(i, 1);
          break;
        }

      }



    
  }


}

class Metadata {

  constructor() {
    this.entries = new Map([
      ['videoName', null],
      ['videoFPS', null],
      ['timestamp', null],
      ['username', null],
      ['actions', null],
      ['individuals', null],
      ['appVersion', null],
    ]);
    
    const mainPlayer = Player.getMainPlayer();
    if (mainPlayer) {
      this.entries.set('videoName', mainPlayer.getName());
      this.entries.set('videoFPS', mainPlayer.getFrameRate());
      this.entries.set('timestamp', mainPlayer.getCurrentTime());
    }

    this.entries.set('username', Player.getUsername());
    this.entries.set('actions', Player.getActionNames());
    this.entries.set('individuals', Player.getIndividualNames());
    this.entries.set('appVersion', Player.getAppVersion());
    
  }

  get(key) {
    return this.entries.get(key);
  }

  get timestamp() {
    return this.entries.get('timestamp');
  }

  get videoName() {
    return this.entries.get('videoName');
  }

  get videoFPS() {
    return this.entries.get('videoFPS');
  }

  get username() {
    return this.entries.get('username');
  }

  get actions() {
    return this.entries.get('actions');
  }

  get individuals() {
    return this.entries.get('individuals');
  }

  has(key) {
    return this.entries.has(key);
  }

  update(key, value) {
    if (this.has(key)) {
      this.entries.set(key, value);
    }
  }

  updateAppVersion() {
    this.update('appVersion', Player.getAppVersion());
  }

  updateVideoName() {
    const mainPlayer = Player.getMainPlayer();
    if (mainPlayer) {
      this.update('videoName', mainPlayer.getName());
    }
  }

  updateVideoFPS() {
    const mainPlayer = Player.getMainPlayer();
    if (mainPlayer) {
      this.update('videoFPS', mainPlayer.getFrameRate());
    }
  }

  updateTimestamp() {
    const mainPlayer = Player.getMainPlayer();
    if (mainPlayer) {
      this.update('timestamp', mainPlayer.getCurrentTime());

    }
  }

  updateUsername() {
    this.update('username', Player.getUsername());
  }

  updateActions() {
    this.update('actions', Player.getActionNames());
  }

  updateIndividuals() {
    this.update('individuals', Player.getIndividualNames());
  }

  async writeToFile() {

    // Always update the timestamp
    this.updateTimestamp();
    
    // Convert entries from a Map to an Object and finally to a JSON string
    let jsonString = JSON.stringify(Object.fromEntries(this.entries), (key, value) => {
      if (value === undefined) {
        return null;
      }
      return value;
     });

    const response = await window.electronAPI.saveMetadata(jsonString, this.videoName);
    return response;

  }




}






// Export the components
export {Player, Observation, Ethogram, Hotkey, Button, Input, ControlBar, Metadata}