// Modules to control application life and create native browser window
const { app, BrowserWindow, Menu, MenuItem, ipcMain, dialog } = require('electron');
const { updateElectronApp, UpdateSourceType } = require('update-electron-app');


// run this as early in the main process as possible 
// https://www.electronforge.io/config/makers/squirrel.windows
if (require('electron-squirrel-startup')) app.quit();

updateElectronApp({
  updateSource: {
    type: UpdateSourceType.StaticStorage,
    baseUrl: `https://bitbucket.org/kanbertay/ethowatch/downloads/`,
  },
});


// Use win32 property for consistency across Windows and MacOS
// https://nodejs.org/api/path.html#windows-vs-posix

const path = process.platform === 'win32' ? require('node:path/win32') : require('node:path/posix');
const fs = require('node:fs')
const readline = require('node:readline');

const videoFormatNames = ['mkv', 'avi', 'mp4'];
const videoExtensions = videoFormatNames.map(name => '.' + name)
const configFileName = 'config.json';

// Define column headers for behavior records
const behaviorColHeaderRow  = [
  'Subject', 'Action', 'Target', 
  'StartFrame', 'EndFrame', 'DurationInFrames', 
  'StartSecond', 'EndSecond', 'DurationInSeconds'
]

// Define CSV delimiter
const csvDelimiter = ',';

// Numbers after decimal point for time-related values in exported files
const precisionForSeconds = 4;

// User data directory path for the app
const userDataDir = app.getPath('userData');
const appDataDir = path.join(userDataDir, 'appData');

// Create the app data folder if it does not exist already
if (!fs.existsSync(appDataDir)) {
  try {
    fs.mkdirSync(appDataDir);
  } catch (err) {
    console.error(err);
  }

}



/**
 * 
 * @param {*} folderPath 
 * @returns 
 */
async function getFilesInFolder(folderPath) {
  try {
    if (fs.existsSync(folderPath)) {
      const isFile = fileName => {
        return fs.lstatSync(fileName).isFile();
      };
      const files = fs.readdirSync(folderPath)
        .map(fileName => {
          return path.join(folderPath, fileName);
        })
        .filter(isFile);
      return files
    }
  } catch (err) {
    console.error(err);
  }

}


function handleClearAppData() {
  
  try {
    fs.rmSync(appDataDir, { recursive: true, force: true });
    console.log(`${appDataDir} is deleted!`);
    return 'success';
  } catch (err) {
    console.error(`${appDataDir} could not be deleted!`, err);
  }


}


function handleResetSettings() {
  const configFilePath = path.join(appDataDir, configFileName);
  try {
    fs.unlinkSync(configFilePath);
    console.log('Config file deleted successfully!', configFileName)
    return configFilePath;
  } catch (err) {
    console.log('Error deleting config file!', err);
    return;
  }
}

/**
 * Copies a file to the user data directory with a given name
 * @param {import('node:fs').PathLike} filePath File path of the original file
 * @param {String} fileName File name of the copy in the user data directory
 * @returns
 */
function handleCopyToUserDataDir(filePath, fileName) {
  const pathInUserDataDir = path.join(appDataDir, fileName);
  
  try {
    fs.copyFileSync(filePath, pathInUserDataDir);
    console.log(`${filePath} was copied to ${pathInUserDataDir}`);
    return {success: true};
  } catch {
    console.error('The file could not be copied to the user data directory!');
  }

}

async function handleOpenDirectory () {      
  const { canceled, filePaths } = await dialog.showOpenDialog(BrowserWindow.getFocusedWindow(), {
    title: 'Choose a directory',
    // filters: [{ name: 'Movies', extensions: ['mkv', 'avi', 'mp4'] }],
    properties: ['openDirectory'],
    message: "Choose a directory"
  })
  
  if (!canceled) {
    const dirPath = filePaths[0];

    // Check read/write access
    try {
      fs.accessSync(dirPath, fs.constants.R_OK | fs.constants.W_OK);
      console.log(`${dirPath} is readable/writable`);
      return dirPath;
    } catch (err) {
      console.error(`${dirPath} is NOT accessible!`);
    } 
    
  }
}


// Get video file path list if user placed them in the specific folder
async function handleGetVideosFromDir(experimentDir) {
  
  const isFile = filePath => {
    return fs.lstatSync(filePath).isFile()
  }
  
  const isVideo = filePath => {
    return videoExtensions.includes(path.extname(filePath))
  }
  
  // const videoDirPath = path.join(__dirname, 'Experiment', 'Videos')
  const videoDirPath = path.join(experimentDir, 'Videos')

  if (fs.existsSync(videoDirPath)) {
    const videoFilePaths = fs.readdirSync(videoDirPath)
    .map(fileName => {return path.join(videoDirPath, fileName)})
    .filter(isFile)
    .filter(isVideo);
    return videoFilePaths
  } else {
    console.log('No video folder could be found!')
    return 
  }

    

}

/**
 * 
 * Function to determine delimiter
 * @param {*} line - A line in a file
 * @returns {String} - file delimiter
 */
function determineDelimiter(line) {

  // Array of potential delimiters to test
  const potentialDelimiters = [',', ';', '\t', '|', ' ', ':'];

  // Iterate through each potential delimiter and count occurrences
  let maxCount = 0;
  let delimiter = '';
  potentialDelimiters.forEach(potentialDelimiter => {
    let count = line.split(potentialDelimiter).length - 1;
    if (count > maxCount) {
      maxCount = count;
      delimiter = potentialDelimiter;
    }

  });

  return delimiter;

}

async function handleReadInteractionFile(filePath) {
  const data = fs.readFileSync(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error(err);
      return;
    }
  });
  
  const delimiter = determineDelimiter(filePath);
  
  // Split the file content into rows
  const rows = data.split('\n').map(row => row.split(delimiter));
  
  // Array to save observations
  let observationArr = [];
  
  // Initialize last observation properties
  let lastSubjectId;
  let lastObjectId;
  let lastLabel;
  let lastFrameNumber;
  let lastObservation;

  // Iterate over rows to collapse each observation (with consecutive properties) into a single entry
  rows.forEach(row => {
    const [frameNumber, subjectId, objectId, label] = row;

    // Check if you encountered a new observation
    if (subjectId !== lastSubjectId || objectId !== lastObjectId || lastLabel !== label) {
      
      // Complete the last observation and add it to the array
      if (lastObservation) {
        lastObservation.timeEnd = lastFrameNumber;
        observationArr.push(lastObservation);
      }

      // Start a new observation
      let newObservation = {
        subjectId: subjectId,
        objectId: objectId,
        label: label,
        timeStart: parseInt(frameNumber),
        timeEnd: 'TBD'
      }

      lastSubjectId = subjectId;
      lastObjectId = objectId;
      lastLabel = label;

      lastObservation = newObservation;

    }

    // Always keep track of frame number
    lastFrameNumber = parseInt(frameNumber);

  })

  if (observationArr.length > 0) {
    return observationArr;
  }

}

  
/**
 * Reads a tracking file with or without identification labels (i.e. individual IDs/names)
 * @param {import('node:fs').PathLike} filePath 
 * @returns - dictionary with rows in the tracking file
 */
async function handleReadTrackingFile(filePath) {

  if (!filePath) return;
  
  const data = fs.readFileSync(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error(err);
      return;
    }

  });

  // const delimiter = determineDelimiter(filePath);
  // if (!delimiter) {
  //   console.log('File delimiter could not be detected!');
  //   return;
  // }

  
  // Skip header lines (metadata) starting with "#" character
  const filteredRows = data.split('\n').filter(row => !row.startsWith('#')) 
  
  // Create an array of rows separated by file delimiter
  const delimiter = determineDelimiter(filteredRows[0]);
  const rows = filteredRows.map(row => row.split(delimiter));

  let trackingMap = new Map();
  let uniqueSpecies = new Set(); // Find unique species values
  let uniqueTrackIds = new Map(); // Find unique track ID values

 
  rows.forEach(row => {
    const [trackNumber, trackId, xCoord, yCoord, width, height, confidenceTrack, species, nameOrder, confidenceId] = row;
    
    // Skip an iteration which corresponds to an empty row
    if (isNaN(parseInt(trackNumber))) {
      return;
    }
    
    let trackInfo = {
      trackNumber: parseInt(trackNumber),
      trackId: parseInt(trackId),
      xCoord: parseFloat(xCoord),
      yCoord: parseFloat(yCoord),
      width: parseFloat(width),
      height: parseFloat(height),
      confidenceTrack: parseFloat(confidenceTrack),
      species: parseInt(species)
    }

    // Check if it is a identification file
    if (nameOrder && confidenceId) {
      trackInfo.nameOrder = parseInt(nameOrder);
      trackInfo.confidenceId = parseFloat(confidenceId);
    }
  
  
    // Populate the map
    if (!trackingMap.has(trackNumber)) {
      trackingMap.set(trackNumber, [trackInfo]);
    } else {
      trackingMap.get(trackNumber).push(trackInfo);
    }

    uniqueSpecies.add(parseInt(trackInfo['species']));
    
    if (!isNaN(trackInfo.trackId)) {
      if (!uniqueTrackIds.has(trackInfo.species)) {
        uniqueTrackIds.set(trackInfo.species, new Set())
      }
      uniqueTrackIds.get(trackInfo.species).add(trackInfo.trackId);
    }
  })
  
  let firstAvailTrackIds = new Map();
  for (let [species, trackIds] of uniqueTrackIds) {
    const firstAvailId = Math.max(...uniqueTrackIds.get(species)) + 1;
    firstAvailTrackIds.set(species, firstAvailId)
  }

  return {
    trackingMap: trackingMap, 
    uniqueSpecies: uniqueSpecies, 
    uniqueTrackIds: uniqueTrackIds,
    firstAvailTrackIds: firstAvailTrackIds
  }
}

/**
 * 
 * @param {*} filePath 
 * @returns - dictionary with rows in the tracking file
 */
async function handleReadIdentificationFile(filePath) {
  // Same structure in tracking file until the end of last tracking coordinate
  // File structure after tracking boxes coordinates:
  // confidence of tracking: ?
  // species id: (e.g. 0)
  // order of identified individual in the individuals.txt file: (e.g. 7)
  // confidence in detection: (e.g. 0.6600000262260437)

  
  // Read the file
  const data = fs.readFileSync(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error(err);
      return;
    }
  });
  
  
  // Determine file delimiter
  const delimiter = determineDelimiter(filePath);
  console.log(delimiter)
  if (!delimiter) {
    console.log('File delimiter could not be detected!');
    return;
  }
  
  let idMap = new Map();
  
  // Split file content into rows and then list of entries for each row
  const rows = data.split('\n').map(row => row.split(delimiter));
  rows.forEach(row => {
    const [trackNumber, trackId, xCoord, yCoord, width, height, confidenceTrack, species, nameOrder, confidenceId] = row;
    const trackInfo = {
      'trackNumber': parseInt(trackNumber),
      'trackId': parseInt(trackId),
      'xCoord': parseFloat(xCoord),
      'yCoord': parseFloat(yCoord),
      'width': parseFloat(width),
      'height': parseFloat(height),
      'confidenceTrack': parseFloat(confidenceTrack),
      'species': parseInt(species),
      'nameOrder': parseInt(nameOrder),
      'confidenceId': parseFloat(confidenceId)
    }

    // Populate the map
    if (!idMap.has(parseInt(trackNumber))) {
      idMap.set(parseInt(trackNumber), [trackInfo]);
    } else {
      idMap.get(parseInt(trackNumber)).push(trackInfo);
    }

  })

  return {idMap: idMap, fileDelimiter: delimiter}


}

function handleReadNameFile(filePath) {
  const data = fs.readFileSync(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error(err);
      return;
    }
  });

  // Split the data into rows
  const rows = data.split('\n');

  // Get the first line to determine file delimiter
  // const delimiter = determineDelimiter(rows[0]);
  // if (!delimiter) {
  //   console.log('File delimiter could not be detected!');
  //   return;
  // }

  const names = rows
  .map(row => row.split(csvDelimiter))
  .filter(row => row.length > 0)
  .flat(Infinity)
  .map(name => name.trim());

  // Attempt to verify the validity of the name file
  const nameCount = names.length;
  const upperThreshold = 20; // Too many entries
  const lowerThreshold = 2; // Too few entries
  const maxStrLength = 30; // Too long strings - Maximum string length 
  
  if (nameCount > upperThreshold) {
    console.log(`Likely not a valid name file! More than ${upperThreshold} names detected.`)
    return;
  }

  if (nameCount < lowerThreshold) {
    console.log(`Likely not a valid name file! Fewer than ${lowerThreshold} names detected.`)
    return;
  }

  if (names.some(name => name.length > maxStrLength)) {
    console.log(`Likely not a valid name file! Some entries have more then ${maxStrLength} characters.`)
    return;
  }

  return names;
    

}



/**
 * Search for the ethogram file for a video in a given directory
 * @param {import('node:fs').PathLike} videoFilePath | Path of the opened video
 * @param {import('node:fs').PathLike} dirPathToSearch | Optional path of the directory to search for. If no path is given, the default user data directory will be searched.
 * @returns {import('node:fs').PathLike} |  File path or undefined if no path was found
 */
function handleFindEthogramFile(videoFilePath, dirPathToSearch) {

  // Search user data directory by default
  const dirPath = dirPathToSearch ? dirPathToSearch : appDataDir;

  // console.log('Dir path', dirPath);
  // console.log('Video file path', videoFilePath);
  
  try {
    // Check access
    try {
      fs.accessSync(dirPath, fs.constants.R_OK | fs.constants.W_OK);
    } catch (err) {
      console.log(err);
    }

    // Get the filename without extension
    const videoFileName = path.parse(videoFilePath).name; 

    // Read the directory contents including any subdirectories within it
    const dirContentPaths = fs.readdirSync(dirPath, {recursive: true});

    // Find directory that have an identical name to the video
    // Return the absolute path of the ethogram file
    const filePaths = dirContentPaths
    .filter(entry => {
      const parsedEntry = path.parse(entry);
      return parsedEntry.name.includes(videoFileName) && 
        (parsedEntry.name.includes('behavior') || parsedEntry.name.includes('ethogram')) &&
        parsedEntry.ext.includes('csv'); 
    })
    .map(filteredEntry => path.join(dirPath, filteredEntry))
    return filePaths[0]

  } catch (err) {
    console.log(err);
  }


}

/**
 * Search for the notes file for a video in a given directory
 * @param {import('node:fs').PathLike} videoFilePath | Path of the opened video
 * @param {import('node:fs').PathLike} dirPathToSearch | Optional path of the directory to search for. If no path is given, the default user data directory will be searched.
 * @returns {import('node:fs').PathLike} |  File path or undefined if no path was found
 */
function handleFindNotesFile(videoFilePath, dirPathToSearch) {

  // Search user data directory by default
  const dirPath = dirPathToSearch ? dirPathToSearch : appDataDir;

  
  try {
    // Check access
    try {
      fs.accessSync(dirPath, fs.constants.R_OK | fs.constants.W_OK);
    } catch (err) {
      console.log(err);
    }

    // Get the filename without extension
    const videoFileName = path.parse(videoFilePath).name; 

    // Read the directory contents including any subdirectories within it
    const dirContentPaths = fs.readdirSync(dirPath, {recursive: true});

    // Find directory that have an identical name to the video
    // Return the absolute path of the notes file
    const filePaths = dirContentPaths
    .filter(entry => {
      const parsedEntry = path.parse(entry);
      return parsedEntry.name.includes(videoFileName) && 
        parsedEntry.name.includes('notes') &&
        parsedEntry.ext.includes('txt'); 
    })
    .map(filteredEntry => path.join(dirPath, filteredEntry))
    return filePaths[0]

  } catch (err) {
    console.log(err);
  }


}


/**
 * Search for the metadata file for a video in a given directory
 * @param {import('node:fs').PathLike} videoFilePath | Path of the opened video
 * @param {import('node:fs').PathLike} dirPathToSearch | Optional path of the directory to search for. If no path is given, the default user data directory will be searched.
 * @returns {import('node:fs').PathLike} |  File path or undefined if no path was found
 */
function handleFindMetadataFile(videoFilePath, dirPathToSearch) {

  // Search user data directory by default
  const dirPath = dirPathToSearch ? dirPathToSearch : appDataDir;

  
  try {

    // Check access
    try {
      fs.accessSync(dirPath, fs.constants.R_OK | fs.constants.W_OK);
    } catch (err) {
      console.log(err);
    }

    // Get the filename without extension
    const videoFileName = path.parse(videoFilePath).name; 

    // Read the directory contents including any subdirectories within it
    const dirContentPaths = fs.readdirSync(dirPath, {recursive: true});

    // Find directory that have an identical name to the video
    // Return the absolute path of the notes file
    const filePaths = dirContentPaths
    .filter(entry => {
      const parsedEntry = path.parse(entry);
      return parsedEntry.name.includes(videoFileName) && 
        parsedEntry.name.includes('metadata') &&
        parsedEntry.ext.includes('json'); 
    })
    .map(filteredEntry => path.join(dirPath, filteredEntry))
    return filePaths[0]

  } catch (err) {
    console.log(err);
  }


}


/**
 * Search for tracking files for a video in a given directory
 * @param {import('node:fs').PathLike} videoFilePath Path of the opened video
 * @param {import('node:fs').PathLike} dirPathToSearch Optional path of the directory to search for. If no path is given, the default user data directory will be searched.
 * @returns {Object | undefined} Object with tracking file path and identification file path
 * @returns {import('node:fs').PathLike | undefined} Tracking file path
 * @returns {import('node:fs').PathLike | undefined} Identification file path
 */
function handleFindTrackingFile(videoFilePath, dirPathToSearch) {
  
  // Search user data directory by default
  const dirPath = dirPathToSearch ? dirPathToSearch : appDataDir;
  
  try {

    // Check access
    try {
      fs.accessSync(dirPath, fs.constants.R_OK | fs.constants.W_OK);
    } catch (err) {
      console.log(err);
    }

    // Get the filename without extension
    const videoFileName = path.parse(videoFilePath).name; 

    // Read the directory contents including any subdirectories within it
    const dirContentPaths = fs.readdirSync(dirPath, {recursive: true});

    // Find directory that have an identical name to the video
    // Return the absolute path of the tracking file
    const filePaths = dirContentPaths
    .filter(entry => {
      const parsedEntry = path.parse(entry);
      return parsedEntry.name.includes(videoFileName) && 
        (parsedEntry.name.includes('tracking') || parsedEntry.name.includes('identification')) &&
        parsedEntry.ext.includes('txt'); 
    })
    .map(filteredEntry => path.join(dirPath, filteredEntry))

    // Return the identification file path if it exists
    const idFilePath = filePaths.filter(path => path.includes('identification'))[0];

    // Return the tracking file path if it exists
    const trackFilePath = filePaths.filter(path => path.includes('tracking'))[0];
    
    return {
      idFilePath: idFilePath,
      trackFilePath: trackFilePath
    }

  } catch (err) {
    console.log(err);
  }


}

function handleGetFileNameWithoutExtension(filePath) {
  
  if (!filePath) return;
    
  // Get the base name of the file (including extension)
  const baseName = path.basename(filePath);
  
  // Get the extension of the file
  const ext = path.extname(filePath);

  // Return the base name without the extension
  return baseName.slice(0, -ext.length);


}

async function handleOpenSingleVideo() {      
  const { canceled, filePaths } = await dialog.showOpenDialog(BrowserWindow.getFocusedWindow(), {
    title: 'Choose a video',
    filters: [{ name: 'Movies', extensions: ['mkv', 'avi', 'mp4'] }],
    properties: ['openFile'],
    message: "Choose a video"
  })
  
  if (!canceled) {
    return filePaths[0]
  }
}

async function handleOpenSingleFile(fileType) {
  const title  = 'Choose a file';
  let message = 'Choose a file';
  if (fileType) {
    message = `Choose a file for ${fileType}`
  }  

  const { canceled, filePaths } = await dialog.showOpenDialog(BrowserWindow.getFocusedWindow(), {
    title: title,
    filters: [{ name: 'Text', extensions: ['txt', 'csv', 'tsv'] }],
    properties: ['openFile'],
    message: message
  })
  
  if (!canceled) {
    return filePaths[0]
  }
}

async function handleOpenMultipleVideos() {      
  const { canceled, filePaths } = await dialog.showOpenDialog(BrowserWindow.getFocusedWindow(), {
    title: 'Choose video files as secondary views',
    filters: [{ name: 'Movies', extensions: ['mkv', 'avi', 'mp4'] }],
    properties: ['openFile', 'multiSelections'],
    message: "Choose video files as secondary views"
  })
  
  if (!canceled) {
    return filePaths
  }
}




/**
 * Exports the behavior records when user clicks the relevant button
 * @param {Object[]} observations - Observations
 * @param {String} fileName - Video filename without extension
 * @param {Number} videoFPS - Video frame rate
 * @returns {Object} Return object consisting of the file path and canceled status
 * @returns {import('node:fs').PathLike | undefined} Path of the exported file or undefined if the operation has failed
 * @returns {boolean} True if the operation is canceled by the user, False otherwise
 */
async function handleExportEthogram(observations, fileName, videoFPS, username, withMetadata) {
  const result = await dialog.showSaveDialog(BrowserWindow.getFocusedWindow(), {
    title: 'Export Behavior Records',
    defaultPath: `${fileName}_behaviors.csv`,
    filters: [{ name: 'CSV Files', extensions: ['csv'] }]
  });

  // Handle cancelation
  if (result.canceled) {
    return { canceled: true }
  }

  // Check if a valid file path was chosen
  if (!result.canceled && result.filePath) {
    
    // Get the selected file path
    const filePath = result.filePath;

    // Pass the arguments to the writer function
    const confirmedPath = handleWriteEthogramToFile(observations, fileName, videoFPS, username, withMetadata, filePath);
    
    return { filePath: confirmedPath, canceled: false };
  
  }

  // Return nothing if dialog is cancelled or no valid path was selected
  // return;


}


async function handleSaveInteractionTable(tableContent, fileName) {
  const result = await dialog.showSaveDialog(BrowserWindow.getFocusedWindow(), {
    title: 'Export Interaction Table',
    defaultPath: `${fileName}_behaviors.csv`,
    filters: [{ name: 'CSV Files', extensions: ['csv'] }]
  })

  if (!result.canceled) {
    try {
      fs.writeFileSync(result.filePath, tableContent);
      console.log('File written successfully');
      // return tableContent
      return result.filePath
    } catch (err) {
      console.error(err);
    }
  }

}


/**
 * Writes the notes to a file associated with the main video.
 * If no file path is given, a hidden file in the user data directory is chosen by default.
 * @param {String} text - Note content
 * @param {String} fileName - File name of the main video
 * @param {String} username - Username
 * @param {import('node:fs').PathLike} filePath - Optional file path for saving the file other than user data directory
 * @returns {import('node:fs').PathLike} - File path
 */
function handleWriteNotesToFile(text, fileName, username, filePath) {

  // Get the date in ISO format
  const date = new Date().toISOString();

  // Construct the metadata header text
  const headerRow = `# Notes for ${fileName}`;
  const usernameRow = `# Username: ${username ? username : 'anonymous'}`;

  // Determine the file name with extension
  const fileNameWithExt = fileName ? `${fileName}_notes.txt` : 'notes.txt';

  // Determine the full file path for the notes file
  // Write either to the user data directory (invisible to user) or some other directory chosen by the user
  const pathInUserDataDir = path.join(appDataDir, fileNameWithExt);
  const filePathToWrite = filePath ? filePath: pathInUserDataDir;

  // Write to the ethogram file in the user data directory 
  // Directory is hidden from user, used as a backup against accidental deletions 
  let writeStream = fs.createWriteStream(filePathToWrite);

  if (!writeStream) {
    console.log('Failed to find the file path for the notes!');
    return;
  }

  try {

    // Write the metadata
    writeStream.write(`${headerRow}\n`);
    writeStream.write(`${usernameRow}\n`);

    // Write the main text
    writeStream.write(text);

    // Close the file stream
    writeStream.end();

    // Check if writing to stream is finished
    writeStream.on('finish', () => {
      console.log(`Finished writing to ${filePathToWrite}`);

    });

    // Handle error with the stream
    writeStream.on('error', (err) => {
      console.error('Cleaning up:', err);
      writeStream.destroy();
      return;
    })
  
    // Return the file path
    return filePathToWrite;
  
  } catch (err) {
    console.error(err);
    return;

  }

}

/**
 * Writes the metadata linked to the main video to a file in the user data directory
 * @param {JSON} metadata Metadata in JSON string format
 * @param {String} fileName File name of the main video
 * @returns {Object} Metadata object
 */
async function handleSaveMetadata(metadata, fileName) {
  if (!fileName) return;

  // Construct file path in user data dir
  if (!fs.existsSync(appDataDir)) {
    try {
      fs.mkdirSync(appDataDir);
    } catch (err) {
      console.log(err);
      return;
    }
  }

  // Write metadata to file
  const filePath = path.join(appDataDir, `${fileName}_metadata.json`);
      
  try {
    fs.writeFileSync(filePath, metadata);
    console.log(`Metadata written to file ${filePath}:`, metadata)
    return JSON.parse(metadata);
  } catch (err) {
    console.log(`Metadata could not be written to file ${filePath}`, err);
    return;
  }
  

}


/**
 * Exports notes to a file chosen by the user via dialog menu
 * @param {String} text Main text for the notes
 * @param {String} fileName Video name associated with the notes
 * @param {String} username Username 
 * @returns {Object} Return object consisting of the file path and canceled status
 * @returns {import('node:fs').PathLike} Path of the exported file or undefined if the operation has failed
 * @returns {boolean} True if the operation is canceled by the user, False otherwise
 */
async function handleExportNotes(text, fileName, username) {
  
  // Show dialog for export
  const result = await dialog.showSaveDialog(BrowserWindow.getFocusedWindow(), {
    title: 'Export Notes',
    defaultPath: `${fileName}_notes.txt`,
    filters: [{ name: 'Text Files', extensions: ['txt'] }]
  });

  // Handle cancelation
  if (result.canceled) {
    return { canceled: true }
  }

  // Check if a valid file path was chosen
  if (!result.canceled && result.filePath) {
    
    // Get the selected file path
    const filePath = result.filePath;

    // Pass the arguments to the writer function
    const confirmedPath = handleWriteNotesToFile(text, fileName, username, filePath);
    return { filePath: confirmedPath, canceled: false }
  
  }


}




/**
 * Save modified tracking data to a file in user directory without a dialog
 * @param {Map} trackingMap | keys: framenumber, values: Array of 
 * @param {String} fileName | Video file name without extension
 * @returns 
 */
async function handleSaveTrackingEdits(trackingMap, fileName, orderedNamesArr, username, fileDelimiter) {

  // Get the date in ISO format
  const date = new Date().toISOString();
 
  // Set string for individuals with no identified names (Unsure)
  const unsureString = 'Uns';
  
  if (orderedNamesArr) {
    // Convert ordered name string array to lowercase
    const lowerOrderedNamesArr = orderedNamesArr.map(name => name.toLowerCase());
    
    // If Unsure string was not in the input array for ordered names
    if (lowerOrderedNamesArr.indexOf(unsureString.toLowerCase()) < 0) {
      
      // Add Unsure string to name array
      orderedNamesArr.push(unsureString);
    }

  }
  
  // Define header for file metadata
  const trackingHeader = {
    // Get the ordered individual names later within the function for outputting tracking file
    username: username ? username : 'anonymous',
    editDate: date, 
    orderedNames: orderedNamesArr,
    dataColumns: ['trackNumber', 'trackId', 'xCoord', 'yCoord', 'width', 'height', 'confidenceTrack', 'species', 'nameOrder', 'confidenceId']
  }

  // Determine file name of the output
  let fileOutName = fileName ? fileName : 'modified';

  // Determine the delimiter
  let delimiter = fileDelimiter ? fileDelimiter : ', ';

  // Determine output path for modified tracking file
  
  // Save path to user data directory by default
  const pathInUserDataDir = path.join(appDataDir, `${fileOutName}_identification.txt`);

  // Write to the tracking file in the user data directory 
  // Directory is hidden from user, used as a backup against accidental deletions 
  let writeStreamUserData
  if (pathInUserDataDir) {
    writeStreamUserData = fs.createWriteStream(pathInUserDataDir);
  }

  if (!writeStreamUserData) {
    console.log('Failed to find the path for tracking file edits!');
    console.log('Tracking file path in user data directory', pathInUserDataDir);
    return;
  }

  try {
    // Write header for metadata
    for (let [key, value] of Object.entries(trackingHeader)) {

      // Separate array values with a comma and space
      const data = Array.isArray(value) ? value.join(', ') : value;
      
      // Write header to both files
      writeStreamUserData.write(`# ${key}: ${data}\n`);

    }

    // Get the index for Uns string
    const unsureIndex = orderedNamesArr ? orderedNamesArr.indexOf(unsureString) : null; 

    // Write tracking rows to both files
    for (const trackInfoArr of trackingMap.values()) {
      trackInfoArr.forEach(trackInfoObj => {
        
        const rowUserDataArr = Object.values(trackInfoObj);
        const rowExportArr = [...rowUserDataArr];
        // Change formats of rows with no nameOrder and confidenceId
        // Set nameOrder to index of Uns (i.e. Unsure) and confidenceId to 0
        if (!trackInfoObj.hasOwnProperty('nameOrder') || !trackInfoObj.hasOwnProperty('confidenceId')) {
          if (unsureIndex) {
            rowExportArr.push(unsureIndex, 0);
          }
        }

        // Write each row to file stream
        // const row = Object.values(trackInfoObj).join(delimiter);
        writeStreamUserData.write(`${rowUserDataArr.join(delimiter)}\n`);

      });
    }

    writeStreamUserData.on('finish', () => {
      console.log(`Finished writing to ${pathInUserDataDir}`);
    })

    writeStreamUserData.on('error', (err) => {
      console.error('Cleaning up:', err);
      writeStreamUserData.destroy();
    })

    // Close the streams
    writeStreamUserData.end();

    return { 
      pathInUserDataDir: pathInUserDataDir,
    };

  
  } catch (err) {
    console.error(err);
    return;
  }


}

/**
 * 
 * @param {Object} behaviorObj 
 * @param {*} precision 
 * @param {*} videoFPS 
 * @param {*} delimiter 
 * @returns 
 */
function generateEthogramRow(behaviorObj, precision, videoFPS, delimiter) {

  let fileDelimiter = delimiter ? delimiter : csvDelimiter;

  // Get the subject name and action of the behavior
  const subjectName = behaviorObj.subjectName;
  const action = behaviorObj.action;
  
  // Use frames for start and end of an action by default
  const startFrame = parseInt(behaviorObj.startFrame);
  const endFrame = parseInt(behaviorObj.endFrame);

  if (subjectName && action &&
    Number.isSafeInteger(startFrame) && Number.isSafeInteger(endFrame)
  ) {

    const targetName = behaviorObj.targetName ? behaviorObj.targetName : 'NA';
    
    const durationInFrames = (endFrame - startFrame);
  
    const rowArr = [
      subjectName, action, targetName, 
      startFrame, endFrame, durationInFrames
    ];
  
    // If video FPS is given convert frames to seconds
    if (videoFPS) {
      const startSecond = (startFrame / parseFloat(videoFPS)).toFixed(precision);
      const endSecond = (endFrame / parseFloat(videoFPS)).toFixed(precision);
  
      const durationInSeconds = (endSecond - startSecond).toFixed(precision);
  
      rowArr.push(startSecond, endSecond, durationInSeconds);
    
    } else {
      // If no video FPS is given, add "NA" to columns related to time in seconds
      rowArr.push('NA', 'NA', 'NA');
    }

    return rowArr.join(fileDelimiter);
  
  }

  // Return nothing if any expected field in behavior object is missing
  return;

}

/**
 * Imports an Ethogram file in CSV format via OS file dialog
 * Calls another function to read the file and convert each line into an observation in Map type
 * @returns {Map[] | undefined} - Array of observations constructed from each line in the file or undefined if the operation failed
 */
async function handleImportEthogramFile() {
  const title  = 'Select a CSV file for the ethogram';
  const message = 'Select a CSV file for the ethogram';

  const { canceled, filePaths } = await dialog.showOpenDialog(BrowserWindow.getFocusedWindow(), {
    title: title,
    filters: [{ name: 'CSV', extensions: ['csv'] }],
    properties: ['openFile'],
    message: message
  });

  if (canceled) {
    console.log('Dialog closed by the user!');
    return;
  }

  if (filePaths.length === 0) {
    console.log('No valid file was chosen!');
    return;
  }

  // Get the chosen file path
  const filePath = filePaths[0];

  // Get the array of observations 
  try {
    const obsArr = handleReadEthogramFile(filePath);
    return obsArr;
  } catch (err) {
    console.log(err);
    return;
  }


}

/**
 * Gets the last modified time of a file
 * @param {import('node:fs').PathLike} filePath - File path
 * @returns {Date} - Last modified date
 */
async function getLastModifiedTime(filePath) { 

  return new Promise((resolve, reject) => {
    try {
      const stats = fs.statSync(filePath);
      const lastModTime = stats.mtime;
      console.log(`File data last modified: ${lastModTime}`);
      resolve(lastModTime);
    } catch (err) {
      console.log(err);
      // reject(err);
    }

  })


}


/**
 * Reads a notes file in plain text format
 * @param {import('node:fs').PathLike} filePath File path for the notes
 * @returns {String} HTML string for the main file content
 */
async function handleReadNotesFile(filePath) {

  return new Promise((resolve, reject) => {

    // Check if a file path is given
    if (!filePath) {
      const reason = 'No file path was given for the notes!';
      console.log(reason);
      reject(reason);
      return;
    }
  
    // Check if the given path exists
    if (!fs.existsSync(filePath)) {
      const reason = `Given path for the notes does not exist! ${filePath}`;
      console.log(reason);
      reject(reason);
      return;
    }
  
    // Check if the given path is accessible
    try {
      fs.accessSync(filePath, fs.constants.R_OK)
    } catch (err) {
      console.log(err);
      const reason = `Given path for the notes cannot be read! ${filePath}`;
      reject(reason);
      return;
  
    }
  
    const readStream = fs.createReadStream(filePath);
  
    const rl = readline.createInterface({
      input: readStream,
      // output: stdout,
      crlfDelay: Infinity // For reading files with \r\n line delimiter

    });
      
    // Array of lines
    const lineArr = [];

    // Read and process each line
    rl.on('line', (line) => {
      // Skip the metadata rows
      if (!line.startsWith('#')){
        lineArr.push(line);
      }
  
    });

    rl.on('close', () => {
      console.log('File reading finished');

      // Join the separate lines with the "\n" element
      // Return a single string for the textarea element
      resolve(lineArr.join('\n'));
  
    });
  
    rl.on('error', (err) => {
      console.error('Cleaning up:', err);
      readStream.destroy();
      reject(err);

    });

    // Handle read stream errors
    readStream.on('error', (err) => {
      console.log('Failed to open the stream:', err);
      readStream.destroy();
      reject(err);

    });


  });


}

/**
 * Reads a metadata file in JSON format
 * @param {import('node:fs').PathLike} filePath File path for the notes
 * @returns {Object} Metadata object
 */
async function handleReadMetadataFile(filePath) {

  // Check if config file exists
  if (!fs.existsSync(filePath)) {
    console.log('Metadata file could not be found!');
    return;
  }

  try {
    const metadata = JSON.parse(fs.readFileSync(filePath), 'utf8');
    return metadata;
  } catch (err) {
    console.log('Metadata file could not be found!', err);  
  }


}


/**
 * Reads an ethogram file in CSV format
 * @param {import('node:fs').PathLike} filePath File path for the ethogram
 * @return {Map[]} Array of observations 
 */
async function handleReadEthogramFile(filePath) {

  // Skip the metadata and header rows
  // Read each row
    // Generate an observation from each row in Map format
    // Push each observation to the array
  // Return the resulting array


  return new Promise((resolve, reject) => {
    
    // Check if a file path is given
    if (!filePath) {
      const reason = 'No file path was given for the ethogram!';
      console.log(reason);
      reject(reason);
      return;
    }

    // Check if the given path exists
    if (!fs.existsSync(filePath)) {
      const reason = `Given path for the ethogram does not exist! ${filePath}`;
      console.log(reason);
      reject(reason);
      return;
    }

    // Check if the given path is accessible
    try {
      fs.accessSync(filePath, fs.constants.R_OK)
    } catch (err) {
      console.log(err);
      const reason = `Given path for the ethogram cannot be read! ${filePath}`;
      reject(reason);
      return;

    }

    const readStream = fs.createReadStream(filePath);

    const rl = readline.createInterface({
      input: readStream,
      // output: stdout,
      crlfDelay: Infinity // For reading files with \r\n line delimiter
    });
      
    // Keep track of observations constructed from each row
    let obsArr = [];
  
    // Initialize the line number counter
    let lineNumber = 0;
    
    // Initialize the column indices
    let subjectIdx, actionIdx, targetIdx, startFrameIdx, endFrameIdx;
    
    // Read and process each line
    rl.on('line', (line) => {

      // Skip the metadata rows
      if (!line.startsWith('#')) {
        
        // Split the line into columns
        const lineArr = line.split((/\s*,\s*/));

        console.log(lineArr)
        
        // Get the headers from the first row
        if (lineNumber === 0) {
          
          // Get the indices of the relevant columns (e.g. find the column containing the string "subject")
          const subjectRegex = /subject/i;
          const actionRegex = /action/i;
          const targetRegex = /target/i;
          const startFrameRegex = /.*start.*frame.*/i;
          const endFrameRegex = /.*end.*frame.*/i;

          // findIndex returns -1 if an element could not be found in the array
          subjectIdx = lineArr.findIndex((column) => subjectRegex.test(column));
          actionIdx = lineArr.findIndex((column) => actionRegex.test(column));
          targetIdx = lineArr.findIndex((column) => targetRegex.test(column));
          startFrameIdx = lineArr.findIndex((column) => startFrameRegex.test(column));
          endFrameIdx = lineArr.findIndex((column) => endFrameRegex.test(column));
          
          // Check for index -1 (indicates element not being found)
          const indexNames = ['Subject', 'Action', 'Target', 'Start Frame', 'End Frame']
          const indices = [subjectIdx, actionIdx, targetIdx, startFrameIdx, endFrameIdx];
          console.log(indices);
          const missingIdx = indices.indexOf(-1);
          if (missingIdx >= 0) {
            const missingName = indexNames.at(missingIdx);
            console.log(`Column for ${missingName} could not be found!`);
            reject(`Column for ${missingName} could not be found!`);
            return;
          }          

        } else {

          // Get the observation properties via their indices in each line array 
          // They should be found by searching the first row in the step above
          const indices = [subjectIdx, actionIdx, targetIdx, startFrameIdx, endFrameIdx];
          if (!indices.every(index => index >= 0)) {
            console.log(`One or more of the required columns are missing!`);
            reject(`One or more of the required columns are missing!`);
            return;
          }

          // Get the observation properties given at specified indices
          const subjectName = lineArr.at(subjectIdx);
          const action = lineArr.at(actionIdx);
          const targetName = lineArr.at(targetIdx);
          const startFrame = parseInt(lineArr.at(startFrameIdx));
          const endFrame = parseInt(lineArr.at(endFrameIdx));

          // Check if line has an element at the specified index
          if (!subjectName || !action || !targetName || !Number.isSafeInteger(startFrame) ||  !Number.isSafeInteger(endFrame)) {
            console.log(`Skipping line ${lineNumber} because at least one of the required columns is missing!`);
            return;
          }

          // Construct a new observation
          const newObs = new Map([
            ['index', lineNumber],
            ['subjectName', subjectName],
            ['subjectId', null],
            ['subjectSpecies', null],
            ['action', action],
            ['targetName', targetName],
            ['targetId', null],
            ['targetSpecies', null],
            ['startFrame', startFrame],
            ['endFrame', endFrame],
          ]);

          obsArr.push(newObs);

        }

        // Increment line number count
        lineNumber++;


      }
  
    });
    
    rl.on('close', () => {
      console.log('File reading finished');
      // readStream.destroy();
      resolve(obsArr);
  
    });
  
    rl.on('error', (err) => {
      console.error('Cleaning up:', err);
      readStream.destroy();
      reject(err);
    });

    // Handle read stream errors
    readStream.on('error', (err) => {
      console.log('Failed to open the stream:', err);
      readStream.destroy();
      reject(err);
    });
  
  
  });
 


}



/**
 * Writes the record of behaviors to a file. 
 * If no file path is given, a hidden file in the user data directory is chosen by default.
 * @param {Array} observations - Map of Observations (key: observationIndex, value: Observation instance)
 * @param {String} fileName - File name of the main video without the extension
 * @param {Number} videoFPS - Frames per second of the main video
 * @param {String} username - Username
 * @param {String} filePath - Optional file path for saving the file other than user data directory
 * @param {Boolean} withMetadata - If true, write metadata header
 * @returns {import('node:fs').PathLike | undefined} - File path where the ethogram was written successfully or undefined if the operation failed
 */
function handleWriteEthogramToFile(observations, fileName, videoFPS, username, withMetadata, filePath) {

  // Get the date in ISO format
  const date = new Date().toISOString();

  // Define header for file metadata
  const metadata = {
    // Get the ordered individual names later within the function for outputting tracking file
    username: username ? username : 'anonymous',
    editDate: date,
    videoName: fileName,
    videoFPS: videoFPS 
  }

  // Determine file name of the output
  const fileOutName = fileName ? fileName : 'unknown';

  // Determine the delimiter
  const delimiter = csvDelimiter;

  // Add the extension to the file name
  const fileNameWithExt = `${fileOutName}_behaviors.csv`;
  
  // Determine the full file path for the ethogram file
  // Write either to the user data directory (invisible to user) or some other directory chosen by the user
  const pathInUserDataDir = path.join(appDataDir, fileNameWithExt);
  const filePathToWrite = filePath ? filePath: pathInUserDataDir;

  // Write to the ethogram file in the user data directory 
  // Directory is hidden from user, used as a backup against accidental deletions 
  let writeStream = fs.createWriteStream(filePathToWrite);

  if (!writeStream) {
    console.log('Failed to find the path for the ethogram file!');
    return;
  }

  try {
    
    // Write header for metadata if preferred
    if (withMetadata) {
      for (let [key, value] of Object.entries(metadata)) {
  
        // Separate array values with a comma and space
        const data = Array.isArray(value) ? value.join(delimiter) : value;
        
        // Write header to the file
        writeStream.write(`# ${key}: ${data}\n`);
  
      }

    }

    // Write column headers
    const colHeaderRow = behaviorColHeaderRow.join(delimiter);
    writeStream.write(`${colHeaderRow}\n`);

    // Write behavior rows to the file
    observations.forEach(observation => {

      // Produce a row in CSV format from the behavior object
      const row = generateEthogramRow(observation, precisionForSeconds, videoFPS, delimiter);
      if (row) {
        // Write rows to files
        writeStream.write(`${row}\n`);
      }

    });

    // Check if writing to stream is finished
    writeStream.on('finish', () => {
      console.log(`Finished writing to ${filePathToWrite}`);

    });

    // Handle error with the stream
    writeStream.on('error', (err) => {
      console.error('Cleaning up:', err);
      writeStream.destroy();
      return;
    })

    // Close the streams
    writeStream.end();

    // Return the file path 
    return filePathToWrite;

  } catch (err) {
    console.error(err);
    return;
  }

}

/**
 * Exports modified tracking data to a file
 * @param {Map} trackingMap | keys: framenumber, values: Array of 
 * @param {String} fileName | Video file name without extension
 * @returns 
 */
async function handleOutputTrackingFile(trackingMap, fileName, individualNamesArr, fileDelimiter) {

  // Get the date in ISO format
  const date = new Date().toISOString();

  // Get the ordered array for indvidual names
  const orderedNamesArr = individualNamesArr ? individualNamesArr : []

  // Define header for file metadata
  const trackingHeader = {
    // Get the ordered individual names later within the function for outputting tracking file
    username: 'anonymous',
    editDate: date, 
    orderedNames: orderedNamesArr,
    dataColumns: ['trackNumber', 'trackId', 'xCoord', 'yCoord', 'width', 'height', 'confidenceTrack', 'species', 'nameOrder', 'confidenceId']
  }

  // Determine file name of the output
  let fileNameOutput = fileName ? fileName : 'modified';

  // Determine the delimiter
  let delimiter = fileDelimiter ? fileDelimiter : ', ';

  const result = await dialog.showSaveDialog(BrowserWindow.getFocusedWindow(), {
    title: 'Export Modified Tracking File',
    defaultPath: `${fileNameOutput}_tracking.txt`,
    filters: [{ name: 'Text Files', extensions: ['txt'] }]
  })


  if (!result.canceled) {
    const writeStream = fs.createWriteStream(result.filePath);
    try {

      // Write header for metadata
      for (let [key, value] of Object.entries(trackingHeader)) {

        // Separate array values with a comma and space
        const data = Array.isArray(value) ? value.join(', ') : value
        writeStream.write(`# ${key}: ${data}\n`);
      }

      for (const trackInfoArr of trackingMap.values()) {
        trackInfoArr.forEach(trackInfoObj => {
          // console.log(trackInfo);
          // if (trackInfoObj.trackNumber) {
            // fileContentArr.push(Object.values(trackInfoObj).join(fileDelimiter));
            const row = Object.values(trackInfoObj).join(delimiter);
            writeStream.write(`${row}\n`);
          // }
        });
      }
      // const fileContent = fileContentArr.join('\n');
      // fs.writeFileSync(result.filePath, fileContent);
      writeStream.end();
      console.log('File written successfully');
      return result.filePath;
    } catch (err) {
      console.error(err);
      return;
    }
  }
}

async function handleOutputTrackingTable(tableContent, fileName) {
  const result = await dialog.showSaveDialog(BrowserWindow.getFocusedWindow(), {
    title: 'Export Tracking Modifications',
    defaultPath: `${fileName}_tracking_log.txt`,
    filters: [{ name: 'Text Files', extensions: ['txt'] }]
  })

  if (!result.canceled) {
    try {
      fs.writeFileSync(result.filePath, tableContent);
      console.log('File written successfully');
      // return tableContent
      return result.filePath;
    } catch (err) {
      console.error(err);
      return;
    }
  }


}

/**
 * 
 * @param {Object} settings | Settings to be saved to config file
 */
async function handleSaveToConfig(inputData) {
  const configFilePath = path.join(appDataDir, configFileName);

  let configData = inputData;

  try {
    // Parse config file if exists
    configData = JSON.parse(fs.readFileSync(configFilePath, 'utf8'));
    for (const [key, value] of Object.entries(inputData)) {
      configData[key] = value;
    }
  } catch (err) {
    console.log(err);
  }

  try {
    fs.writeFileSync(configFilePath, JSON.stringify(configData));
    return {configData: configData, success: true}
  } catch (err) {
    console.log(err)
  }

}



/**
 * 
 * @param {Array} inputData | Keys whose values to be removed from the config file
 * @returns 
 */
async function handleRemoveFromConfig(keys) {
  const configFilePath = path.join(appDataDir, configFileName);

  // let configData = inputData;

  let configData;
  
  try {
    // Parse config file if exists
    configData = JSON.parse(fs.readFileSync(configFilePath, 'utf8'));
    
    if (Array.isArray(keys) && keys.length > 0) {
      // Remove user defined keys from config data
      keys.forEach(key => {
        if (key in configData) {
          delete configData[key]
        }
      })
    } else if (keys === 'all') {
      // Remove all keys from config data
      // Object.keys(configData).forEach(key => delete configData[key]);
      configData = {};
    } 
  } catch (err) {
    console.log(err);
    return
  }

  try {
    fs.writeFileSync(configFilePath, JSON.stringify(configData));
    return {configData: configData, success: true}
  } catch (err) {
    console.log(err)
  }

  
}

function handleGetFromConfig() {
  if (!fs.existsSync(appDataDir)) {
    try {
      fs.mkdirSync(appDataDir)
    } catch (err) {
      console.err(err);
    }
  }
  const configFilePath = path.join(appDataDir, configFileName);

  // Check if config file exists
  if (!fs.existsSync(configFilePath)) {
    console.log('Config file could not be found!');
    console.log('Attempting to create a new config file...');
    try {
      // Create empty fie
      fs.writeFileSync(configFilePath, JSON.stringify({}));
      console.log('Config file created', configFilePath);

    } catch (err) {
      console.log(err);
      return;
    }
  }

  try {
    const data = JSON.parse(fs.readFileSync(configFilePath, 'utf8'))
    return data;
  } catch (err) {
    console.log('Config file could not be found!', err);  
  }
  
}

/**
 * 
 * @param {Object} data - Expects canvasData, videoName, fileExtension, frameNumber
 */
async function handleSaveSnapshot(data) {
  let fileExtension = 'png';
  if (data.fileExtension) {
    fileExtension = data.fileExtension;
  }

  let fileName = 'snapshot';
  if (data.videoName) {
    fileName = data.videoName;
  }

  let frameNumber = 0;
  if (data.frameNumber) {
    frameNumber = data.frameNumber;
  }

  const response = await dialog.showSaveDialog(BrowserWindow.getFocusedWindow(), {
    title: 'Save Snapshot',
    message: 'Save Snapshot',
    defaultPath: `${fileName}_${frameNumber}.${fileExtension}`,
    filters: [{ name: 'Image Files', extensions: [fileExtension] }]
  })

  
  if (!response.canceled && response.filePath) {
    try {
      const base64Data = data.canvasData.replace(/^data:image\/png;base64,/, "");
      const buffer = Buffer.from(base64Data, 'base64');
      fs.writeFileSync(response.filePath, buffer, {encoding: 'base64'});
      return true;
    } catch (err) {
      console.log(err);
    }

  }
  
}


/**
 * Exports all user-edited files by copying them from user data directory to the chosen export directory.
 * If no export directory path is given, searches the config file for previously saved export directory.
 * @param {import('node:fs').PathLike | undefined} dirPath Optional export directory
 * @returns {Object} 
 */
function handleExportAll(dirPath) {
  
  const configData = handleGetFromConfig();
  
  // Check if the main video path exists
  if (!configData.mainVideoPath) return;
    
  // Get the main video path
  const mainVideoPath = configData.mainVideoPath;

  const mainVideoName = handleGetFileNameWithoutExtension(mainVideoPath)
    
  // Sub directory path to export all files for current experiment
  let experimentDirPath;
  
  // Create a directory for the current main video by appending video name to the export directory
  // Either under export directory saved to config or under input directory of this function
  if (dirPath) {
    experimentDirPath = path.join(dirPath, mainVideoName);

  } else {

    if (!configData.exportDirPath) {
      console.error('No export directory path could be found in the config file!')
      return;
    }

    if (!fs.existsSync(configData.exportDirPath)) {
      console.error('Selected export directory path does not exist!');
      return;
      
    }

    experimentDirPath = path.join(configData.exportDirPath, mainVideoName);

  }
  
  // Indicate success/failure for all file exports
  const response = {
    outDirPath: experimentDirPath,
    behaviors: {
      found: false, // True if file was found in user data directory
      exported: false, // True if file was successfully exported
    },
    tracking: {
      found: false,
      exported: false,
    },
    identification: {
      found: false,
      exported: false,
    },
    notes: {
      found: false,
      exported: false,
    },
    metadata: {
      found: false,
      exported: false,
    },
  };

  try {
    if (!fs.existsSync(experimentDirPath)) {
      
      // Create the subdirectory
      fs.mkdirSync(experimentDirPath);

    }
      
    // Find the ethogram file
    const ethogramPathInUserData = handleFindEthogramFile(mainVideoPath);
    if (ethogramPathInUserData) {

      // Mark file search result
      response.behaviors.found = true;
      
      try {

        // Copy ethogram file to the output path
        const outFilePath = path.join(experimentDirPath, path.basename(ethogramPathInUserData));
        fs.copyFileSync(ethogramPathInUserData, outFilePath);

        // Mark export as success
        response.behaviors.exported = true;
        console.log(`Ethogram file was exported to ${outFilePath}`)
  
      } catch (err) {
        console.error('Ethogram file could not be exported!', err);

      }
  
    }

    // Find the tracking file
    const trackingSearchResponse = handleFindTrackingFile(mainVideoPath);
    if (trackingSearchResponse && trackingSearchResponse.trackFilePath) {
      
      // Mark file search result
      response.tracking.found = true;

      try {

        // Copy tracking file to the output path       
        const trackingPathInUserData = trackingSearchResponse.trackFilePath;        
        const outFilePath = path.join(experimentDirPath, path.basename(trackingPathInUserData));

        fs.copyFileSync(trackingPathInUserData, outFilePath);

        // Mark export as success
        response.tracking.exported = true;

        console.log(`Tracking file was exported to ${outFilePath}`)
  
      } catch (err) {
        console.error('Tracking file could not be exported!', err);

      }
  
    }

    // Find the identification file
    if (trackingSearchResponse && trackingSearchResponse.idFilePath) {
      
      // Mark the file search result
      response.identification.found = true;
      
      try {

        // Copy identification file to the output path
        const idPathInUserData = trackingSearchResponse.idFilePath;        
        const outFilePath = path.join(experimentDirPath, path.basename(idPathInUserData));
        fs.copyFileSync(idPathInUserData, outFilePath);

        // Mark export as success
        response.identification.exported = true;

        console.log(`Identification file was exported to ${outFilePath}`)
  
      } catch (err) {
        console.error('Identification file could not be exported!', err);

      }
  
    }

    // Find the notes file
    const notesPathInUserData = handleFindNotesFile(mainVideoPath);
    if (notesPathInUserData) {

      // Mark the file search result
      response.notes.found = true;

      try {

        // Copy notes file to the output path     
        const outFilePath = path.join(experimentDirPath, path.basename(notesPathInUserData));
        fs.copyFileSync(notesPathInUserData, outFilePath);

        // Mark export as success
        response.notes.exported = true;

        console.log(`Notes file was exported to ${outFilePath}`)
  
      } catch (err) {
        console.error('Notes file could not be exported!', err);

      }
  
    }

    
    const metadataPathInUserData = handleFindMetadataFile(mainVideoPath);
    if (metadataPathInUserData) {

      // Mark the file search result
      response.metadata.found = true;

      try {

        // Copy notes file to the output path     
        const outFilePath = path.join(experimentDirPath, path.basename(metadataPathInUserData));
        fs.copyFileSync(metadataPathInUserData, outFilePath);

        // Mark export as success
        response.metadata.exported = true;

        console.log(`Metadata file was exported to ${outFilePath}`)
  
      } catch (err) {
        console.error('Metadata file could not be exported!', err);

      }
  
    }
    
    
    
    // Return success/failure status for all exports
    return response;


  } catch (err) {
    console.error(err);

  }
    

}


let mainWindow;
let quitting = false;

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1920,
    height: 1080,
    // remove the default titlebar
    titleBarStyle: 'hidden',
    // expose window controlls in Windows/Linux
    ...(process.platform !== 'darwin' ? { titleBarOverlay: {
      color: 'rgba(33,37,41,1)', // Bootstrap bg-dark color code
      symbolColor: 'white',
      // height: 100
    } } : {}),
    trafficLightPosition: {x: 10 , y: 10 },
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
      nodeIntegrationInWorker: true
    }
  })

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  mainWindow.on('close', (e) => {
    console.log(quitting);
    if (quitting) return;
    e.preventDefault();
    mainWindow.webContents.send('before-quit');
  })


  // Open the DevTools.
  // mainWindow.webContents.openDevTools()
}

function handleRelaunch() {
  if (mainWindow) {
    app.relaunch();
    app.quit();
  }
}


/**
 * Gets the current app version
 * @returns App version
 */
function handleGetVersion() {
  return app.getVersion();
}


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  ipcMain.handle('getVersion', (e) => {
    const response = handleGetVersion();
    return response
  })
  ipcMain.handle('dialog:openSingleVideo', handleOpenSingleVideo),
  ipcMain.handle('dialog:openMultipleVideos', handleOpenMultipleVideos),
  ipcMain.handle('getVideosFromDir', async (e, dirPath) => {
    const videoPaths = await handleGetVideosFromDir(dirPath)
    return videoPaths;
  })
  ipcMain.handle('findTrackingFile', (e, videoFilePath, dirPath) => {
    const trackingFile = handleFindTrackingFile(videoFilePath, dirPath)
    return trackingFile;
  })
  ipcMain.handle('readTrackingFile', (e, trackingFilePath) => {
    const trackingMap = handleReadTrackingFile(trackingFilePath)
    return trackingMap;
  })
  ipcMain.handle('readInteractionFile', async (e, filePath) => {
    const response = await handleReadInteractionFile(filePath)
    return response;
  })
  ipcMain.handle('readIdentificationFile', async (e, filePath) => {
    const response = await handleReadIdentificationFile(filePath)
    return response;
  })
  ipcMain.handle('readNameFile', (e, filePath) => {
    const response = handleReadNameFile(filePath)
    return response;
  })
  ipcMain.handle('saveTrackingEdits', async (e, trackingMap, fileName, orderedNamesArr, username) => {
    const response = await handleSaveTrackingEdits(trackingMap, fileName, orderedNamesArr, username);
    return response;
  })
  ipcMain.handle('findEthogramFile', (e, videoFilePath, dirPath) => {
    const response = handleFindEthogramFile(videoFilePath, dirPath);
    return response;
  })
  ipcMain.handle('writeEthogramToFile', (e, observations, fileName, videoFPS, username, withMetadata) => {
    const response = handleWriteEthogramToFile(observations, fileName, videoFPS, username, withMetadata);
    return response;
  })
  ipcMain.handle('readEthogramFile', (e, filePath) => {
    const response = handleReadEthogramFile(filePath);
    return response;
  })

  ipcMain.handle('dialog:exportEthogram', async(e, observations, fileName, videoFPS, username, withMetadata) => {
    const response = await handleExportEthogram(observations, fileName, videoFPS, username, withMetadata);
    console.log(response);
    return response;
  })
  ipcMain.handle('dialog:outputTrackingFile', async (e, trackingMap, fileName, individualNamesArr) => {
    const filePath = await handleOutputTrackingFile(trackingMap, fileName, individualNamesArr)
    return filePath;
  })
  ipcMain.handle('dialog:saveInteractionTable', async (e, tableContent, fileName) => {
    const filePath = await handleSaveInteractionTable(tableContent, fileName);
    return filePath;
  })
  ipcMain.handle('dialog:outputTrackingTable', async (e, tableContent, fileName) => {
    const filePath = await handleOutputTrackingTable(tableContent, fileName);
    return filePath;
  })
  ipcMain.handle('dialog:openSingleFile', async (e, fileType) => {
    const filePath = await handleOpenSingleFile(fileType);
    return filePath;
  })
  ipcMain.handle('getFileNameWithoutExtension', (e, fileType) => {
    const filePath = handleGetFileNameWithoutExtension(fileType);
    return filePath;
  })
  ipcMain.handle('dialog:openDirectory', async (e) => {
    const directoryPath = await handleOpenDirectory();
    return directoryPath;
  })
  // ipcMain.handle('dialog:getExperimentDirPath', handleGetExperimentDirPath)
  // ipcMain.handle('dialog:openExperimentDir', handleOpenExperimentDir)
  ipcMain.handle('resetSettings', async (e) => {
    const configFilePath = await handleResetSettings();
    return configFilePath;
  })
  ipcMain.handle('saveToConfig', async (e, settingsObj) => {
    const response = await handleSaveToConfig(settingsObj);
    return response;
  })
  ipcMain.handle('getFromConfig', (e) => {
    const response = handleGetFromConfig();
    return response;
  })
  ipcMain.handle('removeFromConfig', async (e, configKeys) => {
    const response = await handleRemoveFromConfig(configKeys);
    return response;
  })
  ipcMain.handle('dialog:saveSnapshot', async (e, imageData) => {
    const response = await handleSaveSnapshot(imageData);
    return response;
  })
  ipcMain.handle('relaunch', (e) => {
    handleRelaunch();
  })
  ipcMain.handle('findNotesFile', (e, videoFilePath, dirPath) => {
    const response = handleFindNotesFile(videoFilePath, dirPath);
    return response;
  })
  ipcMain.handle('readNotesFile', (e, filePath) => {
    const response = handleReadNotesFile(filePath);
    return response;
  })
  ipcMain.handle('writeNotesToFile', (e, text, fileName, username) => {
    const response = handleWriteNotesToFile(text, fileName, username);
    return response;
  })
  ipcMain.handle('dialog:exportNotes', (e, text, fileName, username) => {
    const response = handleExportNotes(text, fileName, username);
    return response;
  })
  ipcMain.handle('saveTrackingHTML', async (e, tableHTML, videoName) => {
    const response = await handleSaveTrackingHTML(tableHTML, videoName);
    return response;
  })
  ipcMain.handle('dialog:importEthogramFile', () => {
    const response = handleImportEthogramFile();
    return response;
  })
  ipcMain.handle('getLastModifiedTime', (e, filePath) => {
    const response = getLastModifiedTime(filePath);
    return response;
  })
  ipcMain.handle('copyToUserDataDir', (e, filePath, fileName) => {
    const response = handleCopyToUserDataDir(filePath, fileName);
    return response;
  })
  ipcMain.handle('dialog:exportAll', (e, dirPath) => {
    const response = handleExportAll(dirPath);
    return response;
  })
  ipcMain.handle('findMetadataFile', (e, videoFilePath, dirPath) => {
    const response = handleFindMetadataFile(videoFilePath, dirPath);
    return response;
  })
  ipcMain.handle('readMetadataFile', (e, filePath) => {
    const response = handleReadMetadataFile(filePath);
    return response;
  })
  ipcMain.handle('saveMetadata', (e, metadata, fileName) => {
    const response = handleSaveMetadata(metadata, fileName);
    return response;
  })
  ipcMain.handle('clear-app-data', (e) => {
  const response = handleClearAppData();
    return response;
  })
  ipcMain.on('response-before-quit', (e) => {
    quitting = true;
    app.quit();
    mainWindow.close();
  })
  
  createWindow();

  app.on('activate', function() {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })


})

// app.on('before-quit', function (e) {
//   if (quitting) return;
//   e.preventDefault();
//   if (mainWindow) {
//     mainWindow.webContents.send('before-quit');
//   }
// })

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
// app.on('window-all-closed', function() {
//   if (process.platform !== 'darwin') app.quit()
// })




// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
