/**
 * Helper functions
 */

// const fs = require('node:fs');
// const path = require('node:path');
// const bootstrap = require('bootstrap');

import { Player, Observation, Hotkey } from './components.js';



/**
 * Gets the species name for a given species ID
 * @param {Number | String} speciesId - Species IDs for boxes, lemurs, etc.
 * @returns {String} - String corresponding to species ID
 */
function getSpeciesName(speciesId) {

  // TODO: Make the species code modular or user-specified!
  
  const speciesCodes = {
    0: 'lemur',
    1: 'box'
  };

  return speciesCodes[parseInt(speciesId)];

}

// Offset in pixels for drawing highlight rectangles on canvas around selected tracking boxes
// const offsetHighlight = 5; 

/**
 * 
 * @param {Array} objects
 * @param {*} field
 * @returns - Unique values in specified field
 */
function getUniqueFieldValues(objects, field) {
  const uniqueValues = new Set();
  
  objects.forEach(object => {
      uniqueValues.add(object[field]);
  });
  
  return Array.from(uniqueValues);
}





/**
 * 
 * @param {String} message - alert message to be shown
 * @param {String} type - success or error, styles the alert accordingly
 * @param {String} title - title of the alert
 */
function showAlertToast(message, type, title) {
  const toastEl = document.getElementById('alert-toast');
  const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toastEl);
  const toastBody = toastEl.querySelector('.toast-body');
  const toastMessage = toastEl.querySelector('.toast-message');
  const toastIcon = toastEl.querySelector('.toast-icon');
  const toastHeader = toastEl.querySelector('.toast-header');
  const alertTitle = toastEl.querySelector('.alert-title');
  const closeBtn = toastEl.querySelector('#no-header-toast-close-btn');


  if (type === 'success') {
    toastIcon.textContent = 'check_circle';
    toastIcon.classList.remove('text-danger', 'text-warning');
    toastIcon.classList.add('text-success');
  }
  else if (type === 'error') {
    toastIcon.textContent = 'error';
    toastIcon.classList.remove('text-success', 'text-warning');
    toastIcon.classList.add('text-danger');
  } else {
    toastIcon.textContent = 'info';
    toastIcon.classList.remove('text-danger', 'text-success');
    toastIcon.classList.add('text-warning');
  }

  if (toastBody) {
    toastBody.insertBefore(toastIcon, toastMessage);
  }

  // Show the close button in the simplied toast by default
  if (closeBtn) closeBtn.classList.remove('d-none');
  
  // Hide the header by default
  if (toastHeader) {
    toastHeader.classList.add('d-none');
    if (title && alertTitle) {
      // Move the icon to the header
      const firstChild = toastHeader.firstChild;
      toastHeader.insertBefore(toastIcon, firstChild);
      
      // Show the title and header element
      alertTitle.textContent = title;
      toastHeader.classList.remove('d-none');
      
      // Hide close button in the simplified toast to prevent duplication
      if (closeBtn) closeBtn.classList.add('d-none');
    }
  }


  toastMessage.innerHTML = message;
  toastBootstrap.show();
  
}

/**
 * 
 * @param {String} title - Title of the modal
 * @param {String} confirmBtnText - Text for the confirmation button
 * @param {String[]} messages - Alert messages
 */
function showAlertModal(title, messages, confirmBtnText, hideCancelBtn, cancelBtnText) {
  const modal = document.getElementById('alert-modal');
  const modalBootstrap = bootstrap.Modal.getOrCreateInstance(modal);
  const modalTitle = modal.querySelector('.modal-title'); 
  const modalBody = modal.querySelector('.modal-body');
  const confirmBtn = modal.querySelector('#alert-confirm-btn');
  const cancelBtn = modal.querySelector('#alert-cancel-btn');

  // Show cancel button by default
  cancelBtn.classList.remove('d-none');

  // Hide cancel button if necessary
  if (hideCancelBtn) {
    cancelBtn.classList.add('d-none');
  }

  // Default text for confirm button
  confirmBtn.textContent = confirmBtnText ? confirmBtnText : 'Continue';

  // Default text for cancel button
  cancelBtn.textContent = cancelBtnText ? cancelBtnText : 'Cancel';

  // Add the title
  modalTitle.textContent = title;

  // Reset modal's content
  modalBody.textContent = '';

  // Add a paragraph to modal body for each argument for message
  messages.forEach(message => {
    const paragraph = document.createElement('p');
    paragraph.textContent = message;
    modalBody.append(paragraph);

  });

  modalBootstrap.show();



}


/**
 * Hide the alert modal
 */
function hideAlertModal() {
  const modal = document.getElementById('alert-modal');
  const modalBootstrap = bootstrap.Modal.getOrCreateInstance(modal);
  modalBootstrap.hide();
}


/**
 * 
 * @param {*} videoDirPath 
 * @returns - Video file names without the extension
 */
function getVideoFilePaths(videoDirPath) {
  const files = fs.readdirSync(videoDirPath, (err, files) => {
    if (err) {
      console.error('Error reading directory:', err);
      return;
    }
  });

  // Filter files with the .mp4 extension (more to be added)
  const videoFiles = files.filter(file => path.extname(file).toLowerCase() === '.mp4');
  return videoFiles
}


/**
 * 
 * @param {*} minutesString - MM:SS
 * @returns - seconds in total
 */
function formatMinutes(minutesString) {
  const splitStrArray = minutesString.split(':');
  if (splitStrArray.length === 2) {
    const minutes = Number(splitStrArray[0]);
    const seconds = Number(splitStrArray[1]);

    return minutes * 60 + seconds; // return total time

  }

}

function minutesToFrames(minutesString, frameRate) {
  const splitStrArray = minutesString.split(':');
  if (splitStrArray.length === 2) {
    const minutes = Number(splitStrArray[0]);
    const seconds = Number(splitStrArray[1]);
    const totalSeconds = minutes * 60 + seconds;
    // Convert seconds to frame numbers
    return secondsToFrames(totalSeconds, frameRate)
  }

}

function secondsToFrames(seconds, frameRate) {
  if (!frameRate) {
    frameRate = Player.getMainPlayer().getFrameRate();
  }
  const frames = Math.round(parseFloat(seconds) * parseFloat(frameRate));
  return frames;
}

function framesToSeconds(frames, frameRate) {
  if (!frames) return;

  if (!Number.isSafeInteger(parseInt(frames))) return;

  if (!frameRate) {
    frameRate = Player.getMainPlayer().getFrameRate();
  }
  
  const seconds = parseInt(frames) / parseFloat(frameRate);
  return seconds;
  

}

/**
 * @param {*} secondsString
 * @returns - MM:SS
 */
function formatSeconds(secondsString) {
  const seconds = parseFloat(secondsString);
  let minutes = Math.floor(seconds / 60);
  let remainingSeconds = Math.floor(seconds % 60);

  // Add leading zero if needed
  minutes = (minutes < 10 ? '0' : '') + minutes;
  remainingSeconds = (remainingSeconds < 10 ? '0' : '') + remainingSeconds;

  // Return the formatted time
  return minutes + ':' + remainingSeconds;
}


function getFrameFromVideo(videoElement) {
  const canvas = document.createElement('canvas');
  canvas.width = videoElement.videoWidth;
  canvas.height = videoElement.videoHeight;

  const context = canvas.getContext('2d');
  context.drawImage(videoElement, 0, 0, canvas.width, canvas.height);

  return canvas.toDataURL();
}

async function createVideoDivs(domElRow, filePaths) {
  const numberOfVideos = filePaths.length;
  const columnClass = 'col-4';

  
  for (let i = 0; i < numberOfVideos; i++) {
    const fileName = await getFileNameWithoutExtension(filePaths[i])
    const videoColDiv = [
      `<div class="${columnClass} border rounded p-2">`,
        '<div class="d-flex justify-content-between">',
          `<small>${fileName}</small>`,
          '<btn class="btn btn-sm btn-player-frame">Main</btn>',
        '</div>',
        '<div class="position-relative">',
          `<video id="video-${i}" class="video-selection" width="320" height="180" src="${filePaths[i]}" preload="metadata">`,
            'Your browser does not support the video tag.',
          '</video>',         
          '<div class="video-spinner spinner-grow text-primary position-absolute start-50 bottom-50 d-none" role="status">',
            '<span class="visually-hidden">Loading...</span>',
          '</div>',
        '</div>',
      '</div>'
    ].join('')


    // Add this div to the secondary video row
    domElRow.innerHTML += videoColDiv;

  }

  return domElRow.querySelectorAll('video');


}

/**
 * Loads the secondary videos 
 * @param {String[]} videoSrcArr - Array of secondary video source paths
 */
async function loadSecondaryVideos(videoSrcArr) {

  if (!videoSrcArr) return;

  if (!Array.isArray(videoSrcArr) || videoSrcArr.length < 1) return;

  const openSecondaryVideosBtn = document.getElementById('open-secondary-videos-btn');
  if (!openSecondaryVideosBtn) return;

  const secondaryVideoRow = document.querySelector('#secondary-video-row');
  if (!secondaryVideoRow) return;

  // Filter video sources that are already opened
  let filteredArr = [];

  for (const videoSrc of videoSrcArr) {
    // Get the video file name to construct DOM IDs
    const videoFileName = await getFileNameWithoutExtension(videoSrc);
    if (!videoFileName) return;
    const isDuplicate = Player.getSecondaryPlayers().filter(player => player.getName() === videoFileName).length > 0;
    if (!isDuplicate) {
      filteredArr.push({src: videoSrc, name: videoFileName});
    }

  }


  if (filteredArr.length < 1) {
    showAlertToast('Selected videos already opened!', 'info');
    return;
  } 

  // Set the number of columns depending on the number of players
  const rowColsNumber = filteredArr.length === 1 ? 'row-cols-1' : 'row-cols-2';
  secondaryVideoRow.classList.add(rowColsNumber);
  
  // Get the button for changing column number
  const secondaryVidColSizeBtn = document.getElementById('secondary-video-colsize-btn');
  if (secondaryVidColSizeBtn) {
    const buttonIcon = secondaryVidColSizeBtn.querySelector('span');
    const tooltip = bootstrap.Tooltip.getOrCreateInstance(secondaryVidColSizeBtn);
    
    if (rowColsNumber === 'row-cols-1') {
      buttonIcon.textContent = 'grid_view'; // Change the icon to list view
      tooltip.setContent({ '.tooltip-inner': 'Grid view' }); // Change the tooltip
      buttonIcon.dataset.viewType = 'grid'; // Update dataset

    } else {
      buttonIcon.textContent = 'view_list' // Change the icon to list view
      tooltip.setContent({ '.tooltip-inner': 'List view' }); // Change the tooltip
      buttonIcon.dataset.viewType = 'list' // Update dataset
    }

  }



  // Create secondary video HTML divs
  // const secondaryVideoElList = await createSecondaryVideoDivs(videoSrcArr);\
  const filteredDomIdArr = [];
  for (const obj of filteredArr) {
    // Get the video name and source
    const videoName = obj.name;
    const videoSrc = obj.src;
    const domId = `video_${videoName}`;
    filteredDomIdArr.push(domId);

  
    // Construct the column HTML
    const videoColEl = document.createElement('div');
    videoColEl.classList.add('col');

    const videoContainerEl = document.createElement('div');
    videoContainerEl.classList.add('ratio', 'ratio-16x9', 'video-container');

    const spinnerEl = document.createElement('div');
    spinnerEl.classList.add('position-absolute', 'spinner-parent-div');

    const videoEl = document.createElement('video');
    videoEl.id = domId;
    videoEl.classList.add('secondary-video');
    videoEl.src = videoSrc;
    videoEl.textContent = 'Your browser does not support the video tag.';

    const buttonDivEl = document.createElement('div');
    buttonDivEl.classList.add('overlay', 'px-2');

    const videoTitleEl = document.createElement('small');
    videoTitleEl.classList.add('secondary-video-title');
    videoTitleEl.textContent = videoName;

    const disposeBtnEl = document.createElement('button');
    disposeBtnEl.type = 'button';
    disposeBtnEl.classList.add('btn', 'btn-sm', 'btn-icon-small', 'btn-dispose-player');

    const btnIconEl = document.createElement('span');
    btnIconEl.classList.add('material-symbols-rounded', 'dark');
    btnIconEl.textContent = 'close';

    disposeBtnEl.append(btnIconEl);
    buttonDivEl.append(videoTitleEl, disposeBtnEl);
    videoContainerEl.append(spinnerEl, videoEl, buttonDivEl)
    videoColEl.append(videoContainerEl);

    secondaryVideoRow.append(videoColEl);


    // const videoColDiv = [
    //   `<div class="col">`,
    //     '<div class="ratio ratio-16x9 video-container">',
    //       '<div class="position-absolute spinner-parent-div"></div>',
    //       `<video id="${domId}" class="secondary-video" src="${videoSrc}">`,
    //         'Your browser does not support the video tag.',
    //       '</video>',
    //       '<div class="overlay px-2">',
    //         // '<div class="d-flex justify-content-between">',
    //           `<small class="secondary-video-title">${videoName}</small>`,
    //           '<button type="button" class="btn btn-sm btn-icon-small btn-dispose-player">',
    //             '<span class="material-symbols-rounded dark">close</span>',
    //           '</button>',
    //         // '</div>',
    //       '</div>', 
    //     '</div>',
    //   '</div>'
    // ].join('');
    
  
    // // Add this div to the secondary video row
    // secondaryVideoRow.innerHTML += videoColDiv;

  }


  const videoElArr = filteredDomIdArr.map(domId => secondaryVideoRow.querySelector(`#${domId}`));

  if (videoElArr.length < 1) return;

  for (const videoEl of videoElArr) {
    const player = new Player(videoEl.id);
    await player.setSource(videoEl.src);
    player.mute();
    player.load();
    player.setCurrentTime(Player.getMainPlayer().getCurrentTime());
    player.setPlaybackRate(Player.getMainPlayer().getPlaybackRate());

    // Show spinner when video is loading
    player.on('loadstart', () => {
      player.showSpinner();  
        
    });

    // Set frame rate and dismiss button
    player.on('loadeddata', async () => {
        
      // Set frame rate 
      await player.setFrameRate();

      // Dispose player if close button is clicked
      const playerColDiv = player.el.parentNode.parentNode;
      if (playerColDiv) {
        const disposeButton = playerColDiv.querySelector('.btn-dispose-player');
        disposeButton.addEventListener('click', () => player.dispose());
      }
  
    });
  
    // Hide the spinner after the video is loaded
    player.on('canplaythrough', () => player.hideSpinner());

  }

  // const secondaryVideoElList = document.querySelectorAll('.secondary-video');
  // if (!secondaryVideoElList) return;

  // // If there are valid videos, show grid view button and move button for opening videos to upper bar
  // if (secondaryVideoElList.length < 1) return;
    
  // Show the column sizing button
  const colSizeBtn = document.getElementById('secondary-video-colsize-btn');
  if (colSizeBtn) colSizeBtn.classList.remove('d-none');

  // Move the button for opening videos to the control bar above
  const openBtnDiv = document.querySelector('#secondary-videos-control-div .button-div')
  if (!openBtnDiv) return;

  openBtnDiv.prepend(openSecondaryVideosBtn);
  openSecondaryVideosBtn.classList.remove('mt-3');
  openSecondaryVideosBtn.classList.replace('btn-icon-large', 'btn-icon-small');
  
  // Hide the information text
  const infoTextEl = document.getElementById('secondary-video-info-text');
  if (infoTextEl) infoTextEl.classList.add('d-none');


}


// async function createSecondaryVideoDivs(videoFilePaths) {
  
//   if (!videoFilePaths) return;

//   const numberOfVideos = videoFilePaths.length;
//   const secondaryVideoRow = document.querySelector('#secondary-video-row');

//   for (let i = 0; i < numberOfVideos; i++) {
    
//     const videoFileName = await getFileNameWithoutExtension(videoFilePaths[i]);
//     if (!videoFileName) return;
    
//     // Don't proceed if the the same video has already been opened
//     const domId = `video_${videoFileName}`;
//     const isDuplicate = Player.getSecondaryPlayers().filter(player => player.domId === domId).length > 0;
//     if (isDuplicate) return;
    
//     // Set the column size for each secondary player
//     const columnSize = "col";

//     // Construct the column HTML
//     const videoColDiv = [
//       `<div class="${columnSize}">`,
//         '<div class="ratio ratio-16x9 video-container">',
//           '<div class="position-absolute spinner-parent-div"></div>',
//           `<video id="${domId}" class="secondary-video" src="${videoFilePaths[i]}">`,
//             'Your browser does not support the video tag.',
//           '</video>',
//           '<div class="overlay px-2">',
//             // '<div class="d-flex justify-content-between">',
//               `<small class="secondary-video-title">${videoFileName}</small>`,
//               '<button type="button" class="btn btn-sm btn-icon-small btn-dispose-player">',
//                 '<span class="material-symbols-rounded dark">close</span>',
//               '</button>',
//             // '</div>',
//           '</div>', 
//         '</div>',
//       '</div>'
//     ].join('')

//     // Add this div to the secondary video row
//     secondaryVideoRow.innerHTML += videoColDiv;
  

//   }

//   return document.querySelectorAll('.secondary-video');
  
// }


async function getFileNameWithoutExtension(filePath) {
    const fileName = await window.electronAPI.getFileNameWithoutExtension(filePath);
    return fileName
  
}


function getClickedBoxes(rectanglesInFrame, mouseX, mouseY) {
  // console.log(rectanglesInFrame);
  let clickedRectangles = [];
  rectanglesInFrame.forEach(rect => {
    // console.log(rect);
    if (mouseX >= rect.x && mouseX <= rect.x + rect.width && mouseY >= rect.y && mouseY <= rect.y + rect.height) {
      clickedRectangles.push(rect)
    }
  })
  // console.log(clickedRectangles)
  if (clickedRectangles.length > 0) {
    return clickedRectangles;
  }
  return false
}

function showOverlappingTracksToast(clickedRectangles) {
  const toastEl = document.getElementById('overlapping-tracks-toast');
  if (toastEl) {
    const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toastEl);

    // Show choices for overlapping boxes
    const trackSelect = toastEl.querySelector('#overlapping-track-select');

    if (trackSelect) {

      // Clear previous options
      trackSelect.options.length = 0;
  
      // Add default object option
      const defaultOption = document.createElement("option");
      defaultOption.text = 'Choose a subject ID';
      defaultOption.selected = true;
      trackSelect.add(defaultOption);
  
      // Add objects on the current frame as options 
      clickedRectangles.forEach(rectangle => {
        const option = document.createElement("option");
        option.text = produceLabelText(rectangle);
        option.dataset.species = rectangle.species;
        option.value = rectangle.trackId;
        trackSelect.add(option);
      })
  
      toastBootstrap.show();
    }


  }
}

/**
 * Produce a label string from a tracking object
 * @param {Object} trackObj
 * @returns 
 */
function produceLabelText(trackObj) {
  
  if (Number.isInteger(trackObj.species) && Number.isInteger(trackObj.trackId)) {
    let labelText = trackObj.species + '-' + trackObj.trackId;
    const individualNames = Player.getIndividualNames();
    
    // Add individual name if it exists
    if (Number.isInteger(trackObj.nameOrder) && individualNames) {
      labelText += '-' + individualNames[trackObj.nameOrder];
    }
    
    // Add 'Box' to the string if the track is identified as such
    if (getSpeciesName(trackObj.species) === 'box') {
      labelText +=  '-' + 'Box';
    }

    return labelText;

  }

}


function showToastForBehaviorRecording (event, clickedRectangle, rectanglesInFrame, timestamp, frameNumber) {
  // If this is a new observation (i.e. no subject and actions are selected)
    // Show the clicked subject and action options
    // Save the user choice to the Current Observation object
    // Listen for undo key and also show undo button
  // If this is not a new observation (i.e. subject and action are already selected)
    // Listen for user clicks on the whole video frame (to provide no target option) 
    // Show the clicked target option 
    // Show also no target option if user clicks outside of a tracking box
    // Listen for undo key and also show undo button
    // Update the current observation
    // Save the user choice to the Ethogram


    // Get the action types
    const actionNames = Player.getActionNames();

    // Check if action and modifier types have been uploaded
    if (!actionNames || actionNames.length < 1) {
      showAlertToast(
        'Please upload files for actions first!',
        'warning',
        'Interactive Tracking Disabled'
      );
      return;
    }

    // Check if toast element exists
    const toastEl = document.getElementById('labelling-toast');
    if (!toastEl) return;
    
    // Get the relevant DOM elements
    const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toastEl);
    const trackingInfoEl = toastEl.querySelector('#tracking-info-text');
    const toastTitle = toastEl.querySelector('.toast-title');
    const toastAlert = toastEl.querySelector('#toast-alert');
    const behaviorTab = toastEl.querySelector('#behavior-toast-tab');
    const timeStampDiv = toastEl.querySelector('#toast-timestamp');
    const subjectSelect = toastEl.querySelector('#subject-select');
    const targetSelect = toastEl.querySelector('#target-select');
    const actionSelect = toastEl.querySelector('#action-select');
    const nameEditSelect = toastEl.querySelector('#name-edit-select'); // Selection for editting the individual name in a track
    const nameEditDiv = toastEl.querySelector('#name-edit-div'); // Input group div for name edit

    const behaviorRecordBtn = toastEl.querySelector('#label-save-btn');

    // Check if DOM elements within the toast exist
    if (!subjectSelect || !actionSelect || !targetSelect || !nameEditSelect || !nameEditDiv || !behaviorRecordBtn) return;

    // Fill the timestamp element with the time of the start of the current observation
    timeStampDiv.textContent = formatSeconds(timestamp);

    // Add a title 
    // TODO: Add observation ID to the title
    if (toastTitle) toastTitle.textContent = 'Observation'

    // Get individual names
    const individualNames = Player.getIndividualNames();

    if (!individualNames) return;

    // Hide the toast alert by default
    toastAlert.classList.add('d-none');

    // Fill elements for editting names of clicked bounding boxes
    // Hide and disable name edit selection by default 
    // activate only if tracking box contains a name
    nameEditDiv.classList.add('d-none');
    nameEditSelect.disabled = true;
    
    // Make the div for selection visible
    nameEditDiv.classList.remove('d-none');

    // Enable name select element
    nameEditSelect.disabled = false;

    // Clear previous options
    nameEditSelect.options.length = 0;
      
    // Add all individual names to the options for editting tracks
    individualNames.forEach(name => {
      const option = document.createElement('option');
      option.text = name;
      option.value = individualNames.indexOf(name); // Order of name in the individual list file
      option.dataset.trackId = clickedRectangle.trackId;
      option.dataset.species = clickedRectangle.species;
      option.dataset.nameOrder = individualNames.indexOf(name);
      nameEditSelect.add(option);

    }); 

    // Change the title for name edit div depending on existence of a name for the clicked individual
    // Check if clicked rectangle includes an individual name 
    if (Number.isSafeInteger(clickedRectangle.nameOrder)) {

      // Change the title 
      nameEditDiv.querySelector('.description').textContent = 'Change name';
      
    // If clicked rectangle has no individual name assigned by the model, show an option to add it
    } else {

      // Change the title 
      nameEditDiv.querySelector('.description').textContent = 'Add name';
    }
  
    // Check if main player is initialized
    const mainPlayer = Player.getMainPlayer();
    if (!mainPlayer) return;

    // Check if current observation is initialized
    const currentObs = Player.getCurrentObservation();
    if (!currentObs) return;

    // Check if the ethogram is initalized
    const ethogram = mainPlayer.getEthogram();
    if(!ethogram) {
      console.log('No ethogram was found for behavior recording with shortcuts!'); 
      return;
    }

    // Save information about clicked rectangle
    toastEl.dataset.clickedId = clickedRectangle.trackId;
    toastEl.dataset.clickedSpecies = clickedRectangle.species;
    toastEl.dataset.timestamp = timestamp;
    toastEl.dataset.frameNumber = frameNumber;
    toastEl.dataset.clickedNameOrder  = 'none'; // Assign none to clickedNameOrder by default

    if (clickedRectangle.hasOwnProperty('nameOrder')) {
      toastEl.dataset.clickedNameOrder = clickedRectangle.nameOrder; // If clicked rectangle has a name assigned to the model or user before, use it
    }

    // Get the undo Hotkey
    const undoHotkey = Hotkey.findOne({category: 'labelling', name: 'undo'});

    

    /**
     * Forces user to add names to unidentified tracks before recording an observation
     * @param {String | undefined} trackSpecies Which species to apply. Applies to all species by default. Can be "box", "primate".
     * @returns {Boolean} True if popover has been shown, False otherwise
     */
    function showPopoverForUnnamedTrack(trackSpecies) {

      // Determine if the clicked track is identified as a box
      const isBoxClicked = getSpeciesName(clickedRectangle.species) === 'box';

      // Return if the input species and clicked species do not match - i.e. do not show a popover
      if (trackSpecies) {

        if (trackSpecies.toLowerCase() === 'box' && !isBoxClicked) {
          return;
        }
  
        if (trackSpecies.toLowerCase() === 'primate' && isBoxClicked) {
          return;
        }

      } 

      if (!clickedRectangle.hasOwnProperty('nameOrder')) {
  
        // Get the canvas element
        const canvas = mainPlayer.getCanvas();
        if (!canvas) return;
        
        // Get the mouse position
        const canvasRect = canvas.getBoundingClientRect();
        const mouseX = event.clientX - canvasRect.left;
        const mouseY = event.clientY - canvasRect.top;
        
        // Show a popover over the unnamed tracking box
        const popoverDivEl = document.getElementById('popover-canvas-div');
        if (popoverDivEl) {
          popoverDivEl.style.left = mouseX + 'px';
          popoverDivEl.style.top = mouseY + 'px';
          popoverDivEl.style.width = '20px';
          popoverDivEl.style.height = '20px';
          popoverDivEl.classList.remove('d-none');
  
        }
  
        const popover = new bootstrap.Popover(popoverDivEl, {
          container: 'body',
          content: 'Assign a name to proceed!',
  
        });
  
        popover.show();

        return true;
  
      }

      return false;

    }

    // Get the observation status - whether this is a new observation (i.e. no subject and actions are selected)
    const isNewObs = (!currentObs.subjectName || !Number.isSafeInteger(parseInt(currentObs.subjectId)));

    // ==================================
    // 1. Show options for subject and action
    // ==================================
    // Check if this is a new observation (i.e. no subject and actions are selected)
    if (isNewObs) {

      // Change the text of the behavior recording button 
      behaviorRecordBtn.textContent = 'Mark start';

      // Force user to add a name to unnamed track to choose it as the subject - valid even if it is a "box"
      if (showPopoverForUnnamedTrack('all')) return;

      // Hide the alert the if the selection is a primate
      toastAlert.classList.add('d-none'); 

      // Warn users if they choose a box as the subject
      const isBoxClicked = getSpeciesName(clickedRectangle.species) === 'box';
      if (isBoxClicked) {
        toastAlert.textContent = "Selected subject is identified as a box!";
        toastAlert.classList.remove('d-none');
      }

      // Enable subject select element
      subjectSelect.disabled = false;

      // Clear previous options
      subjectSelect.options.length = 0;
      
      // Add default label option
      const defaultSubjectOption = document.createElement('option');
      defaultSubjectOption.text = 'Choose a subject';
      defaultSubjectOption.selected = true;
      subjectSelect.add(defaultSubjectOption);
    
      // Add all label options
      rectanglesInFrame.forEach(rectangle => { 
        const option = document.createElement('option');
        option.text = produceLabelText(rectangle);
        option.value = rectangle.trackId;
        option.dataset.species = rectangle.species;
        
        // Add subject name if it exists
        if (Number.isSafeInteger(rectangle.nameOrder)) {
          option.dataset.nameOrder = rectangle.nameOrder; 
        }
        
        // Add option element to the list
        subjectSelect.add(option);

        // Make the clicked object default option
        if (clickedRectangle.trackId === rectangle.trackId && clickedRectangle.species === rectangle.species) {
          option.selected = true;
        }

      });

      // Show options for all actions
      // Enable action select element
      actionSelect.disabled = false;

      // Clear previous options
      actionSelect.options.length = 0;
  
      // Add default label option
      const defaultLabelOption = document.createElement('option');
      defaultLabelOption.text = 'Choose an action';
      defaultLabelOption.selected = true;
      actionSelect.add(defaultLabelOption);
  
      // Add all action options
      actionNames.forEach(action => { 
        const option = document.createElement('option');
        option.text = action;
        option.value = action;
        actionSelect.add(option);

      });


      // Hide target selection input group div 
      // Target selection will be shown to the user in the last step when user chooses the ending frame of the observation
      targetSelect.parentElement.classList.add('d-none');

    // ==================================
    // 2. Show options for target
    // ==================================
    // If this is an observation in progress (i.e. subject and action are already selected)
    // show the options for target selection
    } else {

      // Reset the button text for marking the start
      behaviorRecordBtn.textContent = 'Mark end'

      // Force user to name an unnamed track ONLY if it is a "primate"
      if (showPopoverForUnnamedTrack('primate')) return;

      // Check if ending frame is later than the starting frame
      if (mainPlayer.getCurrentFrame() < parseInt(currentObs.startFrame)) {
        showAlertToast('End frame must be greater than start frame!', 'error', 'Invalid End Frame');
        return;
      }
      
      // Clear previous options
      targetSelect.options.length = 0;
  
       // Add default object option
      const defaultObjectOption = document.createElement('option');
      defaultObjectOption.text = 'Choose a target ID';
      defaultObjectOption.selected = true;
      targetSelect.add(defaultObjectOption);

      // TODO: SHOW NAME instead of TRACK ID if it exists!!
   
      // Add target on the current frame as options 
      rectanglesInFrame.forEach(rectangle => {
        const option = document.createElement('option');
        option.text = produceLabelText(rectangle);
        option.value = rectangle.trackId;
        option.dataset.species = rectangle.species;

        // Add target name if it exists
        if (Number.isSafeInteger(rectangle.nameOrder)) {
          option.dataset.nameOrder = rectangle.nameOrder; 
        }

        // Add option element to the list
        targetSelect.add(option);

        // Make the clicked target default option
        if (clickedRectangle.trackId === rectangle.trackId && clickedRectangle.species === rectangle.species) {
          option.selected = true;
        }

      });

      // Show object selection input group element
      targetSelect.parentElement.classList.remove('d-none');
  
    }
    
    // Show the toast
    toastBootstrap.show();

    // Enable labelling
    Player.enableLabelling();


}


/**
 * Update tracking table from dropdown menu opened with right clicking on bounding boxes
 * @returns 
 */
async function updateTracksFromNameDropdown() {
  // Get the tracking table
  const trackingTable = document.getElementById('tracking-table');

  const mainPlayer = Player.getMainPlayer();
  if (!mainPlayer) {
    console.log('No video has been opened yet!');
    return;
  }

  const rightClickDiv = document.getElementById('right-click-canvas-div');
  const nameEditSelect = rightClickDiv.querySelector('select');

  if (rightClickDiv && nameEditSelect) {

    // Get the selected option 
    const selectedOption = nameEditSelect.options[nameEditSelect.selectedIndex];

    // If default option is selected warn user
    if (selectedOption.index == 0) {
      showAlertToast('Please select an option!', 'error')
      return;
    } 

    // Get the clicked rectangle info from div dataset
    const species = parseInt(rightClickDiv.dataset.clickedSpecies);
    const oldId = parseInt(rightClickDiv.dataset.clickedId);
    const frameNumber = parseInt(rightClickDiv.dataset.frameNumber);
    
    // Name order of track in the individual name list
    let oldNameOrder = null;
    if ("clickedNameOrder" in rightClickDiv.dataset) {
      oldNameOrder = parseInt(rightClickDiv.dataset.clickedNameOrder); 

    }

    // Which frames to apply
    const applyForValue = 'all';

    // Object for a row in tracking edit table
    let rowArgs = {
      'timeStart': null, 
      'timeEnd': null,
      'frameNumber': frameNumber, 
      'oldId': oldId,
      'newId': null,
      'oldNameOrder': oldNameOrder,
      'newNameOrder': null,
      'species': species,
      'applyFor': applyForValue,
      
    }

    // Get the tracking dict to save user edits on tracks
    let trackingMap = Player.getMainPlayer().getTrackingMap();
    // console.log('Tracking map inside dropdown helper:', trackingMap)

    // Quit if no tracking file exists
    if (trackingMap.isEmpty()) {
      console.log('No tracking object could be found!')
      return;
    }

    // Get video duration to find the end frame number
    const videoDuration = Player.getMainPlayer().getDuration();
    
    // Update rowArgs
    rowArgs.timeEnd = secondsToFrames(videoDuration);
    rowArgs.timeStart = 0;
    rowArgs.newNameOrder = parseInt(selectedOption.dataset.nameOrder);
    rowArgs.newId = parseInt(selectedOption.dataset.trackId);
    rowArgs.editType = 'newName';

    // Update the tracking table
    if (trackingTable) {
      // Get the table body
      const tableBody = trackingTable.querySelector('tbody');

      // Check if table was empty before
      const infoRow = tableBody.querySelector('.empty-table-info');
      if (infoRow) {
        // Hide info row before adding new rows
        // infoRow.classList.add('d-none');
        infoRow.remove();
      }

      // Add a new row to tracking table
      const newRow = tableBody.insertRow(0);
      
      // Add cells to this new row
      const typeCell = newRow.insertCell(0);
      const oldIdCell = newRow.insertCell(1);
      const newIdCell = newRow.insertCell(2);
      const timeStartCell = newRow.insertCell(3);
      const timeEndCell = newRow.insertCell(4);
      // const buttonCell = newRow.insertCell(5);
  
      typeCell.classList.add('edit-type');
      oldIdCell.classList.add('old-id');
      newIdCell.classList.add('new-id');
      timeStartCell.classList.add('edit-start', 'time-cell');
      timeEndCell.classList.add('edit-end', 'time-cell');

      // const tableLength = trackingTable.rows.length;
  
      // Add data attributes to return to timestamp in the row when it is clicked
      // newRow.setAttribute('data-frame-number', frameNumber);
      const currentSeconds = Player.getMainPlayer().getCurrentTime();
      newRow.setAttribute('data-timestamp-start', currentSeconds); 

      // Populate the cells
      timeStartCell.textContent = frameNumber;
  
      // Produce text for old Id (only species + track ID by default)
      let oldIdCellText = `${rowArgs.species}-${rowArgs.oldId}`;
      
      // Add the invidiual name if available
      const individualNames = Player.getIndividualNames();
      if (Number.isInteger(rowArgs.oldNameOrder) && individualNames) {
        oldIdCellText += '-' + individualNames[rowArgs.oldNameOrder];
      }
      oldIdCell.textContent = oldIdCellText;
    
      // Get video duration to find the end frame number
      // const videoDuration = Player.getMainPlayer().getDuration();
  
      // Get the selected time format (frames or seconds)
      let timeFormat = 'frames';
      const trackingTimeFormatBtn = document.querySelector('.time-format-btn');
      if (trackingTimeFormatBtn) {
        timeFormat = trackingTimeFormatBtn.dataset.timeFormat;
      }

      // Change the text content of time depending on the current format
      let videoDurationText, currentFrameText, zeroSecondText;
      if (timeFormat === 'frames') {
        videoDurationText = secondsToFrames(videoDuration);
        currentFrameText = frameNumber;
        zeroSecondText = 0;
      } else if (timeFormat === 'seconds') {
        videoDurationText = formatSeconds(videoDuration);
        currentFrameText = formatSeconds(framesToSeconds(frameNumber));
        zeroSecondText = formatSeconds(0);
      }

      // // Update rowArgs
      // rowArgs.timeEnd = secondsToFrames(videoDuration);
      // rowArgs.timeStart = 0;
      // rowArgs.newNameOrder = parseInt(selectedOption.dataset.nameOrder);
      // rowArgs.newId = parseInt(selectedOption.dataset.trackId);
      // rowArgs.editType = 'newName';
  
      // Fill the new tracking table row and cells within it
      timeStartCell.textContent = zeroSecondText;
      timeEndCell.textContent = videoDurationText;
      timeEndCell.setAttribute('data-frame-number', secondsToFrames(videoDuration));
      timeStartCell.setAttribute('data-frame-number', 0);
      typeCell.textContent = 'new-name';
      newRow.setAttribute('data-edit-start-frame', 0); 
      newRow.setAttribute('data-edit-end-frame', secondsToFrames(videoDuration)); 
      const newName = individualNames[rowArgs.newNameOrder];
      newIdCell.textContent = `${rowArgs.species}-${rowArgs.newId}-${newName}`

    }
    
    // Filter entries according to user choice for edit time span (i.e. start frame to end frame)
    const filteredFrameNumbers = [...trackingMap.getTracks().keys()]
    .filter(frameNumber => frameNumber >= rowArgs.timeStart && frameNumber <= rowArgs.timeEnd)
  
    // Keep track of (frameNumber, trackInfoArr) pairs before changes by user to undo changes later
    let oldFramesArr = [];

    // Show changes on canvas by modifying tracking dict on Player component
    if (filteredFrameNumbers.length > 0) {
      filteredFrameNumbers.forEach(frameNumber => {
        let trackInfoArr = trackingMap.get(frameNumber);
  
        // Iterate over tracks in each filtered frame
        trackInfoArr.forEach((trackInfo, index) => {
  
          // Check for tracks with targeted track IDs and species
          if (trackInfo['trackId'] === rowArgs.oldId && trackInfo['species'] === rowArgs.species) {
            
            // Add old (key, value) pair in tracking map to history array (to undo the change later)
            const oldTrackInfoArr = JSON.parse(JSON.stringify(trackInfoArr)); // Create a deep copy to loose any reference to original array
  
            const oldFrameObj = {frameNumber: frameNumber, trackInfoArr: oldTrackInfoArr};
            oldFramesArr.push(oldFrameObj);
                
            // Create a deep copy to loose any reference to original array
            const newTrackInfoArr = JSON.parse(JSON.stringify(trackInfoArr));

            // Use the name order (integer) of the selected new name
            newTrackInfoArr[index]['nameOrder'] = rowArgs.newNameOrder; 

            // Put the confidence id as 1 after user corrects the name
            newTrackInfoArr[index]['confidenceId'] = 1.0; 

            
            // Update the tracking map
            trackingMap.set(frameNumber, newTrackInfoArr);

          }
         
  
        });
  
  
        
      })

    }
    
    // Add all edited frames to history with their properties prior to edits
    if (oldFramesArr.length > 0) {
      trackingMap.addToHistory(oldFramesArr); 
    }

    // Save tracking edits to file
    const response = trackingMap.saveEditsToFile();

    // If saving successful
    if (response) {
      // Hide the right click menu 
      rightClickDiv.classList.add('d-none');
      
      // Refresh tracking boxes on canvas
      mainPlayer.drawTrackingBoxes();
      
    }



  }
  


}

/**
 * Update tracking table from toast menu
 * @returns 
 */
async function updateTracksFromToast() {
  
  // Get the tracking table
  const trackingTable = document.getElementById('tracking-table');

  const toast = document.getElementById('labelling-toast');
  const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toast);
  const trackEditSelect = document.querySelector("input[name=track-edit-options]:checked");
  const applyForSelect = document.querySelector("input[name=apply-for-selection]:checked");
  const toastAlert = toast.querySelector('#toast-alert');
  const behaviorTab = toast.querySelector('#behavior-toast-tab');

  // Verify user selection
  if (!trackEditSelect) {

    // Warn if no input was provided
    toastAlert.textContent = 'Please select an option!';
    toastAlert.classList.remove('d-none');

  } else {

    // Get the tracking dict to save user edits on tracks
    let trackingMap = Player.getMainPlayer().getTrackingMap();

    // Quit if no tracking file exists
    if (trackingMap.isEmpty()) {
      console.log('No tracking object could be found!')
      return;
    }

    // Get user selection 
    const selectedOption = trackEditSelect.value;
    const currentSeconds = Player.getMainPlayer().getCurrentTime();
    const frameNumber = parseInt(toast.dataset.frameNumber);
    const species = parseInt(toast.dataset.clickedSpecies);
    const oldId = parseInt(toast.dataset.clickedId);

    // Name order of track in the individual name list
    let oldNameOrder = null;
    if ("clickedNameOrder" in toast.dataset) {
      oldNameOrder = parseInt(toast.dataset.clickedNameOrder); 

    }

    // Which frames to apply
    const applyForValue = applyForSelect.value;

    // Object for a row in tracking edit table
    let rowArgs = {
      'timeStart': null, 
      'timeEnd': null,
      'frameNumber': frameNumber, 
      'oldId': oldId,
      'newId': null,
      'oldNameOrder': oldNameOrder,
      'newNameOrder': null,
      'species': species,
      'applyFor': applyForValue,
      
    }

    // Validate manual new id input
    if (selectedOption === 'manual-new-id') {
      const manualIdInput = document.getElementById('manual-new-id').value;
      
      // Reject invalid input
      if (!manualIdInput) {
        toastAlert.textContent = 'Please enter a valid ID!';
        toastAlert.classList.remove('d-none');
        // return
      }
    }

    // Get the individual names
    const individualNames = Player.getIndividualNames();

    // Get video duration to find the end frame number
    const videoDuration = Player.getMainPlayer().getDuration();

    // If "after-current" is selected, assign video duration for end of edit timestamp
    if (applyForValue === 'after-current') {
      rowArgs.timeEnd = secondsToFrames(videoDuration);
      rowArgs.timeStart = frameNumber;
      
    } else if (applyForValue === 'only-current') {
      rowArgs.timeEnd = frameNumber;
      rowArgs.timeStart = frameNumber;

    } else if (applyForValue === 'all') {
      rowArgs.timeEnd = secondsToFrames(videoDuration);
      rowArgs.timeStart = 0;

    }

    // Update tracking table
    if (trackingTable) {
      // Get the table body
      const tableBody = trackingTable.querySelector('tbody');
      
      // Check if table was empty before
      const infoRow = tableBody.querySelector('.empty-table-info');
      if (infoRow) {
        infoRow.remove();
      }
    
      // Add a new row to tracking table
      const newRow = tableBody.insertRow(0);
      
      // Add cells to this new row
      const typeCell = newRow.insertCell(0);
      const oldIdCell = newRow.insertCell(1);
      const newIdCell = newRow.insertCell(2);
      const timeStartCell = newRow.insertCell(3);
      const timeEndCell = newRow.insertCell(4);
      // const buttonCell = newRow.insertCell(5);

      typeCell.classList.add('edit-type');
      oldIdCell.classList.add('old-id');
      newIdCell.classList.add('new-id');
      timeStartCell.classList.add('edit-start', 'time-cell');
      timeEndCell.classList.add('edit-end', 'time-cell');
      
      
      // const tableLength = trackingTable.rows.length;
  
      // Add data attributes to return to timestamp in the row when it is clicked
      // newRow.setAttribute('data-frame-number', frameNumber);
      newRow.setAttribute('data-timestamp-start', currentSeconds); 
  
      // Populate the cells
      timeStartCell.textContent = frameNumber;
      
      // Produce text for old Id (only species + track ID by default)
      let oldIdCellText = `${rowArgs.species}-${rowArgs.oldId}`;
      
      // Add the invidiual name if available
      // const individualNames = Player.getIndividualNames();
      if (Number.isInteger(rowArgs.oldNameOrder) && individualNames) {
        oldIdCellText += '-' + individualNames[rowArgs.oldNameOrder];
      }
      oldIdCell.textContent = oldIdCellText;
    
      // Get video duration to find the end frame number
      // const videoDuration = Player.getMainPlayer().getDuration();
  
      // Get the selected time format (frames or seconds)
      let timeFormat = 'frames';
      const trackingTimeFormatBtn = document.querySelector('.time-format-btn');
      if (trackingTimeFormatBtn) {
        timeFormat = trackingTimeFormatBtn.dataset.timeFormat;
      }
      
      // Change the text content of time depending on the current format
      let videoDurationText;
      let currentFrameText;
      let zeroSecondText;
      if (timeFormat === 'frames') {
        videoDurationText = secondsToFrames(videoDuration);
        currentFrameText = frameNumber;
        zeroSecondText = 0;
      } else if (timeFormat === 'seconds') {
        videoDurationText = formatSeconds(videoDuration);
        currentFrameText = formatSeconds(framesToSeconds(frameNumber));
        zeroSecondText = formatSeconds(0);
      }
      
      // If "after-current" is selected, assign video duration for end of edit timestamp
      if (applyForValue === 'after-current') {
        timeStartCell.textContent = currentFrameText;
        timeEndCell.textContent = videoDurationText;
        timeStartCell.setAttribute('data-frame-number', frameNumber);
        timeEndCell.setAttribute('data-frame-number', secondsToFrames(videoDuration));
        // rowArgs.timeEnd = secondsToFrames(videoDuration);
        // rowArgs.timeStart = frameNumber;
        newRow.setAttribute('data-edit-start-frame', frameNumber); 
        newRow.setAttribute('data-edit-end-frame', secondsToFrames(videoDuration)) ;
        
      } else if (applyForValue === 'only-current') {
        timeStartCell.textContent = currentFrameText;
        timeEndCell.textContent = currentFrameText;
        timeEndCell.setAttribute('data-frame-number', frameNumber);
        timeStartCell.setAttribute('data-frame-number', frameNumber);
        // rowArgs.timeEnd = frameNumber;
        // rowArgs.timeStart = frameNumber;
        newRow.setAttribute('data-edit-start-frame', frameNumber); 
        newRow.setAttribute('data-edit-end-frame', frameNumber) 
      } else if (applyForValue === 'all') {
        timeStartCell.textContent = zeroSecondText;
        timeEndCell.textContent = videoDurationText;
        timeEndCell.setAttribute('data-frame-number', secondsToFrames(videoDuration));
        timeStartCell.setAttribute('data-frame-number', 0);
        // rowArgs.timeEnd = secondsToFrames(videoDuration);
        // rowArgs.timeStart = 0;
        newRow.setAttribute('data-edit-start-frame', 0); 
        newRow.setAttribute('data-edit-end-frame', secondsToFrames(videoDuration)); 
  
      }

    }
    
  
    // const firstAvailTrackId = parseInt(document.getElementById('next-unused-track-id').value);
    let firstAvailTrackIdMap = Player.getMainPlayer().getFirstAvailTrackIds()
    const firstAvailTrackId = firstAvailTrackIdMap.get(rowArgs.species);
    // Process selection depending on edit type (remove track, auto new ID, manual new id)
    if (selectedOption === 'remove') {
      rowArgs.editType = 'remove';
      rowArgs.newId = 'REMOVED';
      
      // typeCell.textContent = 'remove';
      // newIdCell.textContent = '-';
    
    } else if (selectedOption === 'auto-new-id') {
      rowArgs.editType = 'autoNewId';
      rowArgs.newId = firstAvailTrackId;
      
      // typeCell.textContent = 'auto-new-id'
      // newIdCell.textContent =  `${rowArgs.species}-${rowArgs.newId}`;

      // Update first available track id
      firstAvailTrackIdMap.set(rowArgs.species, firstAvailTrackId+1);
      Player.getMainPlayer().setFirstAvailTrackIds(firstAvailTrackIdMap);
    } else if (selectedOption === 'manual-new-id')  {
      rowArgs.editType = 'manual';
      rowArgs.newId = parseInt(toast.querySelector('#manual-new-id').value);

      // typeCell.textContent = 'manual-new-id';
      // newIdCell.textContent = `${rowArgs.species}-${rowArgs.newId}`;

    } else if (selectedOption === 'new-name') {
      const nameEditSelect = toast.querySelector('#name-edit-select');
      if (nameEditSelect && individualNames) {
        // if (Number.isInteger(rowArgs.oldNameOrder)) {
          
        const selectedOption = nameEditSelect.options[nameEditSelect.selectedIndex];
        rowArgs.newNameOrder = parseInt(selectedOption.dataset.nameOrder);
        rowArgs.newId = parseInt(selectedOption.dataset.trackId);
        rowArgs.editType = 'newName';

          // typeCell.textContent = 'new-name';
          // const newName = individualNames[rowArgs.newNameOrder];
          // newIdCell.textContent = `${rowArgs.species}-${rowArgs.newId}-${newName}`
        // }

      }

    }
  
    // Filter entries according to user choice for edit time span (i.e. start frame to end frame)
    const filteredFrameNumbers = [...trackingMap.getTracks().keys()]
    .filter(frameNumber => frameNumber >= rowArgs.timeStart && frameNumber <= rowArgs.timeEnd)
    
    // Keep track of (frameNumber, trackInfoArr) pairs before changes by user to undo changes later
    let oldFramesArr = [];
    
    // Show changes on canvas by modifying tracking dict on Player component
    if (filteredFrameNumbers.length > 0) {
      filteredFrameNumbers.forEach(frameNumber => {
        let trackInfoArr = trackingMap.get(frameNumber);
  
        // Iterate over tracks in each filtered frame
        trackInfoArr.forEach((trackInfo, index) => {
  
          // Check for tracks with targeted track IDs and species
          if (trackInfo['trackId'] === rowArgs.oldId && trackInfo['species'] === rowArgs.species) {
            
            // Add old (key, value) pair in tracking map to history array (to undo the change later)
            const oldTrackInfoArr = JSON.parse(JSON.stringify(trackInfoArr)); // Create a deep copy to loose any reference to original array
  
            const oldFrameObj = {frameNumber: frameNumber, trackInfoArr: oldTrackInfoArr};
            oldFramesArr.push(oldFrameObj);
            
            // Replace the old ID with new ID for all those frames (according to selected options)
            if (selectedOption === 'remove') {
              const newTrackInfoArr = oldTrackInfoArr.toSpliced(index, 1);
              
              // Update the tracking map
              trackingMap.set(frameNumber, newTrackInfoArr);
  
            } else if (selectedOption === 'new-name') {
  
              // Check if nameOrder is present
              // if (trackInfo.hasOwnProperty('nameOrder')) {
                
                // Create a deep copy to loose any reference to original array
                const newTrackInfoArr = JSON.parse(JSON.stringify(trackInfoArr));
  
                // Use the name order (integer) of the selected new name
                newTrackInfoArr[index]['nameOrder'] = rowArgs.newNameOrder; 
  
                // Put the confidence id as 1 after user corrects the name
                newTrackInfoArr[index]['confidenceId'] = 1.0; 
  
                
                // Update the tracking map
                trackingMap.set(frameNumber, newTrackInfoArr);
              // }
  
  
            } else {
              // Change only track ID if "manual-new-id" or "auto-new-id" is selected
              const newTrackInfoArr = JSON.parse(JSON.stringify(trackInfoArr));
  
              newTrackInfoArr[index]['trackId'] = rowArgs.newId;
  
              // Update the tracking map
              trackingMap.set(frameNumber, newTrackInfoArr);
  
            }
  
          }
        });
  
        
  
        
      })

    }
    
    // Add all edited frames to history with their properties prior to edits
    if (oldFramesArr.length > 0) {
      trackingMap.addToHistory(oldFramesArr); 
    }

    // Save tracking edits to file
    trackingMap.saveEditsToFile();

    // Refresh tracking boxes on canvas
    Player.getMainPlayer().drawTrackingBoxes();

    // Dismiss toast after edit is done
    toastBootstrap.dispose();

  }
  


}

// Parse HTML table element to JSON array of objects
// Taken from: https://gist.github.com/johannesjo/6b11ef072a0cb467cc93a885b5a1c19f?permalink_comment_id=3852175#gistcomment-3852175
function parseHTMLTableEl(tableEl) {
  const columns = Array.from(tableEl.querySelectorAll('th')).map(it => it.textContent)
  const rows = tableEl.querySelectorAll('tbody > tr')
  return Array.from(rows).map(row => {
      const cells = Array.from(row.querySelectorAll('td'))
      return columns.reduce((obj, col, idx) => {
          obj[col] = cells[idx].textContent
          return obj
      }, {})
  })
}

function clearTrackingTable() {
  const trackingTable = document.getElementById('tracking-table');
  if (!trackingTable) {
    console.log('No table for the tracks was found!');
    return;
  }

  const tableBody = trackingTable.querySelector('tbody');

  // Check if table was empty before
  if (tableBody) {
    while (tableBody.rows.length > 0) {
      tableBody.deleteRow(0);
    }
    
    // Create the info row and cell to when the table is empty
    
    // Insert the info row to the table
    const infoRow = tableBody.insertRow(0);
    infoRow.classList.add('empty-table-info');
    const infoCell = infoRow.insertCell(0);
    infoCell.textContent = 'Modified tracking labels will appear here';
    infoCell.colSpan = '5';
    
  }

}


function clearEthogramTable() {
  const ethogramTable = document.getElementById('behavior-table');

  if (!ethogramTable) {
    console.log("No table for the ethogram could be found!")
    return;
  }

  const tableBody = ethogramTable.querySelector('tbody');

  // Check if table was empty before
  if (tableBody) {
    while (tableBody.rows.length > 0) {
      tableBody.deleteRow(0);
    }
    
    // Create the info row and cell to when the table is empty
    // Insert the info row to the table
    const infoRow = tableBody.insertRow(0);
    infoRow.classList.add('empty-table-info');
    const infoCell = infoRow.insertCell(0);
    infoCell.textContent = 'Records of behaviors will appear here';
    infoCell.colSpan = '6';
    
  }





}

/**
 * Handles recording a behavior when user interacts with the buttons on the toast 
 * The toast is shown when clicking on a tracking bounding box
 * @param {MouseEvent} event 
 */
function handleBehaviorRecordByClick(event) {

  // Check if the DOM element for behaviors exists
  const behaviorTableEl = document.getElementById('behavior-table');
  if (!behaviorTableEl) {
    console.log("No table for saving the labels was found!")
    return;
  }

  // Get the clicked button element for behavior recording
  const behaviorRecordBtn = this;

  // Check if the toast element exists
  const toastEl = document.getElementById('labelling-toast');
  if (!toastEl) return;

  const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toastEl);
  const subjectSelect = toastEl.querySelector('#subject-select');
  const targetSelect = toastEl.querySelector('#target-select');
  const actionSelect = toastEl.querySelector('#action-select');
  const behaviorTab = toastEl.querySelector('#behavior-toast-tab');

  const toastAlertEl = toastEl.querySelector('#toast-alert');

  if (!toastAlertEl) {
    console.log('No HTML element for toast alert could be found!');
    return;
  }

  // Check if the DOM elements inside the toast exist
  if (!(subjectSelect && targetSelect && actionSelect)) return;

  // Check if the main player is loaded
  const mainPlayer = Player.getMainPlayer();
  if (!mainPlayer) return;

  // Check if individual names are defined
  const individualNames = Player.getIndividualNames();
  if (!individualNames || individualNames.length === 0) return;

  // Check if the current observation exists
  const currentObs = Player.getCurrentObservation();
  if (!currentObs) return;

  // Get the observation status (new or in-progress)
  const isNewObs = (!currentObs.subjectName || !Number.isSafeInteger(parseInt(currentObs.subjectId)));

  // Show warnings if no subject is selected
  if (subjectSelect.selectedIndex === 0) {
    toastAlertEl.textContent = 'Subject must be selected!';
    toastAlertEl.classList.remove('d-none');
    return;

  }

  // Show warnings if no action is selected
  if (actionSelect.selectedIndex === 0) {
    toastAlertEl.textContent = 'Action must be selected!';
    toastAlertEl.classList.remove('d-none');
    return;

  }


  // Get the selected subject element
  const selectedSubjectEl = subjectSelect.options[subjectSelect.selectedIndex];
  
  // Check if the subject name is present on the tracking box
  const subjectNameOrder = parseInt(selectedSubjectEl.dataset.nameOrder);
  if (!Number.isSafeInteger(subjectNameOrder)) {
    toastAlertEl.textContent = 'Assign a name to this track before recording your observation!';
    toastAlertEl.classList.remove('d-none');
    return;

  }

  // Get the selected subject name and track ID
  const subjectName = individualNames[subjectNameOrder];
  const subjectId = parseInt(selectedSubjectEl.value);

  // Check if this is a new observation
  if (isNewObs) {

    // Update the current observation:
    // Add index, starting time, subject ID, subject name (if it exists), and action
    currentObs.update(
      ['index', mainPlayer.getEthogram().getInsertionIndex()],
      ['startFrame', mainPlayer.getCurrentFrame()],
      ['subjectId', subjectId],
      ['subjectName', subjectName],
      ['action', actionSelect.value]
    );

    // Confirm the selection
    const confirmedAction = currentObs.action;
    if (confirmedAction) {

      // Show notification
      const subjectText = subjectName ? subjectName : `${toastEl.dataset.clickedSpecies}-${subjectId}`;
      
      showAlertToast(
        `Subject: <span class="badge text-bg-dark">${subjectText}</span> Action: <span class="badge text-bg-dark">${confirmedAction}</span>`,
        'success',
        'New Observation' 
        
      );

      // Show a message for "no target" selection
      // Get the end observation hotkey
      const endObsHotkey = Hotkey.findOne({name: 'endObservation'});
      if (endObsHotkey) {
        const popover = new bootstrap.Popover('#current-observation-div', {
          container: 'body',
          content: `Press <kbd>${endObsHotkey.key}</kbd> for no target`,
          html: true,
          sanitize: false
  
        });
  
        popover.show();

        setTimeout(() => {
          popover.hide();
        }, 2000);
        

      }


      


    }


  // If this is NOT a new observation
  } else {

    // Get the ethogram
    const ethogram = mainPlayer.getEthogram();
    if (!ethogram) return;

    // Show warnings if no target is selected
    if (targetSelect.selectedIndex === 0) {
      toastAlertEl.textContent = 'Target must be selected!';
      toastAlertEl.classList.remove('d-none');
      return;

    }

    // Get the selected target element
    const selectedTargetEl = targetSelect.options[targetSelect.selectedIndex];

    // Get the target species
    const targetSpecies = selectedTargetEl.dataset.species;

    // Get the selected target name and track ID
    const targetId = parseInt(selectedTargetEl.value);

    // Determine the target name depending on the selected species
    let targetName;
    
    // Check if a box is selected as target
    if (getSpeciesName(targetSpecies) === 'box') { 
      targetName = 'Box';
    } else {
      // Check if the target name is present
      const targetNameOrder = parseInt(selectedTargetEl.dataset.nameOrder);
      if (!Number.isSafeInteger(targetNameOrder)) {
        toastAlertEl.textContent = `Assign a name to track ${targetSpecies}-${targetId} to select it as the target!`;
        toastAlertEl.classList.remove('d-none');
        return;
  
      }

      targetName = individualNames[targetNameOrder];

    }
    

    // Update the current observation:
    // Add index, starting time, subject ID, subject name (if it exists), and action
    currentObs.update(
      ['subjectId', subjectId],
      ['subjectName', subjectName],
      ['action', actionSelect.value],
      ['targetId', targetId],
      ['targetName', targetName],
      ['endFrame', mainPlayer.getCurrentFrame()]
    );


    // Confirm target selection ("no target" option can also be selected)


    // Add the observation to the ethogram
    if (ethogram.add(currentObs)) {

      // Update the HTML table for the ethogram
      addEthogramRow(currentObs);
  
      // Show notification
      showAlertToast(
        `ID ${currentObs.index}: <span class="badge text-bg-dark">${currentObs.subjectName}</span>-<span class="badge text-bg-dark">${currentObs.action}</span>-<span class="badge text-bg-dark">${currentObs.targetName}</span>`,
        'success', 
        `Observation Recorded`,
      );

    }

    // Reset the current observation
    Player.resetCurrentObservation();

    

  }

  // Dismiss the toast
  toastBootstrap.dispose();



}

/**
 * Updates the table for labels
 */
function updateInteractionTable() {
  const interactionTable = document.getElementById('interaction-table');

  if (!interactionTable) {
    console.log("No table for saving the labels was found!")
    return;
  }

  const labelSaveButton = this;
  const toast = document.getElementById('labelling-toast');
  const toastBootstrap = bootstrap.Toast.getOrCreateInstance(toast);
  const subjectSelect = toast.querySelector('#subject-select');
  const objectSelect = toast.querySelector('#target-select');
  const labelSelect = toast.querySelector('#label-select');
  const actionTab = toast.querySelector('#interaction-toast-tab');

  // const isEnd = (actionTab.dataset.isEnd === 'true');

  
  // Get the status of observation (new or in-progress)
  const isNew = (actionTab.dataset.obsStatus === 'new');
  
  // If this is a new observation
  if (isNew) {
    // Object for a row in interaction labelling table
    let observation = {
      'timeStart': null,
      'timeEnd': null,
      'subjectId': null,
      'subjectSpecies': null,
      'subjectName': null,
      'objectId': null,
      'objectSpecies': null,
      'objectName': null,
      'label': null,
      'modifier': null
    }

    // Show warning if no label is selected
    if (labelSelect.selectedIndex === 0){
      const toastAlert = toast.querySelector('#toast-alert');
      toastAlert.textContent = 'Action type must be selected!';
      toastAlert.classList.remove('d-none');
    } else {
      // Mark the start of an interaction
      observation.timeStart = Player.getMainPlayer().getCurrentFrame();;
      observation.subjectId = parseInt(toast.dataset.clickedId);
      observation.subjectSpecies = parseInt(toast.dataset.clickedSpecies);
      observation.label = labelSelect.value;
      // observation.objectId = objectId;

      // Set the current observation
      Player.setCurrentObservation(observation);
      
      // Add observartion row
      // const hideAlert = true;
      // addActionRow(Player.getCurrentObservation(), hideAlert);
      showAlertToast(
        'Go to the frame where the behavior ends and choose the target', 
        'success', 
        'New record started'
      );

      // Save the data to toast element
      // toast.dataset.objectId = objectId;

      // Show success/failure message for added interaction

      // Change the text of save button for interaction
      labelSaveButton.textContent = 'Mark end';
      // actionTab.dataset.isEnd = 'true';
      
      // Update the observation status to "in-progress"
      actionTab.dataset.obsStatus = 'in-progress';

      toastBootstrap.dispose();
    }

      
  // If this is an observation in progress
  } else {
    
    // Show warning if no object is selected
    if (objectSelect.selectedIndex === 0 ){
      const toastAlert = toast.querySelector('#toast-alert');
      toastAlert.textContent = 'Target must be selected!';
      toastAlert.classList.remove('d-none');
    } else {
      // Get the selected object
      const selectedObject = objectSelect.options[objectSelect.selectedIndex];
      
      
      // Verify if the selected end frame is later than the start
      const mainPlayer = Player.getMainPlayer();
      const timeEnd = mainPlayer.getCurrentFrame()
      const timeStart = Player.getCurrentObservation().timeStart;
      if (timeEnd < timeStart) {
        showAlertToast(
          'The end frame must be later than the start frame!',
          'error',
          'Invalid selection'
        );
        return;
      }
      
      // Update the current observation (mark the end of action)
      Player.updateCurrentObservation('objectId', parseInt(selectedObject.value));
      Player.updateCurrentObservation('objectSpecies', parseInt(selectedObject.dataset.species));
      Player.updateCurrentObservation('timeEnd', timeEnd);
      Player.updateCurrentObservation('modifier', selectedModifier.value);

      
      // Get the unique observation index to save it to table (for editing it later)
      const observation = Player.getCurrentObservation();
      const index = mainPlayer.getActionMap().getInsertionIndex();
      
      // Add observation into the Action Map
      if (mainPlayer.getActionMap().add(observation)) {
        // Complete the observation row
        const hideAlert = true;
        completeActionRow(observation, index);
      }
  
      labelSaveButton.textContent = 'Mark start';
      // labelSaveButton.dataset.isNew = 'true';

      // Show success/failure message
      
      // Update the observation status to "new"
      actionTab.dataset.obsStatus = 'new';
  
      // actionTab.dataset.isEnd = 'false';

      

      
      // Dismiss the toast
      toastBootstrap.dispose();
    }

  }
}



/**
 * 
 * @param {Observation} obs - Map for getting information for each observation row in the table
 * @param {Boolean} hideAlert - True if the toast alert needs to be hidden
 */
function addEthogramRow(obs, hideAlert) {

  // Check if both observation object and index is given
  if (!obs) {
    console.log('Observation object must be given!');
    return;
  }

  // Check if inputs are in correct format
  if (!(obs instanceof Observation)) {
    console.log('Input object should be an instance of Observation!');
    return;
  }

  // Get the observation index
  const obsIndex = obs.index;

  // Check the format of the observation index
  if (!Number.isInteger(parseInt(obsIndex))) {
    console.log('Observation index should be an integer!');
    return;
  }
 
  if (parseInt(obsIndex) < 0) {
    console.log('Observation index must be a non-negative integer!');
    return;
  }

  if (!Player.getMainPlayer()) {
    console.log('No main player could be found!');
    return;
  }

  const ethogram = Player.getMainPlayer().getEthogram();
  if (!ethogram) {
    console.log('No Ethogram object could be found!');
    return;
  }

  // Get the HTML table for behavior records
  const tableEl = document.getElementById('behavior-table');
  if (!tableEl) {
    console.log('No HTML table for ethogram could be found!');
    return;
  }

  // Determine whether this is an update to an existing behavior record
  let isUpdate;

  /**
   * Validate user edits of an input element with a datalist
   * @param {HTMLElement} inputEl 
   * @param {String} inputType - Either subject, target, action, starting time or ending time
   * @returns {Boolean} - True if the user edit is valid, false otherwise
   */
  function validateUserEdit(inputEl, inputType) {

    // Get the datalist
    const datalist = inputEl.list;
    if (datalist) {

      // Determine whether an option exists with the current value of the input.
      let optionFound = false;
      for (let i = 0; i < datalist.options.length; i++) {
        if (inputEl.value == datalist.options[i].value) {
          optionFound = true;
          break;
        }
      }

      // use the setCustomValidity function of the Validation API
      // to provide an user feedback if the value does not exist in the datalist
      if (optionFound) {
        inputEl.setCustomValidity('');
      } else {
        // Determine the validation message
        const validationType = inputType ? inputType : 'value';
        inputEl.setCustomValidity(`Please select a valid ${validationType}!`);
      }
  
      inputEl.reportValidity();
  
      return optionFound;

    }
    
  }

  
  // Check if a row for this observation already exists in the HTML table
  const rowEls = tableEl.querySelectorAll(`tr[data-observation-index="${obsIndex}"]`);
  if (rowEls.length === 1) {
    isUpdate = true;
  } else if (rowEls.length < 1) {
    isUpdate = false;
  } else {
    console.log(`There are ${rowEls.length} elements with the same index in the HTML behavior table!`)
    return;
  }

  // Get the properties of the current observation
  const startTime = obs.startFrame;
  const endTime = obs.endFrame;
  const subjectId = parseInt(obs.subjectId);
  const subjectSpecies = parseInt(obs.subjectSpecies);
  const subjectName = obs.subjectName;
  const targetId = parseInt(obs.targetId);
  const targetSpecies = parseInt(obs.targetSpecies);
  const targetName = obs.targetName;
  const actionLabel = obs.action;

  // Get the table body HTML element
  const tableBody = tableEl.querySelector('tbody');

  // Check if table was empty before
  const infoRow = tableBody.querySelector('.empty-table-info');
  if (infoRow) {
    // Remove info row before adding new rows
    infoRow.remove();
  }

  // Define the headers 
  const subjectHeader = 'subject';
  const actionHeader = 'action';
  const targetHeader = 'target';
  const startTimeHeader = 'start';
  const endTimeHeader = 'end';

  // Initialize the row element
  let rowEl;

  // Initialize the cells
  let subjectCell, actionCell, targetCell, startTimeCell, endTimeCell, buttonCell;

  // If this is new observation - i.e. not an update to an existing row
  if (!isUpdate) {
    
    // Create a new row HTML element and data cells within it 
    rowEl = tableBody.insertRow(0);

    // Add observation index to the row element - to connect the rows to the backend
    rowEl.dataset.observationIndex = obsIndex;
  
    subjectCell = rowEl.insertCell(0);
    actionCell = rowEl.insertCell(1);
    targetCell = rowEl.insertCell(2);
    startTimeCell = rowEl.insertCell(3);
    endTimeCell = rowEl.insertCell(4);
    buttonCell = rowEl.insertCell(5); 

    // Link the data cells with their corresponding headers
    subjectCell.headers = subjectHeader;
    actionCell.headers = actionHeader;
    targetCell.headers = targetHeader;
    startTimeCell.headers = startTimeHeader;
    endTimeCell.headers = endTimeHeader;

    // Make the cells editable
    // [subjectCell, actionCell, targetCell, startTimeCell, endTimeCell].forEach(cellEl => {
    //   cellEl.contentEditable = true 
    // });
    
    // Add delete button
    const deleteBtn = document.createElement('span');
    deleteBtn.role = 'button';
    deleteBtn.classList.add('material-symbols-rounded', 'action-table-icon', 'me-1');
    deleteBtn.textContent = 'delete';

    // Delete the observation when user clicks the delete button
    deleteBtn.addEventListener('click', () => {

      // Check if the observation index exists
      const obsIndex = rowEl.dataset.observationIndex;
      if (!obsIndex) return;
      
      // Attempt to remove the observation from ethogram
      ethogram.remove(obsIndex).then(isSuccess => {
        
        // Confirm the removal
        if (isSuccess) {
          // Remove the row element with delay for clearer visual feedback
          // Flash red background on the removed row just before the actual removal
          rowEl.classList.add('table-danger');
          setTimeout(() => {
            rowEl.classList.remove('table-danger');
            rowEl.remove(); 
          }, 500);


          showAlertToast(`Record with ID ${obsIndex} removed!`, 'success');

        } else {

          showAlertToast(`Record with ID ${obsIndex} could not be removed!`, 'error');

        }

      });



    });

    buttonCell.append(deleteBtn);

    // Add "time-cell" class name - which is required for toggleTimeFormat() function
    startTimeCell.classList.add('time-cell');
    endTimeCell.classList.add('time-cell');

    if (!hideAlert) {
      showAlertToast('New behavior added to the table!', 'success');
    }


  // If this is an update to an existing observation/row
  } else {
    // Find the relevant row with its observation index saved previously in its dataset
    rowEl = tableEl.querySelector(`tr[data-observation-index="${obsIndex}"]`)
    
    // Select its data cells
    if (rowEl) {
      subjectCell = rowEl.querySelector(`td[headers=${subjectHeader}]`);
      actionCell = rowEl.querySelector(`td[headers=${actionHeader}]`);
      targetCell = rowEl.querySelector(`td[headers=${targetHeader}]`);
      startTimeCell = rowEl.querySelector(`td[headers=${startTimeHeader}]`);
      endTimeCell = rowEl.querySelector(`td[headers=${endTimeHeader}]`);
    }

  }

  // Flash a colored background on the row element to indicate that a new row has been added
  rowEl.classList.add('table-success');
  setTimeout(() => {
    rowEl.classList.remove('table-success');
  }, 500);


  /**
   * Add input elements inside cells to make them editable
   * Input element is also necessary for using datalists for autocompletion
   *  */ 
  // Add subject input element
  const subjectInputGroupEl = document.createElement('div'); // Bootstrap input group element
  subjectInputGroupEl.classList.add('input-group', 'input-group-sm', 'ethogram-font');
  const subjectInputEl = document.createElement('input');
  subjectInputEl.classList.add('form-control');
  subjectInputGroupEl.append(subjectInputEl);

  // Add action input element
  const actionInputGroupEl = document.createElement('div'); // Bootstrap input group element
  actionInputGroupEl.classList.add('input-group', 'input-group-sm', 'ethogram-font');
  const actionInputEl = document.createElement('input');
  actionInputEl.classList.add('form-control');
  actionInputGroupEl.append(actionInputEl);

  // Add target input element
  const targetInputGroupEl = document.createElement('div'); // Bootstrap input group element
  targetInputGroupEl.classList.add('input-group', 'input-group-sm', 'ethogram-font');
  const targetInputEl = document.createElement('input');
  targetInputEl.classList.add('form-control');
  targetInputGroupEl.append(targetInputEl);

  // Add starting time input element
  const startTimeInputGroupEl = document.createElement('div'); // Bootstrap input group element
  startTimeInputGroupEl.classList.add('input-group', 'input-group-sm', 'ethogram-font');
  const startTimeInputEl = document.createElement('input');
  startTimeInputEl.classList.add('form-control');
  startTimeInputEl.type = 'number';
  startTimeInputEl.min = '0';  // Min frame should be 0
  startTimeInputEl.max = secondsToFrames(Player.getMainPlayer().getDuration());  // Max frame should be the duration of the video
  startTimeInputGroupEl.append(startTimeInputEl);
  
  // Add ending time input element
  const endTimeInputGroupEl = document.createElement('div'); // Bootstrap input group element
  endTimeInputGroupEl.classList.add('input-group', 'input-group-sm', 'ethogram-font');
  const endTimeInputEl = document.createElement('input');
  endTimeInputEl.classList.add('form-control');
  endTimeInputEl.type = 'number';
  endTimeInputEl.min = '0'; // Min frame should be 0
  endTimeInputEl.max = secondsToFrames(Player.getMainPlayer().getDuration());  // Max frame should be the duration of the video
  endTimeInputGroupEl.append(endTimeInputEl);

  // Add subject datalist element
  const subjectDatalistElId = 'subject-datalist';
  const targetDatalistElId = 'target-datalist';
  const actionDatalistElId = 'action-datalist';

  // Add input elements to data cells
  subjectCell.append(subjectInputGroupEl);
  actionCell.append(actionInputGroupEl);
  targetCell.append(targetInputGroupEl)
  startTimeCell.append(startTimeInputGroupEl);
  endTimeCell.append(endTimeInputGroupEl);

  // Connect datalist elements input elements
  subjectInputEl.setAttribute('list', subjectDatalistElId);
  actionInputEl.setAttribute('list', actionDatalistElId);
  targetInputEl.setAttribute('list', targetDatalistElId);

  /**
   * END of adding input elements
   */

  // Get the selected time format (frames or seconds)
  let timeFormat = 'frames';
  const trackingTimeFormatBtn = document.querySelector('.time-format-btn');
  if (trackingTimeFormatBtn) {
    timeFormat = trackingTimeFormatBtn.dataset.timeFormat;
  }

  // Default text for NA values
  const naText = 'NA';
  
  // Produce text for start frame cell
  let startTimeText = naText;
  if (Number.isSafeInteger(parseInt(startTime))) { 
    startTimeText = startTime;
    
    // Change the text content of time depending on the current format
    if (timeFormat === 'seconds') {
      startTimeText = formatSeconds(framesToSeconds(startTime));
    }
    
    // Add data attributes to return to timestamp in the row when it is clicked
    startTimeInputEl.value = startTime;
    rowEl.dataset.startFrame = startTime;
    
  }

  // Produce text for end frame cell
  let endTimeText = naText;
  if (Number.isSafeInteger(parseInt(endTime))) {
    endTimeText = endTime;

    // Change the text content of time depending on the current format
    if (timeFormat === 'seconds') {
      endTimeText = formatSeconds(framesToSeconds(endTime));
    }

    // Add data attributes to return to timestamp in the row when it is clicked
    endTimeInputEl.value = endTime;
    rowEl.dataset.endFrame = endTime;

  }
  
  // // Populate the timestamp cells
  // startTimeCell.textContent = startTimeText;
  // endTimeCell.textContent = endTimeText;

  // Populate the subject cell
  let subjectText = naText;
  if (subjectName) {
    subjectText = subjectName;
    rowEl.dataset.subjectName = subjectName;
  } else if (Number.isInteger(subjectSpecies) && Number.isInteger(subjectId)) {
    subjectText = `${subjectSpecies}-${subjectId}`;
  } else if (Number.isInteger(subjectId)) {
    subjectText = subjectId;
  }
  subjectInputEl.value = subjectText;

  // Populate the action cell and dataset
  const actionText = actionLabel ? actionLabel : naText;
  actionInputEl.value = actionText;
  rowEl.dataset.action = actionText;

  // Populate the target cell
  let targetText = naText;
  if (targetName) {
    targetText = targetName;
    rowEl.dataset.targetName = targetName;
  } else if (Number.isInteger(targetSpecies) && Number.isInteger(targetId)) {
    targetText = `${targetSpecies}-${targetId}`;
  } else if (Number.isInteger(targetId)) {
    targetText = targetId;
  }
  targetInputEl.value = targetText;

  // Listen for user edits to the subject name of a behavior record
  subjectInputEl.addEventListener('change', () => {
    // Check validity of the user input
    const isValid = validateUserEdit(subjectInputEl, 'subject');

    if (isValid) {
      const obsIndex = rowEl.dataset.observationIndex;
      const userSelection = subjectInputEl.value;

      // Update the ethogram for the chosen observation
      const isSuccess = ethogram.update(obsIndex, ['subjectName', userSelection]);

      // Show success notification
      if (isSuccess) {
        showAlertToast(
          `New subject: <span class="badge text-bg-dark">${userSelection}</span>`,
          'success',
          `Observation Updated - ID: ${obsIndex}`
        );
      }

    }

  });

  // Listen for user edits to the action of a behavior record
  actionInputEl.addEventListener('change', () => {
    // Check validity of the user input
    const isValid = validateUserEdit(actionInputEl, 'action');

    if (isValid) {
      const obsIndex = rowEl.dataset.observationIndex;
      const userSelection =  actionInputEl.value;

      // Update the ethogram for the chosen observation
      const isSuccess = ethogram.update(obsIndex, ['action', userSelection]);
      
      // Show success notification
      if (isSuccess) {
        showAlertToast(
          `New action label: <span class="badge text-bg-dark">${userSelection}</span>`,
          'success',
          `Observation Updated - ID: ${obsIndex}`
        );
      }

    }

  });

  // Listen for user edits to the target name of a behavior record
  targetInputEl.addEventListener('change', () => {
    // Check validity of the user input
    const isValid = validateUserEdit(targetInputEl, 'target');

    if (isValid) {
      const obsIndex = rowEl.dataset.observationIndex;
      const userSelection = targetInputEl.value;

      // Update the ethogram for the chosen observation
      const isSuccess = ethogram.update(obsIndex, ['targetName', userSelection]);

      // Show success notification
      if (isSuccess) {
        showAlertToast(
          `New target: <span class="badge text-bg-dark">${userSelection}</span>`,
          'success',
          `Observation Updated - ID: ${obsIndex}`
        );
      }
      
    }

  });


  // Listen for user edits to the starting time of a behavior record
  startTimeInputEl.addEventListener('change', () => {
    
    // Check validity of the user input
    const userSelection = parseInt(startTimeInputEl.value);

    const endTime = parseInt(endTimeInputEl.value); // Get the ending frame of this observation
    
    if (!Number.isInteger(userSelection) || !Number.isFinite(userSelection) ||
     userSelection < 0 ) {
      startTimeInputEl.setCustomValidity('Please select a valid frame number');
      startTimeInputEl.reportValidity();
      return;
    }

    if (userSelection > endTime) {
      startTimeInputEl.setCustomValidity('Start frame ≤ End frame');
      startTimeInputEl.reportValidity();
      return;
    }

    const obsIndex = rowEl.dataset.observationIndex;

    // Update the ethogram for the chosen observation
    const isSuccess = ethogram.update(obsIndex, ['startFrame', userSelection]);

    // Show success notification
    if (isSuccess) {
      showAlertToast(
        `New start frame: <span class="badge text-bg-dark">${userSelection}</span>`,
        'success',
        `Observation Updated - ID: ${obsIndex}`
      );
    }
      

  });


  // Listen for user edits to the ending time of a behavior record
  endTimeInputEl.addEventListener('change', () => {
    
    // Check validity of the user input
    const userSelection = parseInt(endTimeInputEl.value);

    const startTime = parseInt(startTimeInputEl.value); // Get the ending frame of this observation
    
    if (!Number.isInteger(userSelection) || !Number.isFinite(userSelection) ||
     userSelection < 0 ) {
      endTimeInputEl.setCustomValidity('Please select a valid frame number');
      endTimeInputEl.reportValidity();
      return;
    }

    if (userSelection < startTime) {
      endTimeInputEl.setCustomValidity('End frame ≥ Start frame');
      endTimeInputEl.reportValidity();
      return;
    }

    const mainPlayer = Player.getMainPlayer();
    if (mainPlayer) {
      const videoDuration = secondsToFrames(mainPlayer.getDuration());
      if (userSelection > videoDuration) {
        endTimeInputEl.setCustomValidity('End frame ≤ Duation');
        endTimeInputEl.reportValidity();
        return;
      }
    }

    const obsIndex = rowEl.dataset.observationIndex;

    // Update the ethogram for the chosen observation
    const isSuccess = ethogram.update(obsIndex, ['endFrame', userSelection]);

    // Show success notification
    if (isSuccess) {
      showAlertToast(
        `New end frame: <span class="badge text-bg-dark">${userSelection}</span>`,
        'success',
        `Observation Updated - ID: ${obsIndex}`
      );
    }
      

  });

  // Show cursor on start and end frame elements to indicate that you can go the selected frames
  startTimeInputEl.role = 'button';
  endTimeInputEl.role = 'button';

  // Jump to the start frame of the recorded behavior
  startTimeInputGroupEl.addEventListener('click', () => {
    // Applying time change for all players
    const allPlayers = Player.getAllInstances();
    if (allPlayers) {
      allPlayers.forEach(player => {
        player.pause();
        player.setCurrentTime(framesToSeconds(startTimeInputEl.value));
      });
    }
  })

  // Jump to the end frame of the recorded behavior
  endTimeInputGroupEl.addEventListener('click', () => {
    // Applying time change for all players
    const allPlayers = Player.getAllInstances();
    if (allPlayers) {
      allPlayers.forEach(player => {
        player.pause();
        player.setCurrentTime(framesToSeconds(endTimeInputEl.value));
      });
    }
  })

  // Disable/enable labelling mode when any of the input element is focused/blurred
  const inputEls = [subjectInputEl, actionInputEl, targetInputEl, startTimeInputEl, endTimeInputEl];
  inputEls.forEach(inputEl => {
    inputEl.addEventListener('focus', () => {
      Player.setTypingStatus(true);
    });

    inputEl.addEventListener('blur', () => {
      Player.setTypingStatus(false);
    });

  });






  

  
}

/**
 * Update incomplete rows on the interaction table
 * @param {*} rowArgs 
 * @param {Number} obsIndex 
 */
function completeActionRow(rowArgs, obsIndex, hideAlert) {
  const interactionTable = document.getElementById('interaction-table');
  if (interactionTable) {
    const incompleteRow = interactionTable.querySelector('.incomplete');
    if (incompleteRow) {
      const timeEndCell = incompleteRow.querySelector('.timestamp-end');
      const timeEnd = rowArgs.timeEnd;
      if (timeEndCell && timeEnd) {
        timeEndCell.dataset.frameNumber = timeEnd;
      }
      
      
      // Get the selected time format (frames or seconds) to show time in selected format
      const trackingTimeFormatBtn = document.querySelector('.time-format-btn');
      let timeEndText = timeEnd;
      if (trackingTimeFormatBtn) {
        const timeFormat = trackingTimeFormatBtn.dataset.timeFormat;
        if (timeFormat === 'seconds') {
          timeEndText = formatSeconds(framesToSeconds(timeEndCell.dataset.frameNumber));
        }
      }
      timeEndCell.textContent = timeEndText;

      // Add the observation index to row element
      incompleteRow.dataset.observationIndex = obsIndex;
      
      // Update the row status
      incompleteRow.classList.remove("incomplete");
    }
  
    if (!hideAlert) {
      showAlertToast("Observation recorded!", 'success');

      // Go back to the start frame of the action
      if (rowArgs.timeStart) {
        const start = framesToSeconds(rowArgs.timeStart);
        const allPlayers = Player.getAllInstances();
        if (allPlayers) {
          allPlayers.forEach(player => {
            player.pause();
            player.setCurrentTime(start)
          });
        }
      }
    }

    




  }

}

function deleteEthogramRow() {
  // Check if the target element is a row element (TR)
  if (this.parentNode.parentNode.nodeName.toLowerCase() == 'tr') {
    const rowEl = this.parentNode.parentNode;
    const observationIndex = rowEl.dataset.observationIndex;
    // console.log(typeof observationIndex, observationIndex)
    if (Player.getMainPlayer().getActionMap().remove(observationIndex)) {
      rowEl.remove();
      showAlertToast(`Record with ID ${observationIndex} removed!`, 'success');
    } else {
      showAlertToast(`Record with ID ${observationIndex} could not be removed!`, 'error');
    }
  }

}


/**
 * Updates the playback speed list item
 */
function updatePlaybackRateList() {
  const minRate = Player.getMinPlaybackRate();
  const maxRate = Player.getMaxPlaybackRate();
  const stepSize = 0.25 // Step size for playback rate selections

  const playbackRateList = document.getElementById('playback-rate-list');
  if (playbackRateList) {
    // Clear all previous list items
    while (playbackRateList.firstChild) {
      playbackRateList.removeChild(playbackRateList.firstChild);
    }

    // Create updated items
    for (let rate = minRate; rate <= maxRate; rate += stepSize) {
      const itemEl = document.createElement('li');
      const optionEl = document.createElement('a');
      optionEl.classList.add('dropdown-item', 'small');
      optionEl.textContent = rate;
      optionEl.href = '#';
      optionEl.setAttribute('data-playback-rate', rate)
      itemEl.append(optionEl);
      playbackRateList.append(itemEl);
    }
  }
}

/**
 * Updates the zoom scale DOM elements
 * @returns 
 */
function updateZoomScaleDomEls() {
  const zoomScale = Player.getZoomScale();
  // Get the dropdown element located on the zoomed video region panel
  const dropdownEl = document.getElementById('zoom-scale-dropdown');
  if (!dropdownEl) return;

  // Update the button text
  const toggleBtn = dropdownEl.querySelector('.dropdown-toggle');
  if (!toggleBtn) return;
  toggleBtn.textContent = zoomScale + '%';
  
  const listItems = dropdownEl.querySelectorAll('.dropdown-item');
  if (!listItems) return;
  
  // Make the item with the same zoom scale as the main player active
  listItems.forEach(item => {
    if (parseFloat(item.dataset.zoomScale) === zoomScale) {
      item.classList.add('active');
    } else {
      // Remove active states from all items
      item.classList.remove('active');
    }

  });

  // Get the input element on the settings modal
  const changeZoomScaleInput = document.getElementById('change-zoom-scale-input');
  if (!changeZoomScaleInput) return;

  // Update the value of the input element
  changeZoomScaleInput.value = zoomScale;

}



/**
 * Add behavior records with keyboard shortcuts
 *  Workflow for adding a new behavior record
 *  1 - Select a subject
 *  2 - Select an action
 *  3.1 - End record without a target
 *  3.2 - Select a target and end record
 *  3.3 - Select an action, end record and start a new record with the same subject
 * @param {Event} event 
 * @returns 
 */
function handleKeyPress (event) {

  // Check if the main video is loaded
  const mainPlayer = Player.getMainPlayer();
  if (!mainPlayer) return;

  // Check if the shortcuts exist
  const hotkeys = Hotkey.getAll();
  if (!hotkeys) return;

  // Get the selected Hotkey object
  const selectedHotkey = Hotkey.getSelected(event);
  if (!selectedHotkey) return;

  // Check if the selected Hotkey has the playback category
  if (selectedHotkey.category === 'playback') {
    selectedHotkey.execute();
    return;

  }

  // Check if the selected Hotkey is for toggling labelling 
  if (selectedHotkey.name === 'toggleLabelling') {
    selectedHotkey.execute();
    return;

  }

  // Check if current observation is initialized
  const currentObs = Player.getCurrentObservation();
  if (!currentObs) return;

  // Check if the ethogram is initalized
  const ethogram = mainPlayer.getEthogram();
  if (!ethogram) {
    console.log('No ethogram was found for behavior recording with shortcuts!'); 
    return;
  }

  // // Cancel the default action (e.g. for Enter or Space bar or Ctrl+Z)
  // event.preventDefault();

  // Get the undo Hotkey
  const undoHotkey = Hotkey.findOne({category: 'labelling', name: 'undo'});

  // Check if labelling mode is active
  if (Player.isInLabellingMode()) {

    const anyIndividuals = Player.anyIndividuals();
    const anyActions = Player.anyActions();

    // Check if individual names and action types are entered
    // Show alerts for missing files
    if (!anyActions && !anyIndividuals) {
      showAlertToast('Please upload files for action types and individuals!', 'warning', 'Labelling Disabled');
      return;
    } else if (!anyIndividuals) {
      showAlertToast('Please upload a file for individuals!', 'warning', 'Labelling Disabled');
      return;
    } else if (!anyActions) {
      showAlertToast('Please upload a file for action types!', 'warning', 'Labelling Disabled');
      return;
    }

    // ====================
    // 1. Select a subject
    // ====================
    // Check if current observation does not contain a subject
    if (!currentObs.subjectName) {
  
      if (selectedHotkey.category === 'individuals') {
       
        // Pause the player
        mainPlayer.pause();
        
        // Update the current observation - add index, starting time and subject name
        currentObs.update(
          ['index', mainPlayer.getEthogram().getInsertionIndex()],
          ['startFrame', mainPlayer.getCurrentFrame()],
          ['subjectName', selectedHotkey.name]
        );
  
        // Confirm the selection
        const confirmedSubjectName = currentObs.subjectName;
        if (confirmedSubjectName) {
  
          // Show notification
          showAlertToast(
            'Now, choose an action!', 
            'success', 
            `Selected Subject: ${confirmedSubjectName}`
          );
  
        }
      
      }
  
    // ======================
    // 2. Select an action
    // ======================
    // Check if current observation contains a subject but not an action
    } else if (currentObs.subjectName && !currentObs.action) {
    
      // Check for undo hotkey
      if (undoHotkey.isPressed(event)) {
        // Reset the last selection
        currentObs.undo();
        
        // Confirm the undo action
        const confirmedSubject = currentObs.subjectName;
        if (confirmedSubject === null) {
          // Show notification
          showAlertToast('Now, you may select another subject.', 'success', 'Subject Selection Removed');
  
        }
  
        return;
  
      }
  
      // Check if pressed key corresponds to one of hotkeys for actions
      if (selectedHotkey.category === 'actions') {
        // Get the selected action
        const selectedAction = selectedHotkey.name;
  
        // Update the current observation - add action label
        currentObs.update(['action', selectedAction]);
    
        // Confirm the selection
        const confirmedAction = currentObs.action;
        if (confirmedAction) {
  
          // Show notification
          showAlertToast(
            'Now, either end this observation or choose a target or another action.', 
            'success', 
            `Selected action: ${confirmedAction}`
          );
  
        }
        
  
      }
    
  
    // ======================
    // 3. Select a target or end the observation immediately or select another action for the same subject
    // ======================
    // Check if current observation contains a subject, an action but not a target
    } else if (currentObs.subjectName && currentObs.action && !currentObs.targetName) {
    
      // Check for undo
      if (undoHotkey.isPressed(event)) {
        // Reset the last selection
        currentObs.undo();
        // currentObs.update(['action', null]);
  
        // Confirm the undo action
        const confirmedAction = currentObs.action;
        if (confirmedAction === null) {
          // Show notification
          showAlertToast('Now, you may select another action.', 'success', 'Action Selection Removed');
  
        }
  
        return;
  
      }
  
      // ====================
      // 3.1. End the observation directly without a target
      // ====================
      // Check if pressed key corresponds to the shortcut for ending an observation
      if (selectedHotkey.name === 'endObservation') {
        // Add the ending time for the current observation
        currentObs.update(['endFrame', mainPlayer.getCurrentFrame()]);
  
        // Add the observation to the ethogram
        ethogram.add(currentObs);
  
        // Write new observation to the file
        // TODO
  
        // Update the observation table visually
        addEthogramRow(currentObs);
        
        // Show notification
        showAlertToast(
          `ID ${currentObs.get('index')}: ${currentObs.subjectName}-${currentObs.action}`,
          'success', 
          `Observation Recorded`
        );
  
        // Clear the current observation (i.e. set subject and action to null)
        Player.resetCurrentObservation();
  
      // Check for either target or action selection - (3.2 or 3.3)
      } else {
        
        // Initialize the selected target as null 
        let selectedTarget;
        
        // Check if box is selected as target
        if (selectedHotkey.name === 'boxSelection') {
          selectedTarget = 'Box';
  
        // Check if pressed key corresponds to one of the hotkeys for individuals
        } else if (selectedHotkey.category === 'individuals') {
          selectedTarget = selectedHotkey.name;
  
        }
  
        // ====================
        // 3.2. Select a target and end the observation
        // ====================
        // If a target is chosen, 
        //  add it to the current observation and
        //  end the current observation and 
        //  wait for a start of a new observation (subject selection)
        if (selectedTarget) {
          // Update the current observation - add target name and ending time
          currentObs.update(
            ['targetName', selectedTarget], 
            ['endFrame', mainPlayer.getCurrentFrame()]
          );
  
          // Confirm the selection
          const confirmedTargetName = currentObs.targetName;
          if (confirmedTargetName) {
            
            // Add the observation to the ethogram
            if (ethogram.add(currentObs)) {
              
              // Update the HTML table for the ethogram
              addEthogramRow(currentObs);
  
              // Show notification
              showAlertToast(
                `ID ${currentObs.index}: ${currentObs.subjectName}-${currentObs.action}-${currentObs.targetName}`,
                'success', 
                `Observation Recorded`,
              );
      
            }  
            
          }
  
          // Reset the current observation
          Player.resetCurrentObservation();
  
        // ======================
        // 3.3. Select an action
        //  End the current observation
        //  Start a new observation with the same subject
        // =======================
        } else {
          // If no target is selected, check if the pressed key corresponds to one of the shortcuts for actions
          if (selectedHotkey.category === 'actions') {
    
            const newAction = selectedHotkey.name;
    
            // Update the current observation
            currentObs.update(['endFrame', mainPlayer.getCurrentFrame()]);
    
            const subjectName = currentObs.subjectName;
            const previousAction = currentObs.action;
    
            // Add the observation to the ethogram
            if (ethogram.add(currentObs)) {
              // Update the HTML table for ethogram
              addEthogramRow(currentObs);
              
              // Reset the current observation
              Player.resetCurrentObservation();

              // Add a new observation with the same subject name and action 
              const newObs = Player.getCurrentObservation();
              newObs.update(
                ['index', mainPlayer.getEthogram().getInsertionIndex()],
                ['startFrame', mainPlayer.getCurrentFrame()],
                ['subjectName', subjectName],
                ['action', newAction]
              );

              // Show notification
              showAlertToast(
                `New observation: <strong>${subjectName}-${newAction}.</strong><br>Now, either end this observation, choose a target or another action.`,
                'success', 
                `Observation with ID ${currentObs.index} recorded: ${subjectName}-${previousAction}`
              );

            }
    
          }
  
        }
      
      }
    
    }

  }

}


/**
 * Highlights selected individiuals or objects
 * @param {Array} selectedBoxes | Selected box array for in current frame
 */
function highlightBoxes (selectedBoxes) {
  // Offset in pixels for highlight rectangles
  const offset = 0;

  const mainCanvas = Player.getMainPlayer().getCanvas();
  if (mainCanvas && mainCanvas.getContext) {
    const ctx = mainCanvas.getContext('2d')
    selectedBoxes.forEach(box => {
      ctx.beginPath();
      ctx.strokeStyle = 'yellow';
      ctx.lineWidth = 6;
      ctx.rect(box.x + offset, box.y + offset, box.width, box.height);
      ctx.stroke();
    });
  }
}
/**
 * Show individual names and associated keyboard shortcuts
 * @param {Array}
 */
function showNamesModal() {
  const modalEl = document.getElementById('names-list-modal');
  const nameArr = Player.getIndividualNames(); // Get names of individuals
  if (modalEl && nameArr) {
    // Get shortcuts for individual names
    const shortcuts = Player.getHotkeys();

    const divEl = modalEl.querySelector('#names-list-container');
    
    if (divEl) {
      // Clear previous elements
      while (divEl.firstChild) {
        divEl.removeChild(divEl.firstChild);
      }

      // Create a list element
      const listEl = document.createElement('ul');
      listEl.classList.add('list-group', 'list-group-flush', 'small', 'mb-3');
      
      // Populate the list with names
      for (const name of nameArr) {
        const itemEl = document.createElement('li');
        itemEl.classList.add('list-group-item', 'd-flex', 'justify-content-between', 'align-items-center');
        itemEl.textContent = name;
        
        // Add hotket to the list if available
        if (shortcuts && name in shortcuts.individuals) {
          const hotkey = shortcuts.individuals[name].hotkey;
          const hotkeyEl = document.createElement('kbd');
          hotkeyEl.textContent = hotkey;
          itemEl.append(hotkeyEl);
        }

        listEl.append(itemEl);
        
      }

      divEl.append(listEl);

      const modalBootstrap = bootstrap.Modal.getOrCreateInstance(modalEl);
      
      modalBootstrap.show();
    }

  }
}

/**
 * Show modal for keyboard shortcuts
 * @param {Object} shortcuts | Keyboard shortcut description and corresponding hotkeys in an object
 */
function showShortcutsModal(shortcuts) {

  const modalEl = document.getElementById('keyboard-shortcuts-list-modal');
  if (modalEl) {
    
    // Get the hotkey categories
    const hotkeyCategories = Hotkey.getUniqueCategories();

    // Prevent hotkey change by user for the default categories
    // Only allow change for individuals or actions
    const fixedCategories = ['playback', 'labelling'];
    
    // Get the div HTML element inside the modal
    const divEl = modalEl.querySelector('#shortcuts-list-container');

    if (divEl) {
      // Clear the previous elements
      while (divEl.firstChild) {
        divEl.removeChild(divEl.firstChild);
      }

      // Create a list element for each shortcut category
      hotkeyCategories.forEach(category => {

        // Add a title for each category
        const titleEl = document.createElement('h6');
        titleEl.textContent = category.charAt(0).toUpperCase() + category.substring(1); // Capitalize first character

        // Create a list element
        const listEl = document.createElement('ul');
        listEl.classList.add('list-group', 'small', 'mb-3');
 
        // Find all Hotkeys in each category
        const hotkeysInCategory = Hotkey.findAll({category: category});

        // Populate the list within each category
        hotkeysInCategory.forEach(hotkey => {
            
          const itemEl = document.createElement('li');
          
          // Add hotkey item to class list of the element
          itemEl.classList.add('list-group-item', 'd-flex', 'justify-content-between', 'align-items-center');
          
          // Add hotkey category and key in shortcut dict to the element dataset
          itemEl.dataset.hotkeyCategory = category;
          itemEl.dataset.hotkeyName = hotkey.name;
          
          // Add Id by combining the category and the name of the hotkey in shortcuts dict
          // itemEl.id = `${category}-${hotkeyKey}`;

          // Make hotkey item a button
          // When users clicks on it, they can choose a different hotkey
          itemEl.role = 'button';

          const descDivEl = document.createElement('div');
          descDivEl.textContent = hotkey.description;

          const key = hotkey.key;
          
          const hotkeyEl = document.createElement('span');
          hotkeyEl.classList.add('hotkey-kbd')

          let hotkeyText = `<kbd>${Hotkey.htmlMap.has(key) ? Hotkey.htmlMap.get(key) : key}</kbd>`;
          
          // Check if hotkey is a combination of multiple keys (e.g. Ctrl + m)
          if (hotkey.hasModifiers()) {
            const modifiers = hotkey.modifiers;
            // Replace hotkey string with its corresponding HTML code
            // Put a "+" between hotkey components
            const modifierText = modifiers
              .map(modifier => Hotkey.htmlMap.has(modifier) ? Hotkey.htmlMap.get(modifier) : modifier)
              .map(modifier => `<kbd>${modifier}</kbd>`).join(' + ');

            hotkeyText = modifierText + ' + ' + hotkeyText

          }
            
          // Add the hotkey text
          hotkeyEl.innerHTML = hotkeyText;

          // Put hotkey description, text and edit button into a div
          const btnDivEl = document.createElement('div');

          // Add tooltip to hotkey element only for "individuals" and "actions "categories
          if (!fixedCategories.includes(category)) {
            itemEl.setAttribute('data-bs-toggle', 'tooltip');
            itemEl.setAttribute('data-bs-title', 'Click to change shortcut');
            itemEl.setAttribute('data-bs-custom-class', 'custom-tooltip'); // For styling the tooltip element
            itemEl.role = 'button';

            // Badge to prompt user for inputting a hotkey
            const badgeEl = document.createElement('span');
            badgeEl.classList.add('badge', 'hotkey-badge', 'text-bg-primary', 'me-2', 'd-none'); // Hide the info badge by default
            badgeEl.textContent = 'Press shortcut';

            
            // https://getbootstrap.com/docs/5.3/components/list-group/#custom-content
            btnDivEl.append(badgeEl, hotkeyEl);

            // Add class name to indicate a user-editable hotkey item
            itemEl.classList.add('hotkey-item');

          } else {
            btnDivEl.append(hotkeyEl);

          }

          // Add the item to the list
          itemEl.append(descDivEl, btnDivEl);
          listEl.append(itemEl);


        });

        divEl.append(titleEl, listEl);
        
          
      });

    }
    
    // Initialize tooltips
    modalEl.querySelectorAll('.list-group-item').forEach(tooltipTriggerEl => {
      const tooltip = bootstrap.Tooltip.getOrCreateInstance(tooltipTriggerEl);

    });

    handleHotkeyChangeByUser(modalEl);

    
    
    // // Define the keypress handler
    // let keypressHandler = null;

    // // Handle changing hotkeys
    // const hotkeyItemEls = document.querySelectorAll('.hotkey-item');
    // const badgeEls = document.querySelectorAll('.hotkey-badge');
    // hotkeyItemEls.forEach(itemEl => {
    //   itemEl.addEventListener('click', () => {

    //     // Hide the tooltip
    //     const tooltip = bootstrap.Tooltip.getOrCreateInstance(itemEl);
    //     tooltip.hide();

    //     // Clear previously showed badges
    //     badgeEls.forEach(badgeEl => badgeEl.classList.add('d-none'));
        
    //     // Show badge to inform user that you are waiting for a hotkey input
    //     const infoBadge = itemEl.querySelector('.hotkey-badge');
    //     if (infoBadge) {
    //       infoBadge.classList.toggle('d-none');
    //     }

    //     // Remove the old keypress event listener, if it exists
    //     if (keypressHandler) {
    //       document.removeEventListener('keyup', keypressHandler, true);
    //     }


    //     keypressHandler = function(e) {
    //       const pressedKey = e.key;

    //       // For characters outside the alphabet, get the HTML code for it from shortcuts map
    //       const hotkeyText = shortcutHtmlMap.has(pressedKey) ? shortcutHtmlMap.get(pressedKey) : pressedKey;
            
    //       // Show the user input
    //       console.log('You pressed: ', pressedKey, e.code);
  
    //       // Check if input is valid
    //       if (pressedKey === 'Escape') {
    //         return;
    //       }
  
    //       // Check if input conflicts with another hotkey
    //       // Get the category and name of the clicked element
    //       const hotkeyCategory = itemEl.dataset.hotkeyCategory;
    //       const hotkeyName = itemEl.dataset.hotkeyName;
  
    //       // Get the other hotkeys in the same category
    //       // const otherEls = modalEl.querySelectorAll(`[data-hotkey-category="${hotkeyCategory}"]:not([data-hotkey-name="${hotkeyName}"])`);
  
    //       // const conflictList = Object.keys(shortcuts[hotkeyCategory]).filter(
    //       //   hotkeyName => shortcuts[hotkeyCategory][hotkeyName]['hotkey'] === pressedKey
    //       // );

    //       // Keep track of conflicting hotkeys
    //       const conflictingHotkeys = Hotkey.getAll().filter(hotkey => hotkey.key === pressedKey)
  
    //       // If there are conflicting hotkeys
    //       if (conflictingHotkeys.length > 0) {
  
    //         // Get the description of the first conflicting hotkey
    //         const conflictDesc = conflictingHotkeys[0].description;

    //         // Show alert if user input is in conflict with another hotkey
    //         showAlertToast(
    //           `<strong>${hotkeyText}</strong> is already assigned to: <strong>${conflictDesc}</strong>! Please choose another shortcut.`, 
    //           'error', 
    //           'Conflicting Shortcut'
    //         );

    //       } else {
  
    //         // If there are no conflicts, assign the user input key to the clicked shortcut
    //         const hotkey = Hotkey.findOne({category: hotkeyCategory, name: hotkeyName});
    //         if (hotkey) {
    //           hotkey.key = pressedKey;

    //           // Change the list immediately
    //           const kbdEl = itemEl.querySelector('kbd');
    //           if (kbdEl) {
    //             kbdEl.innerText = hotkeyText;
    //           }
    
    //           // Hide the badge element
    //           const badgeEl = itemEl.querySelector('.badge');
    //           if (badgeEl) {
    //             badgeEl.classList.add('d-none');
    //           }
  
    //           // Get the hotkey description
    //           const hotkeyDesc = hotkey.description;
              
    //           // Show success
    //           showAlertToast(
    //             `<strong>${hotkeyText}</strong> is assigned to: <strong>${hotkeyDesc}</strong>!`, 
    //             'success',
    //             'Shortcut Changed'
    //           );
    
              
    //         } 
            
    //         // Remove listening to keypress after change of shortcut was successful
    //         document.removeEventListener('keyup', keypressHandler, true);

    //       }

    //     }

    //     // Listen to user key press
    //     document.addEventListener('keyup', keypressHandler, true);

  
    //   });
  
    // });

    const modalBootstrap = bootstrap.Modal.getOrCreateInstance(modalEl);
    modalBootstrap.show();

    // Remove keypressHandler upon Modal dismissal
    // modalEl.addEventListener('hidden.bs.modal', () => document.removeEventListener('keyup', keypressHandler, true));

    
    // // Add tooltip to show users that they can change a hotkey by clicking on it
    // const hotkeyTooltipList = [...hotkeyEls].map(tooltipTriggerEl => bootstrap.Tooltip.getOrCreateInstance(tooltipTriggerEl))
    // hotkeyTooltipList.forEach(tooltip => tooltip.setContent({ '.tooltip-inner': 'Click to change' }));
    // console.log(hotkeyTooltipList);

  }

}

/**
 * Clears boxes from canvas
 * @param {Array} selectedBoxes | Box array to be cleared with coordinates and dimensions
 */
function clearBoxes (selectedBoxes) {
  // Offset in pixels for highlight rectangles is given
  const offset = 5;
  
  const mainCanvas = Player.getMainPlayer().getCanvas();
  if (mainCanvas && mainCanvas.getContext) {
    const ctx = mainCanvas.getContext('2d')
    selectedBoxes.forEach(box => {
      ctx.clearRect(box.x + offset, box.y + offset, box.width, box.height);
    });
  }

}



// function camelCaseToWords(camelCaseString) {
//   // Use regex to split camelCaseString into words
//   const wordsArray = camelCaseString.split(/(?=[A-Z])/);
  
//   // Capitalize the first word
//   const firstWord = wordsArray[0].charAt(0).toUpperCase() + wordsArray[0].slice(1);

//   // Uncapitalize the rest
//   const otherWords = wordsArray.slice(1).map(word => word.charAt(0).toLowerCase() + word.slice(1))
  
//   // Join the words with spaces
//   const result = firstWord + ' ' + otherWords.join(' ');

//   return result;
// }

/**
 * Update the HTML element associated with actions or individuals
 * @param {String} category - actions or individuals
 */
function updateHotkeyDomElement(category) {

  if (!category) {
    console.log('Category string must be provided!');
    return;
  }

  const validArgs = ['actions', 'individuals'];

  if (!validArgs.includes(category)) {
    console.log(`Provided argument ${category} is not valid! Category string must be either "actions" or "individuals"!`);
    return;
  }
  
  // Determine the div element id depending on the argument
  const outerDivElId = category === 'individuals' ? 'individual-names-div' : 'action-types-div';

  const outerDivEl = document.getElementById(outerDivElId);

  if (outerDivEl) {

    // Clear previous elements
    while (outerDivEl.firstChild) {
      outerDivEl.removeChild(outerDivEl.firstChild);
    }

    // Get the all Hotkeys for a category
    const hotkeyArr = Hotkey.findAll({category: category});

    // Create a list element
    const listEl = document.createElement('ul');
    listEl.classList.add('list-group', 'list-group-flush', 'small', 'lh-1');

    hotkeyArr.forEach(hotkey => {
      
      // Populate the list with Hotkey descriptions and keys
      const itemEl = document.createElement('li');
      itemEl.classList.add('list-group-item', 'hotkey-item', 'd-flex', 'justify-content-between', 'align-items-center');
  
      // Add hotkey category and key in shortcut dict to the element dataset
      itemEl.dataset.hotkeyCategory = hotkey.category;
      itemEl.dataset.hotkeyName = hotkey.name;
      const hotkeyEl = document.createElement('kbd');
      hotkeyEl.classList.add('hotkey-kbd');
      hotkeyEl.textContent = hotkey.key; // Add the key value of the hotkey

      // Add a tooltip to the list item element
      itemEl.setAttribute('data-bs-toggle', 'tooltip');
      itemEl.setAttribute('data-bs-title', 'Click to change shortcut');
      itemEl.setAttribute('data-bs-custom-class', 'custom-tooltip'); // For styling the tooltip element
      itemEl.role = 'button';

      // Add individual names to the description element
      const descDivEl = document.createElement('div');
      descDivEl.textContent = hotkey.description;

      // Badge to prompt user for inputting a hotkey
      const badgeEl = document.createElement('span');
      badgeEl.classList.add('badge', 'hotkey-badge', 'text-bg-primary', 'me-2', 'd-none'); // Hide the info badge by default
      badgeEl.textContent = 'Press shortcut';

      // Put hotkey description, text and edit button into a div
      // https://getbootstrap.com/docs/5.3/components/list-group/#custom-content
      const btnDivEl = document.createElement('div');
      btnDivEl.append(badgeEl, hotkeyEl);

      // Add the child elements to their parent elements
      itemEl.append(descDivEl, btnDivEl);
      listEl.append(itemEl);
    
      // Add the list element to the DOM
      outerDivEl.append(listEl);

    });


    // Initialize tooltips
    outerDivEl.querySelectorAll('.list-group-item').forEach(tooltipTriggerEl => {
      const tooltip = bootstrap.Tooltip.getOrCreateInstance(tooltipTriggerEl);

    });

    // Listen for keypress to change hotkeys
    handleHotkeyChangeByUser(outerDivEl);

    
  }



}


/**
 * Return a random letter which is not in the input array
 * @param {Array} charArr 
 * @returns 
 */
function getRandomLetterNotIn(charArr) {
  let alphabet = "abcdefghijklmnopqrstuvwxyz";
  let letter;
  do {
    letter = alphabet.charAt(Math.floor(Math.random() * alphabet.length));
  } while (charArr.includes(letter));
  return letter;
}

function showHelpModal(markdownPath) {
  const modalEl = document.getElementById('help-modal');
  const modalBootstrap = bootstrap.Modal.getOrCreateInstance(modalEl);
  modalBootstrap.show();

  // Convert markdown to HTML

  // Convert headers (#) in markdown to <h1...h6> HTML tags


}

/**
 * Add empty table info text when a table is empty
 * @param {Element} tableEl - table DOM element
 * @param {String} infoText - empty info string
 */
function addInfoRow(tableEl, infoText) {
  const tableBody = tableEl.querySelector('tbody');
  const tableHead = tableEl.querySelector('thead');
  if (tableBody && tableHead) {
    const infoRow = tableBody.querySelector('.empty-table-info');
    if (!infoRow) {
      const newRow = tableBody.insertRow(0);
      newRow.classList.add('empty-table-info');
      const newCell = newRow.insertCell(0);
      console.log('tableHead cells:', tableHead.rows[0].cells.length)
      const colNum = tableHead.rows[0].cells.length; // Get the column count in the header
      newCell.colSpan = colNum;
      newCell.textContent = infoText;
      return newRow;

    }

  }

}


/**
 * Shows or hides the spinner on the main view for process indication
 * @param {String} visibility | Use 'show' to show spinner, otherwise hide it
 */
function showOrHideSpinner(visibility) {
  let shouldShow = false;
  if (visibility === 'show') {
    shouldShow = true;
  }

  // Get indicator element on the main view to show loading progress 
  // for file processing, video loading, etc.
  const indicatorBtn = document.getElementById('loading-indicator-btn');
  if (indicatorBtn) {
    if (shouldShow) {
      indicatorBtn.classList.remove('d-none');
    } else {
      indicatorBtn.classList.add('d-none');
    }

  }

}


/**
 * Show success if editted tracking file copied to export directory
 * by changing the relevant icon on tracking table
 * @param {Boolean} isSaved | true if tracking edits saved successfully, false otherwise
 */
function showTrackingSaveStatus(isSaved) {

    const trackingSaveStatusBtn = document.getElementById('tracking-save-status-btn');
    const iconEl = trackingSaveStatusBtn.querySelector('span');
    
    if (trackingSaveStatusBtn && iconEl) {
      
      // Get the tooltip
      const tooltip = bootstrap.Tooltip.getInstance(trackingSaveStatusBtn);
      
      if (isSaved) {
        trackingSaveStatusBtn.classList.remove('d-none');
        iconEl.classList.remove('text-danger');
        iconEl.classList.add('text-success');
        iconEl.textContent = 'done_all';
        tooltip.setContent({ '.tooltip-inner': 'Changes saved!' }); // Change the tooltip
      } else {
        iconEl.textContent = 'error';
        iconEl.classList.remove('text-success');
        iconEl.classList.add('text-danger');
        tooltip.setContent({ '.tooltip-inner': 'Changes unsaved!' }); // Change the tooltip

      }



    }

}

/**
 * Handles changing Hotkeys by the user via click and keypress events
 * @param {Element} domEl - DOM element which contains the hotkey items
 */
function handleHotkeyChangeByUser(domEl) {

  // Define the keypress handler
  let keypressHandler = null;

  // Handle changing hotkeys
  const hotkeyItemEls = domEl.querySelectorAll('.hotkey-item');
  const badgeEls = domEl.querySelectorAll('.hotkey-badge');
  hotkeyItemEls.forEach(itemEl => {
    itemEl.addEventListener('click', () => {

      // Hide the tooltip
      const tooltip = bootstrap.Tooltip.getOrCreateInstance(itemEl);
      tooltip.hide();

      // Clear previously showed badges
      badgeEls.forEach(badgeEl => badgeEl.classList.add('d-none'));
      
      // Show badge to inform user that you are waiting for a hotkey input
      const infoBadge = itemEl.querySelector('.hotkey-badge');
      if (!infoBadge) return;
      infoBadge.classList.toggle('d-none');

      // Remove the old keypress event listener, if it exists
      if (keypressHandler) {
        window.removeEventListener('keyup', keypressHandler, true);
      }

      keypressHandler = function(e) {

        e.preventDefault();

        // Get the pressed keys and modifiers in the Keyboard Event
        const pressedObj = Hotkey.getUserKeyPress(e);

        if (!pressedObj || !pressedObj.key) {
          // Remove listening to keypress after user pressed a key
          window.removeEventListener('keydown', keypressHandler, true);

          // Hide the info badge
          infoBadge.classList.add('d-none');

          return;
        }

        // Keep track of pressed keys and modifiers
        const pressedModifiers = pressedObj.modifiers;
        const pressedKey = pressedObj.key;

        const pressedModifiersTextArr = pressedModifiers.map(
          pressedModifier => Hotkey.htmlMap.has(pressedModifier) ? Hotkey.htmlMap.get(pressedModifier) : pressedModifier
        );

        // For characters outside the alphabet, get the HTML code for it from shortcuts map
        const pressedKeyText = Hotkey.htmlMap.has(pressedKey) ? Hotkey.htmlMap.get(pressedKey) : pressedKey;

        // Check if input conflicts with another hotkey
        // Get the category and name of the clicked element
        const hotkeyCategory = itemEl.dataset.hotkeyCategory;
        const hotkeyName = itemEl.dataset.hotkeyName;

        // Keep track of conflicting hotkeys
        // const conflictingHotkeys = Hotkey.getAll().filter(hotkey => 
        //   hotkey.key === pressedKey && 
        //   pressedModifiers.length && 
        //   pressedModifiers.every(pressedModifier => hotkey.modifiers.includes(pressedModifier))
        // );
        const conflictingHotkeys = Hotkey.findConflicts(pressedKey, pressedModifiers);

        // If there are conflicting hotkeys
        if (conflictingHotkeys && conflictingHotkeys.length > 0) {

          // Get the description of the first conflicting hotkey
          const conflictDesc = conflictingHotkeys[0].description;

          // Construct HTML code for the conflicting modifiers
          const conflictingModifierHtml = pressedModifiersTextArr.map(
            modifierText => `<kbd>${modifierText}</kbd>`
          ).join('+');

          // Construct HTML code for the conflicting key
          const conflictingKeyHtml = `<kbd>${pressedKeyText}</kbd>`;

          // Put the two together
          const conflictHtml = conflictingModifierHtml ? conflictingModifierHtml.concat('+', conflictingKeyHtml) : conflictingKeyHtml;

          // Show alert if user input is in conflict with another hotkey
          showAlertToast(
            `${conflictHtml} is already assigned to <span class="badge text-bg-dark">${conflictDesc}</span>! Please choose another shortcut.`, 
            'error', 
            'Conflicting Shortcut'
          );

          // Remove listening to keypress after user pressed a key
          window.removeEventListener('keydown', keypressHandler, true);

          // Hide the info badge
          infoBadge.classList.add('d-none');

          return;

        }

        // If there are no conflicts, assign the user input key to the clicked shortcut
        // Get the Hotkey object linked to the clicked DOM element
        const hotkey = Hotkey.findOne({category: hotkeyCategory, name: hotkeyName});
        if (!hotkey) {

          // Remove listening to keypress after user pressed a key
          window.removeEventListener('keydown', keypressHandler, true);

          // Hide the info badge
          infoBadge.classList.add('d-none');

          console.log('No Hotkey object linked to this DOM element could be found!')
          
          return;

        }

        // If Escape is pressed, do not continue with editing
        if (pressedKey === 'Escape') {

          // Remove listening to keypress after user pressed a key
          window.removeEventListener('keydown', keypressHandler, true);

          // Hide the info badge
          infoBadge.classList.add('d-none');

          // Show feedback
          showAlertToast('Shortcut change cancelled!', 'success');
          
          return;

        }

        // Only allow alphabetic characters for individuals and actions
        if (['individuals', 'actions'].includes(hotkey.category) && !pressedKey.match(/^[a-z]$/)) {

          // Remove listening to keypress after user pressed a key
          window.removeEventListener('keydown', keypressHandler, true);

          // Hide the info badge
          infoBadge.classList.add('d-none');

          // Show warning
          showAlertToast('Only alphabetic characters (a-z) are allowed for individuals and actions!', 
            'error',
            'Invalid Shortcut'
          );

          return;
        }

        // Handle valid user key press
        // Assign the new hotkey values
        hotkey.key = pressedObj.key;
        hotkey.modifiers = pressedObj.modifiers;

        // Get the hotkey description
        const hotkeyDesc = hotkey.description;

        // Construct HTML code for the conflicting modifiers
        const modifierHtml = hotkey.modifiers.map(
          modifierText => `<kbd>${modifierText}</kbd>`
        ).join('+');

        // Construct HTML code for the conflicting key
        const keyHtml = `<kbd>${pressedKeyText}</kbd>`;

        // Put the two together
        const conflictHtml = modifierHtml ? modifierHtml.concat('+', keyHtml) : keyHtml;

        // Show success
        showAlertToast(
          `${conflictHtml} is assigned to <span class="badge text-bg-dark">${hotkeyDesc}</span>!`, 
          'success',
          'Shortcut Changed'
        );

        // Remove listening to keypress after user pressed a key
        window.removeEventListener('keydown', keypressHandler, true);

        // Hide the info badge
        infoBadge.classList.add('d-none');

        return;

      }

      // Listen to user key press
      window.addEventListener('keydown', keypressHandler, true);

      // Remove keypressHandler upon Modal dismissal
      const modalEls = document.querySelectorAll('.modal');
      modalEls.forEach(modalEl => {
        modalEl.addEventListener('hidden.bs.modal', () => {
          window.removeEventListener('keydown', keypressHandler, true)
        });
  
      });

    });

  });

  // Check if the DOM element is a modal
  if (domEl.classList.contains('modal')) {
    
    // Remove the event listener when the modal is closed
    domEl.addEventListener('hidden.bs.modal', () => {
      if (keypressHandler) {
        window.removeEventListener('keyup', keypressHandler, true);
      }

    });

  }

}


/**
 * Example starter JavaScript for disabling form submissions if there are invalid fields
 */
function validateInputs() {

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  const forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.from(forms).forEach(form => {
    const confirmBtn = form.querySelector('.confirm-btn');
    if (confirmBtn) {
      confirmBtn.addEventListener('click', async (e) => {
        if (!form.checkValidity()) {
          e.preventDefault();
          e.stopPropagation();
        }

        // Check if username is valid
        const usernameInputEl = form.querySelector('#username-input');
        const username = usernameInputEl ? usernameInputEl.value : null;
        if (username) {

          // Save username to config
          const response = await window.electronAPI.saveToConfig({username: username});

          // Confirm the save
          if (response) {

            // Save username to Player class
            await Player.setUsername(username);
            
            // Update setting menu
            const usernameSettingsEl = document.getElementById('change-username-input');
            if (usernameSettingsEl) {
              usernameSettingsEl.value = username;
            }

            // Hide the modal
            const bootstrapModal = bootstrap.Modal.getOrCreateInstance('#username-modal');
            bootstrapModal.hide();

            // Show success notification
            showAlertToast(`Username <span class="badge text-bg-dark">${username}</span> saved!`, 'success');

          }

        }

        // Validate the form
        form.classList.add('was-validated');

      }, false);

    }

  });

}


/**
 * Creates a string for a given time difference value in human readable format.
 * E.g. if the difference is 30 minutes, returns "30 minutes ago".
 * @param {Number | Date} timeInMilliseconds - Time difference in milliseconds
 * @returns {String} 
 */
function getTimeDifference(timeInMilliseconds) {

  // Convert milliseconds to seconds, minutes, hours and days
  const seconds = Math.floor(timeInMilliseconds / 1000);
  const minutes = Math.floor(seconds / 60);
  const hours = Math.floor(minutes / 60);
  const days = Math.floor(hours / 24);

  if (days > 0) {
    const dayText = days > 1 ? 'days' : 'day';
    return  `${days} ${dayText} ago`;
  } else if (hours > 0) {
    const hourText = hours > 1 ? 'hours' : 'hour';
    return  `${hours} ${hourText} ago`;
  } else if (minutes > 0) {
    const minuteText = minutes > 1 ? 'minutes' : 'minute';
    return  `${minutes} ${minuteText} ago`;
  } else {
    return 'Just now';
  }

}


/**
 * Shows last modified time of a file on its corresponding HTML element
 * @param {import('original-fs').PathLike} filePath File path 
 * @param {String} domElId DOM element ID which contains save status button and last edit time info element
 * @param {Boolean | undefined} isFailed - True if writing ethogram to file has been failed, False otherwise
 * @returns 
 */
async function showLastEditForFile(filePath, domElId, isFailed) {

  if (!domElId) return;
  
  const outerEl = document.getElementById(domElId);
  if (!outerEl) return;
  
  const statusBtn = outerEl.querySelector('.save-status-btn');
  if (!statusBtn) return;
  
  const iconEl = statusBtn.querySelector('span');
  if (!iconEl) return;
  
  const mainPlayer = Player.getMainPlayer();
  if (!mainPlayer) return;

  // const ethogram = mainPlayer.getEthogram();
  // if (!ethogram) return;
  
  // Show tooltip over save status element
  const tooltip = bootstrap.Tooltip.getOrCreateInstance(statusBtn);

  // Change the icon of the button if edit has been failed
  if (isFailed || !filePath) {
    // Change the tooltip content
    tooltip.setContent({'.tooltip-inner': 'Save failure!'});
    iconEl.textContent = 'unpublished';
    iconEl.classList.remove('text-success');
    iconEl.classList.add('text-danger');

    // Don't progress if it is failed
    return;

  }

  // Reset the icon and color to "success" by default
  if (!isFailed) {
    const iconEl = statusBtn.querySelector('span');
    iconEl.textContent = 'published_with_changes';
    iconEl.classList.remove('text-danger');
    iconEl.classList.add('text-success');

    // Change the tooltip content
    tooltip.setContent({'.tooltip-inner': 'All saved!'});

  }

  // Get the HTML element for displaying the last modified time
  const lastModTimeEl = outerEl.querySelector('.last-edit-time');

  if (!lastModTimeEl) return;

  // Get the last modified time of the file
  const lastModTime = await window.electronAPI.getLastModifiedTime(filePath);

  // Show the time difference
  lastModTimeEl.textContent = getTimeDifference(Date.now() - lastModTime);

  // Display the DOM element
  outerEl.classList.remove('invisible');
  outerEl.classList.add('visible');

  // // Hide the indicator if there is no observation 
  // if (ethogram.size() < 1) {
  //   statusBtn.classList.add('d-none');
  // } else {
  //   statusBtn.classList.remove('d-none');
  // }

  
}


// /**
//  * Shows last modified time for ethogram file on its corresponding HTML element
//  * @param {Boolean | undefined} isFailed - True if writing ethogram to file has been failed, False otherwise
//  * @returns 
//  */
// async function showLastEditForEthogram(isFailed) {
//   const saveStatusBtn = document.getElementById('ethogram-save-status-btn');
//   if (!saveStatusBtn) return;
  
//   const iconEl = saveStatusBtn.querySelector('span');
//   if (!iconEl) return;

//   const mainPlayer = Player.getMainPlayer();
//   if (!mainPlayer) return;

//   const ethogram = mainPlayer.getEthogram();
//   if (!ethogram) return;

//   const tooltip = bootstrap.Tooltip.getOrCreateInstance(saveStatusBtn);

//   // Change the icon of the button if edit has been failed
//   if (isFailed) {
//     // Change the tooltip content
//     tooltip.setContent({'.tooltip-inner': 'Save failure!'});
//     iconEl.textContent = 'unpublished';
//     iconEl.classList.remove('text-success');
//     iconEl.classList.add('text-danger');

//     // Don't progress if it is failed
//     return;

//   }

//   // Reset the icon and color to "success" by default
//   if (!isFailed) {
//     const iconEl = saveStatusBtn.querySelector('span');
//     iconEl.textContent = 'published_with_changes';
//     iconEl.classList.remove('text-danger');
//     iconEl.classList.add('text-success');

//     // Change the tooltip content
//     tooltip.setContent({'.tooltip-inner': 'All saved!'});

//   }

//   // Get the HTML element for displaying the last modified time
//   const lastModTimeEl = document.getElementById('ethogram-last-edit-time');

//   if (!lastModTimeEl) return;
    
//   // Find the ethogram file path in user directory
//   const ethogramFilePath = await window.electronAPI.findEthogramFile(mainPlayer.getSource());

//   if (!ethogramFilePath) return;

//   // Get the last modified time of the ethogram file
//   const lastModTime = await window.electronAPI.getLastModifiedTime(ethogramFilePath);

//   // Show the time difference
//   lastModTimeEl.textContent = getTimeDifference(Date.now() - lastModTime);

//   // Hide the indicator if there is no observation 
//   if (ethogram.size() < 1) {
//     saveStatusBtn.classList.add('d-none');
//   } else {
//     saveStatusBtn.classList.remove('d-none');
//   }


// }

/**
 * Makes a DOM element draggable
 * From: https://www.w3schools.com/howto/howto_js_draggable.asp
 * @param {Element} el 
 */
function dragElement(el) {
  let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

  el.onmousedown = dragMouseDown;

  // const mainCanvas = document.getElementById('main-tracking-canvas');

  function dragMouseDown(e) {
    // if (e.composedPath().includes(mainCanvas)) {
    //   console.log('mousemove over main canvas');
    //   return;
    // }

    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    // if (e.composedPath().includes(mainCanvas)) {
    //   console.log('mousemove over main canvas');
    //   return;
    // }

    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    el.style.top = (el.offsetTop - pos2) + "px";
    el.style.left = (el.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}


export {
  getFrameFromVideo, 
  // createSecondaryVideoDivs, 
  getClickedBoxes, 
  updateInteractionTable,
  updateTracksFromToast, 
  updateTracksFromNameDropdown,
  formatSeconds, 
  formatMinutes,
  minutesToFrames,
  secondsToFrames,
  framesToSeconds,
  getFileNameWithoutExtension,
  getSpeciesName,
  showAlertToast,
  showAlertModal,
  hideAlertModal,
  addEthogramRow,
  completeActionRow,
  updatePlaybackRateList,
  updateZoomScaleDomEls,
  handleKeyPress,
  showShortcutsModal,
  showHelpModal,
  showOverlappingTracksToast,
  showNamesModal,
  getRandomLetterNotIn,
  clearEthogramTable,
  clearTrackingTable,
  showOrHideSpinner,
  addInfoRow,
  showTrackingSaveStatus,
  produceLabelText,
  handleHotkeyChangeByUser,
  updateHotkeyDomElement,
  handleBehaviorRecordByClick,
  showToastForBehaviorRecording,
  validateInputs,
  getTimeDifference,
  showLastEditForFile,
  loadSecondaryVideos,
  dragElement
}
