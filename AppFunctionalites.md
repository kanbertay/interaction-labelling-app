# Current Functionalities of the Richard's Implementation
The content of this document was copied from the corresponding file in GWDG ownCloud.

## Loading
-	Load videos in different formats
-	Load tracking + identification labels (or only tracking labels)
-	Load interaction labels
-	There is a fast track to load the video with the respective tracking labels if they are in a fixed folder structure.

## Display
-	Slide through the video (ideally without lag)
-	Use left/right keys to navigate through video (currently there is no speed function, but one can specify how many frames are skipped, if one want to move faster)
-	Open a second / third … video which is temporally synchronized with the first one (so it is moved with the same slider)
-	If tracking labels are loaded, they are directly displayed as bounding boxes with a trackID on top of the video.
-	Bounding boxes can be disabled / enabled if the user would like to see what is beneath them.
-	The field with the trackID can either be transparent (if the backgrounds are easy) or filled – users can switch between both modes.
-	Users can switch between multi-class and single class mode: in multi-class mode every object has a prefix for the class, e.g. 0-1, 0-2, 1-4. In single class mode it is just the track id 1,2,3 without prefix.
-	If interaction labels are loaded, they are directly displayed below the video.
-	All currently available interactions are listed on the side of the video with their respective keyboard shortcut. Users can modify them in a config file.

## Modify tracking labels
-	Allow user to remove a track
-	Allow user to remove a track after a certain frame
-	Allow user to change the trackID for a track
-	Allow user to change the trackID for a track after a certain frame
-	Allow user to change the trackID for a track (after a certain frame) and assign the next unused label to the track
-	Allow user to undo any of the above changes.

## Add / modify interaction labels
-	Users can add arbitrarily many interaction lines. Each interaction line represents all interactions happening between the same subject and the same object. Their trackIDs can be input behind the interaction line.
-	Users can select the start and end time of an interaction by using the keyboard. They navigate with the slider to the respective location and press the key for the interaction, then they navigate to the end of the interaction and press it again. This will add the interaction visually to the selected interaction line.
-	* Users can remove selected interaction lines completely.
-	Users can change the trackIDs / names in of the involved individual objects.

## Logging
-   Any change in the tracking dataset is logged.

## Saving
-	Users can select whether they would like to save the tracking file, the interaction file or both. The log file is always stored automatically at the same time.


## Current pain points
-   Videos lag a little bit, when navigating through them (makes user experience a little worse)
-   No flexible position for different video panels – actually size of the video panel is constant.
-   No interactive use of the interactions is possible, e.g. clicking of an interaction brings to the right place in the video.